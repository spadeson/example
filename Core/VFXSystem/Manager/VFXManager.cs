using UnityEngine;

namespace VIS.CoreMobile.VFX {
    public class VFXManager : MonoBehaviour, IVFXManager {
        private VFXLibrary _vfxLibrary;
        
        public void initialize(VFXManagerConfig vfxManagerConfig) {
            _vfxLibrary = vfxManagerConfig.VFXLibrary;
        }

        public void playVFX(string vfxName, Vector3 position, Transform parent = null) {
            var vfx = _vfxLibrary.vfxList.Find(v => v.vfxName == vfxName).vfxPrefab;
            var newVFX = Instantiate(vfx, parent, true);
            newVFX.transform.position = position;
        }
    }
}