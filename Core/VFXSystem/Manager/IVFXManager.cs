using UnityEngine;

namespace VIS.CoreMobile.VFX {
    public interface IVFXManager {

        void initialize(VFXManagerConfig vfxManagerConfig);
        void playVFX(string vfxName, Vector3 position, Transform parent = null);
    }
}