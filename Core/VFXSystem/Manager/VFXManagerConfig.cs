using UnityEngine;

namespace VIS.CoreMobile.VFX {
    [CreateAssetMenu(fileName = "VFXManagerConfig", menuName = "VIS/VFX/VFX Manager Config")]
    public class VFXManagerConfig : ScriptableObject {
        public VFXLibrary VFXLibrary;
    }
}