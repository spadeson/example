using System.Collections.Generic;
using UnityEngine;

namespace VIS.CoreMobile.VFX {
    [CreateAssetMenu(fileName = "VFXLibrary", menuName = "VIS/VFX/VFX Library")]
    public class VFXLibrary : ScriptableObject {
        public List<VFXNamePair> vfxList = new List<VFXNamePair>();
    }
}