using System;
using UnityEngine;

namespace VIS.CoreMobile.VFX {
    [Serializable]
    public class VFXNamePair {
        public string vfxName;
        public GameObject vfxPrefab;
    }
}