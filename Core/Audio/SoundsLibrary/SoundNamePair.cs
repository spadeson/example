﻿using System;
using UnityEngine;

namespace Core.Audio.SoundsLibrary {
    [Serializable]
    public class SoundNamePair {
        [HideInInspector] public string soundName;
        [HideInInspector] public AudioClip sound;
    }
}
