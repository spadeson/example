﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Audio.SoundsLibrary {
    [CreateAssetMenu(fileName = "SoundLibrary", menuName = "VIS/Sounds/Sound Library")]
    public class SoundLibrary : ScriptableObject {
        [HideInInspector] public List<SoundNamePair> soundsList = new List<SoundNamePair>();
    }
}