﻿using Core.Audio.Handler;

namespace Core.Audio.Manager {
    public interface IAudioManager {
        /// <summary>
        /// Initializes AudioManager. 
        /// </summary>
        /// <param name="audioManagerConfig">Audio Manager Config.</param>
        void Initialize(AudioManagerConfig audioManagerConfig);
        
        /// <summary>
        /// Plays Audio Clip as Music.
        /// </summary>
        /// <param name="soundClipName">Sound clip key.</param>
        /// <param name="loopSound">Is sound looped.</param>
        /// <param name="volume">Sound volume (0 - 1).</param>
        /// <param name="delay">Delay, before playing the sound.</param>
        /// <returns>Audio handler instance.</returns>
        AudioHandler PlayMusicClip(string soundClipName, bool loopSound = false, float volume = 1f, float delay = 0f);
        
        /// <summary>
        /// Plays Audio Clip as Sound.
        /// </summary>
        /// <param name="soundClipName">Sound clip key.</param>
        /// <param name="loopSound">Is sound looped.</param>
        /// <param name="volume">Sound volume (0 - 1).</param>
        /// <param name="delay">Delay, before playing the sound.</param>
        /// <returns>Audio handler instance.</returns>
        AudioHandler PlaySoundClip(string soundClipName, bool loopSound = false, float volume = 1f, float delay = 0f);
        
        /// <summary>
        /// Pauses Music clip by music clip key.
        /// </summary>
        /// <param name="musicClipName">Music clip key.</param>
        void PauseMusic(string musicClipName);
        
        /// <summary>
        /// Unpauses Music clip by music clip key.
        /// </summary>
        /// <param name="musicClipName">Music clip key.</param>
        void UnPauseMusic(string musicClipName);
        
        /// <summary>
        /// Stops playing music clip by music clip key.
        /// </summary>
        /// <param name="musicClipName">Music clip key.</param>
        void StopPlayingMusic(string musicClipName);
        
        /// <summary>
        /// Stops playing music clip by sound clip key.
        /// </summary>
        /// <param name="soundClipName">Sound clip key.</param>
        void  StopPlayingSound(string soundClipName);
        
        /// <summary>
        /// Returns volume of the Music group from the AudioMixer.
        /// </summary>
        /// <returns>Volume of the Music group from the AudioMixer</returns>
        float GetMusicVolume();
        
        /// <summary>
        /// Returns volume of the Sounds group from the AudioMixer.
        /// </summary>
        /// <returns>Volume of the Sounds group from the AudioMixer.</returns>
        float GetSoundsVolume();
        
        /// <summary>
        /// Sets the volume of the Music group from the AudioMixer.
        /// </summary>
        /// <param name="volume">Volume of the Music group inside the AudioMixer.</param>
        void  SetMusicVolume(float volume);
        
        /// <summary>
        /// Sets the volume of the Sounds group from the AudioMixer.
        /// </summary>
        /// <param name="volume">Volume of the Sounds group inside the AudioMixer.</param>
        void  SetSoundsVolume(float volume);
    }
}