﻿using System.Collections.Generic;
using Core.Audio.Handler;
using Core.Audio.SoundsLibrary;
using UnityEngine;
using UnityEngine.Audio;

namespace Core.Audio.Manager {
    public class AudioManager : MonoBehaviour, IAudioManager {
        [Header("Music Audio Handlers:")]
        [SerializeField] private List<AudioHandler> musicAudioHandlers;

        [Header("Sounds Audio Handlers:")]
        [SerializeField] private List<AudioHandler> soundsAudioHandlers;
        
        private SoundLibrary _soundLibrary;
        private AudioMixer _audioMixer;

        private AudioMixerGroup _musicAudioMixerGroup;
        private AudioMixerGroup _soundsAudioMixerGroup;

        private const float MIN_SOUNDS_VOLUME = -80f;
        private const float MAX_SOUNDS_VOLUME = 20f;

        /// <inheritdoc />
        public void Initialize(AudioManagerConfig audioManagerConfig) {
            musicAudioHandlers = new List<AudioHandler>();
            soundsAudioHandlers = new List<AudioHandler>();

            if (_soundLibrary == null) {
                _soundLibrary = audioManagerConfig.SoundLibrary;
            }

            if (_audioMixer == null) {
                _audioMixer = audioManagerConfig.AudioMixer;
            }

            if (_audioMixer == null) return;

            _musicAudioMixerGroup = _audioMixer.FindMatchingGroups("Music")[0];
            _soundsAudioMixerGroup = _audioMixer.FindMatchingGroups("Sounds")[0];
        }

        /// <inheritdoc />
        public AudioHandler PlayMusicClip(string soundClipName, bool loopSound = false, float volume = 1F, float delay = 0f) {
            var clip = _soundLibrary.soundsList.Find(x => x.soundName == soundClipName).sound;

            var audioHandler = new GameObject("Music-Audio-Handler", typeof(AudioHandler)).GetComponent<AudioHandler>();
            audioHandler.transform.SetParent(transform);
            audioHandler.Initialize(_musicAudioMixerGroup);
            musicAudioHandlers.Add(audioHandler);

            audioHandler.Play(soundClipName, clip, loopSound, volume, delay);

            return audioHandler;
        }

        /// <inheritdoc />
        public AudioHandler PlaySoundClip(string soundClipName, bool loopSound = false, float volume = 1f, float delay = 0f) {
            var clip = _soundLibrary.soundsList.Find(x => x.soundName == soundClipName).sound;

            var audioHandler = new GameObject("Sound-Audio-Handler", typeof(AudioHandler)).GetComponent<AudioHandler>();
            audioHandler.transform.SetParent(transform);
            audioHandler.OnClipEnded += RemoveAudioHandlerFromTheList;
            audioHandler.Initialize(_soundsAudioMixerGroup);
            soundsAudioHandlers.Add(audioHandler);
            audioHandler.Play(soundClipName, clip, loopSound, volume, delay);

            return audioHandler;
        }
        /// <inheritdoc />
        public void PauseMusic(string musicClipName) {
            var handler = musicAudioHandlers.Find(m => m.audioKey == musicClipName);
            if(handler != null) handler.Pause();
        }

        /// <inheritdoc />
        public void UnPauseMusic(string musicClipName) {
            var handler = musicAudioHandlers.Find(m => m.audioKey == musicClipName);
            if(handler != null) handler.UnPause();
        }

        /// <inheritdoc />
        public void StopPlayingMusic(string musicClipName) {
            var handler = musicAudioHandlers.Find(m => m.audioKey == musicClipName);
            if(handler != null) handler.Stop();
        }

        /// <inheritdoc />
        public void StopPlayingSound(string soundClipName) {
            var handler = soundsAudioHandlers.Find(m => m.audioKey == soundClipName);
            if(handler != null) handler.Stop();
        }

        /// <inheritdoc />
        public float GetMusicVolume() {
            _audioMixer.GetFloat("MusicVolume", out var musicGroupVolume);
            return musicGroupVolume;
        }

        /// <inheritdoc />
        public float GetSoundsVolume() {
            _audioMixer.GetFloat("SoundsVolume", out var musicGroupVolume);
            return musicGroupVolume;
        }

        /// <inheritdoc />
        public void SetMusicVolume(float volume) {
            _audioMixer.SetFloat("MusicVolume", Mathf.Log(volume) * MAX_SOUNDS_VOLUME);
        }

        /// <inheritdoc />
        public void SetSoundsVolume(float volume) {
            _audioMixer.SetFloat("SoundsVolume", Mathf.Log(volume) * MAX_SOUNDS_VOLUME);
        }
        
        private void RemoveAudioHandlerFromTheList(AudioHandler audioHandler) {
            audioHandler.OnClipEnded -= RemoveAudioHandlerFromTheList;

            if (musicAudioHandlers.Contains(audioHandler)) {
                musicAudioHandlers.Remove(audioHandler);
            }

            if (soundsAudioHandlers.Contains(audioHandler)) {
                soundsAudioHandlers.Remove(audioHandler);
            }

            audioHandler.Dismiss();

            Destroy(audioHandler.gameObject, 0.1f);
        }
    }
}