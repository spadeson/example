﻿using Core.Audio.SoundsLibrary;
using UnityEngine;
using UnityEngine.Audio;

namespace Core.Audio.Manager {
    [CreateAssetMenu(fileName = "AudioManagerConfig", menuName = "Hephaestus/Core/Sounds/Audio Manager Config")]
    public class AudioManagerConfig : ScriptableObject {
        public SoundLibrary SoundLibrary;
        public AudioMixer AudioMixer;
    }
}