using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

namespace Core.Audio.Handler {
    public class AudioHandler : MonoBehaviour {
        
        public event Action OnClipPlay;
        public event Action<AudioHandler> OnClipEnded;
        public event Action OnClipStop;

        private Coroutine _playingAudionCoroutine;

        public bool IsPlaying => audioSource.isPlaying;

        [SerializeField] private AudioSource audioSource;

        public string audioKey;

        public bool isPaused;

        public void Initialize(AudioMixerGroup audioMixerGroup) {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.outputAudioMixerGroup = audioMixerGroup;
            audioSource.loop = false;
            audioSource.playOnAwake = false;
            audioSource.volume = 1f;
        }

        public void Play(string audioClipKey, AudioClip clip, bool loop = false, float volume = 1f, float delay = 0f) {
            audioKey = audioClipKey;
            audioSource.clip = clip;
            audioSource.loop = loop;
            audioSource.volume = volume;
            audioSource.Play((ulong)delay);
            OnClipPlay?.Invoke();
            if (_playingAudionCoroutine != null) {
                StopCoroutine(WaitUntilClipEnd_Co());
                _playingAudionCoroutine = null;
            } else {
                _playingAudionCoroutine = StartCoroutine(WaitUntilClipEnd_Co());
            }
            
        }

        public void Stop() {
            audioSource.Stop();
            audioSource.clip = null;
            OnClipStop?.Invoke();
            OnClipEnded?.Invoke(this);
            if (_playingAudionCoroutine == null) return;
            StopCoroutine(WaitUntilClipEnd_Co());
            _playingAudionCoroutine = null;
        }

        public void Pause() {
            audioSource.Pause();
            isPaused = true;
        }
        
        public void UnPause() {
            audioSource.UnPause();
            isPaused = false;
        }

        public void Dismiss() {
            audioSource.Stop();
            audioSource.clip = null;
            if (_playingAudionCoroutine == null) return;
            StopCoroutine(WaitUntilClipEnd_Co());
            _playingAudionCoroutine = null;
        }

        IEnumerator WaitUntilClipEnd_Co() {
            yield return new WaitUntil(() => !audioSource.isPlaying);
            OnClipEnded?.Invoke(this);
            _playingAudionCoroutine = null;
        }
    }
}
