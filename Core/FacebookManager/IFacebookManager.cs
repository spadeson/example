using System;
using UnityEngine;

namespace VIS.CoreMobile.Facebook {
    public interface IFacebookManager {
        event Action<string> OnSuccessLogin;
        bool IsInitialized { get; }
        void Initialize();
        void Login();
        void GetPlayerName(Action<string> callBack);
        void GetPlayerUserpic(Action<Sprite> callBack);
        bool IsLogged();
    }
}