using System;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

namespace VIS.CoreMobile.Facebook {
    public class FacebookManager : MonoBehaviour, IFacebookManager {
        
        public event Action<string> OnSuccessLogin;

        public bool IsInitialized => FB.IsInitialized;

        readonly List<string> _loginPermissions = new List<string> {"public_profile", "email"};

        private FacebookAnalyticsProvider _facebookAnalyticsProvider;

        public void Initialize()
        {
            _facebookAnalyticsProvider = new FacebookAnalyticsProvider();
        }

        public void Login() {
            if (!FB.IsInitialized) {
                FB.Init(onInitCallback, onHideCallback);
            }
            else {
                FB.ActivateApp();
            }
        }

        public void GetPlayerName(Action<string> callBack) {
            if (!FB.IsInitialized || !FB.IsLoggedIn) return;
            var query = "me?name";
            FB.API(query, HttpMethod.GET, delegate(IGraphResult result) {
                callBack?.Invoke(result.ResultDictionary["name"].ToString());
            });
        }

        public void GetPlayerUserpic(Action<Sprite> callBack) {
            if (!FB.IsInitialized || !FB.IsLoggedIn) return;
            var query = "me/picture?type=square&height=128&width=128";
            FB.API(query, HttpMethod.GET, delegate(IGraphResult result) {
                var userpic = Sprite.Create(
                    result.Texture,
                    new Rect(0, 0, result.Texture.width, result.Texture.height),
                    Vector2.one * 0.5f,
                    100f
                );
                    
                callBack?.Invoke(userpic);
            });
        }

        public bool IsLogged() {
            return FB.IsLoggedIn;
        }

        private void onInitCallback() {
            if (FB.IsInitialized) {
                // Signal an app activation App Event
                FB.ActivateApp();
                // Continue with Facebook SDK
                //Login
                FB.LogInWithReadPermissions(_loginPermissions, AuthCallback);
            } else {
                Debug.LogError("Failed to Initialize the Facebook SDK");
            }
        }

        private void AuthCallback(ILoginResult result) {
            if (FB.IsLoggedIn) {
                
                // AccessToken class will have session details
                var aToken = AccessToken.CurrentAccessToken;
                
                // Print current access token's User ID
                Debug.Log($"User ID: {aToken.UserId}");
                
                // Print current access token's granted permissions
                foreach (var permission in aToken.Permissions) {
                    Debug.Log($"Permissions: {permission}");
                }
                
                OnSuccessLogin?.Invoke(aToken.TokenString);
                
            } else {
                Debug.Log("User cancelled login");
            }
        }

        private void onHideCallback(bool isunityshown) {
            Time.timeScale = !isunityshown ? 0 : 1;
        }

        public void Share() {
            FB.ShareLink(
                new Uri("https://developers.facebook.com/"),
                callback: ShareCallback
            );
        }

        private void ShareCallback(IShareResult result) {
            if (result.Cancelled || !String.IsNullOrEmpty(result.Error)) {
                Debug.Log("ShareLink Error: " + result.Error);
            } else if (!String.IsNullOrEmpty(result.PostId)) {
                // Print post identifier of the shared content
                Debug.Log(result.PostId);
            } else
            {
                // Share succeeded without postID
                Debug.Log("ShareLink success!");
            }
        }
    }
}