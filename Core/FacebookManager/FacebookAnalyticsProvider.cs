using System.Collections.Generic;
using Core.AnalyticsSubsytem.Providers;
using Facebook.Unity;
using UnityEngine;

namespace VIS.CoreMobile.Facebook {
    public class FacebookAnalyticsProvider : IAnalyticsProvider {

        private FacebookManager _facebookManager;
        
        public bool Initialize() {
            return CoreMobile.Instance.FacebookManager.IsInitialized;
        }

        public void LogEvent(string eventName) {
            FB.LogAppEvent(eventName);
        }

        public void LogEvent(string eventName, object eventParameters) {
            var parameters = eventParameters as Dictionary<string, object>;
            FB.LogAppEvent(eventName, valueToSum: null, parameters);
        }

        public void LogEvent(string eventName, string eventParameterName, double eventParameter) {
            Debug.LogWarning("Not implemented by Facebook Analytics yet!");
        }

        public void LogEvent(string eventName, string eventParameterName, int eventParameter) {
            Debug.LogWarning("Not implemented by Facebook Analytics yet!");
        }

        public void LogEvent(string eventName, string eventParameterName, float eventParameter) {
            Debug.LogWarning("Not implemented by Facebook Analytics yet!");
        }

        public void LogEvent(string eventName, string eventParameterName, long eventParameter) {
            Debug.LogWarning("Not implemented by Facebook Analytics yet!");
        }

        public void LogEvent(string eventName, string eventParameterName, string eventParameter) {
            Debug.LogWarning("Not implemented by Facebook Analytics yet!");
        }

        public void SetUserProperty(string propertyName, string propertyValue) {
            Debug.LogWarning("Unsupported by Facebook Analytics!");
        }
    }
}
