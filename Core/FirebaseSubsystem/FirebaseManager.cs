﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.FirebaseSubsystem.Config;
using Core.FirebaseSubsystem.Services.Analytics;
using Core.FirebaseSubsystem.Services.Authentication;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using Core.FirebaseSubsystem.Services.CloudMessaging;
using Core.FirebaseSubsystem.Services.CrashList;
using Core.FirebaseSubsystem.Services.Database;
using Core.FirebaseSubsystem.Services.DynamicLinks;
using Core.FirebaseSubsystem.Services.RemoteConfigsService;
using UnityEngine;
using VIS.CoreMobile;

namespace Core.FirebaseSubsystem {
    public class FirebaseManager : MonoBehaviour, IFirebaseManager {
        public event Action OnFirebaseInitialized;

        public bool IsFirebaseAnalyticsReady { get; private set; }
        public FirebaseAnalyticsManager FirebaseAnalyticsManager { get; private set; }
        public bool IsFirebaseAuthenticationReady { get; private set; }
        public IFirebaseAuthenticationManager AuthenticationManager { get; private set; }
        public bool IsFirebaseCloudMessagingReady { get; private set; }
        public IFirebaseCloudMessagingManager CloudMessagingManager { get; private set; }

        public bool IsFirebaseCrashListReady { get; private set; }
        public IFirebaseCrashListManager CrashListManager { get; private set; }
        public IDynamicLinksManager DynamicLinksManager { get; private set; }
        public IFirebaseDatabaseManager FirebaseDatabaseManager { get; private set; }
        public IFirebaseRemoteConfigManager FirebaseRemoteConfigManager { get; private set; }
        public bool Initialized { get; set; }

        private FirebaseServicesConfig _firebaseServicesConfig;

        public void Initialize(FirebaseServicesConfig firebaseServicesConfig) {
            Debug.Log("<color=blue>Firebase manager starts initialization...</color>");

            _firebaseServicesConfig = firebaseServicesConfig;

            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                
                if (_firebaseServicesConfig.CreateFirebaseAnalyticsManager) {
                    FirebaseAnalyticsManager = new FirebaseAnalyticsManager();
                    CoreMobile.Instance.AnalyticsManager.AddAnalyticsProvider(FirebaseAnalyticsManager);
                }

                if (_firebaseServicesConfig.CreateFirebaseCloudMessagingManager) {
                    CloudMessagingManager = new FirebaseCloudMessagingManager();
                }

                if (_firebaseServicesConfig.CreateFirebaseAuthenticationManager) {
                    AuthenticationManager = new FirebaseAuthenticationManager();
                }

                if (_firebaseServicesConfig.CreateFirebaseCrashListManager) {
                    CrashListManager = new FirebaseCrashListManager();
                    CrashListManager.Initialize();
                }

                if (_firebaseServicesConfig.CreateFirebaseDeepLinksManager) {
                    DynamicLinksManager = new DynamicLinksManager();
                    DynamicLinksManager.Initialize();
                }

                if (_firebaseServicesConfig.CreateFirebaseDatabaseManager) {
                    FirebaseDatabaseManager = new FirebaseDatabaseManager();
                    FirebaseDatabaseManager.Initialize(firebaseServicesConfig.firebaseDatabaseManagerConfig);
                }

                if (_firebaseServicesConfig.CreateFirebaseRemoteConfigManager) {
                    FirebaseRemoteConfigManager = new FirebaseRemoteConfigManager();
                    FirebaseRemoteConfigManager.Initialize();
                }

                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available) {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    var app = Firebase.FirebaseApp.DefaultInstance;

                    Debug.Log($"Message Sender Id: {app.Options.MessageSenderId}");

                    FirebaseAnalyticsManager.Initialize();
                    IsFirebaseCloudMessagingReady = CloudMessagingManager.Initialize();
                    IsFirebaseAuthenticationReady = AuthenticationManager.Initialize(_firebaseServicesConfig.authenticationTypes);

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                    Initialized = true;

                    Debug.Log($"<color=blue>Firebase dependencies: {dependencyStatus}</color>");
                } else {
                    Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
                    // Firebase Unity SDK is not safe to use here.
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <inheritdoc/>
        public List<FirebaseAuthenticationTypes> GetAuthenticationTypesList() {
            return _firebaseServicesConfig.authenticationTypes;
        }
    }
}