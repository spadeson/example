﻿using System;
using System.Collections.Generic;
using Core.FirebaseSubsystem.Config;
using Core.FirebaseSubsystem.Services.Analytics;
using Core.FirebaseSubsystem.Services.Authentication;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using Core.FirebaseSubsystem.Services.CloudMessaging;
using Core.FirebaseSubsystem.Services.CrashList;
using Core.FirebaseSubsystem.Services.Database;
using Core.FirebaseSubsystem.Services.DynamicLinks;
using Core.FirebaseSubsystem.Services.RemoteConfigsService;
using Firebase.Auth;

namespace Core.FirebaseSubsystem
{
    public interface IFirebaseManager
    {
        bool IsFirebaseAnalyticsReady { get; }
        FirebaseAnalyticsManager FirebaseAnalyticsManager { get; }

        bool IsFirebaseAuthenticationReady { get; }
        IFirebaseAuthenticationManager AuthenticationManager { get; }

        bool IsFirebaseCloudMessagingReady { get; }
        IFirebaseCloudMessagingManager CloudMessagingManager { get; }
        
        bool IsFirebaseCrashListReady { get; }
        IFirebaseCrashListManager CrashListManager { get; }

        IDynamicLinksManager DynamicLinksManager { get; }

        IFirebaseDatabaseManager FirebaseDatabaseManager { get; }
        
        IFirebaseRemoteConfigManager FirebaseRemoteConfigManager { get; }
        event Action OnFirebaseInitialized;
        bool Initialized { get; set; }
        void Initialize(FirebaseServicesConfig firebaseServicesConfig);
        
        /// <summary>
        /// Returns the list of Authentication types.
        /// </summary>
        /// <returns>The list of Authentication types.</returns>
        List<FirebaseAuthenticationTypes> GetAuthenticationTypesList();
    }
}