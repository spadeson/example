using UnityEngine;

namespace Core.FirebaseSubsystem.Services.CrashList {
    public class FirebaseCrashListManager : IFirebaseCrashListManager {
        public void Initialize() {
            Debug.Log("<color=cyan>Initialized Firebase CrashList service.</color>");
        }
    }
}
