using Core.AnalyticsSubsytem;
using Core.AnalyticsSubsytem.Providers;
using Firebase.Analytics;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.Analytics {
    public class FirebaseAnalyticsManager : IAnalyticsProvider {

        public bool Initialize() {
            Debug.Log("<color=red>FirebaseAnalyticsManager initialized.</color>");
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            return true;
        }

        public void LogEvent(string eventName) {
            FirebaseAnalytics.LogEvent(eventName);
        }

        public void LogEvent(string eventName, object eventParameters) {
            var parameters = eventParameters as Parameter[];
            FirebaseAnalytics.LogEvent(eventName, parameters);
        }

        public void LogEvent(string eventName, string eventParameterName, double eventParameter) {
            FirebaseAnalytics.LogEvent(eventName, eventParameterName, eventParameter);
        }

        public void LogEvent(string eventName, string eventParameterName, int eventParameter) {
            FirebaseAnalytics.LogEvent(eventName, eventParameterName, eventParameter);
        }

        public void LogEvent(string eventName, string eventParameterName, float eventParameter) {
            FirebaseAnalytics.LogEvent(eventName, eventParameterName, eventParameter);
        }

        public void LogEvent(string eventName, string eventParameterName, long eventParameter) {
            FirebaseAnalytics.LogEvent(eventName, eventParameterName, eventParameter);
        }

        public void LogEvent(string eventName, string eventParameterName, string eventParameter) {
            FirebaseAnalytics.LogEvent(eventName, eventParameterName, eventParameter);
        }

        public void SetUserProperty(string propertyName, string propertyValue) {
            FirebaseAnalytics.SetUserProperty(propertyName, propertyValue);
        }
    }
}