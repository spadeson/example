using System;

namespace Core.FirebaseSubsystem.Services.Analytics.Data {
    [Serializable]
    public class FirebaseAnalyticsData {
        public string eventName;
        public string timeStamp;
    }
}
