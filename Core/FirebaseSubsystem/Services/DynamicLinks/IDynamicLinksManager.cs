using System.Collections.Generic;
using PoketPet.Network.API.RESTModels.DynamicLinks;

namespace Core.FirebaseSubsystem.Services.DynamicLinks {
    public interface IDynamicLinksManager { 
        List<string> DynamicLinksList { get; set; }
        void                         Initialize();
        void                         AddNewDynamicLink(string dynamicLink);
        DynamicLinkDataWaitForInvite GetWaitForInviteDynamicLinkData();
        DynamicLinkDataInvite GetInviteDynamicLinkData();
    }
}
