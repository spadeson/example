using System.Collections.Generic;
using Firebase.DynamicLinks;
using PoketPet.Network.API.RESTModels.DynamicLinks;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.DynamicLinks {
    public class DynamicLinksManager : IDynamicLinksManager {
        public List<string> DynamicLinksList { get; set; }

        public void Initialize() {
            Debug.Log("<color=purple>FirebaseDeepLinksManager was initialized</color>");
            DynamicLinksList = new List<string>();
            Firebase.DynamicLinks.DynamicLinks.DynamicLinkReceived += OnDynamicLink;
        }

        public void AddNewDynamicLink(string dynamicLink) {
            Debug.Log($"<color=purple>New Dynamic Link: {dynamicLink}</color>");
            DynamicLinksList.Add(dynamicLink);
        }

        public DynamicLinkDataWaitForInvite GetWaitForInviteDynamicLinkData() {
            
            var link = string.Empty;

            foreach (var dynamicLink in DynamicLinksList) {
                if (!dynamicLink.Contains("wait_invite")) continue;
                link = dynamicLink;
                break;
            }

            if (string.IsNullOrEmpty(link)) return null;
            
            var data          = link.Split('?')[1];
            var dataArguments = data.Split('&');

            var dynamicLinkDataWaitForInvite = new DynamicLinkDataWaitForInvite {
                mail = dataArguments[0].Split('=')[1],
                type = dataArguments[1].Split('=')[1],
                @from = dataArguments[2].Split('=')[1],
                pro = dataArguments[3].Split('=')[1],
                time = dataArguments[4].Split('=')[1]
            };

            return dynamicLinkDataWaitForInvite;
        }

        public DynamicLinkDataInvite GetInviteDynamicLinkData() {
            var link = string.Empty;

            foreach (var dynamicLink in DynamicLinksList) {
                if (!dynamicLink.Contains("invite") || dynamicLink.Contains("wait_")) continue;
                link = dynamicLink;
                break;
            }

            if (string.IsNullOrEmpty(link)) return null;
            
            var data          = link.Split('?')[1];
            var dataArguments = data.Split('&');

            var dynamicLinkDataInvite = new DynamicLinkDataInvite {
                mail = dataArguments[0].Split('=')[1],
                type = dataArguments[1].Split('=')[1],
                petID = dataArguments[2].Split('=')[1],
                @from = dataArguments[3].Split('=')[1],
                permissions = dataArguments[4].Split('=')[1],
                pro = dataArguments[5].Split('=')[1],
                time = dataArguments[6].Split('=')[1]
            };

            return dynamicLinkDataInvite;
        }

        private void OnDynamicLink(object sender, ReceivedDynamicLinkEventArgs args) {
            var dynamicLinkEventArgs = args as ReceivedDynamicLinkEventArgs;
            Debug.LogFormat("Received dynamic link {0}", dynamicLinkEventArgs.ReceivedDynamicLink.Url.OriginalString);
            if (!string.IsNullOrEmpty(dynamicLinkEventArgs.ReceivedDynamicLink.Url.OriginalString)) {
                DynamicLinksList.Add(dynamicLinkEventArgs.ReceivedDynamicLink.Url.OriginalString);
            }
        }
    }
}
