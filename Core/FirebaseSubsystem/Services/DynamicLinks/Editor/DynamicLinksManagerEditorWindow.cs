using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;
using CoreMobile;

namespace Core.FirebaseSubsystem.Services.DynamicLinks.Editor {

    public class DynamicLinksManagerEditorWindow : EditorWindow {
        
        private DynamicLinksManager _dynamicLinksManager;

        private string _newDynamicLink;
        
        [MenuItem("Window/PoketPet/DynamicLinks/DynamicLinksManager")]
        private static void Initialize() {
            var window = GetWindow<DynamicLinksManagerEditorWindow>();
            window.titleContent = new GUIContent("Dynamic Links Manager");
            window.Show();
        }

        private void OnGUI() {
            if (_dynamicLinksManager == null) {
                
                EditorGUILayout.HelpBox("DynamicLinksManager instance is null. Please find DynamicLinksManager existing instance.", MessageType.Error);
                
                if (GUILayout.Button("Find Dynamic Links Manager", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                    _dynamicLinksManager = (DynamicLinksManager) VIS.CoreMobile.CoreMobile.Instance.FirebaseManager.DynamicLinksManager;
                }
            } else {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                
                EditorGUILayout.LabelField("Dynamic Links:", EditorStyles.boldLabel);

                foreach (var dynamicLink in _dynamicLinksManager.DynamicLinksList) {
                    EditorGUILayout.LabelField($"{dynamicLink}");
                }
                
                EditorGUILayout.EndVertical();
                
                EditorGUILayout.Space();
                
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                
                EditorGUILayout.LabelField("Add new dynamic Link", EditorStyles.boldLabel);

                _newDynamicLink = EditorGUILayout.TextField("", _newDynamicLink);

                if (!string.IsNullOrEmpty(_newDynamicLink)) {
                    if (GUILayout.Button("Add new Dynamic Link", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                        _dynamicLinksManager.AddNewDynamicLink(_newDynamicLink);       
                    }
                }

                EditorGUILayout.EndVertical();
            }
        }
    }
}
