namespace Core.FirebaseSubsystem.Services.RemoteConfigsService {
    public interface IFirebaseRemoteConfigManager {
        void   Initialize();
        string GetRemoteStringValue(string parameterKey);
    }
}
