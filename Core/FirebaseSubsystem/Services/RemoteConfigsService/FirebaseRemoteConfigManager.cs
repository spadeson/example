using System;
using System.Threading.Tasks;
using Firebase.Extensions;
using Firebase.RemoteConfig;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.RemoteConfigsService {
    public class FirebaseRemoteConfigManager : IFirebaseRemoteConfigManager {
        public void Initialize() {
            #if UNITY_EDITOR
            FirebaseRemoteConfig.Settings = new ConfigSettings {IsDeveloperMode = true};
            #else
            FirebaseRemoteConfig.Settings = new ConfigSettings {IsDeveloperMode = false};
            #endif

            FetchDataAsync();
        }

        private Task FetchDataAsync() {
            Debug.Log("Fetching data...");
            var fetchTask = FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
            return fetchTask.ContinueWithOnMainThread(FetchComplete);
        }

        private void FetchComplete(Task obj) {
            Debug.Log("FirebaseRemoteConfigManager: FetchCompleted");
            FirebaseRemoteConfig.ActivateFetched();
        }

        public string GetRemoteStringValue(string parameterKey) {
            return FirebaseRemoteConfig.GetValue(parameterKey).StringValue;
        }
    }
}