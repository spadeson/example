﻿using System;

namespace Core.FirebaseSubsystem.Services.CloudMessaging
{
    public interface IFirebaseCloudMessagingManager
    {
        bool Initialize();
        event Action OnMessageReceived;
        event Action<string> OnTokenReceived;
        string FirebaseToken { get; set; }
        void FCMTokenEnable();
        void FCMTokenDisable();
    }
}