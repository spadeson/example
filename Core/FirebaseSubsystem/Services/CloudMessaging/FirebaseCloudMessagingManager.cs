﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.FCM;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;

namespace Core.FirebaseSubsystem.Services.CloudMessaging {
    public class FirebaseCloudMessagingManager : IFirebaseCloudMessagingManager {
        public event Action OnMessageReceived;
        public event Action<string> OnTokenReceived;
        public string FirebaseToken { get; set; }
        public void FCMTokenEnable() {
            var requestData             = new RequestData<FCMToggleRequestModel> {from = UserProfileManager.CurrentUser.id, action = "on_fcm"};
            var enableFcmTokenOperation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("fcm", requestData);
        }

        public void FCMTokenDisable() {
            var requestData             = new RequestData<FCMToggleRequestModel> {from = UserProfileManager.CurrentUser.id, action = "off_fcm"};
            var disableFcmTokenOperation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("fcm", requestData);
        }

        public void OnDestroy() {
            //Firebase.Messaging.FirebaseMessaging.MessageReceived -= OnMessageReceivedHandler;
            Firebase.Messaging.FirebaseMessaging.TokenReceived -= OnTokenReceivedHandler;
        }

        // Setup message event handlers.
        public bool Initialize() {
            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceivedHandler;
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceivedHandler;
            Debug.Log("<color=blue>FirebaseCloudMessagingManager has been initialized.</color>");
            return true;
        }

        private void OnMessageReceivedHandler(object sender, Firebase.Messaging.MessageReceivedEventArgs e) {
            Debug.Log("Received a new message");
            var notification = e.Message.Notification;
            if (notification != null) {
                Debug.Log("title: " + notification.Title);
                Debug.Log("body: "  + notification.Body);
                var android = notification.Android;
                if (android != null) {
                    Debug.Log("android channel_id: " + android.ChannelId);
                }
            }

            if (e.Message.From.Length > 0) Debug.Log("from: " + e.Message.From);
            if (e.Message.Link != null) {
                Debug.Log("link: " + e.Message.Link);
            }

            if (e.Message.Data.Count > 0) {
                Debug.Log("data:");
                foreach (KeyValuePair<string, string> iter in e.Message.Data) {
                    Debug.Log("  " + iter.Key + ": " + iter.Value);
                }
            }

            OnMessageReceived?.Invoke();
        }

        private void OnTokenReceivedHandler(object sender, Firebase.Messaging.TokenReceivedEventArgs token) {
            Debug.Log("Received Registration Token: " + token.Token);
            FirebaseToken = token.Token;
            OnTokenReceived?.Invoke(FirebaseToken);
        }

        private bool LogTaskCompletion(Task task, string operation) {
            var complete = false;
            if (task.IsCanceled) {
                Debug.Log(operation + " canceled.");
            } else if (task.IsFaulted) {
                Debug.Log(operation + " encounted an error.");
                if (task.Exception != null)
                    foreach (var exception in task.Exception.Flatten().InnerExceptions) {
                        var errorCode  = "";
                        var firebaseEx = exception as Firebase.FirebaseException;
                        if (firebaseEx != null) {
                            errorCode = $"Error.{((Firebase.Messaging.Error) firebaseEx.ErrorCode).ToString()}: ";
                        }

                        Debug.Log(errorCode + exception);
                    }
            } else if (task.IsCompleted) {
                Debug.Log(operation + " completed");
                complete = true;
            }

            return complete;
        }

        public void ToggleTokenOnInit() {
            bool newValue = !Firebase.Messaging.FirebaseMessaging.TokenRegistrationOnInitEnabled;
            Firebase.Messaging.FirebaseMessaging.TokenRegistrationOnInitEnabled = newValue;
            Debug.Log("Set TokenRegistrationOnInitEnabled to " + newValue);
        }
    }
}