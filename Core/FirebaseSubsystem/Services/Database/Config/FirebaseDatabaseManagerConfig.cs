using UnityEngine;

namespace Core.FirebaseSubsystem.Services.Database.Config {
    [CreateAssetMenu(fileName = "FirebaseDatabaseManagerConfig", menuName = "PoketPet/Core/Firebase/Database/Firebase Database Manager Config", order = 0)]
    public class FirebaseDatabaseManagerConfig : ScriptableObject {
        public string databaseName;
    }
}
