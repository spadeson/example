using System;
using System.Threading.Tasks;
using Core.FirebaseSubsystem.Services.Database.Config;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.Database {

    public class FirebaseDatabaseManager : IFirebaseDatabaseManager {
        
        private FirebaseDatabaseManagerConfig FirebaseDatabaseManagerConfig { get; set; }
        
        public event Action<string> OnValueGet;
        
        public bool Initialize(FirebaseDatabaseManagerConfig firebaseDatabaseManagerConfig) {
            Debug.Log("<color=blue>FirebaseDatabaseManager has been initialized.</color>");
            FirebaseDatabaseManagerConfig = firebaseDatabaseManagerConfig;
            return true;
        }
        
        public void SetEditorDatabaseUrl(string url) {
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(url);
        }
        
        public void GetValueAsync(string path) {
            FirebaseDatabase.DefaultInstance
                .GetReference(path)
                .GetValueAsync().ContinueWith(task => {
                    if (task.IsFaulted) {
                        // Handle the error...
                    }
                    else if (task.IsCompleted) {
                        DataSnapshot snapshot = task.Result;
                        Debug.Log("FirebaseGetReference: " + snapshot.GetRawJsonValue());
                        OnValueGet?.Invoke(snapshot.GetRawJsonValue());
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}