using System;
using Core.FirebaseSubsystem.Services.Database.Config;

namespace Core.FirebaseSubsystem.Services.Database {
    
    public interface IFirebaseDatabaseManager {
        event Action<string> OnValueGet;
        bool Initialize(FirebaseDatabaseManagerConfig firebaseDatabaseManagerConfig);
        void SetEditorDatabaseUrl(string url);
        void GetValueAsync(string path);
    }
}