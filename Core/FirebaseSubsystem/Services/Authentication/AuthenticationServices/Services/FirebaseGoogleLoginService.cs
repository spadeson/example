﻿using System;
using System.Threading.Tasks;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Firebase.Auth;
using UnityEngine;
using VIS.CoreMobile;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services {
    public class FirebaseGoogleLoginService : IFirebaseLoginService {
        public FirebaseAuthenticationTypes AuthType { get; set; }
        public event Action<FirebaseUser> OnLoginCompleted;
        public event Action<Exception> OnLoginFailed;

        private readonly IFirebaseAuthenticationManager _authenticationManager;

        public FirebaseGoogleLoginService(FirebaseAuthenticationTypes authType, IFirebaseAuthenticationManager authenticationManager) {
            AuthType = authType;
            _authenticationManager = authenticationManager;
        }

        public void Login(IFirebaseServiceLoginData data) {
            CoreMobile.Instance.GoogleManager.OnSuccessLogin += FirebaseLoginWithGoogleTokens;
            
            #if !UNITY_EDITOR
            CoreMobile.Instance.GoogleManager.SignIn();
            #endif
        }

        private void FirebaseLoginWithGoogleTokens(string googleIdToken) {
            var credential = GoogleAuthProvider.GetCredential(googleIdToken, null);

            _authenticationManager.Auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("SignInWithCredentialAsync was canceled.");
                    return;
                }

                if (task.IsFaulted) {
                    if (task.Exception == null) return;
                    foreach (var firebaseException in  task.Exception.InnerExceptions) {
                        if (firebaseException.InnerException == null) continue;
                        Debug.LogError("FirebaseEmailLoginService encountered an error: " + firebaseException.InnerException.Message);
                        OnLoginFailed?.Invoke(firebaseException.InnerException);
                    }

                    return;
                }

                _authenticationManager.User = task.Result;
                OnLoginCompleted?.Invoke(task.Result);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
