using System;
using System.Threading.Tasks;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Firebase.Auth;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services {
    public class FirebaseEmailLoginService : IFirebaseLoginService {
        public FirebaseAuthenticationTypes AuthType { get; set; }
        public event Action<FirebaseUser> OnLoginCompleted;
        public event Action<Exception> OnLoginFailed;

        private readonly IFirebaseAuthenticationManager _authenticationManager;

        public FirebaseEmailLoginService(FirebaseAuthenticationTypes authType, IFirebaseAuthenticationManager authenticationManager) {
            AuthType = authType;
            _authenticationManager = authenticationManager;
        }

        public void Login(IFirebaseServiceLoginData data) {
            
            Debug.Log("Firebase login with email...");
            
            var email = data.GetDataItem<string>("email");
            var password = data.GetDataItem<string>("password");

            _authenticationManager.Auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {

                if (task.IsCanceled) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                
                if (task.IsFaulted) {
                    if (task.Exception == null) return;
                    foreach (var firebaseException in  task.Exception.InnerExceptions) {
                        if (firebaseException.InnerException == null) continue;
                        Debug.LogError("FirebaseEmailLoginService encountered an error: " + firebaseException.InnerException.Message);
                        OnLoginFailed?.Invoke(firebaseException.InnerException);
                    }

                    return;
                }

                Debug.Log("SignInWithEmailAndPasswordAsync was completed!");
                _authenticationManager.User = task.Result;
                OnLoginCompleted?.Invoke(task.Result);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}

