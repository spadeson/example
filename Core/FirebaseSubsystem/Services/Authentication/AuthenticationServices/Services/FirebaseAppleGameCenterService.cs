using System;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Firebase.Auth;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services {
    public class FirebaseAppleGameCenterService : IFirebaseLoginService {
        public FirebaseAuthenticationTypes AuthType { get; set; }

        public event Action<FirebaseUser> OnLoginCompleted;

        public event Action<Exception> OnLoginFailed;

        private IFirebaseAuthenticationManager _authenticationManager;

        public FirebaseAppleGameCenterService(FirebaseAuthenticationTypes authType, IFirebaseAuthenticationManager authenticationManager) {
            AuthType = authType;
            _authenticationManager = authenticationManager;
        }

        public void Login(IFirebaseServiceLoginData data) {
            Debug.Log("firebase login with Apple Game Center");
            
            GameCenterAuthProvider.GetCredentialAsync().ContinueWith(credentialTask => {
                
                if (credentialTask.IsCanceled) {
                    Debug.LogWarning("GameCenterAuthCredentialAsync was canceled.");
                    return;
                }

                if (credentialTask.IsFaulted) {
                    Debug.LogError("GameCenterAuthCredentialAsync encountered an error: " + credentialTask.Exception);
                    return;
                }

                var credential = credentialTask.Result;
                Debug.LogFormat($"GameCenterAuthCredentialAsync successfully");

                _authenticationManager.Auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                    if (task.IsCanceled) {
                        Debug.LogWarning("SignInWithGameCenterAsync was canceled.");
                        return;
                    }

                    if (task.IsFaulted) {
                        Debug.LogError("SignInWithGameCenterAsync encountered an error: " + task.Exception);
                        Debug.LogError("Credential: "                                     + credential.Provider);

                        _authenticationManager.LinkUserWithCredential(_authenticationManager.User, credential, OnLoginCompleted);

                        if (_authenticationManager.User == null) {
                            _authenticationManager.Auth.SignInAndRetrieveDataWithCredentialAsync(credential).ContinueWith(ts => {
                                if (ts.IsCanceled) {
                                    Debug.LogError($"TS was canceled");
                                }
                                
                                if (ts.IsFaulted) {
                                    Debug.LogError($"TS Error: {ts.Exception}");
                                }

                                Debug.Log($"SignInAndRetrieveDataWithCredentialAsync: {task.Result}");
                                _authenticationManager.User = ts.Result.User;
                                OnLoginCompleted?.Invoke(_authenticationManager.User);
                            });
                        }

                        return;
                    }

                    Debug.LogFormat($"User signed in successfully: {task.Result}");
                    OnLoginCompleted?.Invoke(task.Result);
                    _authenticationManager.User = task.Result;
                });
            });
        }
    }
}
