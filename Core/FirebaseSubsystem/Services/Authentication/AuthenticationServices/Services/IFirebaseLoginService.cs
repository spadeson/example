﻿using System;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Firebase.Auth;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services
{
    public interface IFirebaseLoginService {
        FirebaseAuthenticationTypes AuthType { get; set; }
        event Action<FirebaseUser> OnLoginCompleted;
        event Action<Exception> OnLoginFailed;
        void Login(IFirebaseServiceLoginData data);
    }
}