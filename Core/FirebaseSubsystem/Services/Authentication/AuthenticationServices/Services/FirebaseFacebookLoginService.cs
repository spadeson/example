﻿using System;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Firebase.Auth;
using UnityEngine;
using VIS.CoreMobile;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services
{
    public class FirebaseFacebookLoginService : IFirebaseLoginService
    {
        public FirebaseAuthenticationTypes AuthType { get; set; }
        public event Action<FirebaseUser> OnLoginCompleted;
        public event Action<Exception> OnLoginFailed;
        
        private IFirebaseAuthenticationManager _authenticationManager;

        public FirebaseFacebookLoginService() {
            
        }
        
        public FirebaseFacebookLoginService(FirebaseAuthenticationTypes authType, IFirebaseAuthenticationManager authenticationManager) {
            AuthType = authType;
            _authenticationManager = authenticationManager;
        }

        public void Login(IFirebaseServiceLoginData data) {
            
            Debug.Log("firebase login with facebook");

            if (!CoreMobile.Instance.FacebookManager.IsLogged()) {
                CoreMobile.Instance.FacebookManager.Login();
            }
            
            CoreMobile.Instance.FacebookManager.OnSuccessLogin += accessToken => {

                var credential = FacebookAuthProvider.GetCredential(accessToken);

                _authenticationManager.Auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
                    
                    if (task.IsCanceled) {
                        Debug.LogWarning("SignInWithFacebookAsync was canceled.");
                        return;
                    }

                    if (task.IsFaulted) {
                        
                        Debug.LogError("SignInWithFacebookAsync encountered an error: " + task.Exception);
                        Debug.LogError("Credential: " + credential.Provider);
                        
                        _authenticationManager.LinkUserWithCredential(_authenticationManager.User, credential, OnLoginCompleted);

                        if (_authenticationManager.User == null) {
                            _authenticationManager.Auth.SignInAndRetrieveDataWithCredentialAsync(credential).ContinueWith(ts => {
                                if (ts.IsFaulted) {
                                    Debug.LogError($"TS Error: {ts.Exception}");
                                }

                                Debug.Log($"SignInAndRetrieveDataWithCredentialAsync: {task.Result}");
                                _authenticationManager.User = ts.Result.User;
                                OnLoginCompleted?.Invoke(_authenticationManager.User);
                            });
                        }

                        return;
                    }

                    var newUser = task.Result;
                    Debug.LogFormat($"User signed in successfully: {newUser.DisplayName}");
                    OnLoginCompleted?.Invoke(newUser);
                });
            };
        }
    }
}