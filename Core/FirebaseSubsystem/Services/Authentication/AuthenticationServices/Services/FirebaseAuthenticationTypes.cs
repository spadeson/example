namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services {
    public enum FirebaseAuthenticationTypes {
        EMAIL = 0,
        FACEBOOK = 1,
        GOOGLE = 2,
        GAME_CENTER = 3
    }
}
