using System;
using System.Collections.Generic;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data {
    [Serializable]
    public class FirebaseEmailLoginData : IFirebaseServiceLoginData {
        public Dictionary<string, object> FirebaseLoginDataItems { get; set; }

        public FirebaseEmailLoginData() {
            FirebaseLoginDataItems = new Dictionary<string, object>();
        }

        public void AddNewDataItem<T>(string key, T data) {
            if (!FirebaseLoginDataItems.ContainsKey(key)) {
                FirebaseLoginDataItems.Add(key, data);
            }
        }

        public T GetDataItem<T>(string key) {
            return (T)FirebaseLoginDataItems[key];
        }
    }
}
