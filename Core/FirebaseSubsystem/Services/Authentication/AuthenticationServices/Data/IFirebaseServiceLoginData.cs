using System.Collections.Generic;

namespace Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data {
    public interface IFirebaseServiceLoginData {
        Dictionary<string, object> FirebaseLoginDataItems { get; set; }

        void AddNewDataItem<T>(string key, T data);
        T GetDataItem<T>(string key);
    }
}
