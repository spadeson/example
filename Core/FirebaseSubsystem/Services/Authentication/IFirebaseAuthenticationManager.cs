﻿using System;
using System.Collections.Generic;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using Firebase.Auth;

namespace Core.FirebaseSubsystem.Services.Authentication
{
    public interface IFirebaseAuthenticationManager
    {
        FirebaseAuth Auth { get; set; }
        FirebaseUser User { get; set; }
        string FirebaseToken { get; set; }
        
        bool Initialize(List<FirebaseAuthenticationTypes> authenticationTypes);
        void CreateNewUser(string email, string password);
        void Login(FirebaseAuthenticationTypes authType, IFirebaseServiceLoginData data = null);
        void LinkUserWithCredential(FirebaseUser currentUser, Credential credential, Action<FirebaseUser> success);
        void DisplayIdentityProviders(String email);
        void Logout();
        
        event Action<FirebaseUser> OnLoginCompleted;
        event Action<Exception> OnLoginFailed;
        event Action OnNewUserCreationStart;
        event Action<FirebaseUser> OnNewUserCreationCompleted;
        event Action<Exception> OnNewUserCreationFailed;
        event Action<string> OnFirebaseTokenRecieved;
        event Action OnLogout;
    }
}