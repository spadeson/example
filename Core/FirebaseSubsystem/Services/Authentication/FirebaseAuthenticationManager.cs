﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using Firebase.Auth;
using UnityEngine;

namespace Core.FirebaseSubsystem.Services.Authentication {
    public class FirebaseAuthenticationManager : IFirebaseAuthenticationManager {
        
        private List<FirebaseAuthenticationTypes> _authenticationTypes;
        private Dictionary<FirebaseAuthenticationTypes, IFirebaseLoginService> AuthServicesDictionary { get; set; }
        public event Action<FirebaseUser> OnLoginCompleted;
        public event Action<Exception> OnLoginFailed;
        public event Action OnNewUserCreationStart;
        public event Action<FirebaseUser> OnNewUserCreationCompleted;
        public event Action<Exception> OnNewUserCreationFailed;
        public event Action<string> OnFirebaseTokenRecieved;
        public event Action OnLogout;

        //Firebase Authentication Instance
        public FirebaseAuth Auth { get; set; }
        
        //Firebase User
        public FirebaseUser User { get; set; }
        
        //Firebase Token
        public string FirebaseToken { get; set; }

        public bool Initialize(List<FirebaseAuthenticationTypes> authenticationTypes) {
            
            Debug.Log("<color=green>Firebase Authentication manager has been initialized.</color>");
            
            _authenticationTypes = authenticationTypes;
            CreateLoginServices(_authenticationTypes);

            Auth = FirebaseAuth.DefaultInstance;
            Auth.StateChanged += AuthOnStateChanged;
            AuthOnStateChanged(this, null);

            return true;
        }

        public void CreateNewUser(string email, string password) {
            
            OnNewUserCreationStart?.Invoke();
            
            Auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(userCreationTask => {
                if (userCreationTask.IsCanceled) {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }

                if (userCreationTask.IsFaulted) {
                    if (userCreationTask.Exception == null) return;
                    foreach (var firebaseException in userCreationTask.Exception.InnerExceptions) {
                        OnNewUserCreationFailed?.Invoke(firebaseException.InnerException);
                    }

                    return;
                }

                // Firebase user has been created.
                User = userCreationTask.Result;

                OnNewUserCreationCompleted?.Invoke(User);

                User.SendEmailVerificationAsync().ContinueWith(verification => {
                    if (verification.IsCanceled) {
                        Debug.LogError("verification was canceled.");
                        return;
                    }

                    if (verification.IsFaulted) {
                        Debug.LogError("verification send has an error canceled.");
                        return;
                    }

                    Debug.Log("Verification was send successful!");
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void AuthOnStateChanged(object sender, EventArgs e) {
            if (Auth.CurrentUser == User) return;
            var signedIn = User != Auth.CurrentUser && Auth.CurrentUser != null;
            if (!signedIn && User != null) {
                Debug.Log("Signed out " + User.UserId);
                OnLogout?.Invoke();
            }
            User = Auth.CurrentUser;
            if (signedIn) {
                Debug.Log("Signed in " + User.UserId);
                GetFirebaseToken();
            }
        }

        private void GetFirebaseToken() {
            User.TokenAsync(true).ContinueWith(task => {
                
                if (task.IsCanceled) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                
                if (task.IsFaulted) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.LogFormat($"Firebase Token gets successfully: {task.Result}");

                FirebaseToken = task.Result;
                
                OnFirebaseTokenRecieved?.Invoke(FirebaseToken);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void Login(FirebaseAuthenticationTypes authType, IFirebaseServiceLoginData dataObject = null) {
            var authService = AuthServicesDictionary[authType];
            
            authService.OnLoginFailed += error => {
                OnLoginFailed?.Invoke(error);
            };
            
            authService.OnLoginCompleted += firebaseUser => {
                AuthOnStateChanged(this, null);
                OnLoginCompleted?.Invoke(firebaseUser);
            };
            
            authService.Login(dataObject);
        }

        public void Logout() {
            Auth?.SignOut();
        }

        public void LinkUserWithCredential(FirebaseUser user, Credential credential, Action<FirebaseUser> success) {
            //current user
            user.LinkWithCredentialAsync(credential).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("LinkWithCredentialAsync was canceled.");
                    return;
                }

                if (task.IsFaulted) {
                    Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
                    OnLoginFailed?.Invoke(task.Exception);
                    return;
                }

                FirebaseUser newUser = task.Result;
                success.Invoke(newUser);
                Debug.LogFormat("Credentials successfully linked to Firebase user: {0} ({1})", newUser.DisplayName, newUser.UserId);
            });
        }

        public void DisplayIdentityProviders(String email) {
            Auth.FetchProvidersForEmailAsync(email).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("LinkWithCredentialAsync was canceled.");
                    return;
                }

                if (task.IsFaulted) {
                    Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.Log("Email Providers:");
                foreach (string provider in task.Result) {
                    Debug.Log(provider);
                }
            });
        }

        private void CreateLoginServices(List<FirebaseAuthenticationTypes> authenticationTypes) {
            
            AuthServicesDictionary = new Dictionary<FirebaseAuthenticationTypes, IFirebaseLoginService>();

            foreach (var type in authenticationTypes) {
                switch (type) {
                    case FirebaseAuthenticationTypes.EMAIL:
                        AuthServicesDictionary.Add(type, new FirebaseEmailLoginService(type, this));
                        break;
                    case FirebaseAuthenticationTypes.FACEBOOK:
                        AuthServicesDictionary.Add(type, new FirebaseFacebookLoginService(type, this));
                        break;
                    case FirebaseAuthenticationTypes.GOOGLE:
                        AuthServicesDictionary.Add(type, new FirebaseGoogleLoginService(type, this));
                        break;
                    case FirebaseAuthenticationTypes.GAME_CENTER:
                        //TODO Implement login service with Game Center
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
