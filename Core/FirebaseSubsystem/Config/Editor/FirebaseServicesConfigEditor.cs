﻿using System.Collections.Generic;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using Core.FirebaseSubsystem.Services.Database.Config;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Core.FirebaseSubsystem.Config.Editor
{
    [CustomEditor(typeof(FirebaseServicesConfig))]
    public class FirebaseServicesConfigEditor : UnityEditor.Editor {

        Vector2 _scrollPosition;

        private FirebaseServicesConfig _firebaseServicesConfig;

        public override void OnInspectorGUI() {

            base.OnInspectorGUI();

            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            #region Firebase Analytics

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase Analytics:", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase Analytics services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseAnalyticsManager = EditorGUILayout.Toggle("Firebase Analytics", _firebaseServicesConfig.CreateFirebaseAnalyticsManager);
            EditorGUILayout.EndVertical();

            #endregion

            #region Firebase Authentication
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase Authentication:", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase Authentication services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseAuthenticationManager = EditorGUILayout.Toggle("Firebase Analytics", _firebaseServicesConfig.CreateFirebaseAuthenticationManager);
            
            EditorGUILayout.Space();

            if (_firebaseServicesConfig.CreateFirebaseAuthenticationManager) {
                reorderableList.DoLayoutList();
            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            #endregion
        
            #region Firebase Cloud Messaging
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase Cloud Messaging", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase Cloud Messaging services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseCloudMessagingManager = EditorGUILayout.Toggle("Firebase Cloud Messaging", _firebaseServicesConfig.CreateFirebaseCloudMessagingManager);
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            #endregion
            
            #region Firebase Crashlist
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase CrashList", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase CrashList services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseCrashListManager = EditorGUILayout.Toggle("Firebase CrashList", _firebaseServicesConfig.CreateFirebaseCrashListManager);
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            #endregion
            
            #region Firebase DeepLinks
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase DeepLinks", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase DeepLinks services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseDeepLinksManager = EditorGUILayout.Toggle("Firebase DeepLinks", _firebaseServicesConfig.CreateFirebaseDeepLinksManager);
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            #endregion
            
            #region Firebase Database
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase Database", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase Database services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseDatabaseManager = EditorGUILayout.Toggle("Firebase Database", _firebaseServicesConfig.CreateFirebaseDatabaseManager);
            EditorGUILayout.EndVertical();

            if (_firebaseServicesConfig.CreateFirebaseDatabaseManager) {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Firebase Database Manager Config", EditorStyles.boldLabel);
                _firebaseServicesConfig.firebaseDatabaseManagerConfig = (FirebaseDatabaseManagerConfig)EditorGUILayout.ObjectField("Database Manager Config", _firebaseServicesConfig.firebaseDatabaseManagerConfig, typeof(FirebaseDatabaseManagerConfig), false);   
            }

            EditorGUILayout.Space();
            #endregion
            
            #region Firebase Remote Config
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Firebase Remote Config", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("Check it to use Firebase Remote Config services.", MessageType.Info, true);
            _firebaseServicesConfig.CreateFirebaseRemoteConfigManager = EditorGUILayout.Toggle("Firebase Remote Config", _firebaseServicesConfig.CreateFirebaseRemoteConfigManager);
            EditorGUILayout.EndVertical();

            // if (_firebaseServicesConfig.CreateFirebaseRemoteConfigManager) {
            //     EditorGUILayout.Space();
            //     EditorGUILayout.LabelField("Firebase Database Manager Config", EditorStyles.boldLabel);
            //     _firebaseServicesConfig.firebaseDatabaseManagerConfig = (FirebaseDatabaseManagerConfig)EditorGUILayout.ObjectField("Database Manager Config", _firebaseServicesConfig.firebaseDatabaseManagerConfig, typeof(FirebaseDatabaseManagerConfig), false);   
            // }

            EditorGUILayout.Space();
            #endregion
            
            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }

            EditorGUILayout.EndScrollView();
        }
    
        private ReorderableList reorderableList;
        
        private List<FirebaseAuthenticationTypes> FirebaseAuthenticationTypesList => _firebaseServicesConfig.authenticationTypes;

        private void OnEnable()
        {
            _firebaseServicesConfig = (FirebaseServicesConfig)target;
            
            if (_firebaseServicesConfig == null && !_firebaseServicesConfig.CreateFirebaseAuthenticationManager) return;
            
            reorderableList = new ReorderableList(FirebaseAuthenticationTypesList, typeof(List<FirebaseAuthenticationTypes>), true, true, true, true);

            // This could be used aswell, but I only advise this your class inherrits from UnityEngine.Object or has a CustomPropertyDrawer
            // Since you'll find your item using: serializedObject.FindProperty("list").GetArrayElementAtIndex(index).objectReferenceValue
            // which is a UnityEngine.Object
            // reorderableList = new ReorderableList(serializedObject, serializedObject.FindProperty("list"), true, true, true, true);

            // Add listeners to draw events
            reorderableList.drawHeaderCallback += DrawHeader;
            reorderableList.drawElementCallback += DrawElement;

            reorderableList.onAddCallback += AddItem;
            reorderableList.onRemoveCallback += RemoveItem;
        }
        private void OnDisable()
        {
            if (_firebaseServicesConfig == null && !_firebaseServicesConfig.CreateFirebaseAuthenticationManager) return;
            
            // Make sure we don't get memory leaks etc.
            reorderableList.drawHeaderCallback -= DrawHeader;
            reorderableList.drawElementCallback -= DrawElement;

            reorderableList.onAddCallback -= AddItem;
            reorderableList.onRemoveCallback -= RemoveItem;
        }
        private void DrawHeader(Rect rect)
        {
            GUI.Label(rect, "Firebase authorization types:");
        }

        private void DrawElement(Rect rect, int index, bool active, bool focused) {
            EditorGUI.BeginChangeCheck();
            FirebaseAuthenticationTypesList[index] = (FirebaseAuthenticationTypes) EditorGUI.EnumPopup(new Rect(rect.x, rect.y, 220, rect.height), FirebaseAuthenticationTypesList[index]);

            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(target);
            }

            // If you are using a custom PropertyDrawer, this is probably better
            // EditorGUI.PropertyField(rect, serializedObject.FindProperty("list").GetArrayElementAtIndex(index));
            // Although it is probably smart to cach the list as a private variable ;)
        }

        private void AddItem(ReorderableList list)
        {
            FirebaseAuthenticationTypesList.Add(FirebaseAuthenticationTypes.EMAIL);

            EditorUtility.SetDirty(target);
        }
        private void RemoveItem(ReorderableList list)
        {
            FirebaseAuthenticationTypesList.RemoveAt(list.index);

            EditorUtility.SetDirty(target);
        }
    }
}
