﻿using UnityEngine;
using System.Collections.Generic;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using Core.FirebaseSubsystem.Services.Database.Config;

namespace Core.FirebaseSubsystem.Config {
    [CreateAssetMenu(fileName = "FirebaseServicesConfig", menuName = "VIS/Firebase/Firebase Services Config")]
    public class FirebaseServicesConfig : ScriptableObject {
        [HideInInspector] public bool CreateFirebaseAnalyticsManager;
        [HideInInspector] public bool CreateFirebaseAuthenticationManager;
        [HideInInspector] public List<FirebaseAuthenticationTypes> authenticationTypes = new List<FirebaseAuthenticationTypes>();
        [HideInInspector] public bool CreateFirebaseCloudMessagingManager;
        [HideInInspector] public bool CreateFirebaseCrashListManager;
        [HideInInspector] public bool CreateFirebaseDeepLinksManager;
        [HideInInspector] public bool CreateFirebaseDatabaseManager;
        [HideInInspector] public bool CreateFirebaseRemoteConfigManager;
        [HideInInspector] public FirebaseDatabaseManagerConfig firebaseDatabaseManagerConfig;
    }
}