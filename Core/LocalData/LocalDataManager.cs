﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

namespace VIS.CoreMobile.LocalData {
    public class LocalDataManager : MonoBehaviour, ILocalDataManager {
        private ILocalDataManager _localDataManagerImplementation;

        #region ILocalDataManager

        public void Initialize() { }

        public bool IsDataExists(string dataKey) {
            return PlayerPrefs.HasKey(dataKey);
        }
        
        public string GetLocalDataString(string dataKey) {
            return PlayerPrefs.GetString(dataKey);
        }

        public int GetLocalDataInt(string dataKey) {
            return PlayerPrefs.GetInt(dataKey);
        }

        public float GetLocalDataFloat(string dataKey) {
            return PlayerPrefs.GetFloat(dataKey);
        }

        public bool GetLocalDataBoolean(string dataKey) {
            var value = PlayerPrefs.GetInt(dataKey);
            return value == 1;
        }

        public void SaveLocalDataString(string dataKey, string data, Action callback = null) {
            PlayerPrefs.SetString(dataKey, data);
            Save();
            callback?.Invoke();
        }

        public void SaveLocalDataInt(string dataKey, int data, Action callback = null) {
            PlayerPrefs.SetInt(dataKey, data);
            Save();
            callback?.Invoke();
        }

        public void SaveLocalDataFloat(string dataKey, float data, Action callback = null) {
            PlayerPrefs.SetFloat(dataKey, data);
            Save();
            callback?.Invoke();
        }

        public void SaveLocalDataBoolean(string dataKey, bool data, Action callback = null) {
            PlayerPrefs.SetInt(dataKey, data ? 1 : 0);
            Save();
        }

        public void SaveLocalImage(Texture2D image, string fileName = default) {
            StartCoroutine(SaveLocalImage_co(image, fileName));
        }

        private IEnumerator SaveLocalImage_co(Texture2D image, string fileName = default) {

            var path = string.Empty;
            
            switch (Application.platform) {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer: {
                    var osxPath = Application.persistentDataPath + "/" + fileName + ".png";
                    path = osxPath;
                    break;
                }
                case RuntimePlatform.IPhonePlayer: {
                    var iosPath = Application.persistentDataPath + "/" + fileName + ".png";
                    path = iosPath;
                    break;
                }
            }
            
            var bytes = image.EncodeToPNG();
            
            File.WriteAllBytes(path, bytes);

            yield return new WaitForEndOfFrame();
        }
        
        public Sprite GetSavedImage(string fileName) {
            var path = string.Empty;
            
            switch (Application.platform) {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer: {
                    var osxPath = Application.persistentDataPath + "/" + fileName + ".png";
                    path = osxPath;
                    break;
                }
                case RuntimePlatform.IPhonePlayer: {
                    var iosPath = Application.persistentDataPath + "/" + fileName + ".png";
                    path = iosPath;
                    break;
                }
            }

            if (!File.Exists(path)) return null;
            var bytes   = File.ReadAllBytes(path);
            var texture = new Texture2D(8, 8);
            texture.LoadImage(bytes);
            return Sprite.Create(texture, new Rect(0,0,texture.width,texture.height), new Vector2(0.5f, 0.5f));

        }

        public void Save() {
            PlayerPrefs.Save();
        }

        #endregion
    }

}