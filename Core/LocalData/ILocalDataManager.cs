﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VIS.CoreMobile.LocalData {
    public interface ILocalDataManager {

        void   Initialize();
        bool   IsDataExists(string dataKey);
        string GetLocalDataString(string dataKey);
        int    GetLocalDataInt(string dataKey);
        float  GetLocalDataFloat(string dataKey);
        bool   GetLocalDataBoolean(string dataKey);
        void   SaveLocalDataString(string dataKey, string data, Action callback = null);
        void   SaveLocalDataInt(string dataKey, int data, Action callback = null);
        void   SaveLocalDataFloat(string dataKey, float data, Action callback = null);
        void   SaveLocalDataBoolean(string dataKey, bool data, Action callback = null);
        void   SaveLocalImage(Texture2D image, string fileName = default);
        Sprite GetSavedImage(string fileName);
        void   Save();
    }
}
