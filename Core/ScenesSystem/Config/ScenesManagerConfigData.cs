using System;
using UnityEngine.SceneManagement;

namespace Core.ScenesSystem.Config {
    [Serializable]
    public class ScenesManagerConfigData {
        public string sceneKey;
        public string sceneName;
        public bool loadAsync;
    }
}
