using System.Collections.Generic;
using UnityEngine;

namespace Core.ScenesSystem.Config {
    [CreateAssetMenu(fileName = "ScenesManagerConfig", menuName = "PoketPet/Scenes/Scenes Manager Config", order = 0)]
    public class ScenesManagerConfig : ScriptableObject {
        [HideInInspector] public List<ScenesManagerConfigData> scenesDataList;
    }
}