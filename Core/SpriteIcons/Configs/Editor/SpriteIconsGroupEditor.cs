using Core.SpriteIcons.Data;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Core.SpriteIcons.Configs.Editor {
    [CustomEditor(typeof(SpriteIconsGroup))]
    public class SpriteIconsGroupEditor : UnityEditor.Editor {
        
        private ReorderableList _list;

        private const int SPRITE_AREA_SIZE = 42;

        private SpriteIconsGroup spriteIconsGroup {
            get { return target as SpriteIconsGroup; }
        }

        private void OnEnable() {
            _list = new ReorderableList(spriteIconsGroup.iconsDataList, typeof(SpriteIconsData), true, true, true, true);

            Debug.Log(EditorGUIUtility.singleLineHeight);
            
            _list.elementHeight = EditorGUIUtility.singleLineHeight * 3f;
            
            _list.onAddCallback += AddElementCallback;
            _list.onRemoveCallback += RemoveElementCallback;

            _list.drawHeaderCallback += DrawHeader;
            _list.drawElementCallback += DrawElement;
            _list.onChangedCallback += OnChangedCallback;
        }


        private void OnDisable() {
            _list.drawElementCallback -= DrawElement;
        }

        private void OnChangedCallback(ReorderableList list) {
            EditorUtility.SetDirty(target);
        }

        private void DrawHeader(Rect rect) {
            EditorGUI.LabelField(rect, "Icons List:", EditorStyles.boldLabel);
        }

        private void AddElementCallback(ReorderableList reorderableList) {
            spriteIconsGroup.iconsDataList.Add(new SpriteIconsData());
            EditorUtility.SetDirty(target);
        }

        private void RemoveElementCallback(ReorderableList reorderableList) {
            spriteIconsGroup.iconsDataList.RemoveAt(reorderableList.index);
            EditorUtility.SetDirty(target);
        }

        private void DrawElement(Rect rect, int index, bool active, bool focused) {
            var item = spriteIconsGroup.iconsDataList[index];

            EditorGUI.BeginChangeCheck();
            
            var textFieldWidth = rect.width - SPRITE_AREA_SIZE - 8;
            item.iconKey = EditorGUI.TextField(new Rect(rect.x, rect.y, textFieldWidth, 18), item.iconKey);
            item.iconSprite = (Sprite) EditorGUI.ObjectField(new Rect(textFieldWidth + SPRITE_AREA_SIZE + 8, rect.y, SPRITE_AREA_SIZE, SPRITE_AREA_SIZE), item.iconSprite, typeof(Sprite), false);

            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(target);
            }
        }

        public override void OnInspectorGUI() {
            EditorGUILayout.Space();
            
            EditorGUILayout.HelpBox("Here you can specify key to get access for this icons group", MessageType.Info);

            EditorGUILayout.Space();
            
            var iconsGroup = (SpriteIconsGroup)target;
            iconsGroup.groupKey = EditorGUILayout.TextField("Group Key:", !string.IsNullOrEmpty(iconsGroup.groupKey) ? iconsGroup.groupKey.ToLower() : iconsGroup.groupKey);

            EditorGUILayout.Space();

            // Actually draw the list in the inspector
            _list.DoLayoutList();

            EditorGUILayout.Space();

            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}
