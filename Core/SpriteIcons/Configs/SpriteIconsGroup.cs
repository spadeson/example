using System.Collections.Generic;
using Core.SpriteIcons.Data;
using UnityEngine;

namespace Core.SpriteIcons.Configs {
    [CreateAssetMenu(fileName = "SpriteIconsGroup", menuName = "PoketPet/Icons/Sprite Icons Group", order = 0)]
    public class SpriteIconsGroup : ScriptableObject {
        public string groupKey;
        public List<SpriteIconsData> iconsDataList = new List<SpriteIconsData>();
    }
}
