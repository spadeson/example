using System.Collections.Generic;
using UnityEngine;

namespace Core.SpriteIcons.Configs {
    [CreateAssetMenu(fileName = "SpritesIconsManagerConfig", menuName = "PoketPet/Icons/Sprites Icons Manager Config", order = 0)]
    public class SpritesIconsManagerConfig : ScriptableObject {
        public List<SpriteIconsGroup> spriteIconsGroups;
    }
}