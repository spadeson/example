using System;
using UnityEngine;

namespace Core.SpriteIcons.Data {
    [Serializable]
    public class SpriteIconsData {
        public string iconKey;
        public Sprite iconSprite;
    }
}
