using Core.SpriteIcons.Configs;
using UnityEngine;

namespace Core.SpriteIcons.Manager {
    public class SpriteIconsManager : MonoBehaviour {

        private SpritesIconsManagerConfig _spritesIconsManagerConfig;
        
        public void Initialize(SpritesIconsManagerConfig spritesIconsManagerConfig) {
            _spritesIconsManagerConfig = spritesIconsManagerConfig;
        }
        
        public Sprite GetSpriteIconByKey(string groupKey, string iconKey) {
            var iconsGroup = _spritesIconsManagerConfig.spriteIconsGroups.Find(g => g.groupKey == groupKey);
            var icon       = iconsGroup.iconsDataList.Find(s => s.iconKey == iconKey).iconSprite;
            return icon;
        }
    }
}
