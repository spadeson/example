﻿using System;
using System.Collections.Generic;

namespace VIS.CoreMobile.ObjectPool {

    public class ObjectPoolManager : IObjectPoolManager<IPoolableObject> {

        /// <inheritdoc />
        public Queue<IPoolableObject> ObjectsPool { get; set; }

        /// <inheritdoc />
        public void initialize() {
            ObjectsPool = new Queue<IPoolableObject>();
        }

        /// <inheritdoc />
        public IPoolableObject getObject() {
            return ObjectsPool.Dequeue();
        }

        /// <inheritdoc />
        public void addObject(IPoolableObject poolObject) {
            ObjectsPool.Enqueue(poolObject);
        }
    }
}
