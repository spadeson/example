﻿namespace VIS.CoreMobile.ObjectPool {
    public interface IPoolableObject {
        void Activate();
        void Deactivate();
    }
}