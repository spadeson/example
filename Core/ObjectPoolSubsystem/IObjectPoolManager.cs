﻿using System;
using System.Collections.Generic;

namespace VIS.CoreMobile.ObjectPool {
    public interface IObjectPoolManager<T> {
        /// <summary>
        /// Gets or sets the objects pool.
        /// </summary>
        /// <value>The objects pool.</value>
        Queue<IPoolableObject> ObjectsPool { get; set; }

        /// <summary>
        /// Initialize this instance.
        /// </summary>
        void initialize();

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <returns>The object.</returns>
        T getObject();

        /// <summary>
        /// Adds the object.
        /// </summary>
        /// <param name="poolObject">Pool object.</param>
        void addObject(T poolObject);
    }
}
