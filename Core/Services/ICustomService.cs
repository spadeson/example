namespace VIS.CoreMobile.Services {
    public interface ICustomService {
        void init();
        
        void execute();
        
        void execute(object data);
    }
}