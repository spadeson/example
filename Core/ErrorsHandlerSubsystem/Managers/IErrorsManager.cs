using Core.ErrorsHandlerSubsystem.Config;
using Core.ErrorsHandlerSubsystem.Data;
using UnityEngine;

namespace Core.ErrorsHandlerSubsystem.Managers {
    public interface IErrorsManager {
        void Initialize(ErrorsManagerConfig errorsManagerConfig);
        string ValidateForError(InputFieldsTypes type, string data, string dataToCompare = "");
    }
}
