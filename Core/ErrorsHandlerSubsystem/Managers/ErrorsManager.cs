using System;
using Core.ErrorsHandlerSubsystem.Config;
using Core.ErrorsHandlerSubsystem.Data;
using Core.ErrorsHandlerSubsystem.Handlers;
using UnityEngine;

namespace Core.ErrorsHandlerSubsystem.Managers {
    public class ErrorsManager : MonoBehaviour, IErrorsManager {
        
        [SerializeField] private ErrorsManagerConfig errorsManagerConfig;

        private IErrorHandler _emailInputFieldErrorsHandler;
        private IErrorHandler _passwordInputFieldErrorsHandler;
        private IErrorHandler _passwordRepeatInputFieldErrorsHandler;
        
        public void Initialize(ErrorsManagerConfig errorsManagerConfig) {
            this.errorsManagerConfig = errorsManagerConfig;
            _emailInputFieldErrorsHandler = new EmailInputFieldErrorsHandler();
            _passwordInputFieldErrorsHandler = new PasswordInputFieldErrorsHandler();
            _passwordRepeatInputFieldErrorsHandler = new PasswordRepeatInputFieldErrorsHandler();
        }

        public string ValidateForError(InputFieldsTypes type, string data, string dataToCompare = "") {
            
            var errorsLibrary = errorsManagerConfig.errorsLibrary.Find(x => x.type.Equals(type)).errorConfigsList;
            
            IErrorHandler errorHandler = null;
            
            switch (type) {
                case InputFieldsTypes.EMAIL:
                    errorHandler = _emailInputFieldErrorsHandler;
                    break;
                case InputFieldsTypes.PASSWORD:
                    errorHandler = _passwordInputFieldErrorsHandler;
                    break;
                case InputFieldsTypes.PASSWORD_REPEAT:
                    errorHandler = _passwordRepeatInputFieldErrorsHandler;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
            
            return errorHandler.ValidateData(errorsLibrary, data, dataToCompare);
        }
    }
}

