using System.Collections.Generic;
using Core.ErrorsHandlerSubsystem.Config;

namespace Core.ErrorsHandlerSubsystem.Handlers {
    public class PasswordRepeatInputFieldErrorsHandler : IErrorHandler {

        private List<ErrorConfig> _errorConfigsList;

        private const int MIN_PASSWORD_LENGTH = 6;

        public string ValidateData(List<ErrorConfig> errorConfigsList, string data, string dataToCompare = "") {
            _errorConfigsList = errorConfigsList;
            
            var error = string.Empty;

            if (string.IsNullOrEmpty(data)) {
                error = _errorConfigsList.Find(e => e.Key.Equals("EMPTY_FIELD")).Message;
            } else {
                if(data.Contains(" ")){
                    error = _errorConfigsList.Find(e => e.Key.Equals("SPACE_CHARACTER")).Message;
                }

                if (data.Length < MIN_PASSWORD_LENGTH) {
                    error = _errorConfigsList.Find(e => e.Key.Equals("PASSWORD_TO_SHORT")).Message;
                }
            
                if (data != dataToCompare) {
                    error = _errorConfigsList.Find(e => e.Key.Equals("PASSWORD_MISMATCH")).Message;
                }
            }

            return error;
        }
    }
}

