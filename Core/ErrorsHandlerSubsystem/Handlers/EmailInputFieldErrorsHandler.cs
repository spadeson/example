using System.Collections.Generic;
using Core.ErrorsHandlerSubsystem.Config;

namespace Core.ErrorsHandlerSubsystem.Handlers {
    public class EmailInputFieldErrorsHandler : IErrorHandler {

        private List<ErrorConfig> _errorConfigsList;

        public string ValidateData(List<ErrorConfig> errorConfigsList, string data, string dataToCompare = "") {
            _errorConfigsList = errorConfigsList;
            
            var error = string.Empty;
            

            if (string.IsNullOrEmpty(data)) {
                error = _errorConfigsList.Find(e => e.Key.Equals("EMPTY_FIELD")).Message;
            } else {
                const string matchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                                 + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                                 + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                                 + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
                if (!Core.Utilities.RegExFormatValidator.ValidateData(matchEmailPattern, data)) {
                    error = _errorConfigsList.Find(e => e.Key.Equals("WRONG_EMAIL")).Message;
                }
            }

            return error;
        }
    }
}

