using System.Collections.Generic;
using Core.ErrorsHandlerSubsystem.Config;
using Core.ErrorsHandlerSubsystem.Data;

namespace Core.ErrorsHandlerSubsystem.Handlers {
    public interface IErrorHandler {
        string ValidateData(List<ErrorConfig> errorConfigsList, string data, string dataToCompare = "");
    }
}
