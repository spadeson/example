using System;
using System.Collections.Generic;
using Core.ErrorsHandlerSubsystem.Config;

namespace Core.ErrorsHandlerSubsystem.Data {
    [Serializable]
    public class ErrorsKeyPair {
        public InputFieldsTypes type;
        public List<ErrorConfig> errorConfigsList = new List<ErrorConfig>();
    }
}

