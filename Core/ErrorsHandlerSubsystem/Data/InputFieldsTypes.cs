namespace Core.ErrorsHandlerSubsystem.Data {
    public enum InputFieldsTypes {
        EMAIL = 0,
        PASSWORD = 1,
        PASSWORD_REPEAT = 2
    }
}
