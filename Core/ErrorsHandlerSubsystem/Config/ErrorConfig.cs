using UnityEngine;

namespace Core.ErrorsHandlerSubsystem.Config {
    [CreateAssetMenu(fileName = "ErrorConfig", menuName = "PoketPet/Errors/ErrorConfig", order = 0)]
    public class ErrorConfig : ScriptableObject {
        public int ErrorCode;
        public string Key;
        public string Message;
    }
}
