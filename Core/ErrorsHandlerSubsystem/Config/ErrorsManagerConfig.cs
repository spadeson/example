using System.Collections.Generic;
using Core.ErrorsHandlerSubsystem.Data;
using UnityEngine;

namespace Core.ErrorsHandlerSubsystem.Config {
    [CreateAssetMenu(fileName = "ErrorManagerConfig", menuName = "PoketPet/Core/Errors/ErrorsManagerConfig", order = 0)]
    public class ErrorsManagerConfig : ScriptableObject {
        public List<ErrorsKeyPair> errorsLibrary = new List<ErrorsKeyPair>();
    }
}
