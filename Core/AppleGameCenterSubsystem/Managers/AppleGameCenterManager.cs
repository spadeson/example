using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

namespace Core.AppleGameCenterSubsystem.Managers {
    public class AppleGameCenterManager : MonoBehaviour, IAppleGameCenterManager {
        
        public bool IsAuthenticated { get; private set; }
        public ILocalUser LocalUser { get; private set; }
        public void Initialize() {
            Social.localUser.Authenticate(ProcessAuthentication);
        }

        private void ProcessAuthentication(bool success) {
            IsAuthenticated = success;
            LocalUser = Social.localUser;
            if (success) {
                Debug.Log("Social authenticated success");
                Debug.Log($"Username: {Social.localUser.userName}\nUser ID: {Social.localUser.id}");
            }
            else
                Debug.Log("Failed to authenticate");
        }
        
        public void LoadAchievements(Action<IAchievement[]> callback)
        {
            Social.LoadAchievements(achievements =>
            {
                Debug.Log(achievements.Length == 0 ? "No achievements found" : "Got " + achievements.Length + " achievements");
                callback?.Invoke(achievements);
            });
        }
        
        public void LoadScores(string leaderboardId, Action<IScore[]> callback)
        {
            Social.LoadScores(leaderboardId, scores =>
            {
                Debug.Log(scores.Length == 0 ? "No scores found" : "Got " + scores.Length + " achievements");
                callback?.Invoke(scores);
            });
        }
        
        public void ReportAchievementProgress(string achievementsId, long progress, [CanBeNull] Action callback = null)
        {
            Social.ReportProgress(achievementsId, progress, success =>
            {
                Debug.Log(success ? "Reported progress successfully" : "Failed to report progress");
                Debug.Log($"New progress:{progress}");
                callback?.Invoke();
            });
        }
        
        public void ReportScore(long score, string leaderboardId, Action callback = null)
        {
            Social.ReportScore(score, leaderboardId, success =>
            {
                Debug.Log(success ? "Reported score successfully" : "Failed to report score");
                Debug.Log($"New Score:{score}");
                callback?.Invoke();
            });
        }
        
        public void ShowLeaderboard()
        {
            Social.ShowLeaderboardUI();
        }
    }
}