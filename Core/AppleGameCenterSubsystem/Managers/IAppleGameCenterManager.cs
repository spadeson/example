using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Core.AppleGameCenterSubsystem.Managers {
    
    public interface IAppleGameCenterManager {
        bool IsAuthenticated { get; }
        ILocalUser LocalUser { get; }
        void Initialize();

        void LoadAchievements(Action<IAchievement[]> callback);

        void LoadScores(string leaderboardId, Action<IScore[]> callback);

        void ReportAchievementProgress(string achievementsId, long progress, [CanBeNull] Action callback = null);

        void ReportScore(long score, string leaderboardId, [CanBeNull] Action callback = null);

        void ShowLeaderboard();
    }
}