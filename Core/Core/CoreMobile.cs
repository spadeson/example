﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.AnalyticsSubsytem.Managers;
using Core.AppleGameCenterSubsystem.Managers;
using Core.Audio.Manager;
using Core.ErrorsHandlerSubsystem.Managers;
using Core.FirebaseSubsystem;
using Core.GoogleManager;
using Core.NetworkSubsystem.Managers;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using Core.SpriteIcons.Manager;
using Core.TimeManager;
using Core.TimersSubsystem.Managers;
using Core.TimeSubsystem.Managers;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using VIS.CoreMobile.Facebook;
using VIS.CoreMobile.GesturesSystem;
using VIS.CoreMobile.LocalData;
using VIS.CoreMobile.ObjectPool;
using VIS.CoreMobile.ScenesSystem;
using VIS.CoreMobile.Services;
using VIS.CoreMobile.UISystem;
using VIS.CoreMobile.VFX;    

namespace VIS.CoreMobile {
    public class CoreMobile : MonoBehaviour {

        private static CoreMobile s_instance;
        public static CoreMobile Instance {
            get {
                if (s_instance != null) return s_instance;
                try {
                    s_instance = FindObjectOfType<CoreMobile>();
                } catch (Exception e) {
                    Debug.LogWarning(e);
                } finally {
                    s_instance = new GameObject("Core").AddComponent<CoreMobile>();
                }
                return s_instance;
            }
        }

        #region Global systems
        public CoreMobileConfig CoreMobileConfig;

        public IAnalyticsManager AnalyticsManager { get; private set; }
        public IAudioManager AudioManager { get; private set; }
        public ILocalDataManager LocalDataManager { get; private set; }
        public IUIManager UIManager { get; private set; }
        public EventSystem EventSystem { get; private set; }
        public ITimeManager TimeManager { get; private set; }
        public ITimersManager TimersManager { get; private set; }
        public IScenesManager SceneManager { get; private set; }
        public IGesturesManager GesturesManager { get; private set; }
        public VFXManager VFXManager { get; private set; }
        public FacebookManager FacebookManager { get; private set; }
        public GoogleManager GoogleManager { get; private set; }
        public IObjectPoolManager<IPoolableObject> PoolManager { get; set; }
        public INetworkManager NetworkManager { get; private set; }
        public ISocketNetworkManager SocketNetworkManager { get; private set; }
        public IFirebaseManager FirebaseManager { get; private set; }
        public IErrorsManager ErrorsManager { get; private set; }
        public IAppleGameCenterManager AppleGameCenterManager { get; private set; }
        
        public SpriteIconsManager SpritesIconsManager { get; private set; }

        public bool Initialized { get; private set; }

        #endregion

        #region Local Services
        private List<ICustomService> _customServices = new List<ICustomService>();
        #endregion

        void Awake() {
            if(s_instance != null) {
                Destroy(gameObject);
                return;
            }
            s_instance = this;
            
            DontDestroyOnLoad(this);
        }

        private void Start() {
            initServices();

            if (CoreMobileConfig.CreateFirebaseManager && CoreMobileConfig.FirebaseServicesConfig != null) {
                StartCoroutine(WaitForFirebaseInitializations());
            } else {
                Initialized = true;
            }
        }

        void initServices() {

            #if  UNITY_EDITOR

            if (CoreMobileConfig == null) {
                CoreMobileConfig = AssetDatabase.LoadAssetAtPath<CoreMobileConfig>("Assets/Data/CoreMobileConfig.asset");
            }

            #endif

            if (CoreMobileConfig.CreateAnalyticsManager) {
                CreateAnalyticsManager();
            }

            if(CoreMobileConfig.CreateAudioManager) {
                createAudioManager();
            }

            if(CoreMobileConfig.CreateLocalDataManager) {
                createLocalDataManager();
            }

            if(CoreMobileConfig.CreateUIManager) {
                createUIManager();
            }
            
            if(CoreMobileConfig.CreateTimeManager) {
                CreateTimeManager();
            }

            if(CoreMobileConfig.CreateTimersManager) {
                CreateTimersManager();
            }

            if(CoreMobileConfig.CreateSceneManager) {
                CreateSceneManager();
            }

            if(CoreMobileConfig.CreateGestureManager) {
                createGestureManager(CoreMobileConfig.GesturesManagerConfig);
            }

            if (CoreMobileConfig.CreateVFXManager) {
                createVFXManager();
            }

            if (CoreMobileConfig.CreateFacebookManager) {
                createFacebookManager();
            }
            
            if (CoreMobileConfig.CreateGoogleManager) {
                createGoogleManager();
            }

            if (CoreMobileConfig.CreateNetworkManager)
            {
                createNetworkManager();
            }
            
            if (CoreMobileConfig.CreateSocketNetworkManager)
            {
                createSocketNetworkManager();
            }

            if (CoreMobileConfig.CreateFirebaseManager)
            {
                CreateFirebaseManager();
            }

            if (CoreMobileConfig.CreateErrorsManager) {
                CreateErrorsManager();
            }
            
            if (CoreMobileConfig.CreateGameCenterManager) {
                createGameCenterManager();
            }

            if (CoreMobileConfig.CreateSpriteIconsManager) {
                CreateSpriteIconsManager();
            }
        }

        IEnumerator WaitForFirebaseInitializations() {
            yield return new WaitUntil(() => FirebaseManager.Initialized);
            Initialized = true;
        }

        private void CreateErrorsManager() {
            var firebaseManagerGO = new GameObject("ErrorsManager", typeof(ErrorsManager));
            firebaseManagerGO.transform.SetParent(transform);
            ErrorsManager = firebaseManagerGO.GetComponent<ErrorsManager>(); 
            ErrorsManager.Initialize(CoreMobileConfig.ErrorsManagerConfig);
        }

        private void CreateFirebaseManager()
        {
            var firebaseManagerGO = new GameObject("FirebaseManager", typeof(FirebaseManager));
            firebaseManagerGO.transform.SetParent(transform);
            FirebaseManager = firebaseManagerGO.GetComponent<FirebaseManager>(); 
            FirebaseManager.Initialize(CoreMobileConfig.FirebaseServicesConfig);
        }
        
        private void CreateAnalyticsManager() {
            AnalyticsManager = new AnalyticsManager();
            AnalyticsManager.Initialize();
        }


        /// <summary>
        /// Creates the audio manager.
        /// </summary>
        void createAudioManager() {
            GameObject audioManagerGO = new GameObject("AudioManager", typeof(AudioManager));
            audioManagerGO.tag = "AudioManager";
            audioManagerGO.transform.SetParent(transform);
            AudioManager = audioManagerGO.GetComponent<AudioManager>();
            AudioManager.Initialize(CoreMobileConfig.AudioManagerConfig);
        }

        /// <summary>
        /// Creates the local data manager.
        /// </summary>

        void createLocalDataManager() {
            GameObject localDataManagerGO = new GameObject("LocalDataManager", typeof(LocalDataManager));
            localDataManagerGO.tag = "LocalDataManager";
            localDataManagerGO.transform.SetParent(transform);
            LocalDataManager = localDataManagerGO.GetComponent<LocalDataManager>();
            LocalDataManager.Initialize();
        }

        void createUIManager() {
            GameObject uiManagerGO = new GameObject("UIManager", typeof(UIManager));
            uiManagerGO.tag = "UIManager";
            uiManagerGO.layer = LayerMask.NameToLayer("UI");
            uiManagerGO.transform.SetParent(transform);
            UIManager = uiManagerGO.GetComponent<UIManager>();
            UIManager.Initialize(CoreMobileConfig.UIManagerConfig);

            EventSystem = FindObjectOfType<EventSystem>();
            
            if(EventSystem == null) {
                GameObject eventSystemGO = new GameObject("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
                EventSystem = eventSystemGO.GetComponent<EventSystem>();
            }
            
            EventSystem.transform.SetParent(transform);
        }
        
        private void CreateTimeManager() {
            var timeManagerGo = new GameObject("TimeManager", typeof(TimeManager)) {tag = "TimeManager"};
            timeManagerGo.transform.SetParent(transform);
            TimeManager = timeManagerGo.GetComponent<TimeManager>();
            TimeManager.Initialize();
        }

        private void CreateTimersManager() {
            var timersManagerGo = new GameObject("TimersManager", typeof(TimersManager)) {tag = "TimersManager"};
            timersManagerGo.transform.SetParent(transform);
            TimersManager = timersManagerGo.GetComponent<TimersManager>();
            TimersManager.Initialize();
        }

        /// <summary>
        /// Creates the scene manager.
        /// </summary>
        void CreateSceneManager() {
            var sceneManagerGO = new GameObject("ScenesManager", typeof(ScenesManager));
            sceneManagerGO.tag = "ScenesManager";
            sceneManagerGO.transform.SetParent(transform);
            SceneManager = sceneManagerGO.GetComponent<ScenesManager>();
            SceneManager.Initialize(CoreMobileConfig.ScenesManagerConfig);
        }

        /// <summary>
        /// Creates the gesture manager.
        /// </summary>
        void createGestureManager(GesturesManagerConfig gesturesManagerConfig) {
            GameObject gesturesManagerGO = new GameObject("GesturesManager");
            gesturesManagerGO.tag = "GesturesManager";
            gesturesManagerGO.transform.SetParent(transform);

            if (gesturesManagerConfig.UseDiscreteStates) {
                GesturesManager = gesturesManagerGO.AddComponent<DiscreteGesturesManager>();    
            } else {
                GesturesManager = gesturesManagerGO.AddComponent<GesturesManager>();
            }

            GesturesManager.initialize(CoreMobileConfig.GesturesManagerConfig);
        }
        
        /// <summary>
        /// Creates VFX Manager.
        /// </summary>
        void createVFXManager() {
            GameObject vfxManagerGO = new GameObject("VFXManager", typeof(VFXManager));
            //vfxManagerGO.tag = "AudioManager";
            vfxManagerGO.transform.SetParent(transform);
            VFXManager = vfxManagerGO.GetComponent<VFXManager>();
            VFXManager.initialize(CoreMobileConfig.VFXManagerConfig);
        }
        
        /// <summary>
        /// Creates Facebook manager.
        /// </summary>
        void createFacebookManager() {
            var facebookManagerGO = new GameObject("FacebookManager", typeof(FacebookManager));
            facebookManagerGO.transform.SetParent(transform);
            FacebookManager = facebookManagerGO.GetComponent<FacebookManager>();
            FacebookManager.Initialize();
        }
        
        void createGoogleManager() {
            var googleManagerGO = new GameObject("GoogleManager", typeof(GoogleManager));
            googleManagerGO.transform.SetParent(transform);
            GoogleManager = googleManagerGO.GetComponent<GoogleManager>();
            GoogleManager.Init();
        }

        /// <summary>
        /// Creates the object pool manager.
        /// </summary>
        void createObjectPoolManager() {

        }

        public void registerCustomService(ICustomService service) {
            _customServices.Add(service);
        }

        public ICustomService getCustomService<T>() {
            return _customServices.FirstOrDefault(s => s.GetType() == typeof(T));
        }

        public void deregisterCustomService<T>() {
            _customServices.RemoveAll(s => s.GetType() == typeof(T));
        }
        
        private void createNetworkManager()
        {
            var networkManagerGO = new GameObject("NetworkManager", typeof(NetworkManager));
            networkManagerGO.transform.SetParent(transform);
            NetworkManager = networkManagerGO.GetComponent<NetworkManager>();
            NetworkManager.Initialize(CoreMobileConfig.NetworkManagerConfig);
        }
        
        private void createSocketNetworkManager()
        {
            var socketNetworkManagerGO = new GameObject("SocketNetworkManager", typeof(SocketNetworkManager));
            socketNetworkManagerGO.transform.SetParent(transform);
            SocketNetworkManager = socketNetworkManagerGO.GetComponent<SocketNetworkManager>();
            SocketNetworkManager.Initialize(CoreMobileConfig.SocketNetworkManagerConfig);
        }
        
        private void createGameCenterManager()
        {
            var gameCenterManagerGO = new GameObject("AppleGameCenterManager", typeof(AppleGameCenterManager));
            gameCenterManagerGO.transform.SetParent(transform);
            AppleGameCenterManager = gameCenterManagerGO.GetComponent<AppleGameCenterManager>();
            AppleGameCenterManager.Initialize();
        }
        
        private void CreateSpriteIconsManager() {
            if (!CoreMobileConfig.CreateSpriteIconsManager) return;
            var spriteIconsManagerGo = new GameObject("SpriteIconsManager", typeof(SpriteIconsManager));
            spriteIconsManagerGo.transform.SetParent(transform);
            SpritesIconsManager = spriteIconsManagerGo.GetComponent<SpriteIconsManager>();
            SpritesIconsManager.Initialize(CoreMobileConfig.spritesIconsManagerConfig);
        }
    }
}