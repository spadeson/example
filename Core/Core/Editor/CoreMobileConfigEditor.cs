﻿using Core.Audio.Manager;
using Core.ErrorsHandlerSubsystem.Config;
using Core.FirebaseSubsystem.Config;
using Core.NetworkSubsystem.Config;
using Core.ScenesSystem.Config;
using Core.SpriteIcons.Configs;
using UnityEngine;
using UnityEditor;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using VIS.CoreMobile.GesturesSystem;
using VIS.CoreMobile.VFX;

[CustomEditor(typeof(CoreMobileConfig))]
public class CoreMobileConfigEditor : Editor {

    Vector2 _scrollPosition;
    
    //bool _createAudioManager;
    bool _createLocalDataManager;
    bool _createUiManager;
    bool _createTimeManager;
    bool _createSceneManager;
    bool _createGestureManager;

    public override void OnInspectorGUI() {

        base.OnInspectorGUI();

        var coreConfig = (CoreMobileConfig)target;

        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        #region Analytics Subsystem

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Analytics Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Analytics Manager - Subsystem to send analytics data.", MessageType.Info, true);
        
        coreConfig.CreateAnalyticsManager = EditorGUILayout.Toggle("Create Analytics Manager", coreConfig.CreateAnalyticsManager);

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        #endregion

        #region Audio Subsystem
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Audio Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox(
            "Audio Manager - Subsystem to control sounds.",
            MessageType.Info,
            true
        );
        
        coreConfig.CreateAudioManager = EditorGUILayout.Toggle("Create Audio Manager", coreConfig.CreateAudioManager);
        if (coreConfig.CreateAudioManager) {
            coreConfig.AudioManagerConfig = EditorGUILayout.ObjectField("Audio Manager Config", coreConfig.AudioManagerConfig, typeof(AudioManagerConfig), false) as AudioManagerConfig;   
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        #endregion

        #region Local Data Subsystem
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Local Data Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Local Data Subsystem - Subsystem to load and save data at local storage.", MessageType.Info, true);
        coreConfig.CreateLocalDataManager = EditorGUILayout.Toggle("Create Local Data Manager", coreConfig.CreateLocalDataManager);
        EditorGUILayout.EndVertical();
        
        EditorGUILayout.Space();
        #endregion

        #region UI Manager Subsystem
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("UI Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("UI Subsystem - Subsystem to operate UI.", MessageType.Info, true);
        
        coreConfig.CreateUIManager = EditorGUILayout.Toggle("Create UI Manager", coreConfig.CreateUIManager);
        
        if (coreConfig.CreateUIManager) {
            coreConfig.UIManagerConfig = EditorGUILayout.ObjectField("UI Manager Config", coreConfig.UIManagerConfig, typeof(UIManagerConfig), false) as UIManagerConfig;
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        #endregion

        #region Timers Subsystem
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Timers Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Timers Subsystem - Subsystem to operate timers.", MessageType.Info, true);

        coreConfig.CreateTimersManager = EditorGUILayout.Toggle("Create Timers Manager", coreConfig.CreateTimersManager);

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        #endregion

        #region Time Subsystem
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Time Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Time Subsystem - Subsystem to operate time.", MessageType.Info, true);
        coreConfig.CreateTimeManager = EditorGUILayout.Toggle("Create Time Manager", coreConfig.CreateTimeManager);
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        #endregion

        #region Scene Manager
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Scene Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Scene Subsystem - using UnityEngine.SceneManagement to operate Unity scenes.", MessageType.Info, true);
        coreConfig.CreateSceneManager = EditorGUILayout.Toggle("Create Scene Manager", coreConfig.CreateSceneManager);
        
        if (coreConfig.CreateSceneManager) {
            coreConfig.ScenesManagerConfig = EditorGUILayout.ObjectField("Scenes Manager Config", coreConfig.ScenesManagerConfig, typeof(ScenesManagerConfig), false) as ScenesManagerConfig;
        }
        
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        #endregion

        #region Gesture Manager
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Gesture Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Gesture Manager - use to recognize gestures at mobile devices.", MessageType.Info, true);
        
        coreConfig.CreateGestureManager = EditorGUILayout.Toggle("Create Gesture Manager", coreConfig.CreateGestureManager);
        if (coreConfig.CreateGestureManager) {
            coreConfig.GesturesManagerConfig = EditorGUILayout.ObjectField("Gesture Manager Config", coreConfig.GesturesManagerConfig, typeof(GesturesManagerConfig), false) as GesturesManagerConfig;   
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        #endregion

        #region VFX Manager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("VFX Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("VFX Manager - use to play particles effects.", MessageType.Info, true);

        coreConfig.CreateVFXManager = EditorGUILayout.Toggle("Create VFX Manager", coreConfig.CreateVFXManager);
        
        if (coreConfig.CreateVFXManager) {
            coreConfig.VFXManagerConfig = EditorGUILayout.ObjectField("VFX Manager Config", coreConfig.VFXManagerConfig, typeof(VFXManagerConfig), false) as VFXManagerConfig;
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        #endregion

        #region Facebook MManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Facebook Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Facebook Manager - used as wrapper for Facebook Unity plugin.", MessageType.Info, true);

        coreConfig.CreateFacebookManager = EditorGUILayout.Toggle("Create Facebook Manager", coreConfig.CreateFacebookManager);

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        #endregion
        
        #region Google MManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Google Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Google Manager - used as wrapper for Google Unity plugin.", MessageType.Info, true);
        coreConfig.CreateGoogleManager = EditorGUILayout.Toggle("Create Google Manager", coreConfig.CreateGoogleManager);
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        #endregion

        #region NetworkManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Network Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Network Manager - used for communication with server.", MessageType.Info, true);
        coreConfig.CreateNetworkManager = EditorGUILayout.Toggle("Create Network Manager", coreConfig.CreateNetworkManager);
        if (coreConfig.CreateNetworkManager) {
            coreConfig.NetworkManagerConfig = EditorGUILayout.ObjectField("Network Manager Config", coreConfig.NetworkManagerConfig, typeof(NetworkManagerConfig), false) as NetworkManagerConfig;
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        
        #endregion
        
        #region SocketNetworkManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Socket Network Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Socket Network Manager - used for communication with server with socket.", MessageType.Info, true);
        coreConfig.CreateSocketNetworkManager = EditorGUILayout.Toggle("Create Socket Network Manager", coreConfig.CreateSocketNetworkManager);
        if (coreConfig.CreateSocketNetworkManager) {
            coreConfig.SocketNetworkManagerConfig = EditorGUILayout.ObjectField("Socket Manager Config", coreConfig.SocketNetworkManagerConfig, typeof(ConnectionsNetworkLibrary), false) as ConnectionsNetworkLibrary;
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        
        #endregion
        
        #region FirebaseManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Firebase Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Firebase Manager - used for Firebase.", MessageType.Info, true);

        coreConfig.CreateFirebaseManager = EditorGUILayout.Toggle("Create Firebase Manager", coreConfig.CreateFirebaseManager);
        
        if (coreConfig.CreateFirebaseManager) {
            coreConfig.FirebaseServicesConfig = EditorGUILayout.ObjectField("Firebase Manager Config", coreConfig.FirebaseServicesConfig, typeof(FirebaseServicesConfig), false) as FirebaseServicesConfig;
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        
        #endregion

        #region ErrorsManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Errors Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Errors Manager - used for handle Errors.", MessageType.Info, true);

        coreConfig.CreateErrorsManager = EditorGUILayout.Toggle("Create Errors Manager", coreConfig.CreateErrorsManager);
        
        if (coreConfig.CreateErrorsManager) {
            coreConfig.ErrorsManagerConfig = EditorGUILayout.ObjectField("Errors Manager Config", coreConfig.ErrorsManagerConfig, typeof(ErrorsManagerConfig), false) as ErrorsManagerConfig;
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        #endregion
        
        #region ErrorsManager

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Apple Game Center Manager", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Apple Game Center Manager - used for handle Errors.", MessageType.Info, true);

        coreConfig.CreateGameCenterManager = EditorGUILayout.Toggle("Create Apple Game Center Manager", coreConfig.CreateGameCenterManager);
        
        //if (coreConfig.CreateGameCenterManager) {
        //    coreConfig.ErrorsManagerConfig = EditorGUILayout.ObjectField("Errors Manager Config", coreConfig.ErrorsManagerConfig, typeof(ErrorsManagerConfig), false) as ErrorsManagerConfig;
        //}

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        #endregion
        
        #region SpriteIconsLibrary

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        EditorGUILayout.LabelField("Sprite Icons Library", EditorStyles.boldLabel);
        EditorGUILayout.HelpBox("Sprite Icons Library - used to get sprite by string key.", MessageType.Info, true);
        coreConfig.CreateSpriteIconsManager = EditorGUILayout.Toggle("Create Sprite Icons", coreConfig.CreateSpriteIconsManager);
        if (coreConfig.CreateSpriteIconsManager) {
            coreConfig.spritesIconsManagerConfig = (SpritesIconsManagerConfig)EditorGUILayout.ObjectField("Sprite Icons Library", coreConfig.spritesIconsManagerConfig, typeof(SpritesIconsManagerConfig), false);
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.Space();

        #endregion

        if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }
        
        EditorGUILayout.EndScrollView();
        
        serializedObject.ApplyModifiedProperties();
    }
}
