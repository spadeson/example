﻿using Core.Audio.Manager;
using Core.ErrorsHandlerSubsystem.Config;
using Core.FirebaseSubsystem.Config;
using Core.NetworkSubsystem.Config;
using Core.ScenesSystem.Config;
using Core.SpriteIcons.Configs;
using UnityEngine;
using VIS.CoreMobile.GesturesSystem;
using VIS.CoreMobile.UISystem;
using VIS.CoreMobile.VFX;

namespace VIS.CoreMobile {
    [CreateAssetMenu(fileName = "CoreMobileConfig", menuName = "VIS/Core/Core Mobile Config")]
    public class CoreMobileConfig : ScriptableObject {
        [HideInInspector] public bool CreateAnalyticsManager;
        [HideInInspector] public bool CreateAudioManager;
        [HideInInspector] public bool CreateLocalDataManager;
        [HideInInspector] public bool CreateUIManager;
        [HideInInspector] public bool CreateTimersManager;
        [HideInInspector] public bool CreateTimeManager;
        [HideInInspector] public bool CreateSceneManager;
        [HideInInspector] public bool CreateGestureManager;
        [HideInInspector] public bool CreateVFXManager;
        [HideInInspector] public bool CreateFacebookManager;
        [HideInInspector] public bool CreateGoogleManager;
        [HideInInspector] public bool CreateNetworkManager;
        [HideInInspector] public bool CreateSocketNetworkManager;
        [HideInInspector] public bool CreateFirebaseManager;
        [HideInInspector] public bool CreateErrorsManager;
        [HideInInspector] public bool CreateGameCenterManager;
        [HideInInspector] public bool CreateSpriteIconsManager;
        
        [HideInInspector] public AudioManagerConfig AudioManagerConfig;
        [HideInInspector] public UIManagerConfig UIManagerConfig;
        [HideInInspector] public ScenesManagerConfig ScenesManagerConfig;
        [HideInInspector] public VFXManagerConfig VFXManagerConfig;
        [HideInInspector] public GesturesManagerConfig GesturesManagerConfig;
        [HideInInspector] public NetworkManagerConfig NetworkManagerConfig;
        [HideInInspector] public ConnectionsNetworkLibrary SocketNetworkManagerConfig;
        [HideInInspector] public FirebaseServicesConfig FirebaseServicesConfig;
        [HideInInspector] public ErrorsManagerConfig ErrorsManagerConfig;
        [HideInInspector] public SpritesIconsManagerConfig spritesIconsManagerConfig;
    }
}