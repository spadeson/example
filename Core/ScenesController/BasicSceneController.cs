using System.Collections;
using UnityEngine;
using VIS.CoreMobile;

namespace Core.ScenesController {
    public class BasicSceneController : MonoBehaviour {
        
        private void Start() {
            StartCoroutine(WaitForCoreInitializationCoroutine());
        }

        IEnumerator WaitForCoreInitializationCoroutine() {
            yield return new WaitUntil( () => CoreMobile.Instance.Initialized );
            ExecuteSceneController();
        }

        protected virtual void ExecuteSceneController() {
            
        }
    }
}