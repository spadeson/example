using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_URP
using UnityEngine.Rendering.Universal;
#endif

using UnityEngine.UI;

namespace VIS.CoreMobile.UISystem {
    [RequireComponent(typeof(Canvas), typeof(CanvasScaler), typeof(GraphicRaycaster))]
    public class UIManager : MonoBehaviour, IUIManager {

        #region Public Variables
        public Camera UiCamera { get; private set; }

        #endregion
        
        #region Private Variables

        private WidgetsLibrary _widgetLibrary;
        private List<UILayer> _uiLayers;
        
        private Canvas _canvas;
        private CanvasScaler _canvasScaler;

        private GameObject _preloaderPrefab;

        #endregion

        /// <inheritdoc />
        public void Initialize(UIManagerConfig uiManagerConfig) {

            if(_widgetLibrary == null) {
                _widgetLibrary = uiManagerConfig.WidgetsLibrary;
            }

            if(_canvas == null) {
                _canvas = GetComponent<Canvas>();
            }
            _canvas.renderMode = uiManagerConfig.RenderMode;
            _canvas.planeDistance = uiManagerConfig.planeDistance;

            if(_canvasScaler == null) {
                _canvasScaler = GetComponent<CanvasScaler>();
            }
            _canvasScaler = GetComponent<CanvasScaler>();

            _canvasScaler.uiScaleMode = uiManagerConfig.CanvasScaleMode;
            _canvasScaler.screenMatchMode = uiManagerConfig.canvasScreenMatchMode;
            _canvasScaler.referenceResolution = new Vector2(uiManagerConfig.referenceResolution.x, uiManagerConfig.referenceResolution.y);
            _canvasScaler.matchWidthOrHeight = uiManagerConfig.matchWidthOrHeight;

            CreateUILayers(uiManagerConfig);

            if (UiCamera != null || !uiManagerConfig.CreateUICamera) return;
            
            var uiCameraGo = new GameObject("UICamera");
            uiCameraGo.transform.position = new Vector3(0, 0, -10);
            DontDestroyOnLoad(uiCameraGo);

            UiCamera                  = uiCameraGo.AddComponent<Camera>();
            UiCamera.cullingMask      = 1 << LayerMask.NameToLayer("UI");
            UiCamera.orthographic     = uiManagerConfig.orthographic;
            UiCamera.orthographicSize = uiManagerConfig.orthographicSize;
            UiCamera.clearFlags       = uiManagerConfig.cameraClearFlags;
            UiCamera.backgroundColor  = Color.grey;
            UiCamera.allowHDR         = false;
            UiCamera.allowMSAA        = false;

            #if UNITY_URP
            var universalRenderPipelineData = UiCamera.GetUniversalAdditionalCameraData();
            universalRenderPipelineData.renderType = uiManagerConfig.cameraRenderType;
            #endif
        }

        void Start() {
            _canvas = GetComponent<Canvas>();
            _canvas.worldCamera = UiCamera;
        }

        #region Preloader
        public void ShowPreloader() {
            //if(_preloaderWidget != null) {
            //    _preloaderWidget.Activate(false);
            //    _preloaderWidget.SetLoadingProgress(0);
            //}
        }

        public void HidePreloader() {
            //if(_preloaderWidget != null) {
            //    _preloaderWidget.Dismiss(true);
            //}
        }

        public void SetPreloaderProgress(float progress) {
            //if(_preloaderWidget != null) {
            //    _preloaderWidget.SetLoadingProgress(progress);
            //}
        }
        #endregion

        #region Public Methods

        /// <inheritdoc />
        public Widget CreateUiWidgetWithData(string widgetType, object data = null, bool animate = false, bool allowDuplicates = false) {

            //Find related UILayer
            var layer = _uiLayers[_widgetLibrary.GetLayerByType(widgetType)];

            //Check for UI Widgets Duplicates
            if(layer.IsWidgetTypeAlreadyExists(widgetType) && !allowDuplicates) {
                Debug.LogWarning($"Widget of type {widgetType}, already exists at: {layer.name}");
                return null;
            }
            
            var widgetGuid = Guid.NewGuid().ToString();

            //Instantiate new Widget Prefab
            var widgetPrefab = Instantiate(_widgetLibrary.GetPrefabByType(widgetType));
            widgetPrefab.name = $"{widgetType.ToLowerInvariant()}-{widgetGuid}";
            
            var widget = widgetPrefab.GetComponent<Widget>();

            //Register it in UILayer
            layer.RegisterWidget(allowDuplicates ? widgetGuid : widgetType, widget);

            var controller = widgetPrefab.GetComponent<IWidgetControllerWithData>();
            controller.Initialize(widget, data);

            widget.Create();
            widget.Activate(animate);

            return widget;
        }

        /// <inheritdoc />
        public void ActivateWidgetByType(string widgetType, bool animated = false) {
            //Find related UILayer
            var layer = _uiLayers[_widgetLibrary.GetLayerByType(widgetType)];

            //Check for UI Widgets exists in layer
            if(!layer.IsWidgetTypeAlreadyExists(widgetType)) {
                return;
            }

            var widget = layer.GetWidgetByType(widgetType).GetComponent<Widget>();
            widget.Activate(animated);
        }

        /// <inheritdoc />
        public void DeactivateWidgetByType(string widgetType, bool animated = false) {
            //Find related UILayer
            var layer = _uiLayers[_widgetLibrary.GetLayerByType(widgetType)];

            //Check for UI Widgets exists in layer
            if(!layer.IsWidgetTypeAlreadyExists(widgetType)) {
                return;
            }

            var widget = layer.GetWidgetByType(widgetType).GetComponent<Widget>();
            widget.Deactivate(animated);
        }
        
        /// <inheritdoc />
        public void DismissWidgetByType(string widgetType) {
            //Find related UILayer
            var layer = _uiLayers[_widgetLibrary.GetLayerByType(widgetType)];

            //Check for UI Widgets exists in layer
            if(!layer.IsWidgetTypeAlreadyExists(widgetType)) {
                return;
            }

            var widget = layer.GetWidgetByType(widgetType).GetComponent<Widget>();
            widget.Dismiss();
        }

        /// <inheritdoc />
        public void DismissAllWidgets() {
            foreach (var widget in _uiLayers.SelectMany(layer => layer.GetAllWidgetsInLayer())) {
                widget.Dismiss();
            }
        }

        public UILayer GetUiLayerById(int layerId) {
            return _uiLayers[layerId];
        }

        public UILayer GetUiLayerByName(string layerName) {
            return _uiLayers.Find(l => l.name == layerName);
        }
        
        public List<UILayer> GetUiLayers() {
            return _uiLayers;
        }

        public void DissmissWidgetsInLayer(int layerIndex) {
            int widgetsCount = _uiLayers[layerIndex].GetWidgetsCount();
            if(_uiLayers[layerIndex] != null && widgetsCount > 0) {
                _uiLayers[layerIndex].GetLastWidget().Dismiss();
            }
        }
        #endregion

        #region Private Methods
        void CreateUILayers(UIManagerConfig uiManagerConfig) {

            _uiLayers = new List<UILayer>();

            for(var i = 0; i < uiManagerConfig.UILayersList.Count; i++) {
                var layerName = $"UI-Layer-{i}-{uiManagerConfig.UILayersList[i]}";
                var uiLayer = new GameObject(layerName).AddComponent<UILayer>();
                uiLayer.transform.SetParent(transform, false);
                uiLayer.Init(this, i, renderMode: uiManagerConfig.RenderMode);
                _uiLayers.Add(uiLayer);
            }
        }
        #endregion
    }

}