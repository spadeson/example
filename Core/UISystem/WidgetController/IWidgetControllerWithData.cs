﻿namespace VIS.CoreMobile.UISystem {
    public interface IWidgetControllerWithData {
        void Initialize(object widget, object data);
    }
}