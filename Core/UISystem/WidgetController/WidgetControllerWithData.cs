using UnityEngine;

namespace VIS.CoreMobile.UISystem {
    public abstract class WidgetControllerWithData<TWidget, TData> : MonoBehaviour, IWidgetControllerWithData where TWidget : Widget where TData : WidgetData {

        protected TWidget Widget { get; private set; }
        protected TData WidgetData { get; private set; }

        public void Initialize(object widget, object data) {
            Widget = (TWidget)widget;
            WidgetData = (TData) data;

            Widget.OnCreated += OnWidgetCreated;
            Widget.OnActivated += OnWidgetActivated;
            Widget.OnDeactivated += OnWidgetDeactivated;
            Widget.OnDismissed += OnWidgetDismissed;
        }

        private void OnDestroy() {
            if(Widget == null) return;
            Widget.OnCreated -= OnWidgetCreated;
            Widget.OnActivated -= OnWidgetActivated;
            Widget.OnDeactivated -= OnWidgetDeactivated;
            Widget.OnDismissed -= OnWidgetDismissed;
        }

        protected abstract void OnWidgetCreated(Widget widget);
        protected abstract void OnWidgetActivated(Widget widget);
        protected abstract void OnWidgetDeactivated(Widget widget);
        protected abstract void OnWidgetDismissed(Widget widget);
    }
}