using UnityEngine;
using UnityEditor;
#if UNITY_URP
using UnityEngine.Rendering.Universal;
#endif
using UnityEngine.UI;
using UnityEngine.UIElements;
using VIS.CoreMobile.UISystem;

[CustomEditor(typeof(UIManagerConfig))]
public class UIManagerConfigEditor : Editor {
    
    private GUIStyle _mainTitleStyle;

    public override VisualElement CreateInspectorGUI() {

        _mainTitleStyle = new GUIStyle {fontSize = 14, fontStyle = FontStyle.Bold, alignment = TextAnchor.UpperCenter, stretchWidth = true};

        return base.CreateInspectorGUI();
    }

    public override void OnInspectorGUI() {

        var uiManagerConfig = (UIManagerConfig)target;
        
        EditorGUILayout.LabelField("UI Manager Config", _mainTitleStyle);
        
        EditorGUILayout.Space();
        
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        
        EditorGUILayout.LabelField("UI Layers:", EditorStyles.boldLabel);
        
        EditorGUILayout.Space();

        for (var i = 0; i < uiManagerConfig.UILayersList.Count; i++) {
            uiManagerConfig.UILayersList[i] = EditorGUILayout.TextField("Layer Name:", uiManagerConfig.UILayersList[i]);
        }
        
        EditorGUILayout.Space();
        
        EditorGUILayout.BeginHorizontal();
        
        if (GUILayout.Button("Add Layer", GUILayout.Width(64), GUILayout.Height(24))) {
            uiManagerConfig.UILayersList.Add("New UI Layer");
        }
        
        if (GUILayout.Button("Remove Layer", GUILayout.Width(86), GUILayout.Height(24))) {
            if (uiManagerConfig.UILayersList.Count >= 1) {
                uiManagerConfig.UILayersList.RemoveAt(uiManagerConfig.UILayersList.Count - 1);   
            }
        }
        
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        
        uiManagerConfig.WidgetsLibrary = (WidgetsLibrary)EditorGUILayout.ObjectField("Widgets Library:", uiManagerConfig.WidgetsLibrary, typeof(WidgetsLibrary),false);
        
        EditorGUILayout.Space();
        
        EditorGUILayout.LabelField("Canvas:", EditorStyles.boldLabel);

        uiManagerConfig.RenderMode = (RenderMode)EditorGUILayout.EnumPopup("Canvas Render Mode:", uiManagerConfig.RenderMode);
        
        EditorGUILayout.Space();
        
        EditorGUILayout.LabelField("Canvas Scaler:", EditorStyles.boldLabel);
        
        uiManagerConfig.CanvasScaleMode = (CanvasScaler.ScaleMode)EditorGUILayout.EnumPopup("Canvas Scale Mode:", uiManagerConfig.CanvasScaleMode);

        switch (uiManagerConfig.CanvasScaleMode) {
        
            case CanvasScaler.ScaleMode.ConstantPixelSize:
                uiManagerConfig.scaleFactor = EditorGUILayout.FloatField("Scale Factor:", uiManagerConfig.scaleFactor);
                break;
            
            case CanvasScaler.ScaleMode.ScaleWithScreenSize:
                uiManagerConfig.referenceResolution = EditorGUILayout.Vector2IntField("Reference Resolution", uiManagerConfig.referenceResolution);
                uiManagerConfig.canvasScreenMatchMode = (CanvasScaler.ScreenMatchMode)EditorGUILayout.EnumPopup("Canvas Scale Mode:", uiManagerConfig.canvasScreenMatchMode);
                if (uiManagerConfig.canvasScreenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight) {
                    uiManagerConfig.matchWidthOrHeight = EditorGUILayout.Slider("Match: ", uiManagerConfig.matchWidthOrHeight, 0, 1f);
                }
                break;
            
            case CanvasScaler.ScaleMode.ConstantPhysicalSize:
                uiManagerConfig.physicalUnits = (CanvasScaler.Unit)EditorGUILayout.EnumPopup("Physical Units:", uiManagerConfig.physicalUnits);
                uiManagerConfig.fallbackScreenDpi = EditorGUILayout.FloatField("Fallback Screen Dpi: ", uiManagerConfig.fallbackScreenDpi);
                uiManagerConfig.fallbackSpriteDpi = EditorGUILayout.FloatField("Fallback Sprite Dpi: ", uiManagerConfig.fallbackSpriteDpi);
                break;
            
        }
        
        uiManagerConfig.referencePixelPerUnit = EditorGUILayout.FloatField("Reference Pixel Per Unit:", uiManagerConfig.referencePixelPerUnit);
        
        EditorGUILayout.Space();
        
        EditorGUILayout.LabelField("UI Camera:", EditorStyles.boldLabel);

        uiManagerConfig.CreateUICamera = EditorGUILayout.Toggle("Create UI Camera", uiManagerConfig.CreateUICamera);
        uiManagerConfig.CameraDepth = EditorGUILayout.IntField("UI Camera Depth:", uiManagerConfig.CameraDepth);
        uiManagerConfig.orthographic = EditorGUILayout.Toggle("IS UI Camera Orthographic:", uiManagerConfig.orthographic);
        uiManagerConfig.orthographicSize = EditorGUILayout.FloatField("Orthographic Size:", uiManagerConfig.orthographicSize);
        uiManagerConfig.cameraClearFlags = (CameraClearFlags)EditorGUILayout.EnumPopup("Camera Clear Flags:", uiManagerConfig.cameraClearFlags);
        uiManagerConfig.backgroundColor = EditorGUILayout.ColorField("Background Color:", uiManagerConfig.backgroundColor);
        #if UNITY_URP
        uiManagerConfig.cameraRenderType = (CameraRenderType)EditorGUILayout.EnumPopup("Camera Type:", uiManagerConfig.cameraRenderType);
        #endif

        EditorGUILayout.Space();

        if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }

    }
}

