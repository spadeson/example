using UnityEditor;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace Core.UISystem.Editor {
    [CustomEditor(typeof(BaseUIWidget), true)]
    public class BaseUIWidgetEditor : UnityEditor.Editor {

        private bool _animateWidget;
        
        public override void OnInspectorGUI() {
            
            base.OnInspectorGUI();

            GUILayout.BeginVertical(EditorStyles.helpBox);
            
            GUILayout.Space(16);

            _animateWidget = EditorGUILayout.Toggle("Animate Widget", _animateWidget);
            
            GUILayout.Space(12);
            
            var baseUIWidget = (BaseUIWidget)target;

            if (GUILayout.Button("Create", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                baseUIWidget.Create();
            }
            
            if (GUILayout.Button("Activate", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                baseUIWidget.Activate(_animateWidget);
            }
            
            if (GUILayout.Button("Deactivate", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                baseUIWidget.Deactivate(_animateWidget);
            }
            
            if (GUILayout.Button("Dismiss", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                baseUIWidget.Dismiss();
            }
            
            GUILayout.EndVertical();
        }
    }
}
