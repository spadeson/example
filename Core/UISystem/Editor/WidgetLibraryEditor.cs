﻿using UnityEngine;
using UnityEditor;
using VIS.CoreMobile.UISystem;
using UnityEditorInternal;

[CustomEditor(typeof(WidgetsLibrary))]
public class WidgetLibraryEditor : Editor
{
    private ReorderableList _list;

    private WidgetsLibrary WidgetLibrary
    {
        get
        {
            return target as WidgetsLibrary;
        }
    }

    private void OnEnable()
    {
        _list = new ReorderableList(WidgetLibrary.widgetLinks, typeof(WidgetsLibraryData), true, true, true, true);

        _list.onAddCallback += AddElementCallback;
        _list.onRemoveCallback += RemoveElementCallback;
        
        _list.drawHeaderCallback += DrawHeader;
        _list.drawElementCallback += DrawElement;
        _list.onChangedCallback += OnChangedCallback;
    }


    private void OnDisable()
    {
        _list.drawElementCallback -= DrawElement;
    }
    
    private void OnChangedCallback(ReorderableList list) {
        EditorUtility.SetDirty(target);
    }

    private void DrawHeader(Rect rect) {
        EditorGUI.LabelField (rect, "UI Widgets List:", EditorStyles.boldLabel);
    }
    
    private void AddElementCallback(ReorderableList reorderableList) {
        WidgetLibrary.widgetLinks.Add(new WidgetsLibraryData());
        EditorUtility.SetDirty(target);
    }

    private void RemoveElementCallback(ReorderableList reorderableList) {
        WidgetLibrary.widgetLinks.RemoveAt(reorderableList.index);
        EditorUtility.SetDirty(target);
    }

    private void DrawElement(Rect rect, int index, bool active, bool focused)
    {
        var item = WidgetLibrary.widgetLinks[index];

        EditorGUI.BeginChangeCheck();
        //item.boolValue = EditorGUI.Toggle(new Rect(rect.x, rect.y, 18, rect.height), item.boolValue);
        item.WidgetType = EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width * 0.45f - 18, rect.height), item.WidgetType);
        item.WidgetPrefab = (GameObject)EditorGUI.ObjectField(new Rect(rect.width * 0.45f + 18, rect.y, rect.width * 0.4f, rect.height), item.WidgetPrefab, typeof(GameObject), false);
        item.WidgetLayer = EditorGUI.IntField(new Rect(rect.width * 0.85f + 18, rect.y, rect.width * 0.15f - 18, rect.height), item.WidgetLayer);

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
        }

    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        // Actually draw the list in the inspector
        _list.DoLayoutList();

        EditorGUILayout.Space();

        if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32)))
        {
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }
    }
}
