﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_URP
using UnityEngine.Rendering.Universal;
#endif

using UnityEngine.UI;

namespace VIS.CoreMobile.UISystem {
    
    [CreateAssetMenu(fileName = "UIManagerConfig", menuName = "VIS/UI/UIManagerConfig")]
    public class UIManagerConfig : ScriptableObject {

        public List<string> UILayersList = new List<string>();
        
        public WidgetsLibrary WidgetsLibrary;

        public RenderMode RenderMode;
        public CanvasScaler.ScaleMode CanvasScaleMode;

        //Values for ConstantPixelSize
        public float scaleFactor = 1;

        //Values for ScaleWithScreenSize
        public Vector2Int referenceResolution = new Vector2Int(800, 600);
        public CanvasScaler.ScreenMatchMode canvasScreenMatchMode;
        public float matchWidthOrHeight = 0.5f;

        //Values for ConstantPhysicalSize
        public CanvasScaler.Unit physicalUnits = CanvasScaler.Unit.Points;
        public float fallbackScreenDpi = 96;
        public float fallbackSpriteDpi = 96;
        
        public float referencePixelPerUnit = 100;
        
        //UI Camera
        public bool CreateUICamera;
        public int CameraDepth;
        public float planeDistance = 10;
        public bool orthographic;
        public float orthographicSize = 2.5f;
        public Color backgroundColor = Color.grey;
        public CameraClearFlags cameraClearFlags;
        
        #if UNITY_URP
        public CameraRenderType cameraRenderType;
        #endif
    }
}
