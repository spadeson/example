﻿using UnityEngine;

namespace VIS.CoreMobile.UISystem {
    [RequireComponent(typeof(CanvasGroup))]
    public class BaseUIWidget : Widget {
        [SerializeField] protected CanvasGroup canvasGroup;

        public override void Create() {
            NotifyOnCreated();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public override void Activate(bool animated) {
            NotifyOnActivated();
            canvasGroup.alpha = 1f;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }

        public override void Deactivate(bool animated) {
            NotifyOnDeactivated();
            if(animated) return;
            canvasGroup.alpha = 0f;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        public override void Dismiss() {
            NotifyOnDismissed();
            Destroy(gameObject);
        }
    }
}