﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VIS.CoreMobile.UISystem {
    public abstract class Widget : MonoBehaviour {
        #region Actions
        public event Action<Widget> OnCreated;
        public event Action<Widget> OnActivated;
        public event Action<Widget> OnDeactivated;
        public event Action<Widget> OnDismissed;
        #endregion

        public abstract void Create();
        public abstract void Activate(bool animated);
        public abstract void Deactivate(bool animated);
        public abstract void Dismiss();

        protected void NotifyOnCreated() {
            OnCreated?.Invoke(this);
        }
        
        protected void NotifyOnActivated() {
            OnActivated?.Invoke(this);
        }

        protected void NotifyOnDeactivated() {
            OnDeactivated?.Invoke(this);
        }

        protected void NotifyOnDismissed() {
            OnDismissed?.Invoke(this);
        }
    }
}
