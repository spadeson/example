﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VIS.CoreMobile.UISystem {
    [Serializable]
    public class WidgetsLibraryData {
        public string WidgetType;
        public GameObject WidgetPrefab;
        public int WidgetLayer;
    }
}