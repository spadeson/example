﻿using System.Collections.Generic;
using UnityEngine;

namespace VIS.CoreMobile.UISystem {
    [CreateAssetMenu(fileName = "WidgetsLibrary", menuName = "VIS/UI/WidgetsLibrary")]
    public class WidgetsLibrary : ScriptableObject {

        [HideInInspector] public List<WidgetsLibraryData> widgetLinks = new List<WidgetsLibraryData>();

        public GameObject GetPrefabByType(string widgetType) {
            GameObject prefab = null;
            prefab = widgetLinks.Find(w => w.WidgetType == widgetType).WidgetPrefab;
            return prefab;
        }

        public Widget GetWidgetByType(string widgetType) {
            Widget widget = widgetLinks.Find(w => w.WidgetType == widgetType).WidgetPrefab.GetComponent<Widget>();
            return widget;
        }

        public int GetLayerByType(string widgetType) {
            int layer = widgetLinks.Find(w => w.WidgetType == widgetType).WidgetLayer;
            return layer;
        }
    }
}