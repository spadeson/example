﻿// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using System;
using UnityEngine;

namespace Core.ThirdParty {

	/// <summary>
	/// Reference to a class <see cref="System.Type"/> with support for Unity serialization.
	/// </summary>
	[Serializable]
    public sealed class EnumTypeReference : ISerializationCallbackReceiver {

        public static string GetClassRef(Type type,int value) {
			return type != null
                ? type.FullName + ", " + type.Assembly.GetName().Name + "#" + value.ToString()
				: "";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EnumTypeReference"/> class.
		/// </summary>
        public EnumTypeReference() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EnumTypeReference"/> class.
		/// </summary>
		/// <param name="assemblyQualifiedClassName">Assembly qualified class name.</param>
        public EnumTypeReference(string assemblyQualifiedClassName) {
			Type = !string.IsNullOrEmpty(assemblyQualifiedClassName)
				? Type.GetType(assemblyQualifiedClassName)
				: null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EnumTypeReference"/> class.
		/// </summary>
		/// <param name="type">Class type.</param>
		/// <exception cref="System.ArgumentException">
		/// If <paramref name="type"/> is not a class type.
		/// </exception>
        public EnumTypeReference(Type type) {
			Type = type;
		}

		[SerializeField]
		private string _classRef;

		#region ISerializationCallbackReceiver Members

		void ISerializationCallbackReceiver.OnAfterDeserialize() {
            if (!string.IsNullOrEmpty(_classRef)) {
                string [] arr = _classRef.Split('#');
                if (arr.Length == 2) {
                    string classRef = arr[0];
                    string valueStr = arr[1];
                    _type = System.Type.GetType(classRef);
                    int.TryParse(valueStr,out _value);
                }
				if (_type == null)
					Debug.LogWarning(string.Format("'{0}' was referenced but enum type was not found.", _classRef));
			}
			else {
				_type = null;
                _value = 0;
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize() {
		}

		#endregion

        private Type _type;

        private int _value;

		/// <summary>
		/// Gets or sets type of class reference.
		/// </summary>
		/// <exception cref="System.ArgumentException">
		/// If <paramref name="value"/> is not a class type.
		/// </exception>
		public Type Type {
			get { return _type; }
			set {
				if (value != null && !value.IsClass)
					throw new ArgumentException(string.Format("'{0}' is not a class type.", value.FullName), "value");

				_type = value;
                _classRef = GetClassRef(value ,_value);
			}
		}

		public static implicit operator string(EnumTypeReference typeReference) {
			return typeReference._classRef;
		}

		public static implicit operator Type(EnumTypeReference typeReference) {
			return typeReference.Type;
		}

		public static implicit operator EnumTypeReference(Type type) {
			return new EnumTypeReference(type);
		}

		public override string ToString() {
			return Type != null ? Type.FullName : "(None)";
		}

	}

}
