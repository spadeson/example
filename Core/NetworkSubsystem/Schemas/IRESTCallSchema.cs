﻿using System;
using System.Collections.Generic;
using Core.NetworkSubsystem.Data;

namespace Core.NetworkSubsystem.Schemas {
    /// <summary>
    /// REST request schema desctiption inrefrace
    /// </summary>
    public interface IRESTCallSchema {
        /// <summary>
        /// Gets the name of current REST schema
        /// </summary>
        string GetName ();

        /// <summary>
        /// Gets the REST request endpoint
        /// </summary>
        string GetEndpoint ();

        /// <summary>
        /// HTTP Request Method
        /// </summary>
        /// <returns>The method.</returns>
        HttpRequestMethods GetMethod ();

        /// <summary>
        /// Request headers collection
        /// </summary>
        List<HttpHeader> GetHeaders ();
    }

}