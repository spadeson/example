﻿using System;
using System.Collections.Generic;
using Core.NetworkSubsystem.Data;
using Core.NetworkSubsystem.Data.Request;
using Core.NetworkSubsystem.Data.Responce;
using Core.ThirdParty;
using UnityEngine;

namespace Core.NetworkSubsystem.Schemas {
    /// <summary>
    /// REST call schema description
    /// </summary>
    [CreateAssetMenu (fileName = "RESTSchema", menuName = "VIS/Network/APIv1/RESTSchema")]
    public class RESTCallSchema : ScriptableObject, IRESTCallSchema {
        #region Members
        #region Serialized Fields
        /// <summary>
        /// The name.
        /// </summary>
        [SerializeField] string _name;

        /// <summary>
        /// The endpoint.
        /// </summary>
        [SerializeField] string _endpoint;

        /// <summary>
        /// The method.
        /// </summary>
        [SerializeField] HttpRequestMethods _method;

        /// <summary>
        /// The headers.
        /// </summary>
        [SerializeField] List<HttpHeader> _headers;
        #endregion

        #region IRESTCallSchema implementation
        /// <summary>
        /// Gets the name of current REST schema
        /// </summary>
        public string GetName () {
            return _name;
        }

        /// <summary>
        /// Gets the REST request endpoint
        /// </summary>
        public string GetEndpoint () {
            return _endpoint;
        }

        /// <summary>
        /// HTTP Request Method
        /// </summary>
        /// <returns>The method.</returns>
        public HttpRequestMethods GetMethod () {
            return _method;
        }

        /// <summary>
        /// Request headers collection
        /// </summary>
        public List<HttpHeader> GetHeaders () {
            return _headers;
        }
        #endregion
        #endregion
    }
}