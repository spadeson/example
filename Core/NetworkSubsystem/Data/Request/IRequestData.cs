namespace Core.NetworkSubsystem.Data.Request {
    /// <summary>
    /// Interface used to identify class as a Request Data
    /// </summary>
    public interface IRequestData {
    }
}