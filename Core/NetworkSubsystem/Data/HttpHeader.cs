﻿using System;

namespace Core.NetworkSubsystem.Data {
    /// <summary>
    /// HTTP request header
    /// </summary>
    [Serializable]
    public class HttpHeader {
        #region Members
        /// <summary>
        /// HTTP request header key
        /// </summary>
        public string Name;

        /// <summary>
        /// HTTP request header value
        /// </summary>
        public string Value;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public HttpHeader (string name, string value) {
            Name = name;
            Value = value;
        }
        #endregion
    }
}