namespace Core.NetworkSubsystem.Data {
    public enum NetworkOperationType {
        WebRequest           = 1,
        BundleDownload       = 2,
        TextureDownload      = 3,
        ExternalAssetLoading = 4
    }
}