﻿namespace Core.NetworkSubsystem.Data {
    /// <summary>
    /// HTTP request method.
    /// </summary>
    public enum HttpRequestMethods {
        GET = 0,
        POST = 1,
        UPDATE = 2,
        DELETE = 3
    }
}