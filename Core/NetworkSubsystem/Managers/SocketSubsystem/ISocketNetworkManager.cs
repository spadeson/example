using System;
using Core.NetworkSubsystem.Config;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Command;
using PoketPet.Network.API.SocketModels.Files;
using PoketPet.Network.API.SocketModels.Game;
using PoketPet.Network.API.SocketModels.Messages;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.API.SocketModels.Tasks;
using PoketPet.Network.API.SocketModels.Users;
using Quobject.SocketIoClientDotNet.Client;

namespace Core.NetworkSubsystem.Managers.SocketSubsystem {
    public interface ISocketNetworkManager {
        
        void Initialize(ConnectionsNetworkLibrary config);
        void SetConnectionConfig(int connectionConfigId);
        void SetBind(string key, ISocketBind connector);
        void Connect();
        void Disconnect();
    }
}