using Quobject.SocketIoClientDotNet.Client;

namespace Core.NetworkSubsystem.Managers.SocketSubsystem {
    public interface ISocketBind {
        void SetSocket(Socket socket);
    }
}