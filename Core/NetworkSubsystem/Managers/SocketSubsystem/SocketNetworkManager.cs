using System;
using System.Collections;
using System.Collections.Generic;
using Core.NetworkSubsystem.Config;
using Core.Utilities;
using PoketPet.Network.API.SocketModels.Login;
using PoketPet.Network.API.SocketModels.Ping;
using PoketPet.Network.Dispatchers;
using PoketPet.User;
using UnityEngine;
using Quobject.SocketIoClientDotNet.Client;

namespace Core.NetworkSubsystem.Managers.SocketSubsystem {
    public class SocketNetworkManager : MonoBehaviour, ISocketNetworkManager {

        private Socket _socket;

        private ConnectionsNetworkLibrary _connectionConfigsLibrary;

        private ConnectionConfig _config;
        private bool _ping;
        private Coroutine _pingCoroutine;

        private readonly Dictionary<string, ISocketBind> binds = new Dictionary<string, ISocketBind>();

        public void Initialize(ConnectionsNetworkLibrary connectionConfigsLibrary) {
            _connectionConfigsLibrary = connectionConfigsLibrary;
            _config = _connectionConfigsLibrary.ConnectionConfigsList[_connectionConfigsLibrary.currentConnectionConfig];
        }

        public void SetConnectionConfig(ConnectionConfig connectionConfig) {
            _config = connectionConfig;
        }

        public void SetConnectionConfig(int connectionConfigId) {
            _config = _connectionConfigsLibrary.ConnectionConfigsList[connectionConfigId];
        }

        /// <summary>
        /// Connect socket. Connection Config and device unique id is needed.
        /// </summary>
        public void Connect() {
            var protocol = _config.networkConnectionConfigModel.ProtocolSocket;
            var host = _config.networkConnectionConfigModel.Host;
            var port = _config.networkConnectionConfigModel.PortSocket;

            var url = $"{protocol}://{host}:{port}";
            
            _socket = IO.Socket(url);
            
            foreach (var keyValuePair in binds) {
                keyValuePair.Value.SetSocket(_socket);
            }

            _socket.On("login-active",(data) => {
                var deserializedData = data.ToString();
                Debug.Log($"socket login active {deserializedData}");
                _socket.Emit("login-active", data.ToString());
            });

            StartPing();
        }
        
        /// <summary>
        /// Disconnecting current socket connection.
        /// </summary>
        public void Disconnect() {
            StopPing();
            _socket.Disconnect();
        }
        
        /// <summary>
        /// Set socket bind for events.
        /// </summary>
        /// <param name="key">Unique key event.</param>
        /// <param name="bind">Bind with event realisation.</param>
        public void SetBind(string key, ISocketBind bind) {
            if (binds.ContainsKey(key)) { return; }
            bind.SetSocket(_socket);
            binds.Add(key, bind);
        }

        #region Ping Methods
        private void StartPing(int pingDelay = 5) {
            _ping = true;
            if (_pingCoroutine != null) {
                StopCoroutine(_pingCoroutine);
                _pingCoroutine = null;
            }
            
            _pingCoroutine = StartCoroutine(PingCoroutine(pingDelay));
        }
        
        private void StopPing() {
            _ping = true;
            if (_pingCoroutine == null) return;
            StopCoroutine(_pingCoroutine);
            _pingCoroutine = null;
        }

        private IEnumerator PingCoroutine(int pingDelay) {
            while (_ping) {
                var ts   = TimeUtil.GetTimestamp();
                var ctx  = new StatusPing {ts = ts};
                var data = JsonUtility.ToJson(ctx);
                _socket.Emit("status-ping", data);
                yield return new WaitForSeconds(pingDelay);
            }
        }
        #endregion

        private void OnDestroy() {
            if (_socket == null) return;
            _socket.Close();
            _socket.Disconnect();
        }
    }
}