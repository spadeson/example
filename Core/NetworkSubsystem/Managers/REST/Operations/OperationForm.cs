using System;
using System.Collections;
using Core.NetworkSubsystem.Schemas;
using UnityEngine;
using UnityEngine.Networking;

namespace Core.NetworkSubsystem.Managers.REST {
    public class OperationForm : MonoBehaviour {
        public event Action<string> OnDataReceived;
        public event Action<byte[]> OnDataReceivedBytes;
        public event Action<string> OnErrorReceived;
        public event Action<float> OnDownloadProgress;

        public static event Action<OperationSchema, string> NotifyConsoleOnDataReceived;
        public static event Action<OperationSchema, string> NotifyConsoleOnErrorReceived;

        public bool isCompleted;
        
        public void PerformOperation(string url, RESTCallSchema schema,  WWWForm data) {
            StartCoroutine(CreateRequest_CO(url, schema, data));
        }
        
        private IEnumerator CreateRequest_CO(string url, RESTCallSchema schema, WWWForm data) {

            var request = UnityWebRequest.Post(url, data);

            //Set HTTP method
            request.method = UnityWebRequest.kHttpVerbPOST;
            
            request.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();

            //Set Headers
            foreach (var httpHeader in schema.GetHeaders()) {
                request.SetRequestHeader(httpHeader.Name, httpHeader.Value);
            }

            yield return request.SendWebRequest();
            
            OnDownloadProgress?.Invoke(request.downloadProgress);

            if (request.isNetworkError || request.isHttpError) {
                OnErrorReceived?.Invoke(request.error);
                //NotifyConsoleOnErrorReceived?.Invoke(this, request.error);
            } else {
                OnDataReceived?.Invoke(request.downloadHandler.text);
                //NotifyConsoleOnDataReceived?.Invoke(this, request.downloadHandler.text);
                OnDataReceivedBytes?.Invoke(request.downloadHandler.data);
            }
            
            isCompleted = true;
            
            request.Dispose();
            
            Destroy(gameObject, 0.3f);
        }
    }
}
