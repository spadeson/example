using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Core.NetworkSubsystem.Managers.REST {
    public class OperationURL : MonoBehaviour {
        
        public event Action<string> OnDataReceived;
        public event Action<byte[]> OnDataReceivedBytes;
        public event Action<string> OnErrorReceived;
        public event Action<float> OnDownloadProgress;

        public bool isCompleted;


        public void PerformOperation(string url, Dictionary<string, string> data) {
            StartCoroutine(CreateRequest_CO(url, data));
        }
        
        private IEnumerator CreateRequest_CO(string url, Dictionary<string, string> data) {

            var request = UnityWebRequest.Post(url, data);

            //Set HTTP method
            request.method = UnityWebRequest.kHttpVerbPOST;
            
            request.certificateHandler = new AcceptAllCertificatesSignedWithASpecificKeyPublicKey();

            yield return request.SendWebRequest();
            
            OnDownloadProgress?.Invoke(request.downloadProgress);

            if (request.isNetworkError || request.isHttpError) {
                OnErrorReceived?.Invoke(request.error);
                //NotifyConsoleOnErrorReceived?.Invoke(this, request.error);
            } else {
                OnDataReceived?.Invoke(request.downloadHandler.text);
                //NotifyConsoleOnDataReceived?.Invoke(this, request.downloadHandler.text);
                OnDataReceivedBytes?.Invoke(request.downloadHandler.data);
            }
            
            isCompleted = true;
            
            request.Dispose();
            
            Destroy(gameObject, 0.3f);
        }
    }
}
