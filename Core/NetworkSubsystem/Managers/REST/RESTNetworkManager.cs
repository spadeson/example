using System;
using System.Collections.Generic;
using Core.NetworkSubsystem.Config;
using Core.NetworkSubsystem.Data.Request;
using Core.NetworkSubsystem.Schemas;
using UnityEngine;
using ConnectionConfig = Core.NetworkSubsystem.Config.ConnectionConfig;

namespace Core.NetworkSubsystem.Managers.REST {
    public class RESTNetworkManager : MonoBehaviour, IRESTNetworkManager {
        
        private ConnectionsNetworkLibrary _connectionsNetworkLibrary;

        private List<RESTCallSchema> _schemas;

        private ConnectionConfig _currentConnectionConfig;

        public event Action<OperationSchema, string> OnDataReceived;
        public event Action<OperationSchema, byte[]> OnDataReceivedBytes;
        public event Action<OperationSchema, string> OnErrorReceived;

        public void Initialize(NetworkManagerConfig networkManagerConfig) {
            _connectionsNetworkLibrary = networkManagerConfig.ConnectionConfigsLibrary;
            _schemas = networkManagerConfig.SchemesMap;
        }

        public void SetConnectionConfig(ConnectionConfig connectionConfig) {
            _currentConnectionConfig = connectionConfig;
        }

        public OperationSchema CreateRestRequest(string schemaName, IRequestData data) {
            if (_currentConnectionConfig == null) {
                _currentConnectionConfig = _connectionsNetworkLibrary.ConnectionConfigsList[_connectionsNetworkLibrary.currentConnectionConfig];
            }

            //Form URL
            var protocol = _currentConnectionConfig.networkConnectionConfigModel.Protocol;
            var host     = _currentConnectionConfig.networkConnectionConfigModel.Host;
            var port     = _currentConnectionConfig.networkConnectionConfigModel.Port;

            //Find related RESTSchema by name
            var callSchema = _schemas.Find(e => e.GetName().Equals(schemaName));

            var url = $"{protocol}://{host}:{port}/{callSchema.GetEndpoint()}";

            var serializedData = JsonUtility.ToJson(data);
            Debug.Log($"CreateRestRequest: {serializedData}");
            var newOperationGo = new GameObject($"Operation-{callSchema.name}", typeof(OperationSchema));
            newOperationGo.transform.SetParent(transform);
            var newOperation = newOperationGo.GetComponent<OperationSchema>();
            newOperation.PerformOperation(url, callSchema, serializedData);
            return newOperation;
        }

        public OperationURL CreateRestRequest(string url, Dictionary<string, string> data) {
            var newOperationGo = new GameObject($"Operation-URL", typeof(OperationURL));
            newOperationGo.transform.SetParent(transform);
            var newOperation = newOperationGo.GetComponent<OperationURL>();
            newOperation.PerformOperation(url, data);
            return newOperation;
        }

        public OperationForm CreateRestRequest(string schemaName, WWWForm data) {
            if (_currentConnectionConfig == null) {
                _currentConnectionConfig = _connectionsNetworkLibrary.ConnectionConfigsList[_connectionsNetworkLibrary.currentConnectionConfig];
            }

            //Form URL
            var protocol = _currentConnectionConfig.networkConnectionConfigModel.Protocol;
            var host     = _currentConnectionConfig.networkConnectionConfigModel.Host;
            var port     = _currentConnectionConfig.networkConnectionConfigModel.Port;

            //Find related RESTSchema by name
            var callSchema = _schemas.Find(e => e.GetName().Equals(schemaName));

            var url = $"{protocol}://{host}:{port}/{callSchema.GetEndpoint()}";

            var newOperationGo = new GameObject($"Operation-{callSchema.name}", typeof(OperationForm));
            newOperationGo.transform.SetParent(transform);
            var newOperation = newOperationGo.GetComponent<OperationForm>();
            newOperation.PerformOperation(url, callSchema, data);
            return newOperation;
        }
    }
}