using System;
using System.Collections.Generic;
using Core.NetworkSubsystem.Config;
using Core.NetworkSubsystem.Data.Request;
using UnityEngine;
using ConnectionConfig = Core.NetworkSubsystem.Config.ConnectionConfig;

namespace Core.NetworkSubsystem.Managers.REST {
    public interface IRESTNetworkManager {
        event Action<OperationSchema, string> OnDataReceived;
        event Action<OperationSchema, byte[]> OnDataReceivedBytes;
        event Action<OperationSchema, string> OnErrorReceived;

        void            Initialize(NetworkManagerConfig networkManagerConfig);
        void            SetConnectionConfig(ConnectionConfig connectionConfig);
        OperationSchema CreateRestRequest(string schemaName, IRequestData data);
        OperationURL    CreateRestRequest(string url, Dictionary<string, string> data);
        OperationForm   CreateRestRequest(string schemaName, WWWForm data);
    }
}