using System;
using UnityEngine;

namespace Core.NetworkSubsystem.Managers.ImagesLoading {
    public interface IImagesDownloader {
        void DownloaderTexture(string url, Action<Texture2D> handler);
    }
}
