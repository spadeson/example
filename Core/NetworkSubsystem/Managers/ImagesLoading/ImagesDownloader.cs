using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Core.NetworkSubsystem.Managers.ImagesLoading {
    public class ImagesDownloader : MonoBehaviour, IImagesDownloader {

        public event Action<Texture2D> OnTextureDownloadedSuccess;
        public event Action<Texture2D, Action<Sprite>> OnTextureDownloadedSuccessWithCallback;

        public void DownloaderTexture(string url, Action<Texture2D> handler) {
            StartCoroutine(GetTexture_Coroutine(url, handler));
        }

        private IEnumerator GetTexture_Coroutine(string url, Action<Texture2D> handler) {
            
            var www = UnityWebRequestTexture.GetTexture(url);
            
            yield return www.SendWebRequest();

            if(www.isNetworkError || www.isHttpError) {
                Debug.LogError(www.error);
            }
            else {
                var result = ((DownloadHandlerTexture)www.downloadHandler).texture;
                handler?.Invoke(result);
            }
        }
    }
}
