using Core.NetworkSubsystem.Config;
using Core.NetworkSubsystem.Managers.ImagesLoading;
using Core.NetworkSubsystem.Managers.REST;
using Core.NetworkSubsystem.Managers.SocketSubsystem;

namespace Core.NetworkSubsystem.Managers {
    public interface INetworkManager {

        IRESTNetworkManager RESTNetworkManager { get; set; }
        ISocketNetworkManager SocketsNetworkManager { get; set; }
        IImagesDownloader ImagesDownloader { get; set; }

        #region Initialization

        /// <summary>
        /// Initializes network manager.
        /// </summary>
        /// <param name="networkManagerConfig">Network manager config.</param>
        void Initialize(NetworkManagerConfig networkManagerConfig);

        void SetConnectionConfig(int connectionConfigId, out ConnectionConfig connectionConfig);

        #endregion
    }
}
