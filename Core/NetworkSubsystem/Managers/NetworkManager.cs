using System.Collections.Generic;
using Core.NetworkSubsystem.Config;
using Core.NetworkSubsystem.Managers.ImagesLoading;
using Core.NetworkSubsystem.Managers.REST;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using Core.NetworkSubsystem.Schemas;
using UnityEngine;

namespace Core.NetworkSubsystem.Managers {
    public class NetworkManager : MonoBehaviour, INetworkManager {

        #region Internal Members
        
        /// <summary>
        /// NetworkLibrary.
        /// </summary>
        private ConnectionsNetworkLibrary _connectionsNetworkLibrary;

        /// <summary>
        /// REST Schemas
        /// </summary>
        [SerializeField] private List<RESTCallSchema> schemas;

        #endregion

        public IRESTNetworkManager RESTNetworkManager { get; set; }
        public ISocketNetworkManager SocketsNetworkManager { get; set; }
        public IImagesDownloader ImagesDownloader { get; set; }

        ///<inheritdoc/>
        public void Initialize(NetworkManagerConfig networkManagerConfig) {
            _connectionsNetworkLibrary = networkManagerConfig.ConnectionConfigsLibrary;
            schemas = networkManagerConfig.SchemesMap;

            var restNetworkManagerGo = new GameObject("RESTNetworkManager", typeof(RESTNetworkManager));
            restNetworkManagerGo.transform.SetParent(transform);
            RESTNetworkManager = restNetworkManagerGo.GetComponent<RESTNetworkManager>();
            RESTNetworkManager.Initialize(networkManagerConfig);
            
            //var socketsNetworkManagerGo = new GameObject("SocketsNetworkManager", typeof(SocketNetworkManager));
            //socketsNetworkManagerGo.transform.SetParent(transform);
            //SocketsNetworkManager = socketsNetworkManagerGo.GetComponent<SocketNetworkManager>();
            //SocketsNetworkManager.Initialize();
            
            var imagesDownloaderGo = new GameObject("ImagesDownloader", typeof(ImagesDownloader));
            imagesDownloaderGo.transform.SetParent(transform);
            ImagesDownloader = imagesDownloaderGo.GetComponent<ImagesDownloader>();
        }

        public void SetConnectionConfig(int connectionConfigId, out ConnectionConfig connectionConfig) {
            var newConnectionConfig = _connectionsNetworkLibrary.GetConnectionConfigById(connectionConfigId);
            connectionConfig = newConnectionConfig;
            RESTNetworkManager.SetConnectionConfig(newConnectionConfig);
        }
    }
}
