using UnityEditor;
using UnityEngine;

namespace Core.NetworkSubsystem.Config.Editor {
    
    [CustomEditor(typeof(ConnectionConfig))]
    public class ConnectionConfigEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {

            var config = (ConnectionConfig)target;

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("Connection Config - Generic Info", EditorStyles.boldLabel);
            config.networkConnectionConfigModel.ConnectionName = EditorGUILayout.TextField("Connection Name: ", config.networkConnectionConfigModel.ConnectionName);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.LabelField("Connection Config - Rest", EditorStyles.boldLabel);
            config.networkConnectionConfigModel.Protocol = EditorGUILayout.TextField("Protocol: ", config.networkConnectionConfigModel.Protocol);
            config.networkConnectionConfigModel.Host = EditorGUILayout.TextField("Host: ", config.networkConnectionConfigModel.Host);
            config.networkConnectionConfigModel.Port = EditorGUILayout.TextField("Port: ", config.networkConnectionConfigModel.Port);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.LabelField("Connection Config - Sockets", EditorStyles.boldLabel);
            config.networkConnectionConfigModel.ProtocolSocket = EditorGUILayout.TextField("Protocol Sockets: ", config.networkConnectionConfigModel.ProtocolSocket);
            config.networkConnectionConfigModel.PortSocket = EditorGUILayout.TextField("Port Sockets: ", config.networkConnectionConfigModel.PortSocket);
            
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            
            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}

