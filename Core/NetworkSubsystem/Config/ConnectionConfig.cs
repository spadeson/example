using System;
using PoketPet.Network.NetworkLibrary;
using UnityEngine;

namespace Core.NetworkSubsystem.Config {
    [Serializable, CreateAssetMenu(fileName = "NetworkConfig", menuName = "PoketPet/Core/Network/New Network Config")]
    public class ConnectionConfig : ScriptableObject {
        public NetworkConnectionConfigModel networkConnectionConfigModel;
    }
}