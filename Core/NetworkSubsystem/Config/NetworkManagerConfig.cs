using System.Collections.Generic;
using Core.NetworkSubsystem.Schemas;
using PoketPet.Network.NetworkLibrary;
using UnityEngine;

namespace Core.NetworkSubsystem.Config
{
    [CreateAssetMenu(fileName = "NetworkManagerConfig", menuName = "VIS/Network/Network Manager Config")]
    public class NetworkManagerConfig : ScriptableObject
    {
        /// <summary>
        /// Connections configs.
        /// </summary>
        public ConnectionsNetworkLibrary ConnectionConfigsLibrary;
        
        /// <summary>
        /// REST Schemes maps. 
        /// </summary>
        public List<RESTCallSchema> SchemesMap;
        
    }
}