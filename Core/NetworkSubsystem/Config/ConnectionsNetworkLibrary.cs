using System.Collections.Generic;
using PoketPet.Network.NetworkLibrary;
using UnityEngine;

namespace Core.NetworkSubsystem.Config {
    [CreateAssetMenu(fileName = "ConnectionsNetworkLibrary", menuName = "PoketPet/Core/Network/Connections Network Library Config")]
    public class ConnectionsNetworkLibrary : ScriptableObject {
        public int currentConnectionConfig;
        public List<ConnectionConfig> ConnectionConfigsList = new List<ConnectionConfig>();

        public NetworkConnectionConfigModel GetConnectionConfigModel(string environmentName) {
            return ConnectionConfigsList.Find(c => c.networkConnectionConfigModel.ConnectionName == environmentName)
                .networkConnectionConfigModel;
        }

        public ConnectionConfig GetConnectionConfigById(int configId) {
            return ConnectionConfigsList[configId];
        }
    }
}