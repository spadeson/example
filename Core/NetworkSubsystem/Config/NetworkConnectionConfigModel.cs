using System;

namespace PoketPet.Network.NetworkLibrary
{
    [Serializable]
    public class NetworkConnectionConfigModel
    {
        public string ConnectionName;
        
        public string Protocol;
        public string ProtocolSocket;
        public string Host;
        public string Port;
        public string PortSocket;
    }
}