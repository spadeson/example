namespace Core.GesturesSystem.Data {
    public enum GesturesConfigInputTypes {
        TOUCH = 0,
        MOUSE = 1,
        KEYBOARD = 2
    }
}