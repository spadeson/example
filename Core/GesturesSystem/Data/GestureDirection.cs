﻿namespace Core.GesturesSystem.Data {
    public enum GestureDirection {
        NONE = 0,
        UP = 1,
        DOWN = 2,
        LEFT = 3,
        RIGHT = 4
    }
}