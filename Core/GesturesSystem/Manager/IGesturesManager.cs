﻿using System;
using Core.GesturesSystem.Data;

namespace VIS.CoreMobile.GesturesSystem {
    public interface IGesturesManager {
        event Action<GestureDirection> OnGestureDetected;
        event Action<float, float> OnMouseGestureDetected;
        event Action<float> OnMouseWheelGestureDetected;
        event Action OnMouseGestureFinished;
        event Action OnTabButtonPressed;
        void initialize(GesturesManagerConfig gesturesManagerConfig);
    }
}
