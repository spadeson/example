using System;
using Core.GesturesSystem.Data;
using UnityEngine;

namespace VIS.CoreMobile.GesturesSystem {
    public class DiscreteGesturesManager : MonoBehaviour, IGesturesManager {

        #region Public Variables

        public event Action<GestureDirection> OnGestureDetected;
        public event Action<float, float>     OnMouseGestureDetected;
        public event Action<float>            OnMouseWheelGestureDetected;
        public event Action                   OnMouseGestureFinished;
        public event Action OnTabButtonPressed;

        #endregion

        #region Private Variables
        
        //Config File
        private GesturesManagerConfig _gesturesManagerConfig;
        
        private bool _isSwipe;
        private float _fingerStartTime;
        private Vector2 _fingerStartPosition;
        
        private const float GESTURE_DISTANCE_THRESHOLD = 50.0f;
        private const float GESTURE_TIME_THRESHOLD = 0.5f;

        //Gestures Direction
        GestureDirection _gestureDirection = GestureDirection.NONE;
        
        //Keyboard
        private const string HORIZONTAL_AXIS = "Horizontal";
        private const string VERTICAL_AXIS = "Vertical";
        private const float KEYBOARD_AXIS_THRESHOLD = 0.01f;
        
        //Mouse Variables block
        private const string MOUSE_X = "Mouse X";
        private const string MOUSE_Y = "Mouse Y";

        private float _horizontal;
        private float _vertical;

        #endregion

        #region Public Methods

        public void initialize(GesturesManagerConfig gesturesManagerConfig) {
            _gesturesManagerConfig = gesturesManagerConfig;
        }

        #endregion

        private void Update() {

            switch (_gesturesManagerConfig.GesturesConfigInputTypes) {
                case GesturesConfigInputTypes.TOUCH:
                    UseTouchGestures();
                    break;
                case GesturesConfigInputTypes.MOUSE:
                    UseMouseGestures();
                    break;
                case GesturesConfigInputTypes.KEYBOARD:
                    UseKeyboardGestures();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (Mathf.Abs(_horizontal) > KEYBOARD_AXIS_THRESHOLD) {
                _gestureDirection = _horizontal > KEYBOARD_AXIS_THRESHOLD ? GestureDirection.RIGHT : GestureDirection.LEFT;
                OnGestureDetected?.Invoke(_gestureDirection);
            } else if (Mathf.Abs(_vertical) > KEYBOARD_AXIS_THRESHOLD) {
                _gestureDirection = _vertical > KEYBOARD_AXIS_THRESHOLD ? GestureDirection.UP : GestureDirection.DOWN;
                OnGestureDetected?.Invoke(_gestureDirection);
            } else {
                _gestureDirection = GestureDirection.NONE;
            }
            
            #if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Tab)) {
                OnTabButtonPressed?.Invoke();
            }
            #endif
        }

        private void UseTouchGestures() {
                //If no touches was registered just return
            if (Input.touchCount <= 0) return;

            //Iterate through each touch
            foreach (Touch touch in Input.touches) {
                //Check touch state
                if (touch.phase == TouchPhase.Began) {
                    _isSwipe             = true;
                    _fingerStartTime     = Time.time;
                    _fingerStartPosition = touch.position;
                } else if (touch.phase == TouchPhase.Canceled) {
                    _isSwipe = false;
                }
                else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Ended) {
                    //Time from Touch began until touch ended
                    var gestureTime = Time.time - _fingerStartTime;

                    //Distance of the touch
                    var gestureDist = (touch.position - _fingerStartPosition).magnitude;

                    if (_isSwipe && gestureTime < GESTURE_TIME_THRESHOLD && gestureDist > GESTURE_DISTANCE_THRESHOLD) {
                        var gestureDirection = touch.position - _fingerStartPosition;

                        Vector2 swipeValues;

                        if (Mathf.Abs(gestureDirection.x) > Mathf.Abs(gestureDirection.y)) {
                            //The swipe is horizontal
                            swipeValues = Vector2.right * Mathf.Sign(gestureDirection.x);
                        }
                        else {
                            //The swipe is vertical
                            swipeValues = Vector2.up * Mathf.Sign(gestureDirection.y);
                        }

                        if (Math.Abs(swipeValues.x) > 0.01f) {
                            _gestureDirection = swipeValues.x > 0f ? GestureDirection.RIGHT : GestureDirection.LEFT;
                        }
                        else if (Mathf.Abs(swipeValues.y) > 0.01f) {
                            _gestureDirection = swipeValues.y > 0f ? GestureDirection.UP : GestureDirection.DOWN;
                        }
                        else {
                            _gestureDirection = GestureDirection.NONE;
                        }

                        OnGestureDetected?.Invoke(_gestureDirection);
                    }
                }
            }
        }

        private void UseMouseGestures() {
            if (Input.GetMouseButton(0)) {
                _horizontal = Input.GetAxis(MOUSE_X);
                _vertical   = Input.GetAxis(MOUSE_Y);
            } else {
                _horizontal = 0;
                _vertical   = 0;
            }
        }

        private void UseKeyboardGestures() {
            _horizontal = Input.GetAxis(HORIZONTAL_AXIS);
            _vertical   = Input.GetAxis(VERTICAL_AXIS);
        }
    }
}
