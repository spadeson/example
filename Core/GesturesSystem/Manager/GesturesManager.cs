﻿using UnityEngine;
using System.Collections;
using System;
using Core.GesturesSystem.Data;

namespace VIS.CoreMobile.GesturesSystem {
    
    public class GesturesManager : MonoBehaviour, IGesturesManager {
        #region Public Variables

        public event Action<GestureDirection> OnGestureDetected;
        public event Action<float, float>     OnMouseGestureDetected;
        public event Action<float>            OnMouseWheelGestureDetected;
        public event Action                   OnMouseGestureFinished;
        public event Action OnTabButtonPressed;

        #endregion

        #region Private Variables

        //Config File
        private GesturesManagerConfig _gesturesManagerConfig;

        //Touch Screen Variables block
        private float _fingerStartTime;
        private Vector2 _fingerStartPosition;

        private bool _isSwipe;
        private const float GESTURE_DISTANCE_THRESHOLD = 50.0f;
        private const float GESTURE_TIME_THRESHOLD = 1f;

        //Keyboard
        private const string HORIZONTAL_AXIS = "Horizontal";
        private const string VERTICAL_AXIS = "Vertical";
        private const float KEYBOARD_AXIS_THRESHOLD = 0.01f;

        //Mouse Variables block
        private const string MOUSE_X = "Mouse X";
        private const string MOUSE_Y = "Mouse Y";
        private const string MOUSE_SCROLL_WHEEL = "Mouse ScrollWheel";
        private const float MOUSE_MOVE_THRESHOLD = 0.05f;
        private const float MOUSE_WHEEL_THRESHOLD = 0.01f;
        
        private float _horizontal;
        private float _vertical;

        #endregion

        public void initialize(GesturesManagerConfig gesturesManagerConfig) {
            _gesturesManagerConfig = gesturesManagerConfig;
        }

        void Update() {
            #if UNITY_EDITOR
            //If use discrete mode GestureManager
            if (_gesturesManagerConfig.GesturesConfigInputTypes == GesturesConfigInputTypes.MOUSE) {
                if (Input.GetMouseButton(0)) {
                    _horizontal = Input.GetAxis(MOUSE_X);
                    _vertical = Input.GetAxis(MOUSE_Y);
                }
            } else {
                _horizontal = Input.GetAxis(HORIZONTAL_AXIS);
                _vertical   = Input.GetAxis(VERTICAL_AXIS);
            }
            
            if (Mathf.Abs(_horizontal) > MOUSE_MOVE_THRESHOLD || Mathf.Abs(_vertical) > MOUSE_MOVE_THRESHOLD) {
                Debug.Log($"MSX: {_horizontal}, MSY: {_vertical}");
                OnMouseGestureDetected?.Invoke(_horizontal, _vertical);
            } else {
                OnMouseGestureFinished?.Invoke();
            }
            
            if (Input.GetKeyDown(KeyCode.Tab)) {
                OnTabButtonPressed?.Invoke();
            }

            #elif !UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS

            //If no touches was registered just return
            if (Input.touchCount <= 0) return;

            //Iterate through each touch
            foreach (Touch touch in Input.touches) {
                //Check touch state
                switch (touch.phase) {
                    //This is a new touch
                    case TouchPhase.Began:
                        _isSwipe         = true;
                        _fingerStartTime = Time.time;
                        _fingerStartPosition  = touch.position;
                        break;
                    
                    case TouchPhase.Moved:
                        _isSwipe             = true;
                        _fingerStartTime     = Time.time;
                        _fingerStartPosition = touch.position;
                        
                        //Time from Touch began until touch ended
                        var gestureTime = Time.time - _fingerStartTime;

                        //Distance of the touch
                        var gestureDist = (touch.position - _fingerStartPosition).magnitude;
                        
                        if (_isSwipe && gestureTime < GESTURE_TIME_THRESHOLD && gestureDist > GESTURE_DISTANCE_THRESHOLD) {

                            var gestureDirection = touch.position - _fingerStartPosition;

                            _horizontal = gestureDirection.normalized.x;
                            _vertical   = gestureDirection.normalized.y;
                            
                            Debug.Log($"GT: {gestureTime}, GX: {_horizontal}, GY: {_vertical}");
                            
                            OnMouseGestureDetected?.Invoke(_horizontal, _vertical);
                        }
                        
                        break;

                    //The touch is being canceled
                    case TouchPhase.Canceled:
                        _isSwipe = false;
                        break;
                }
            }

            #endif
        }
    }
}
