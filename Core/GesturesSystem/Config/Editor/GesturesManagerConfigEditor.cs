using Core.GesturesSystem.Data;
using UnityEditor;
using UnityEngine;

namespace VIS.CoreMobile.GesturesSystem {
    [CustomEditor(typeof(GesturesManagerConfig))]
    public class GesturesManagerConfigEditor : Editor {
        public override void OnInspectorGUI() {
            //base.OnInspectorGUI();

            var gesturesManagerConfig = (GesturesManagerConfig)target;
            
            EditorGUILayout.LabelField("GesturesManagerConfig", EditorStyles.boldLabel);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            gesturesManagerConfig.GesturesConfigInputTypes = (GesturesConfigInputTypes)EditorGUILayout.EnumPopup("Config Input Types", gesturesManagerConfig.GesturesConfigInputTypes);
            
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            gesturesManagerConfig.UseDiscreteStates = EditorGUILayout.Toggle("Use Discrete States", gesturesManagerConfig.UseDiscreteStates);
            
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.Space();
            
            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}