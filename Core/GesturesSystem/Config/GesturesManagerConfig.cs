﻿using System;
using Core.GesturesSystem.Data;
using UnityEngine;

namespace VIS.CoreMobile.GesturesSystem {
    [CreateAssetMenu(fileName = "GesturesManagerConfig", menuName = "VIS/Gestures/Gestures Manager Config")]
    public class GesturesManagerConfig : ScriptableObject {
        //Should be X and Y finger positions work with touches, mouse or keyboard
        public GesturesConfigInputTypes GesturesConfigInputTypes;
        //Should be used gestures like as D-PAD or like X and Y floats. 
        public bool UseDiscreteStates;
    }
}