using System;

namespace Core.GoogleManager {
    [Serializable]
    public class GoogleIdTokenResponse {
        public string access_token;
        public int expires_in;
        public string id_token;
        public string refresh_token;
        public string scope;
        public string token_type;
    }
}