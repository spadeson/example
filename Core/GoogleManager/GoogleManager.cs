﻿// <copyright file="SigninSampleScript.cs" company="Google Inc.">
// Copyright (C) 2017 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Google;
using VIS.CoreMobile;

namespace Core.GoogleManager {
    public class GoogleManager : MonoBehaviour, IGoogleManager {
        public event Action<string> OnSuccessLogin;

        #if UNITY_EDITOR
        public string webClientId = "1092447243135-rqme8huthng09m2dleaofgabd5o3ltne.apps.googleusercontent.com";
        #else
        public string webClientId = "1092447243135-5hjfr4cp6lhc928hao87rbnn6h67h3ul.apps.googleusercontent.com";
        #endif
        
        public string clientSecret = "vpFCCJDicdauZEmHzfz3rh5J";

        // Defer the configuration creation until Awake so the web Client ID
        // Can be set via the property inspector in the Editor.

        public void Init() {
            GoogleSignIn.Configuration = new GoogleSignInConfiguration {RequestIdToken = true, UseGameSignIn = false, WebClientId = webClientId};
        }

        public void SignIn() {
            GoogleSignIn.DefaultInstance.SignIn().ContinueWith(AuthenticationFinished, TaskContinuationOptions.ExecuteSynchronously);
        }

        public void SignIn(string code) {
            if(string.IsNullOrEmpty(code)) return;
            var url  = "https://oauth2.googleapis.com/token";
            var data = new Dictionary<string, string> {
                {"client_id", webClientId},
                {"client_secret", clientSecret},
                {"code", code},
                {"redirect_uri", "urn:ietf:wg:oauth:2.0:oob"},
                {"grant_type", "authorization_code"}
            };
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest(url, data);
            
            operation.OnDataReceived += response => {
                var googleIdTokenResponse = JsonUtility.FromJson<GoogleIdTokenResponse>(response);
                OnSuccessLogin?.Invoke(googleIdTokenResponse.id_token);
            };
        }

        public void SignOut() {
            Debug.Log("Calling SignOut");
            GoogleSignIn.DefaultInstance.SignOut();
        }

        public void Disconnect() {
            Debug.Log("Calling Disconnect");
            GoogleSignIn.DefaultInstance.Disconnect();
        }

        private void AuthenticationFinished(Task<GoogleSignInUser> task) {
            if (task.IsFaulted) {
                
                if (task.Exception == null) return;
                
                using (var enumerator = task.Exception.InnerExceptions.GetEnumerator()) {
                    if (enumerator.MoveNext()) {
                        var error = (GoogleSignIn.SignInException) enumerator.Current;
                        Debug.Log("Got Error: " + error.Status + " " + error.Message);
                    } else {
                        Debug.Log("Got Unexpected Exception?!?" + task.Exception);
                    }
                }
            } else if (task.IsCanceled) {
                Debug.Log("Canceled");
            } else {
                OnSuccessLogin?.Invoke(task.Result.IdToken);
                Debug.Log("Welcome: " + task.Result.DisplayName + "!");
            }
        }

        public void SignInSilently() {
            GoogleSignIn.Configuration.UseGameSignIn  = false;
            GoogleSignIn.Configuration.RequestIdToken = true;
            Debug.Log("Calling SignIn Silently");

            GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(AuthenticationFinished, TaskScheduler.FromCurrentSynchronizationContext());
        }


        public void GamesSignIn() {
            GoogleSignIn.Configuration.UseGameSignIn  = true;
            GoogleSignIn.Configuration.RequestIdToken = false;

            Debug.Log("Calling Games SignIn");

            GoogleSignIn.DefaultInstance.SignIn().ContinueWith(AuthenticationFinished, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
