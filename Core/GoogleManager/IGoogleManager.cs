﻿using System;

namespace Core.GoogleManager {
    public interface IGoogleManager {
        event Action<string> OnSuccessLogin;
        void Init();
        void SignIn();
        void SignIn(string code);
        void SignOut();
        void Disconnect();
        void SignInSilently();
        void GamesSignIn();
    }
}
