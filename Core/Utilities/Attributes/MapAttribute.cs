﻿// -------------------------------------------------------------------------
// DCORE
//
// MapAttribute.cs
//
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.
//
// Copyright (c) 2017 DGN Games, LLC - All Rights Reserved
// -------------------------------------------------------------------------

using UnityEngine;

/// <summary>
/// Map attribute mark Serializeble dictionary to show  in reorderable style
/// </summary>
public class MapAttribute : PropertyAttribute {
    #region Contrustors

    public MapAttribute () {
    }

    #endregion
}

