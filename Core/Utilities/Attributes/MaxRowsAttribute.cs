﻿// -------------------------------------------------------------------------
// DCORE
//
// ReorderableAttribute.cs
//
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.
//
// Copyright (c) 2017 DGN Games, LLC - All Rights Reserved
// -------------------------------------------------------------------------

using UnityEngine;

/// <summary>
/// Sets maxixmun row to show in scroll value -1 means infinity(no scroll) 0 mean default value = 15 row
/// </summary>
public class MaxRowsAttribute : PropertyAttribute {
    #region Public Properties
    /// <summary>
    /// Maxixmun row to show in scroll value -1 means infinity(no scroll) 0 mean default value = 15 row
    /// </summary>
    /// <value>The max rows.</value>
    public int MaxRows { get; protected set; }

    #endregion

    #region Contrustors

    /// <summary>
    /// Display a List/Array as a sortable list in the inspector
    /// </summary>
    public MaxRowsAttribute (int maxRows) {
        MaxRows = maxRows;
    }

    #endregion
}
