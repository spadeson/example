﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

namespace Core.Utilities {
    /// <summary>
    /// Utility Class for String helper functions
    /// </summary>
    public static class StringUtil {
        /// <summary>
        /// Generates a random string with a specified amount of characters
        /// </summary>
        public static string generateRandomString(int length=32) {
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%&=_-";
            System.Random random = new System.Random();
			return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Gets the formatted currency string according to ISO 4217 currency code.
        /// </summary>
        public static string getFormattedCurrencyString(decimal price, string isoCurrencyCode) {
            string res = string.Empty;
            string priceString = price.ToString ("N", CultureInfo.CurrentCulture);
            if (string.CompareOrdinal (isoCurrencyCode, "USD") == 0) {
                res = string.Format ("${0}", priceString);
            } else if (string.CompareOrdinal (isoCurrencyCode, "CAD") == 0) {
                res = string.Format ("${0} CAD", priceString);
            } else {
                res = string.Format ("{0} {1}", priceString, isoCurrencyCode);
            }
            return res;
        }

        /// <summary>
        /// Gets the price without appending the currency code
        /// </summary>
        public static string getFormattedPrice(decimal price, string isoCurrencyCode) {
            string res = string.Empty;
            string priceString = price.ToString("N", CultureInfo.CurrentCulture);
            if(string.CompareOrdinal(isoCurrencyCode, "USD") == 0) {
                res = string.Format("${0}", priceString);
            } else if(string.CompareOrdinal(isoCurrencyCode, "CAD") == 0) {
                res = string.Format("${0}", priceString);
            } else {
                res = string.Format("{0}", priceString);
            }
            return res;
        }

        /// <summary>
        /// Gets the price without appending the currency code
        /// </summary>
        public static string getFormattedCurrency(string isoCurrencyCode) {
            if (string.CompareOrdinal(isoCurrencyCode, "USD") == 0) {
                return string.Empty;
            } else if(string.CompareOrdinal(isoCurrencyCode, "CAD") == 0) {
                return "CAD";
            } else {
                return string.Format("{0}", isoCurrencyCode);
            }
        }
        /// <summary>
        /// Calculate the MD5 hash of a given string.
        /// </summary>
        public static string calculateMD5(string input) {
            string md5Value = string.Empty;
            var md5 = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash       = md5.ComputeHash(inputBytes);

            foreach (byte b in hash) {
                md5Value += b.ToString("X2");
            }
            return md5Value;
        }

        /// <summary>
        /// Get the Long time string NOW
        /// </summary>
        public static string getLongTimeString() {
            return DateTime.Now.ToLongTimeString();
        }

        /// <summary>
        /// Checks whether a specified string is valid mail address.
        /// </summary>
        public static bool isValidMailAddress(string address) {
            bool invalid = false;
            if (String.IsNullOrEmpty(address))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try {
                address = Regex.Replace(address, @"(@)(.+)$", DomainMapper, RegexOptions.None);
            } catch (Exception) {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try {
                return Regex.IsMatch(address,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase);
            }
            catch (Exception) {
                return false;
            }
        }

        private static string DomainMapper(Match match) {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            domainName = idn.GetAscii(domainName);
            
            return match.Groups[1].Value + domainName;
        }

        /// <summary>
        /// Appends the URL parameters to URL
        /// 
        /// If urlParams are null or empty - returns initial URL
        /// </summary>
        /// <returns>The resulting URL.</returns>
        /// <param name="url">URL.</param>
        /// <param name="urlParams">URL parameters.</param>
        public static string appendURLParamsToURL(string url, Dictionary<string, string> urlParams) {
            if (urlParams == null || urlParams.Count == 0) {
                return url;
            }

            StringBuilder urlBuilder = new StringBuilder (url);

            urlBuilder.Append ("?");

            int currentVarIndex = 0;
            foreach (var kvp in urlParams) {
                urlBuilder.AppendFormat ("{0}={1}", kvp.Key, UnityWebRequest.EscapeURL(kvp.Value));
                if (currentVarIndex < urlParams.Count - 1) {
                    urlBuilder.Append ("&");
                }
                currentVarIndex++;
            }

            return urlBuilder.ToString ();
        }

        public static string getUSFormat(long number) {
            string result = string.Empty;

            if (number < Mathf.Pow(10, 5)) {
                result = number.ToString("N0", CultureInfo.CreateSpecificCulture("en-US"));
            } else if (number < Mathf.Pow(10, 6)) {
                string resultNumber = (number / Mathf.Pow(10, 3)).ToString();
                result = string.Format("{0}K", cutString(resultNumber, 5));
            } else if (number < Mathf.Pow(10, 9)) {
                string resultNumber = (number / Mathf.Pow(10, 6)).ToString();
                result = string.Format("{0}M", cutString(resultNumber, 5));
            } else if (number < Mathf.Pow(10, 12)) {
                string resultNumber = (number / Mathf.Pow(10, 9)).ToString();
                result = string.Format("{0}B", cutString(resultNumber, 5));
            } else if (number < Mathf.Pow(10, 15)) {
                string resultNumber = (number / Mathf.Pow(10, 12)).ToString();
                result = string.Format("{0}T", cutString(resultNumber, 5));
            } else if (number >= Mathf.Pow(10, 15)) {
                string resultNumber = (number / Mathf.Pow(10, 15)).ToString();
                result = string.Format("{0}Q", cutString(resultNumber, 5));
            }

            return result;
        }

        static string cutString(string text, int length) {
            if (text.Length > length) {
                return text.Remove(length);
            }
            return text;
        }
    }
}
