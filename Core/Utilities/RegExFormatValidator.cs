using System.Text.RegularExpressions;

namespace Core.Utilities {
    public static class RegExFormatValidator {
        public static bool ValidateData(string pattern, string val) {
            return val != null && Regex.IsMatch(val, pattern);
        }
    }
}