﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;


namespace Core.Utilities.Editor {
    public class UITexturesImporter : AssetPostprocessor {

        private void OnPreprocessTexture() {
            if (assetPath.ToLower().Contains("ui")) {
                var textureImporter = (TextureImporter)assetImporter;
                textureImporter.textureType = TextureImporterType.Sprite;
                textureImporter.spriteImportMode = SpriteImportMode.Single;

                var textureSettings = new TextureImporterSettings();
                textureImporter.ReadTextureSettings(textureSettings);

                textureSettings.spriteAlignment = (int)SpriteAlignment.Center;
                textureSettings.spriteGenerateFallbackPhysicsShape = false;

                if (assetPath.ToLower().Contains("@")) {
                    var borders = assetPath.ToLower().Split('@');
                    var pattern = @"^b\d*";
                    var regex = Regex.Match(borders[1], pattern);
                    var parsedBordersValue = regex.Value.Substring(1, regex.Length - 1);
                    var bordersValue = int.Parse(parsedBordersValue);
                    textureSettings.spriteBorder = new Vector4(bordersValue, bordersValue, bordersValue, bordersValue);
                    textureImporter.SetTextureSettings(textureSettings);
                }

                if (textureSettings.spriteMeshType == SpriteMeshType.FullRect) return;
                
                textureSettings.spriteMeshType = SpriteMeshType.FullRect;
                
                textureImporter.SetTextureSettings(textureSettings);
                textureImporter.SaveAndReimport();
            }
            
            
        }

        private void OnPostprocessTexture(Texture2D texture) {
            var lowerCaseAssetPath = assetPath.ToLower();
            if (lowerCaseAssetPath.IndexOf("/ui/", StringComparison.Ordinal) == -1)
                return;

            texture.alphaIsTransparency = true;
        }
    }
}
