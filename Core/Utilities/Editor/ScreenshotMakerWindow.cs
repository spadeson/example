using System.IO;
using UnityEditor;
using UnityEngine;

namespace CoreMobile.Utilities.Editor {
    public class ScreenshotMakerWindow : EditorWindow {
        [MenuItem("Utilities/Screenshot Maker - Window")]
        public static void initialize() {
            ScreenshotMakerWindow window = (ScreenshotMakerWindow) CreateInstance(typeof(ScreenshotMakerWindow));
            window.Show();
        }

        void OnGUI() {
            if (GUILayout.Button("Make Screenshot", GUILayout.ExpandWidth(true), GUILayout.Height(32f))) {
                var filename = string.Format("screenshot-{0}.png", System.DateTime.Now.ToString("yy-MM-dd-hh-mm-ss"));

                string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) +
                              Path.DirectorySeparatorChar + filename;
                
                ScreenCapture.CaptureScreenshot(path);
            }
        }
    }
}