﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEditor;
using UnityEngine;

namespace Core.Utilities.Editor
{
    public class DateTimeConvertorEditorWindow : UnityEditor.EditorWindow
    {
        private List<string> _timeZonesNames;
        private ReadOnlyCollection<TimeZoneInfo> _timeZones;
    
        private int _selectedTimeZone;
        private TimeZoneInfo _selectedTimeZoneInfo;

        //dd.MM.yyyy - HH:mm:ss
        private string _selectedTimeZoneFormat;
        private string _utcFormat;
    
        private string _selectedTimeZoneFormatted;
        private string _utcFormatted;
    
        private DateTime _selectedDateTime;

        private const long BASE_DATE_TICKS = 621355968000000000;

        [MenuItem("Window/DateTimeConvertor")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<DateTimeConvertorEditorWindow>("DateTimeConvector");
        }

        private void OnEnable()
        {
            _timeZones = TimeZoneInfo.GetSystemTimeZones();
            _timeZonesNames = new List<string>();
        
            foreach (var timezone in _timeZones)
            {
                Debug.Log(timezone);
                _timeZonesNames.Add(timezone.DisplayName);
            }
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Select A Time Zone:", EditorStyles.boldLabel);
            _selectedTimeZone = EditorGUILayout.Popup(_selectedTimeZone, _timeZonesNames.ToArray());
            _selectedTimeZoneInfo = _timeZones[_selectedTimeZone]; 
            EditorGUILayout.LabelField($"Select A Time Zone: {_selectedTimeZoneInfo.DisplayName}");

            EditorGUILayout.Space();

            #region Target Time Zone

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            EditorGUILayout.LabelField("Formatted Target Time Zone", EditorStyles.centeredGreyMiniLabel);
        
            EditorGUILayout.LabelField("ISO 8601 Format:", EditorStyles.boldLabel);
            _selectedTimeZoneFormat = EditorGUILayout.TextField ("", _selectedTimeZoneFormat);
        
            _selectedDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, _selectedTimeZoneInfo);

            _selectedTimeZoneFormatted = _selectedDateTime.ToString(_selectedTimeZoneFormat);
            EditorGUILayout.LabelField(_selectedTimeZoneFormatted);
        
            EditorGUILayout.Space();
        
            EditorGUILayout.LabelField("DotNet Ticks:", EditorStyles.boldLabel);
            EditorGUILayout.LabelField(_selectedDateTime.Ticks.ToString());
        
            EditorGUILayout.Space();
        
            EditorGUILayout.LabelField("Unix Ticks:", EditorStyles.boldLabel);
            EditorGUILayout.LabelField(FromDotNetTicksToUnix(_selectedDateTime.Ticks).ToString());

            EditorGUILayout.EndVertical();
        
            EditorGUILayout.Space();

            #endregion

            #region UTC

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            EditorGUILayout.LabelField("UTC", EditorStyles.centeredGreyMiniLabel);
        
            EditorGUILayout.LabelField("ISO 8601 Format:", EditorStyles.boldLabel);
            _utcFormat = EditorGUILayout.TextField ("", _utcFormat);

            _utcFormatted = DateTime.UtcNow.ToString(_utcFormat);
            EditorGUILayout.LabelField(_utcFormatted);
        
            EditorGUILayout.Space();
        
            EditorGUILayout.LabelField("DotNet Ticks:", EditorStyles.boldLabel);
            EditorGUILayout.LabelField(DateTime.UtcNow.Ticks.ToString());
        
            EditorGUILayout.Space();
        
            EditorGUILayout.LabelField("Unix Ticks:", EditorStyles.boldLabel);
            EditorGUILayout.LabelField(FromDotNetTicksToUnix(DateTime.UtcNow.Ticks).ToString());

            EditorGUILayout.EndVertical();

            #endregion

        }
    
        private long FromDotNetTicksToUnix(long ticks)
        {
            var result = (ticks - BASE_DATE_TICKS) / 10000000;
            return result;
        }
    
    }
}
