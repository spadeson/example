﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace CoreMobile.Utilities.Editor {
    public class TextureGeneratorEditorWindow : EditorWindow {
	    
	    Color _solidTextureColor;
	    Color _gradientTextureColor;
	    
	    int _textureWidth;
	    int _textureHeight;
	    private TextureFormat _textureFormat;
	    private Texture2D _generatedTexture;

	    private string _textureName;
	    private string _textureNamePrefix;
	    private string _textureNameSuffix;
	    private string _textureFullName;

        [MenuItem("Utilities/Textures Generator - Window")]
        public static void initialize() {
	        TextureGeneratorEditorWindow window = (TextureGeneratorEditorWindow) CreateInstance(typeof(TextureGeneratorEditorWindow));
	        window.Show();
        }

        void OnGUI() {
	        
	        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

	        EditorGUILayout.BeginHorizontal();
	        
	        if (_generatedTexture != null) {
		        EditorGUILayout.LabelField(new GUIContent(_generatedTexture), GUILayout.Width(64), GUILayout.Height(64));   
	        }
	        
	        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
	        
	        _solidTextureColor = EditorGUILayout.ColorField("First Color", _solidTextureColor);
	        _gradientTextureColor = EditorGUILayout.ColorField("Second Color ", _gradientTextureColor);
	        
	        _textureWidth  = EditorGUILayout.IntField("Texture Width", _textureWidth);
	        _textureHeight = EditorGUILayout.IntField("Texture Height", _textureHeight);

	        _textureFormat = (TextureFormat)EditorGUILayout.EnumPopup("Texture Format", _textureFormat);
	        
	        EditorGUILayout.EndVertical();
	        
	        EditorGUILayout.EndHorizontal();
	        
	        EditorGUILayout.Space();
	        
	        if (GUILayout.Button("Generate Solid Texture", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
		        _generatedTexture = generatedSolidColorTextue(_textureWidth, _textureHeight, _textureFormat, false, _solidTextureColor);
            }
	        
	        EditorGUILayout.Space();
	        
	        if (GUILayout.Button("Generate Noise Texture", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
		        _generatedTexture = generateNoiseTexture(_textureWidth, _textureHeight, _textureFormat, false);
	        }
	        
	        EditorGUILayout.Space();
	        
	        if (GUILayout.Button("Generate Linear Gradient Texture", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
		        _generatedTexture = generateLinearGradientTexture(_textureWidth, _textureHeight, _textureFormat, false, _solidTextureColor, _gradientTextureColor);
	        }
	        
	        EditorGUILayout.Space();

	        _textureName = EditorGUILayout.TextField("Texture Base Name", _textureName);
	        _textureNamePrefix = EditorGUILayout.TextField("Texture Name Prefix", _textureNamePrefix);
	        _textureNameSuffix = EditorGUILayout.TextField("Texture Name Suffix", _textureNameSuffix);
	        
	        if (GUILayout.Button("Save Generated Texture", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
		        saveGeneratedTexture(_generatedTexture);
	        }

	        EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Generates texture filled with solid color.
        /// </summary>
        /// <param name="textureWidth"></param>
        /// <param name="textureHeight"></param>
        /// <param name="textureFormat"></param>
        /// <param name="useMipMaps"></param>
        /// <param name="textureColor"></param>
        /// <returns></returns>
        Texture2D generatedSolidColorTextue(int textureWidth, int textureHeight, TextureFormat textureFormat, bool useMipMaps = false, Color textureColor = default) {
	        
	        var texture = new Texture2D(textureWidth, textureHeight, textureFormat, useMipMaps);

	        for (var x = 0; x < texture.width; x++) {
		        for (var y = 0; y < texture.height; y++) {
			        texture.SetPixel(x, y, textureColor);
		        }
	        }
	        
	        texture.Apply(useMipMaps);

	        return texture;
        }

        /// <summary>
        /// Generates texture filled with random niose.
        /// </summary>
        /// <param name="textureWidth"></param>
        /// <param name="textureHeight"></param>
        /// <param name="textureFormat"></param>
        /// <param name="useMipMaps"></param>
        /// <returns></returns>
        Texture2D generateNoiseTexture(int textureWidth, int textureHeight, TextureFormat textureFormat, bool useMipMaps = false) {
	        
	        var texture = new Texture2D(textureWidth, textureHeight, textureFormat, useMipMaps);

	        for (var x = 0; x < texture.width; x++) {
		        for (var y = 0; y < texture.height; y++) {
			        texture.SetPixel(
				        x, y,
				        new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f),
				                  Random.Range(0f, 1f)));
		        }
	        }

	        texture.Apply(useMipMaps);

	        return texture;
        }

        /// <summary>
        /// Generates texture filled with linear gradient.
        /// </summary>
        /// <param name="textureWidth"></param>
        /// <param name="textureHeight"></param>
        /// <param name="textureFormat"></param>
        /// <param name="useMipMaps"></param>
        /// <param name="firstColor"></param>
        /// <param name="secondColor"></param>
        /// <returns></returns>
        Texture2D generateLinearGradientTexture(int           textureWidth, int textureHeight,
                                                TextureFormat textureFormat,
                                                bool          useMipMaps  = false,
                                                Color firstColor = default,
                                                Color secondColor = default) {
	        var texture = new Texture2D(textureWidth, textureHeight, textureFormat, useMipMaps);

	        for (var x = 0; x < texture.width; x++) {
		        for (var y = 0; y < texture.height; y++) {
			        var resultColor = Color.Lerp(firstColor, secondColor, y / (float) texture.height);
			        texture.SetPixel(x, y, resultColor);
		        }
	        }

	        texture.Apply(useMipMaps);

	        return texture;
        }

        private void saveGeneratedTexture(Texture2D texture) {
	        if (texture == null) return;
	        var png  = texture.EncodeToPNG();
	        var path = Path.Combine(Application.dataPath, "Textures");
	        _textureFullName = _textureNamePrefix + _textureName + _textureNameSuffix + ".png";
	        path = Path.Combine(path, _textureFullName);
	        File.WriteAllBytes(path, png);
	        AssetDatabase.Refresh();
        }
    }
}