﻿using System;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using TMPro;
using UnityEditor.SceneManagement;
using UnityEngine.UI;

public class UIWidgetsCreatorWindow : EditorWindow {
    private string _baseClassName;
    private string _instructions;

    private const string SPRITES_BASE_PATH = "Assets/UI/CommonUI/Sprites";

    [MenuItem("Utilities/UI Widgets Creator - Window")]
    public static void Initialize() {
        UIWidgetsCreatorWindow window = (UIWidgetsCreatorWindow) CreateInstance(typeof(UIWidgetsCreatorWindow));
        window.Show();
    }

    void OnEnable() {
        Debug.Log("Enabled");

        var stringBuilder = new StringBuilder();
        stringBuilder.Append("How to use\n\n");
        stringBuilder.Append("This utility will help you to create fast and simple assets or (and) scripts.\n");
        stringBuilder.Append("1. Enter name for you widget, example: MyWidget\n");
        stringBuilder.Append("2. Click \"Create UI Widget Assets\" to create assets in Assets/UI/MyWidget folder\n");
        stringBuilder.Append("3. Click \"Create UI Widget Classes\" to create MVC scripts in Assets/Scripts/MyProject/UIMyWidget folder\n");
        stringBuilder.Append("\nHave fun!");

        _instructions = stringBuilder.ToString();
    }

    void OnGUI() {
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("UI Widgets Creator", EditorStyles.boldLabel);

        EditorGUILayout.Space();

        EditorGUILayout.HelpBox(_instructions, MessageType.Info);

        EditorGUILayout.Space();

        _baseClassName = EditorGUILayout.TextField("Base Class Name:", _baseClassName);

        EditorGUILayout.Space();

        if (string.IsNullOrEmpty(_baseClassName)) {
            return;
        }

        if (GUILayout.Button("Create UI Widget Assets", GUILayout.ExpandWidth(true), GUILayout.Height(32f))) {
            var path = Path.Combine(Application.dataPath, "UI");
            CreateUiWidgetAssetStructure(path, _baseClassName);
        }

        if (GUILayout.Button("Create UI Widget Classes", GUILayout.ExpandWidth(true), GUILayout.Height(32f))) {
            var path = Path.Combine(Application.dataPath, "Scripts");
            path = Path.Combine(path, Application.productName);
            path = Path.Combine(path, "UI");

            CreateWidgetDataClass(path, _baseClassName);
            CreateWidgetClass(path, _baseClassName);
            CreateWidgetControllerClass(path, _baseClassName);
        }

        if (GUILayout.Button("Create UI Canvas", GUILayout.ExpandWidth(true), GUILayout.Height(32f))) {
            CreateUICanvas();
        }
    }

    void CreateWidgetDataClass(string path, string className) {
        var pathAndName = Path.Combine(path, className);

        var relativePath = AbsolutePathToRelative(pathAndName);

        if (!AssetDatabase.IsValidFolder(relativePath)) {
            AssetDatabase.CreateFolder(AbsolutePathToRelative(path), _baseClassName);
            AssetDatabase.Refresh();
        }

        var stringBuilder = new StringBuilder();

        stringBuilder.Append("using System;\n");
        stringBuilder.Append("using UnityEngine;\n\n");
        stringBuilder.Append("using VIS.CoreMobile.UISystem;\n\n");
        stringBuilder.Append($"namespace {Application.productName}.UI.{_baseClassName}\n");
        stringBuilder.Append("{\n");
        stringBuilder.Append("\t[Serializable]\n");
        stringBuilder.Append($"\tpublic class {_baseClassName}UIWidgetData : WidgetData\t");
        stringBuilder.Append("{\t}\n");
        stringBuilder.Append("}");

        if (!AssetDatabase.IsValidFolder(pathAndName)) {
            Directory.CreateDirectory(pathAndName);
            AssetDatabase.Refresh();
        }

        File.WriteAllText(Path.Combine(pathAndName, _baseClassName + "UIWidgetData.cs"), stringBuilder.ToString());

        AssetDatabase.Refresh();
    }

    void CreateWidgetClass(string path, string className) {
        var pathAndName = Path.Combine(path, className);

        var relativePath = AbsolutePathToRelative(pathAndName);

        if (!AssetDatabase.IsValidFolder(relativePath)) {
            AssetDatabase.CreateFolder(AbsolutePathToRelative(path), _baseClassName);
            AssetDatabase.Refresh();
        }

        var stringBuilder = new StringBuilder();

        stringBuilder.Append("using System;\n");
        stringBuilder.Append("using UnityEngine;\n");
        stringBuilder.Append("using UnityEngine.UI;\n");
        stringBuilder.Append("using VIS.CoreMobile.UISystem;\n\n");

        stringBuilder.Append($"namespace {Application.productName}.UI.{_baseClassName}\n");
        stringBuilder.Append("{\n");
        stringBuilder.Append($"\tpublic class {_baseClassName}UIWidget : BaseUIWidget\t");
        stringBuilder.Append("{\n\n");

        //Create Create Method
        stringBuilder.Append("\t\tpublic override void Create() {\n");
        stringBuilder.Append("\t\t\tbase.Create();\n");
        stringBuilder.Append("\t\t}\n\n");
        
        //Create Activate Method
        stringBuilder.Append("\t\tpublic override void Activate(bool animated) {\n");
        stringBuilder.Append("\t\t\tbase.Activate(animated);\n");
        stringBuilder.Append("\t\t}\n\n");

        //Create Deactivate Method
        stringBuilder.Append("\t\tpublic override void Deactivate(bool animated) {\n");
        stringBuilder.Append("\t\t\tbase.Deactivate(animated);\n");
        stringBuilder.Append("\t\t}\n\n");

        //Create Dismiss Method
        stringBuilder.Append("\t\tpublic override void Dismiss() {\n");
        stringBuilder.Append("\t\t\tbase.Dismiss();\n");
        stringBuilder.Append("\t\t}\n\n");

        stringBuilder.Append("\t}\n");
        stringBuilder.Append("}");

        File.WriteAllText(Path.Combine(pathAndName, _baseClassName + "UIWidget.cs"), stringBuilder.ToString());

        AssetDatabase.Refresh();
    }

    void CreateWidgetControllerClass(string path, string className) {
        var pathAndName = Path.Combine(path, className);

        var relativePath = AbsolutePathToRelative(pathAndName);

        if (!AssetDatabase.IsValidFolder(relativePath)) {
            AssetDatabase.CreateFolder(AbsolutePathToRelative(path), _baseClassName);
            AssetDatabase.Refresh();
        }

        var stringBuilder = new StringBuilder();
        
        stringBuilder.Append("using VIS.CoreMobile.UISystem;\n\n");

        stringBuilder.Append($"namespace {Application.productName}.UI.{_baseClassName}\n");
        stringBuilder.Append("{\n");
        stringBuilder.Append($"\tpublic class {_baseClassName}UIWidgetController : WidgetControllerWithData<{_baseClassName}UIWidget, {_baseClassName}UIWidgetData>\t");
        stringBuilder.Append("{\n\n");

        //Create Widget Created Method
        stringBuilder.Append("\t\tprotected override void OnWidgetCreated(Widget widget) { }\n\n");
        
        //Create Widget Activated Method
        stringBuilder.Append("\t\tprotected override void OnWidgetActivated(Widget widget) { }\n\n");

        //Create Widget Deactivate Method
        stringBuilder.Append("\t\tprotected override void OnWidgetDeactivated(Widget widget) { }\n\n");

        //Create Widget Dismissed Method
        stringBuilder.Append("\t\tprotected override void OnWidgetDismissed(Widget widget) { }\n");

        stringBuilder.Append("\t}\n");
        stringBuilder.Append("}");

        File.WriteAllText(Path.Combine(pathAndName, _baseClassName + "UIWidgetController.cs"), stringBuilder.ToString());

        AssetDatabase.Refresh();
    }

    void CreateUiWidgetAssetStructure(string path, string widgetName) {
        //Check for main folder, example: Assets/UI/MyWidget
        var pathAndName  = Path.Combine(path, widgetName);
        var relativePath = AbsolutePathToRelative(pathAndName);

        var basePath = Path.Combine(AbsolutePathToRelative(path), _baseClassName);

        if (!AssetDatabase.IsValidFolder(relativePath)) {
            AssetDatabase.CreateFolder(AbsolutePathToRelative(path), _baseClassName);
            AssetDatabase.Refresh();
        }

        //Create Atlas Folder
        var atlasFolderPath = Path.Combine(basePath, "Atlas");
        if (!AssetDatabase.IsValidFolder(atlasFolderPath)) {
            AssetDatabase.CreateFolder(basePath, "Atlas");
            AssetDatabase.Refresh();
        }

        //Create Prefabs Folder
        var prefabsFolderPath = Path.Combine(basePath, "Prefabs");
        if (!AssetDatabase.IsValidFolder(prefabsFolderPath)) {
            AssetDatabase.CreateFolder(basePath, "Prefabs");
            AssetDatabase.Refresh();
        }

        //Create Scenes Folder
        var scenesFolderPath = Path.Combine(basePath, "Scenes");
        if (!AssetDatabase.IsValidFolder(scenesFolderPath)) {
            AssetDatabase.CreateFolder(basePath, "Scenes");
            AssetDatabase.Refresh();
        }

        //Create Scene
        var namePattern  = new Regex(@"(?<=[A-Z])(?=[A-Z][a-z]) | (?<=[^A-Z])(?=[A-Z]) | (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);
        var newSceneName = namePattern.Replace(widgetName, "_");

        var newScene          = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
        var combinedSceneName = Path.Combine(scenesFolderPath, newSceneName.ToLower() + "_ui.unity");
        EditorSceneManager.SaveScene(newScene, combinedSceneName);

        //Create Sprites Folder
        var spritesFolderPath = Path.Combine(basePath, "Sprites");
        if (!AssetDatabase.IsValidFolder(spritesFolderPath)) {
            AssetDatabase.CreateFolder(basePath, "Sprites");
            AssetDatabase.Refresh();
        }
    }

    private void CreateUICanvas() {
        if (FindObjectOfType<Canvas>() != null) {
            DestroyImmediate(FindObjectOfType<Canvas>().gameObject);
        }

        var canvasGo = new GameObject("Canvas", typeof(Canvas), typeof(CanvasScaler), typeof(GraphicRaycaster));
        var canvas   = canvasGo.GetComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;

        var canvasScaler = canvasGo.GetComponent<CanvasScaler>();
        canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(1125, 2436);
        canvasScaler.matchWidthOrHeight = 1f;

        CreateCamera();

        //Create Widget
        var widget = CreateUiElementAdaptive(canvasGo.transform, $"{_baseClassName}UIWidget", Vector2.zero, Vector2.one, Vector2.one * 0.5f);

        //Create Shroud Image
        var shroudImage = CreateUiElementAdaptive(widget, "Shroud-Image", typeof(Image));
        AdjustImage(shroudImage.GetComponent<Image>(), new Color(0, 0, 0, 0.65f), "tile", "",Image.Type.Sliced, true);

        //Create Container
        var containerAnchorMin   = new Vector2(0.1f, 0.15f);
        var containerAnchorMax   = new Vector2(0.9f, 0.85f);
        var containerAnchorPivot = Vector2.one * 0.5f;
        var container            = CreateUiElementAdaptive(widget, "Container", containerAnchorMin, containerAnchorMax, containerAnchorPivot);

        //Create Background Container
        var bgContainerAnchorMin   = Vector2.zero;
        var bgContainerAnchorMax   = Vector2.one;
        var bgContainerAnchorPivot = Vector2.one * 0.5f;
        var bgContainer            = CreateUiElementAdaptive(container, "Background-Container", bgContainerAnchorMin, bgContainerAnchorMax, bgContainerAnchorPivot);
        bgContainer.offsetMax = new Vector2(bgContainer.offsetMax.x, -102);

        //Create Background Image
        var bgImage = CreateUiElementAdaptive(bgContainer, "Background-Image", Vector2.zero, Vector2.one, Vector2.one * 0.5f, typeof(Image));
        AdjustImage(bgImage.GetComponent<Image>(), Color.white, "popup-background", "", Image.Type.Sliced, true);

        //Create Title Container
        var titleContainerAnchorMin        = new Vector2(0.5f, 1f);
        var titleContainerAnchorMax        = new Vector2(0.5f, 1f);
        var titleContainerPivot            = new Vector2(0.5f, 1f);
        var titleContainerSizeDelta        = new Vector2(735, 308);
        var titleContainerAnchoredPosition = new Vector2(-12, 0);
        var titleContainer                 = CreateUiElementFixed(container, "Title-Container", titleContainerSizeDelta, titleContainerAnchoredPosition, titleContainerPivot);
        titleContainer.anchorMin = titleContainerAnchorMin;
        titleContainer.anchorMax = titleContainerAnchorMax;

        //Create Title Image
        var titleImage = CreateUiElementAdaptive(titleContainer, "Title-Blue-Image", Vector2.zero, Vector2.one, Vector2.one * 0.5f, typeof(Image));
        AdjustImage(titleImage.GetComponent<Image>(), Color.white, "blue-title-background");

        //Create Title Text
        var titleText = CreateUiElementAdaptive(titleContainer, "Title-Text", Vector2.zero, Vector2.one, Vector2.one * 0.5f, typeof(TextMeshProUGUI));
        titleText.offsetMin = new Vector2(titleText.offsetMin.x, 32);
        AdjustTextMeshProUGUI(titleText.GetComponent<TextMeshProUGUI>(), 90f, Color.white);

        //Create Close Button Container
        var closeButtonContainerAnchorMin        = new Vector2(1f, 1f);
        var closeButtonContainerAnchorMax        = new Vector2(1f, 1f);
        var closeButtonContainerPivot            = new Vector2(1f, 1f);
        var closeButtonContainerSizeDelta        = new Vector2(120, 120);
        var closeButtonContainerAnchoredPosition = new Vector2(0, -58);
        var closeButtonContainer                 = CreateUiElementFixed(container, "Close-Button-Container", closeButtonContainerSizeDelta, closeButtonContainerAnchoredPosition, closeButtonContainerPivot);
        closeButtonContainer.anchorMin = closeButtonContainerAnchorMin;
        closeButtonContainer.anchorMax = closeButtonContainerAnchorMax;

        //Create Close Button
        var closeButton = CreateUiElementFixed(closeButtonContainer, "Close-Button", Vector2.one * 120, new Vector2(10, 0), Vector2.one * 0.5f, typeof(Image), typeof(Button));
        AdjustImage(closeButton.GetComponent<Image>(), Color.white, "close-button", raycastTarget: true);

        //Create Close Button Shadow
        var closeButtonShadow = CreateUiElementFixed(closeButton, "Close-Button-Shadow-Image", Vector2.one * 163, new Vector2(0, -6), Vector2.one * 0.5f, typeof(Image));
        AdjustImage(closeButtonShadow.GetComponent<Image>(), Color.white, "close-button-shadow");

        //Create Inner Container
        var innerContainerAnchorMin   = new Vector2(0.145f, 0.195f);
        var innerContainerAnchorMax   = new Vector2(0.875f, 0.805f);
        var innerContainerAnchorPivot = Vector2.one * 0.5f;
        var innerContainer            = CreateUiElementAdaptive(container, "Inner-Container", innerContainerAnchorMin, innerContainerAnchorMax, innerContainerAnchorPivot);
    }

    private RectTransform CreateUiElementAdaptive(Transform parent, string elementName, Type component = null) {
        var elementGo            = new GameObject(elementName, typeof(RectTransform), component);
        var elementRectTransform = elementGo.GetComponent<RectTransform>();
        elementRectTransform.SetParent(parent);
        elementRectTransform.anchorMin = Vector2.zero;
        elementRectTransform.anchorMax = Vector2.one;
        elementRectTransform.pivot = Vector2.one * 0.5f;
        elementRectTransform.sizeDelta = Vector2.zero;
        elementRectTransform.anchoredPosition = Vector2.zero;
        return elementRectTransform;
    }

    private RectTransform CreateUiElementAdaptive(Transform parent, string elementName, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Type component = null) {
        var elementGo            = new GameObject(elementName, typeof(RectTransform), component);
        var elementRectTransform = elementGo.GetComponent<RectTransform>();
        elementRectTransform.SetParent(parent);
        elementRectTransform.anchorMin = anchorMin;
        elementRectTransform.anchorMax = anchorMax;
        elementRectTransform.pivot = pivot;
        elementRectTransform.sizeDelta = Vector2.zero;
        elementRectTransform.anchoredPosition = Vector2.zero;
        return elementRectTransform;
    }

    private RectTransform CreateUiElementFixed(Transform parent, string elementName, Vector2 sizeDelta, Vector2 anchoredPosition, Vector2 pivot, Type componentA = null, Type componentB = null) {
        var elementGo            = new GameObject(elementName, typeof(RectTransform), componentA, componentB);
        var elementRectTransform = elementGo.GetComponent<RectTransform>();
        elementRectTransform.SetParent(parent);
        elementRectTransform.anchorMin = Vector2.one * 0.5f;
        elementRectTransform.anchorMax = Vector2.one * 0.5f;
        elementRectTransform.pivot = pivot;
        elementRectTransform.sizeDelta = sizeDelta;
        elementRectTransform.anchoredPosition = anchoredPosition;
        return elementRectTransform;
    }

    private void AdjustImage(Image image, Color color, string spriteName, string subFolder = "", UnityEngine.UI.Image.Type type = Image.Type.Simple, bool raycastTarget = false) {

        var spritePath = SPRITES_BASE_PATH;

        if (!string.IsNullOrEmpty(subFolder)) {
            spritePath = Path.Combine(spritePath, subFolder);
        }
        
        spritePath = Path.Combine(spritePath, spriteName + ".png");

        Debug.Log($"SPTH: {spritePath}");

        image.color = color;
        image.type = type;

        if (!AssetDatabase.IsValidFolder(SPRITES_BASE_PATH)) return;
        var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(spritePath);

        Debug.Log($"sprite: {sprite}");

        image.sprite = sprite;
        image.raycastTarget = raycastTarget;
    }

    private void AdjustTextMeshProUGUI(TextMeshProUGUI text, float fontSize, Color color) {
        text.font = AssetDatabase.LoadAssetAtPath<TMP_FontAsset>("Assets/Fonts/GillSans-UltraBold SDF.asset");
        text.text = "Title";
        text.fontSize = fontSize;
        text.color = color;
        text.alignment = TextAlignmentOptions.Center;
        text.raycastTarget = false;
    }

    private void CreateCamera() {
        if (FindObjectOfType<Camera>() != null) {
            DestroyImmediate(FindObjectOfType<Camera>().gameObject);
        }

        var cameraGo = new GameObject("MainCamera", typeof(Camera));
        cameraGo.tag = "MainCamera";
        cameraGo.transform.position = Vector3.forward * -10f;
        cameraGo.transform.rotation = Quaternion.Euler(Vector3.zero);
        cameraGo.transform.localScale = Vector3.one;

        var cameraComponent = cameraGo.GetComponent<Camera>();
        cameraComponent.orthographic = true;
        cameraComponent.orthographicSize = 5f;
    }

    string AbsolutePathToRelative(string absolutePath) {
        return "Assets" + absolutePath.Substring(Application.dataPath.Length);
    }
}
