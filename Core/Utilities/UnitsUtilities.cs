using UnityEngine;

namespace Core.Utilities {
    public static class UnitsUtilities {

        private const float POUNDS_IN_ONE_KILOGRAM = 2.20462262185f;
        private const float KILOGRAMS_IN_ONE_POUND = 0.45359237f;
        
        /// <summary>
        /// Converts weight in kilograms into pounds.  
        /// </summary>
        /// <param name="kilograms">Weight in kilograms.</param>
        /// <returns>Weight in pounds.</returns>
        public static int KilogramToPounds(int kilograms) {
            return Mathf.RoundToInt(kilograms * POUNDS_IN_ONE_KILOGRAM);
        }
        
        /// <summary>
        /// Converts weight in kilograms into pounds.  
        /// </summary>
        /// <param name="kilograms">Weight in kilograms.</param>
        /// <returns>Weight in pounds.</returns>
        public static float KilogramToPounds(float kilograms) {
            return kilograms * POUNDS_IN_ONE_KILOGRAM;
        }
        
        /// <summary>
        /// Converts weight in kilograms into pounds.  
        /// </summary>
        /// <param name="kilograms">Weight in kilograms.</param>
        /// <returns>Weight in pounds.</returns>
        public static long KilogramToPounds(long kilograms) {
            return (long)(kilograms * POUNDS_IN_ONE_KILOGRAM);
        }
        
        /// <summary>
        /// Converts weight in kilograms into pounds.  
        /// </summary>
        /// <param name="pounds">Weight in pounds.</param>
        /// <returns>Weight in kilograms.</returns>
        public static int PoundsToKilograms(int pounds) {
            return Mathf.RoundToInt(pounds * KILOGRAMS_IN_ONE_POUND);
        }
        
        /// <summary>
        /// Converts weight in kilograms into pounds.  
        /// </summary>
        /// <param name="pounds">Weight in pounds.</param>
        /// <returns>Weight in kilograms.</returns>
        public static float PoundsToKilograms(float pounds) {
            return pounds * KILOGRAMS_IN_ONE_POUND;
        }
        
        /// <summary>
        /// Converts weight in kilograms into pounds.  
        /// </summary>
        /// <param name="pounds">Weight in pounds.</param>
        /// <returns>Weight in kilograms.</returns>
        public static long PoundsToKilograms(long pounds) {
            return (long)(pounds * KILOGRAMS_IN_ONE_POUND);
        }
    }
}

