using System;
using UnityEngine;

namespace Core.Utilities {
    
    public static class TimeUtil {
        
        public static long GetTimestamp() {
            var now = DateTime.UtcNow;
            var zero = new DateTime(1970, 1, 1, 0, 0, 0);
            var diff = now - zero;
            var tst = (long) (diff.TotalMilliseconds);
            return tst;
        }
        
        public static DateTime ConvertUtcToLocal(DateTime date) {
            var delta = DateTime.Now.Hour - DateTime.UtcNow.Hour;
            if (delta < 0) delta += 24;
            return date.AddHours(delta);
        }
        
        public static DateTime ConvertLocalToUtc(DateTime date) {
            var delta = DateTime.Now.Hour - DateTime.UtcNow.Hour;
            if (delta < 0) delta += 24;
            return date.AddHours(-1 * delta);
        }
        
        public static DateTime RoundToNearest(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            var roundUp = delta > d.Ticks / 2;
            var offset = roundUp ? d.Ticks : 0;

            return new DateTime(dt.Ticks + offset - delta, dt.Kind);
        }
        
        public static DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            var modTicks = dt.Ticks % d.Ticks;
            var delta = modTicks != 0 ? d.Ticks - modTicks : 0;
            return new DateTime(dt.Ticks + delta, dt.Kind);
        }

        public static DateTime RoundDown(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            return new DateTime(dt.Ticks - delta, dt.Kind);
        }
    }
}