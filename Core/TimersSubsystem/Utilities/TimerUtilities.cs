﻿using System;
using UnityEngine;

namespace Core.TimersSubsystem.Utilities {
    public static class TimerUtilities {

        /// <summary>
        /// Returns wrapped non-zero year.
        /// </summary>
        /// <param name="year">Current year value.</param>
        /// <returns></returns>
        public static int GetClampedYear(int year) {
            return Mathf.Clamp(year, 1, 9999);
        }
        
        /// <summary>
        /// Returns wrapped non-zero month.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        public static int GetClampedMonth(int month) {
            return Mathf.Clamp(month, 1, 12);
        }

        /// <summary>
        /// Returns days count int the current month of current year.
        /// </summary>
        /// <param name="year">Current year.</param>
        /// <param name="month">Current month.</param>
        /// <returns></returns>
        public static int GetDaysCountInMonth(int year, int month) {
            return DateTime.DaysInMonth(year, month);
        }
    }
}
