using System;
using System.Collections;
using System.Collections.Generic;
using Core.TimersManager.Data;
using Core.TimersSubsystem.Data;
using Core.TimersSubsystem.Managers;
using DG.Tweening;
using UnityEngine;

namespace Core.TimeManager {
    public class TimersManager : MonoBehaviour, ITimersManager {

        public List<TimerDataModel> TimersList { get; set; }
        public DateTime CurrentDateTime => DateTime.Now;
        public event Action<TimerDataModel> OnTimerCreated;
        public event Action<TimerDataModel> OnTimerPaused;
        public event Action<TimerDataModel> OnTimerResumed;
        public event Action<TimerDataModel> OnTimerEvaluated;
        public event Action<TimerDataModel> OnTimerCompleted;
        
        private Tween _timersEvaluationCoroutine;

        #region Public Methods
        
        /// <inheritdoc />
        public void Initialize() {
            TimersList                 = new List<TimerDataModel>();
            _timersEvaluationCoroutine = DOVirtual.DelayedCall(1, EvalueteTimer);
        }

        /// <inheritdoc />
        public TimerDataModel CreateTimer(long dueDate, bool startImmediately = true) {
            var newTimer = new TimerDataModel(dueDate);
            if (_timersEvaluationCoroutine == null)
            {
                _timersEvaluationCoroutine = DOVirtual.DelayedCall(1, EvalueteTimer);
                //_timersEvaluationCoroutine = StartCoroutine(EvalueteTimer());
            }
            TimersList.Add(newTimer);
            if (startImmediately) {
                StartTimer(newTimer);
                newTimer.timerState = TimerState.ACTIVE;
            } else {
                newTimer.timerState = TimerState.PENDING;
            }
            //OnTimerCreated?.Invoke(newTimer);
            return newTimer;
        }

        /// <inheritdoc />
        public void StartTimer(TimerDataModel timerDataModel) {
            var timer = TimersList.Find(t => t.timerId == timerDataModel.timerId);
            timer.timerState = TimerState.ACTIVE;
        }
        
        /// <inheritdoc />
        public void PauseTimer(TimerDataModel timerDataModel) {
            var timer = TimersList.Find(t => t.timerId == timerDataModel.timerId);
            if (timer == null) return;
            timer.timerState = TimerState.PENDING;
            timer.PauseTimer();

            OnTimerPaused?.Invoke(timer);
        }
        
        /// <inheritdoc />
        public void ResumeTimer(TimerDataModel timerDataModel) {
            var timer = TimersList.Find(t => t.timerId == timerDataModel.timerId);
            if (timer == null) return;
            timer.timerState = TimerState.ACTIVE;
            timer.ResumeTimer();
            OnTimerResumed?.Invoke(timer);
        }
        
        /// <inheritdoc />
        public void CompleteTimer(TimerDataModel timerDataModel) {
            var timer = TimersList.Find(t => t.timerId == timerDataModel.timerId);
            timer.timerState = TimerState.COMPLETED;
          
            OnTimerCompleted?.Invoke(timer);
        }

        #endregion

        #region Private Methods

        void EvalueteTimer()
        {
            if (TimersList.Count > 0) _timersEvaluationCoroutine = DOVirtual.DelayedCall(1, EvalueteTimer);
            //while (TimersList.Count > 0) {
                for (var i = 0; i < TimersList.Count; i++) {
                    var timer = TimersList[i];
                    timer.UpdateProgress();
                    if (timer.timerState == TimerState.ACTIVE && CurrentDateTime.Ticks >= timer.dueTimeStamp) {
                                               
                        OnTimerEvaluated?.Invoke(timer);
                        timer.secondsPassed++;
                        timer.UpdateProgress();
                        CompleteTimer(timer);
                        TimersList.Remove(timer);
                    } else {
                        if (timer.timerState == TimerState.PENDING || timer.timerState == TimerState.COMPLETED)
                            continue;
                        timer.secondsPassed++;
                        timer.UpdateProgress();
                        OnTimerEvaluated?.Invoke(timer);
                    }
                }
               // yield return new WaitForSeconds(0.95f);
            //}
        }

        #endregion
    }
}
