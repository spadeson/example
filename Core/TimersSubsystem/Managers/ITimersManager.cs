﻿using System;
using System.Collections.Generic;
using Core.TimersManager.Data;

namespace Core.TimersSubsystem.Managers {
    public interface ITimersManager {
        List<TimerDataModel> TimersList { get; }
        DateTime CurrentDateTime { get; }
        event Action<TimerDataModel> OnTimerCreated;
        event Action<TimerDataModel> OnTimerPaused;
        event Action<TimerDataModel> OnTimerResumed;
        event Action<TimerDataModel> OnTimerEvaluated;
        event Action<TimerDataModel> OnTimerCompleted;

        /// <summary>
        /// Initialize TimersManager. 
        /// </summary>
        void Initialize();
        
        /// <summary>
        /// Creates new instance of timer.
        /// </summary>
        /// <param name="dueDate">Ticks representation of the due date.</param>
        /// <param name="startImmediately">Should timer be started just after creation.</param>
        /// <returns></returns>
        TimerDataModel CreateTimer(long dueDate, bool startImmediately = true);
        
        /// <summary>
        ///Starts the timer. 
        /// </summary>
        /// <param name="timerDataModel">TimerDataModel of timer that should be started.</param>
        void StartTimer(TimerDataModel timerDataModel);
        
        /// <summary>
        /// Pauses the timer.
        /// </summary>
        /// <param name="timerDataModel">TimerDataModel of timer that should be paused.</param>
        void PauseTimer(TimerDataModel timerDataModel);
        
        /// <summary>
        /// Resumes the timer.
        /// </summary>
        /// <param name="timerDataModel">TimerDataModel of timer that should be resumed.</param>
        void ResumeTimer(TimerDataModel timerDataModel);
        
        /// <summary>
        /// Completes the timer.
        /// </summary>
        /// <param name="timerDataModel">TimerDataModel of timer that should be completed.</param>
        void CompleteTimer(TimerDataModel timerDataModel);
    }
}
