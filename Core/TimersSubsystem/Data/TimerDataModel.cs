﻿using System;
using Core.TimeManager;
using Core.TimersSubsystem.Data;
using Facebook.Unity;

namespace Core.TimersManager.Data {
    [Serializable]
    public class TimerDataModel {
        public string timerId;
        public long dueTimeStamp;
        public int secondsToDueDate;
        public int secondsPassed;
        public float duration;

        public float progress ;

        public TimerState timerState;

        private int _timeDeltaInSeconds;
        private DateTime _lastSuspend;
        private DateTime _startTime;
        private DateTime _endTime;
        

        public TimerDataModel(long dueDate) {
            timerId           = Guid.NewGuid().ToString();
            dueTimeStamp      = dueDate;
            secondsToDueDate  = new DateTime(dueTimeStamp).Subtract(DateTime.Now).Seconds+1;
            timerState        = TimerState.UNKNOWN;
            progress = 0;
            _startTime = DateTime.Now;
            _endTime = new DateTime(dueDate);
            
        }

        public void PauseTimer() {
            _timeDeltaInSeconds = secondsToDueDate - secondsPassed;
            _lastSuspend = DateTime.Now;
        }

        public void ResumeTimer() {
            var timePassed = DateTime.Now.Subtract(_lastSuspend).Seconds;
            dueTimeStamp = new DateTime(dueTimeStamp).AddSeconds(timePassed).Ticks;
            _endTime = new DateTime(dueTimeStamp);
        }

        public void UpdateProgress() {
            if (_startTime == null) 
                _startTime = DateTime.Now;
            
            var d = _endTime.Subtract(_startTime).Ticks ;
            duration = (float) TimeSpan.FromTicks(d).TotalMilliseconds;

            var passed = TimeSpan.FromTicks(DateTime.Now.Subtract(_startTime).Ticks).TotalMilliseconds;
            secondsPassed = (int)passed/1000;
            progress = (float) (passed / duration);
            if (progress > 1) progress = 1;
            else if (progress < 0) progress = 0;

        }
    }
}