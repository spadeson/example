namespace Core.TimersSubsystem.Data {
    public enum TimerState {
        UNKNOWN = -1,
        ACTIVE = 0,
        PENDING = 1,
        COMPLETED = 2
    }
}