﻿using System;
using Core.TimeManager;
using Core.TimersSubsystem.Data;
using Core.TimersSubsystem.Utilities;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TimersManager))]
public class TimerManagerEditor : Editor {
    #region Privat Variables
    
    private int _year;
    private int _month;
    private int _day;
    private int _hours;
    private int _minutes;
    private int _seconds;

    #endregion

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        var timerManager = (TimersManager) target;

        EditorGUILayout.Space();

        #region Pending Timers

        EditorGUILayout.LabelField("Pending Timers", EditorStyles.boldLabel);

        var pendingTimers = timerManager.TimersList.FindAll(t => t.timerState == TimerState.PENDING);

        for (var i = 0; i < pendingTimers.Count; i++) {
            var timer = pendingTimers[i];

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(timer.timerId);
            EditorGUILayout.LabelField(new DateTime(timer.dueTimeStamp).ToString(), GUILayout.Width(140));

            if (GUILayout.Button("Resume Timer", GUILayout.Width(96), GUILayout.Height(24))) {
                timerManager.ResumeTimer(timer);
            }

            EditorGUILayout.EndHorizontal();
        }

        #endregion

        EditorGUILayout.Space();

        #region Active Timers

        EditorGUILayout.LabelField("Active Timers", EditorStyles.boldLabel);

        var activeTimers = timerManager.TimersList.FindAll(t => t.timerState == TimerState.ACTIVE);

        for (var i = 0; i < activeTimers.Count; i++) {
            var timer = activeTimers[i];

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(timer.timerId);
            EditorGUILayout.LabelField(new DateTime(timer.dueTimeStamp).ToString(), GUILayout.Width(140));

            if (GUILayout.Button("Pause Timer", GUILayout.Width(76), GUILayout.Height(24))) {
                timerManager.PauseTimer(timer);
            }

            if (GUILayout.Button("Complete Timer", GUILayout.Width(96), GUILayout.Height(24))) {
                timerManager.CompleteTimer(timer);
            }

            EditorGUILayout.EndHorizontal();
        }

        #endregion

        EditorGUILayout.Space();

        #region Completed Timers

        EditorGUILayout.LabelField("Completed Timers", EditorStyles.boldLabel);

        var completedTimers = timerManager.TimersList.FindAll(t => t.timerState == TimerState.COMPLETED);

        for (var i = 0; i < completedTimers.Count; i++) {
            var timer = completedTimers[i];
            EditorGUILayout.LabelField(timer.timerId);
        }

        #endregion

        #region Timer Creation

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Create New Timer", EditorStyles.boldLabel);

        _year    = EditorGUILayout.IntField("Year", TimerUtilities.GetClampedYear(_year));
        _month   = EditorGUILayout.IntSlider("Month", TimerUtilities.GetClampedMonth(_month), 1, 12);
        _day     = EditorGUILayout.IntSlider("Day", _day, 1, TimerUtilities.GetDaysCountInMonth(_year, _month));
        _hours   = EditorGUILayout.IntSlider("Hour/s", _hours, 1, 24);
        _minutes = EditorGUILayout.IntSlider("Minute/s", _minutes, 0, 59);
        _seconds = EditorGUILayout.IntSlider("Seconds/s", _seconds, 0, 59);

        if (GUILayout.Button("Create Timer", GUILayout.ExpandWidth(true), GUILayout.Height(32), GUILayout.Width(128))) {
            var dueDate = new DateTime(_year, _month, _day, _hours, _minutes, _seconds).Ticks;
            timerManager.CreateTimer(dueDate);
        }

        #endregion
    }
}