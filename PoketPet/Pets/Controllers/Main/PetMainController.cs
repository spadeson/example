﻿using System.Collections.Generic;
using PoketPet.Pets.Controllers.AI;
using PoketPet.Pets.Controllers.AnimatorController;
using PoketPet.Pets.Controllers.Movement;
using PoketPet.Pets.Controllers.Poop;
using PoketPet.Pets.Controllers.States;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;

namespace PoketPet.Pets.Controllers.Main {
    public class PetMainController : MonoBehaviour, IPetMainController {
        
        public IPetAIController petAiController;
        public IPetMovementController petMovementController;
        public IPetAnimatorController petAnimatorController;
        public IPetPoopController poopController;

        [Header("Pet Rigidbody")]
        public Rigidbody rigidbodyComponent;

        [Header("Pet Collider")]
        public CapsuleCollider petCollider;

        [Header("Jaw Bone")]
        [SerializeField] private Transform jawBone;

        private IPetState _currentPetState;

        public Transform Transform { get; private set; }
        public GameObject GameObject { get; private set; }
        public PetStates CurrentPetState { get; set; }

        public void Initialize(PetConfig petConfigModel) {
            Debug.Log("PetController.Initialize");

            gameObject.tag = "Pet";

            Transform = transform;
            GameObject = gameObject;

            if (jawBone == null) jawBone = GameObject.Find("Fbdog_jaw").transform;

            //Add Animator Controller
            petAnimatorController = gameObject.AddComponent<PetAnimatorController>();
            petAnimatorController.Initialize(petConfigModel.petAnimationStatesLibrary);
            petAnimatorController.OnCompletedAllAnimationsForState += () => {
                petAiController.SetNewRandomState();
            };

            //Add AI controller
            if (petConfigModel.createAIController) {
                petAiController = gameObject.AddComponent<PetAIController>();

                petAiController.Initialize(this);

                petAiController.OnPetStateChanged += petState => {
                    
                    petAnimatorController.SetNewPetState(petState);
                    
                    if (petState == PetStates.MOVING) {
                        MovePetToRandomPoint();
                    }
                };

                petAiController.SetNewPetState(PetStates.STAND);
            }

            CreateCollider(petConfigModel, petConfigModel.createCollider);

            if (!petConfigModel.createNavMesh) return;

            //Add Movement Controller
            petMovementController = gameObject.AddComponent<PetMovementController>();
            petMovementController.Initialize(this, petConfigModel);

            petMovementController.OnVelocityUpdated += delegate(Vector3 velocity) {
                //Debug.Log($"Velocity.magnitude: {velocity.magnitude}");
                petAnimatorController.SetFloat("Speed", velocity.magnitude);
            };

            petMovementController.OnPetWalking += delegate(Vector3 velocity, float speed) {
                //Debug.Log($"Velocity.magnitude: {velocity.magnitude}");
                petAnimatorController.ResetAllLayers(0);
                petAnimatorController.SetFloat("Speed", velocity.magnitude);
                petAnimatorController.SetLayerWeight(1, 1f);
                petAnimatorController.SetLayerWeight(3, 0f);
            };

            petMovementController.OnPetRunning += delegate(Vector3 velocity, float speed) {
                //Debug.Log($"Velocity.magnitude: {velocity.magnitude}");
                petAnimatorController.ResetAllLayers(0);
                petAnimatorController.SetFloat("Speed", velocity.magnitude);
                petAnimatorController.SetLayerWeight(1, 0f);
                petAnimatorController.SetLayerWeight(3, 1f);
            };

            petMovementController.OnCompletedMovement += () => {
                petAiController.SetNewRandomState();
            };

            petMovementController.OnPetIdleCompleted += OnPetIdleCompetedHandler;
        }

        public float GetCurrentAnimationClipLength() {
            return petAnimatorController.CurrentAnimationClipLength;
        }

        public Vector3 GetPetPosition() {
            return transform.position;
        }

        public void SetMovementWaypoints(List<Transform> waypointsList) {
            petMovementController.SetWaypoints(waypointsList);
        }

        public void TakeAssetInMouth(GameObject pickableAsset, Vector3 offset, bool resetRotation = false, float scaleFactor = 1f) {
            pickableAsset.transform.SetParent(jawBone, false);

            pickableAsset.transform.localPosition = offset;
            pickableAsset.transform.localRotation = Quaternion.Euler(Vector3.zero);

            if (resetRotation) {
                pickableAsset.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }

            pickableAsset.transform.localScale = Vector3.one * scaleFactor;
        }

        public void SetNewState(PetStates petState) {
            petAiController.SetNewPetState(petState);
        }

        private void CreateCollider(PetConfig petConfigModel, bool createCollider) {
            if (!createCollider) return;
            //Create Rigidbody
            rigidbodyComponent = gameObject.AddComponent<Rigidbody>();
            rigidbodyComponent.isKinematic = true;

            //Create CapsuleCollider
            petCollider = gameObject.AddComponent<CapsuleCollider>();
            petCollider.direction = petConfigModel.colliderDirection;
            petCollider.center = Vector3.up * 0.15f;
            petCollider.radius = petConfigModel.colliderRadius;
            petCollider.height = petConfigModel.height;
        }

        private void OnPetIdleCompetedHandler() {
            petAnimatorController.ResetAllLayers(0f);
        }

        private void MovePetToRandomPoint() {
            var nextTarget = petMovementController.GetNextTarget();
            petMovementController.MoveCharacterToTarget(nextTarget);
        }
    }
}