using System.Collections.Generic;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;

namespace PoketPet.Pets.Controllers.Main {
    public interface IPetMainController {
        Transform Transform { get; }
        GameObject GameObject { get; }
        PetStates CurrentPetState { get; set; }
        
        void Initialize(PetConfig petConfigModel);
        void SetMovementWaypoints(List<Transform> waypointsList);
        void TakeAssetInMouth(GameObject pickableAsset, Vector3 offset, bool resetRotation = false, float scaleFactor = 1f);
        void SetNewState(PetStates petState);
    }
}