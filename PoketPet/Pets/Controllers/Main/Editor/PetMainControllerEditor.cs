using UnityEditor;
using UnityEngine;

namespace PoketPet.Pets.Controllers.Main.Editor {
    [CustomEditor(typeof(PetMainController))]
    public class PetMainControllerEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var petController = (PetMainController) target;

            if (GUILayout.Button("Poop", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petController.petAnimatorController.SetTrigger("Poo");
                petController.poopController.Poop(petController.GetPetPosition(), 3f, 1f);
            }
        }
    }
}