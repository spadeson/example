using System.Collections.Generic;
using PoketPet.Pets.Controllers.Movement;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;

namespace PoketPet.Pets.Controllers.Main {
    public class PetMainControllerTutorial : MonoBehaviour, IPetMainController {
        
        private IPetMovementController _petMovementController;
        
        private PetConfig _petConfigModel;

        public Transform Transform { get; private set; }
        public GameObject GameObject { get; private set; }
        public PetStates CurrentPetState { get; set; }

        [Header("Jaw Bone")]
        [SerializeField] private Transform jawBone;

        public void Initialize(PetConfig petConfigModel) {
            _petConfigModel = petConfigModel;
            Transform = transform;
            GameObject = gameObject;
            
            if (jawBone == null) jawBone = GameObject.Find("Fbdog_jaw").transform;
            
            if (!petConfigModel.createNavMesh) return;
            
            //Add Movement Controller
            _petMovementController = gameObject.AddComponent<PetMovementController>();
            _petMovementController.Initialize(this, petConfigModel);
        }

        public void SetMovementWaypoints(List<Transform> waypointsList) {
            _petMovementController.SetWaypoints(waypointsList);
        }

        public void TakeAssetInMouth(GameObject pickableAsset, Vector3 offset, bool resetRotation = false, float scaleFactor = 1) {
            pickableAsset.transform.SetParent(jawBone, false);
            
            pickableAsset.transform.localPosition = offset;
            pickableAsset.transform.localRotation = Quaternion.Euler(Vector3.zero);

            if (resetRotation) {
                pickableAsset.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }

            pickableAsset.transform.localScale = Vector3.one * scaleFactor;
        }
        
        public void SetNewState(PetStates petState) {
            //petAiController.SetNewPetState(petState);
        }
    }
}
