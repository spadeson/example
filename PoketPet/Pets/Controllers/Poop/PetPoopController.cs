using System.Collections;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Pets.Controllers.Poop {
    public class PetPoopController : MonoBehaviour, IPetPoopController {
        public void Poop(Vector3 petPosition, float duration, float delay) {
            var offset = petPosition + Vector3.back * 0.165f + Vector3.up * 0.1f;
            StartCoroutine(CreatePooCo(offset, delay));
        }

        IEnumerator CreatePooCo(Vector3 position, float delay) {
            yield return new WaitForSeconds(delay);
            CoreMobile.Instance.VFXManager.playVFX("POOP_VFX", position);
        }
    }
}
