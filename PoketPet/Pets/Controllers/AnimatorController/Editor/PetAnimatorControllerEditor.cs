using UnityEditor;
using UnityEngine;

namespace PoketPet.Pets.Controllers.AnimatorController.Editor {
    [CustomEditor(typeof(PetAnimatorController))]
    public class PetAnimatorControllerEditor : UnityEditor.Editor {

        private int _layerIndex;
        private float _layerWeight;
        
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            
            var petAnimatorController = (PetAnimatorController) target;
            EditorGUILayout.Space(32);
            
            EditorGUILayout.LabelField("Debug Info:", EditorStyles.boldLabel);
            
            EditorGUILayout.LabelField($"Current State Name: {petAnimatorController.CurrentPetState}");
            
            EditorGUILayout.LabelField($"State Cycles Count: {petAnimatorController.StateCyclesCount}");
            
            EditorGUILayout.Space(8);

            _layerIndex = EditorGUILayout.IntField("Layer Index:", _layerIndex);
            _layerWeight = EditorGUILayout.Slider("Later Weight: ", _layerWeight, 0f, 1f);

            if (GUILayout.Button("Set Layer Weight", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petAnimatorController.SetFloat("Speed", 1f);
                petAnimatorController.SetLayerWeight(_layerIndex, _layerWeight);
            }
            
            EditorGUILayout.Space(8);
            
            EditorGUILayout.LabelField($"Animation Length: {petAnimatorController.CurrentAnimationClipLength}");
            EditorGUILayout.LabelField($"Animation Name: {petAnimatorController.CurrentAnimationClipName}");
        }
    }
}
