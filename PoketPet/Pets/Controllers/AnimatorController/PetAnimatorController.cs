﻿using System;
using System.Collections;
using System.Linq;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetAnimationConfigs;
using UnityEngine;
using Random = System.Random;

namespace PoketPet.Pets.Controllers.AnimatorController {
    [RequireComponent(typeof(Animator))]
    public class PetAnimatorController : MonoBehaviour, IPetAnimatorController {
        
        public event Action OnCompletedAllAnimationsForState;
        
        [SerializeField] private Animator animator;

        [Header("Pet Animation States Library")]
        [SerializeField] private PetAnimationStatesLibrary petAnimationStatesLibrary;

        public int StateCyclesCount { get; private set; }

        private Random _prng = new Random();
        
        public float CurrentAnimationClipLength { get; set; }
        public string CurrentAnimationClipName { get; set; }
        
        private Coroutine _penAnimationCoroutine;

        public PetStates CurrentPetState { get; private set; }
        
        private PetAnimationStateConfig _currentPetStateAnimationConfig;

        private const int PET_STATE_REPEAT_MIN = 1;
        private const int PET_STATE_REPEAT_MAX = 2;

        public void Initialize(PetAnimationStatesLibrary animationStatesLibrary) {
            
            if (animator == null) {
                animator = GetComponent<Animator>();
            }
            
            petAnimationStatesLibrary = animationStatesLibrary;
        }

        public void SetNewPetState(PetStates petState) {
            
            CurrentPetState = petState;

            if (CurrentPetState == PetStates.MOVING) return;
            
            StateCyclesCount = _prng.Next(PET_STATE_REPEAT_MIN, PET_STATE_REPEAT_MAX);
            
            SelectAnimationsForNewState(CurrentPetState);
        }

        public void SetAnimatorSpeed(float speed) {
            animator.speed = speed;
        }

        public void SetTrigger(string triggerName) {
            animator.SetTrigger(triggerName);
        }

        public void SetLayerAndTrigger(string layerName, string triggerName) {
            
        }

        public void SetBool(string parameterName, bool value) {
            animator.SetBool(parameterName, value);
        }

        public void SetInt(string parameterName, int value) {
            animator.SetInteger(parameterName, value);
        }

        public void SetFloat(string parameterName, float value) {
            animator.SetFloat(parameterName, value);
        }

        public void RebindAnimator() {
            animator.Rebind();
        }

        public void SetLayerWeight(string layerName, float layerWeight) {
            animator.SetLayerWeight(animator.GetLayerIndex(layerName), layerWeight);
        }

        public void SetLayerWeight(int layerIndex, float layerWeight) {
            animator.SetLayerWeight(layerIndex, layerWeight);
        }

        public void ResetAllLayers(float blendTime) {
            if (blendTime > 0) {
                StartCoroutine(BlendingLayersWeightCoroutine(blendTime));
            } else {
                for (var layerIndex = 1; layerIndex < animator.layerCount; layerIndex++) {
                    var layerWeight = animator.GetLayerWeight(layerIndex);
                    if(layerWeight == 0) continue;
                    animator.SetLayerWeight(layerIndex, 0);
                }
            }
        }

        private IEnumerator BlendingLayersWeightCoroutine(float blendingTime) {
            
            var blendingProgress = blendingTime;
            
            while (blendingProgress > 0) {
                for (var layerIndex = 1; layerIndex < animator.layerCount; layerIndex++) {

                    if(layerIndex == 1 || layerIndex == 3) continue;
                    
                    var layerWeight = animator.GetLayerWeight(layerIndex);
                    
                    if(layerWeight == 0) continue;
                    
                    animator.SetLayerWeight(layerIndex, blendingProgress / blendingTime);
                }
                
                blendingProgress -= Time.deltaTime;
                
                yield return null;
            }
        }

        private void SelectAnimationsForNewState(PetStates petState) {
            
            //Stop Coroutine for choosing new animation clips 
            if (_penAnimationCoroutine != null) {
                StopCoroutine(_penAnimationCoroutine);
                _penAnimationCoroutine = null;
            }
            
            //Get animation state config by the current pet's state
            _currentPetStateAnimationConfig = petAnimationStatesLibrary.GetPetAnimationConfigByType(petState);

            //Reset Animator layers
            ResetAllLayers(0f);
            
            //Get layer index from the animation config
            var layerIndexFromName = animator.GetLayerIndex(_currentPetStateAnimationConfig.animationLayerName);
            
            //Set Weight for that specific layer
            SetLayerWeight(layerIndexFromName, 1f);

            //Get Animation clip length
            var clipLength = animator.GetCurrentAnimatorStateInfo(layerIndexFromName).length;
            CurrentAnimationClipLength = clipLength;

            //Get Animation clip name
            var clipName = animator.GetCurrentAnimatorClipInfo(layerIndexFromName)[0].clip.name;
            CurrentAnimationClipName = clipName;

            //Start Coroutine for select new animation clip
            _penAnimationCoroutine = StartCoroutine(SelectNextRandomAnimation_Co());
        }

        private IEnumerator SelectNextRandomAnimation_Co() {

            while (StateCyclesCount > 0) {
                yield return new WaitForSeconds(CurrentAnimationClipLength);
                StateCyclesCount--;
                SelectNextAnimationClip();
            }

            OnCompletedAllAnimationsForState?.Invoke();
        }
        
        private void SelectNextAnimationClip() {
            var probability = _prng.Next(1, 100);
            var triggers    = _currentPetStateAnimationConfig.animationClipTriggers.Where(a => a.animationClipProbability > probability).ToList();
            var randomClip  = triggers[_prng.Next(0, triggers.Count)];
            SetTrigger(randomClip.animationClipTrigger);
        }
    }
}