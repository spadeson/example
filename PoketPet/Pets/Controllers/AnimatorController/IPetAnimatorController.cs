using System;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetAnimationConfigs;

namespace PoketPet.Pets.Controllers.AnimatorController {
    public interface IPetAnimatorController {
        float CurrentAnimationClipLength { get; }
        string CurrentAnimationClipName { get; }
        
        event Action OnCompletedAllAnimationsForState;

        void Initialize(PetAnimationStatesLibrary animationStatesLibrary);
        void SetNewPetState(PetStates petState);
        void SetAnimatorSpeed(float speed);
        void SetTrigger(string triggerName);
        void SetLayerAndTrigger(string layerName, string triggerName);
        void SetBool(string parameterName, bool value);
        void SetInt(string parameterName, int value);
        void SetFloat(string parameterName, float value);
        void RebindAnimator();
        void SetLayerWeight(string layerName, float layerWeight);
        void SetLayerWeight(int layerIndex, float layerWeight);
        void ResetAllLayers(float blendTime);
    }
}
