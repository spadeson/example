using PoketPet.Pets.Data;
using UnityEditor;
using UnityEngine;

namespace PoketPet.Pets.Controllers.AI.Editor {
    [CustomEditor(typeof(PetAIController))]
    public class PetAIControllerEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var petAiController = (PetAIController) target;

            EditorGUILayout.Space(32);

            EditorGUILayout.LabelField($"Current Pet State: {petAiController.CurrentPetState}");
            
            EditorGUILayout.Space(16);

            if (GUILayout.Button($"Set {PetStates.STAND} state", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petAiController.SetNewPetState(PetStates.STAND);
            }
            
            if (GUILayout.Button($"Set {PetStates.SIT} state", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petAiController.SetNewPetState(PetStates.SIT);
            }
            
            if (GUILayout.Button($"Set {PetStates.LAY} state", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petAiController.SetNewPetState(PetStates.LAY);
            }
            
            if (GUILayout.Button($"Set {PetStates.MOVING} state", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petAiController.SetNewPetState(PetStates.MOVING);
            }
        }
    }
}