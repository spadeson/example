using System;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetAnimationConfigs;

namespace PoketPet.Pets.Controllers.AI {
    public interface IPetAIController {
        event Action<PetStates> OnPetStateChanged;
        PetStates CurrentPetState { get; set; }
        void        Initialize(PetMainController petMainController);
        void        SetNewPetState(PetStates petState);
        void SetNewRandomState();
    }
}
