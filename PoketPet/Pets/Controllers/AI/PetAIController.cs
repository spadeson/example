using System;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.Data;
using UnityEngine;
using Random = System.Random; 

namespace PoketPet.Pets.Controllers.AI {
    public class PetAIController : MonoBehaviour, IPetAIController {
        public event Action<PetStates> OnPetStateChanged;
        public PetStates CurrentPetState { get; set; }

        private PetMainController _petMainController;
        
        private Random _prng = new Random();

        public void Initialize(PetMainController petMainController) {
            _petMainController = petMainController;
        }

        public void SetNewPetState(PetStates petState) {
            //Set new pet state as current
            CurrentPetState = petState;
            
            //Invoke event to notify subscribers
            OnPetStateChanged?.Invoke(CurrentPetState);
        }

        public void SetNewRandomState() {
            var petStates = Enum.GetValues(typeof(PetStates));
            var percent   = _prng.Next(0, 100);
            PetStates randomPetState;
            if (percent < 60) {
                randomPetState = (PetStates) petStates.GetValue(_prng.Next(petStates.Length));
            } else {
                randomPetState = PetStates.MOVING;
            }
            
            SetNewPetState(randomPetState);
        }
    }
}
