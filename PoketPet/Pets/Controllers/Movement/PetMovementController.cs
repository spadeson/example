﻿using System;
using System.Collections;
using System.Collections.Generic;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace PoketPet.Pets.Controllers.Movement
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class PetMovementController : MonoBehaviour, IPetMovementController {
        
        public event Action<Vector3, float> OnPetWalking;
        public event Action<Vector3, float> OnPetRunning;
        public event Action                 OnPetStopped;
        public event Action                 OnCompletedMovement;
        public event Action<Vector3>        OnVelocityUpdated;
        public event Action                 OnPetIdleCompleted;

        private const float WALKING_SPEED = 1f;
        private const float RUNNING_SPEED = 2f;
        private const float RUNNING_THRESHOLD = 6f;
        
        public bool InPatrolling { get; set; }

        [SerializeField] private NavMeshAgent navMeshAgent;
        [SerializeField] private List<Transform> waypointsList;
        [SerializeField] private float waypointDistanceThreshold = 1.5f;

        private bool _isInitialized;
        private float _speed;

        private IPetMainController _petMainController;

        void Update() {

            if (!_isInitialized) return;
            
            if (waypointsList == null || waypointsList.Count == 0 || _petMainController.CurrentPetState != PetStates.MOVING) return;
            
            var remainingDistance = navMeshAgent.remainingDistance;
            
            if (!float.IsPositiveInfinity(remainingDistance) && navMeshAgent.pathStatus == NavMeshPathStatus.PathComplete && Math.Abs(remainingDistance) < navMeshAgent.stoppingDistance) {
                if (!InPatrolling) {
                    //Stop character
                    StopCharacter();
                    OnCompletedMovement?.Invoke();
                } else {
                    // Choose the next destination point
                    MoveCharacterToTarget(GetNextTarget(), true);
                }
            }

            if (_petMainController.CurrentPetState == PetStates.MOVING) {
                OnVelocityUpdated?.Invoke(navMeshAgent.desiredVelocity);
            }
        }

        public void Initialize(IPetMainController petMainController, PetConfig petConfigModel) {

            _petMainController = petMainController;
            
            InPatrolling = false;

            navMeshAgent = GetComponent<NavMeshAgent>();

            navMeshAgent.baseOffset = petConfigModel.baseOffset;
            navMeshAgent.speed = petConfigModel.speed;
            navMeshAgent.angularSpeed = petConfigModel.angularSpeed;
            navMeshAgent.stoppingDistance = petConfigModel.stoppingDistance;
            navMeshAgent.autoBraking = petConfigModel.autoBraking;

            navMeshAgent.radius = petConfigModel.radius;
            navMeshAgent.height = petConfigModel.height;

            _isInitialized = true;
        }

        public void MoveCharacterToTarget(Vector3 targetPosition, bool continuePatrolling = false) {

            InPatrolling = continuePatrolling;
            
            _petMainController.CurrentPetState = PetStates.MOVING;

            var distance = GetDistanceToTarget(targetPosition);

            if (distance <= RUNNING_THRESHOLD) {
                WalkCharacter(targetPosition);
            } else {
                RunCharacter(targetPosition);
            }
        }

        public void BeginPatrolling() {
            InPatrolling = true;
            MoveCharacterToTarget(GetNextTarget(), true);
        }

        public void StopCharacter() {
            InPatrolling = false;
            if (_petMainController.CurrentPetState == PetStates.STAND) return;
            _petMainController.CurrentPetState = PetStates.STAND;
            navMeshAgent.isStopped  = true;
            OnPetStopped?.Invoke();
        }

        public void WalkCharacter(Vector3 targetPosition) {
            _speed                 = WALKING_SPEED;
            navMeshAgent.speed     = _speed;
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(targetPosition);
            OnPetWalking?.Invoke(navMeshAgent.desiredVelocity, _speed);
        }

        public void RunCharacter(Vector3 targetPosition) {
            _speed                 = RUNNING_SPEED;
            navMeshAgent.speed     = _speed;
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(targetPosition);
            OnPetRunning?.Invoke(navMeshAgent.desiredVelocity, _speed);
        }

        public void StandCharacter(float waitTime) {
            StartCoroutine(StandCharacter_CO(waitTime));
        }

        public void SetWaypoints(List<Transform> waypoints) {
            waypointsList = waypoints;
        }

        IEnumerator StandCharacter_CO(float waitTime) {
            navMeshAgent.isStopped = true;
            yield return new WaitForSeconds(waitTime);
            OnPetIdleCompleted?.Invoke();
        }

        public Vector3 GetNextTarget() {
            var waypoint = waypointsList[Random.Range(0, waypointsList.Count)];
            var nextWaypoint = waypoint.position;

            var distance = GetDistanceToTarget(nextWaypoint);

            while (distance <= waypointDistanceThreshold) {
                nextWaypoint = waypointsList[Random.Range(0, waypointsList.Count)].position;
                distance     = Vector3.Distance(transform.position, nextWaypoint);
            }

            return nextWaypoint;
        }

        private float GetDistanceToTarget(Vector3 targetPosition) {
            return Vector3.Distance(transform.position, targetPosition);
        }
    }
}
