using UnityEditor;
using UnityEngine;

namespace PoketPet.Pets.Controllers.Movement.Editor {
    [CustomEditor(typeof(PetMovementController))]
    public class PetMovementControllerEditor : UnityEditor.Editor {

        private Vector3 _newTargetPosition;
        
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var petMovementController = (PetMovementController)target;

            _newTargetPosition = EditorGUILayout.Vector3Field("New target position:", _newTargetPosition);
            
            if (GUILayout.Button("Move to Target", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petMovementController.MoveCharacterToTarget(_newTargetPosition);
            }
            
            if (GUILayout.Button("Begin Patrolling", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petMovementController.BeginPatrolling();
            }

            if (GUILayout.Button("Stop Movement", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                petMovementController.StopCharacter();
            }
        }
    }
}
