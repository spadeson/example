using System;
using System.Collections.Generic;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;

namespace PoketPet.Pets.Controllers.Movement
{
    public interface IPetMovementController
    {
        event Action<Vector3, float> OnPetWalking;
        event Action<Vector3, float> OnPetRunning;
        event Action OnPetStopped;
        event Action OnCompletedMovement;
        event Action<Vector3> OnVelocityUpdated;
        event Action OnPetIdleCompleted;
        bool InPatrolling { get; set; }

        void    Initialize(IPetMainController petMainController, PetConfig petConfigModel);
        void    BeginPatrolling();
        void    StopCharacter();
        void    MoveCharacterToTarget(Vector3 targetPosition, bool continuePatrolling = false);
        void    WalkCharacter(Vector3 targetPosition);
        void    RunCharacter(Vector3 targetPosition);
        Vector3 GetNextTarget();
        void    StandCharacter(float waitTime);
        void    SetWaypoints(List<Transform> waypoints);
    }
}