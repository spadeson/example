using PoketPet.Pets.Data;

namespace PoketPet.Pets.Controllers.States {
    public interface IPetState {
        PetStates PetStateType { get; set; }
        void StartState();
    }
}
