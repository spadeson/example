using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.Data;

namespace PoketPet.Pets.Controllers.States {
    public class PetStateStand : IPetState {
        
        public PetStates PetStateType { get; set; }

        private PetMainController _petMainController;

        public PetStateStand(PetMainController petMainController) {
            _petMainController = petMainController;
            PetStateType = PetStates.STAND;
        }

        public void StartState() {
            
        }
    }
}
