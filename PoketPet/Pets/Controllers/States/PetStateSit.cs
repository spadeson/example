using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.Data;

namespace PoketPet.Pets.Controllers.States {
    public class PetStateSit : IPetState {
        
        public PetStates PetStateType { get; set; }
        
        private PetMainController _petMainController;

        public PetStateSit(PetMainController petMainController) {
            _petMainController = petMainController;
            PetStateType = PetStates.SIT;
        }

        public void StartState() {
            
        }
    }
}
