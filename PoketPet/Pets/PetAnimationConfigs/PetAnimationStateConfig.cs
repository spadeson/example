using System.Collections.Generic;
using PoketPet.Pets.Data;
using UnityEngine;

namespace PoketPet.Pets.PetAnimationConfigs {
    [CreateAssetMenu(fileName = "PetAnimationStateConfig", menuName = "PoketPet/Pets/Animation/Pet Animation State Config", order = 0)]
    public class PetAnimationStateConfig : ScriptableObject {
        public PetStates petState;
        public string animationLayerName;
        public List<PetAnimationStateData> animationClipTriggers = new List<PetAnimationStateData>();
    }
}
