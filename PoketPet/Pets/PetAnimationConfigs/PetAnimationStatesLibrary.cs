using System.Collections.Generic;
using PoketPet.Pets.Data;
using UnityEngine;

namespace PoketPet.Pets.PetAnimationConfigs {
    [CreateAssetMenu(fileName = "PetAnimationStatesLibrary", menuName = "PoketPet/Pets/Animation/Pet Animation States Library", order = 0)]
    public class PetAnimationStatesLibrary : ScriptableObject {
        public List<PetAnimationStateConfig> petAnimationConfigsList = new List<PetAnimationStateConfig>();

        public PetAnimationStateConfig GetPetAnimationConfigByType(PetStates petState) {
            return petAnimationConfigsList.Find(c => c.petState == petState);
        }
    }
}
