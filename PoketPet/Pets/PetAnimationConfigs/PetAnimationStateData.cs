using System;

namespace PoketPet.Pets.PetAnimationConfigs {
    [Serializable]
    public class PetAnimationStateData {
        public string animationClipTrigger;
        public int animationClipProbability;
    }
}
