using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsSystem.PetsLibrary;

namespace PoketPet.Pets.PetsBuilder {
    public interface IPetBuilder {
        void Initialize(PetsLibraryConfig petsLibraryConfig);
        IPetMainController BuildPet(PetResponseData petsDataModel);
    }
}
