using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;

namespace PoketPet.Pets.PetsBuilder {
    public class PetBuilderTutorial : MonoBehaviour, IPetBuilder {
        
        private PetsLibraryConfig _petsLibraryConfig;

        public void Initialize(PetsLibraryConfig petsLibraryConfig) {
            _petsLibraryConfig = petsLibraryConfig;
        }

        public IPetMainController BuildPet(PetResponseData petsDataModel) {
            
            var petConfig = _petsLibraryConfig.ConfigsList.Find(c => c.petBreed == "tutorial_dog");

            //Create Pet Visual
            var petVisual = Instantiate(petConfig.petPrefab);
            petVisual.transform.localScale = Vector3.one * petConfig.visualScale;
            
            var petController = petVisual.AddComponent<PetMainControllerTutorial>();
            
            petController.Initialize(petConfig);
            
            return petController;
        }
    }
}
