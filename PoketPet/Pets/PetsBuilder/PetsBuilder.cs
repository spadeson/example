using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using UnityEngine;

namespace PoketPet.Pets.PetsBuilder {
    public class PetsBuilder : MonoBehaviour, IPetBuilder {

        private PetsLibraryConfig _petsLibraryConfig;
        
        public void Initialize(PetsLibraryConfig petsLibraryConfig) {
            _petsLibraryConfig = petsLibraryConfig;
        }

        public IPetMainController BuildPet() {
            
            var petConfig = _petsLibraryConfig.ConfigsList[0];

            //Create Pet Visual
            var petVisual = Instantiate(petConfig.petPrefab);
            petVisual.transform.localScale = Vector3.one * petConfig.visualScale;
            
            var petController = petVisual.AddComponent<PetMainController>();
            petController.Initialize(petConfig);
            
            return petController;
        }
        
        public IPetMainController BuildPet(PetResponseData petsDataModel) {
            //var petConfig = petsLibraryConfig.ConfigsList.Find(c => c.petBreed.Equals(petsDataModel.Breed));
            var petConfig = _petsLibraryConfig.ConfigsList[0];

            //Create Pet Visual
            var petVisual = Instantiate(petConfig.petPrefab);
            petVisual.transform.localScale = Vector3.one * petConfig.visualScale;
            
            var petController = petVisual.AddComponent<PetMainController>();
            petController.Initialize(petConfig);
            
            return petController;
        }
    }
}