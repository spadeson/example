using System;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Network.SocketBinds.Avatars;
using PoketPet.Network.SocketBinds.Pets;
using PoketPet.User;
using PoketPet.Utilities;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Pets.PetsSystem.Manager {
    public static class PetsProfileManager {
        
        public static event Action<PetResponseData> OnPetUpdated; 
        public static event Action<PetResponseData> OnPetReceived;
        public static event Action OnPetAddedToUser;
        
        
        public static PetResponseData CurrentPet { get; private set; }
        
        
        public static event Action<string> OnPetAvatarUpdated;
        
        #region Public Methods
        
        /// <summary>
        /// Create blank Pet Model.
        /// </summary>
        public static void CreateNewPetModel() {
            CurrentPet = new PetResponseData();
        }

        public static DateTime GetPetBirthDay() {
            return DateTimeConversion.TimestampToDateTime(CurrentPet.birth_day);
        }

        public static void SetPetsBirthDate(DateTime birthdate) {
            CurrentPet.birth_day = DateTimeConversion.DateTimeToTimestamp(birthdate);
        }
        
        public static void AddSocketListeners() {

            PetModelSocketBind.Instance.OnUpdateByUserAction += data => {
                CurrentPet = data;
                OnPetUpdated?.Invoke(CurrentPet);
            };
            
            PetWatcherSocketBind.Instance.OnAddAction  += data => {
                if (UserProfileManager.CurrentUser.id == data.userID) {
                    UserProfileManager.AddPetToUser(data.petID, data.permission);
                }
                GetPetById(data.petID);
            };

            AvatarUpdateSocketBind.Instance.OnPetAvatarUpdatedAction += (id, avatar) => {
                CurrentPet.avatar = avatar;
                OnPetAvatarUpdated?.Invoke(CurrentPet.avatar);
            };
        }

        public static void CreateNewPet() {
            Debug.Log("CreateNewPetRequest");
            var requestData = new RequestData<CreateNewPetRequestData> (
                from: UserProfileManager.CurrentUser.id,
                data: new CreateNewPetRequestData {
                    nickname = CurrentPet.nickname,
                    type = CurrentPet.type,
                    breed = CurrentPet.breed,
                    birth_day = CurrentPet.birth_day,
                    gender = CurrentPet.gender,
                    weight = CurrentPet.weight
                }, 
                action: "create"
            );

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("pets", requestData);
            operation.OnDataReceived += PetReceivedHandler;
        }
        
        public static void GetPetById(string id) {
            var userId = UserProfileManager.CurrentUser.id;
            var requestData = new RequestData<GetPetByIdRequestData>(userId, new GetPetByIdRequestData(id), "get") {from = userId};
            Debug.Log($"User Id: {userId}, PetId: {id}");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("pets", requestData);
            operation.OnDataReceived += PetReceivedHandler;
        }
        
        public static void UpdatePet(PetResponseData pet) {
            var petUpdateRequest = new RequestData<UpdatePetRequestData> {
                from = UserProfileManager.CurrentUser.id,
                data = new UpdatePetRequestData(pet),
                action = "update"
            };
            
            var petUpdateOperation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("pets", petUpdateRequest);
            petUpdateOperation.OnDataReceived += UpdatePetHandler;
        }
        
        #endregion
        
        #region Private Methods
        
        private static void PetReceivedHandler(string data) {
            Debug.Log($"New Pet Creation Data: {data}");
            var response = JsonUtility.FromJson<ResponseData<PetResponseData>>(data);
            CurrentPet = response.data;

            OnPetReceived?.Invoke(CurrentPet);
            OnPetAddedToUser?.Invoke();
        }

        private static void UpdatePetHandler(string data) {
            Debug.Log($"Update Pet JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<PetResponseData>>(data);
            CurrentPet = response.data;
            
            OnPetUpdated?.Invoke(CurrentPet);
        }
        
        #endregion
    }
}