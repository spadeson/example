using PoketPet.Pets.PetAnimationConfigs;
using UnityEditor;
using UnityEngine;

namespace PoketPet.Pets.PetsSystem.PetsLibrary.Editor {
    [CustomEditor(typeof(PetConfig))]
    public class PetConfigEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var config = (PetConfig)target;

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("Pet Breed", EditorStyles.boldLabel);
            config.petBreed = EditorGUILayout.TextField("Pet Breed", config.petBreed);
            
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("Pet Visual Prefab", EditorStyles.boldLabel);
            config.petPrefab = (GameObject)EditorGUILayout.ObjectField("Prefab", config.petPrefab, typeof(GameObject), false);
            config.visualScale = EditorGUILayout.FloatField("Visual Scale", config.visualScale);
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("Nav Mesh Parameters", EditorStyles.boldLabel);
            config.createNavMesh = EditorGUILayout.Toggle("Create Nav Mesh", config.createNavMesh);
            if (config.createNavMesh) {
                config.speed = EditorGUILayout.FloatField("Speed", config.speed);
                config.angularSpeed = EditorGUILayout.FloatField("Angular Speed", config.angularSpeed);
                config.acceleration = EditorGUILayout.FloatField("Acceleration", config.acceleration);
                config.stoppingDistance = EditorGUILayout.FloatField("Stopping Distance", config.stoppingDistance);
                config.autoBraking = EditorGUILayout.Toggle("AutoBraking", config.autoBraking);
                config.radius = EditorGUILayout.FloatField("Radius", config.radius);
                config.height = EditorGUILayout.FloatField("Height", config.height);
                config.baseOffset = EditorGUILayout.FloatField("Base Offset", config.baseOffset);
            }
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("Capsule Collider Parameters", EditorStyles.boldLabel);
            config.createCollider = EditorGUILayout.Toggle("Create Collider", config.createCollider);
            if (config.createCollider) {
                config.colliderRadius = EditorGUILayout.FloatField("Collider Radius", config.colliderRadius);
                config.colliderHeight = EditorGUILayout.FloatField("Collider Height", config.colliderHeight);
                config.colliderOffset = EditorGUILayout.Vector3Field("Collider Offset", config.colliderOffset);
                config.colliderDirection = EditorGUILayout.IntSlider("Collider Direction", config.colliderDirection, 0, 2);
            }
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Create AI Controller", EditorStyles.boldLabel);
            config.createAIController = EditorGUILayout.Toggle("Create AI Controller", config.createAIController);
            if (config.createAIController) {
                config.petAnimationStatesLibrary = (PetAnimationStatesLibrary)EditorGUILayout.ObjectField("Pet Animations Library", config.petAnimationStatesLibrary, typeof(PetAnimationStatesLibrary), false);
            }
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.Space();
            
            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}
