using System;
using PoketPet.Pets.PetAnimationConfigs;
using UnityEngine;

namespace PoketPet.Pets.PetsSystem.PetsLibrary
{
    [Serializable, CreateAssetMenu(fileName = "New Pet Config", menuName = "VIS/Pets/Pet Config")]
    public class PetConfig : ScriptableObject {
        
        [HideInInspector] public string petBreed;
        [HideInInspector] public GameObject petPrefab;
        [HideInInspector] public float visualScale;

        #region NamMesh

        [HideInInspector] public bool createNavMesh;
        [HideInInspector] public float speed;
        [HideInInspector] public float angularSpeed;
        [HideInInspector] public float acceleration;
        [HideInInspector] public float stoppingDistance;
        [HideInInspector] public bool autoBraking;
        [HideInInspector] public float radius;
        [HideInInspector] public float height;
        [HideInInspector] public float baseOffset;

        #endregion

        #region Collider
        [HideInInspector] public bool createCollider;
        [HideInInspector] public float colliderRadius;
        [HideInInspector] public float colliderHeight;
        [HideInInspector] public Vector3 colliderOffset;
        [HideInInspector] public int colliderDirection;

        #endregion

        #region AI

        [HideInInspector] public bool createAIController;
        [HideInInspector] public PetAnimationStatesLibrary petAnimationStatesLibrary;

        #endregion
    }
}