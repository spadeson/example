﻿using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Pets.PetsSystem.PetsLibrary {
    [CreateAssetMenu(fileName = "PetsLibraryConfig", menuName = "VIS/Pets/New Pets Config Library")]
    public class PetsLibraryConfig : ScriptableObject
    {
        public List<PetConfig> ConfigsList = new List<PetConfig>();
    }
}
