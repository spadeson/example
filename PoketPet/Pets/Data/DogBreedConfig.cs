using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Pets.Data {
    [CreateAssetMenu(fileName = "DogBreedConfig", menuName = "PoketPet/Pets/Breeds/Dog Breed Config", order = 0)]
    public class DogBreedConfig : ScriptableObject {
        [HideInInspector] public List<DogBreedsData> breedList;
    }
}
