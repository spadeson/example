﻿using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Pets.Data {
    public class PetsDataModel {
        public string petID;
        public List<string> masters = new List<string>();
        public string petName;
        public float weight;
        public float petHeight;
        public long birth_day;
        public string breed;
        public string RemoteId;
        public string type;
        public string gender;
        public string avatar;
        public Sprite image;
    }
}