using System;

namespace PoketPet.Pets.Data {
    [Serializable]
    public class DogBreedsData {
        public DogBreeds breedType;
        public string breedName;
    }
}
