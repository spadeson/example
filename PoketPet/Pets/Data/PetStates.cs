namespace PoketPet.Pets.Data {
    public enum PetStates : byte {
        STAND = 0,
        SIT = 1,
        LAY = 2,
        MOVING = 3
    }
}