using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace PoketPet.Pets.Data.Editor {
    [CustomEditor(typeof(DogBreedConfig))]
    public class DogBreedConfigEditor : UnityEditor.Editor {

        private TextAsset _beedsTextAssetFile;
        
        private ReorderableList _list;
        
        private DogBreedConfig DogBreedConfig => target as DogBreedConfig;
        
        private void OnEnable()
        {
            _list = new ReorderableList(DogBreedConfig.breedList, typeof(DogBreedsData), true, true, true, true);

            _list.onAddCallback += AddElementCallback;
            _list.onRemoveCallback += RemoveElementCallback;
        
            _list.drawHeaderCallback += DrawHeader;
            _list.drawElementCallback += DrawElement;
            _list.onChangedCallback += OnChangedCallback;
        }

        private void OnDisable()
        {
            _list.drawElementCallback -= DrawElement;
        }
        
        private void OnChangedCallback(ReorderableList list) {
            EditorUtility.SetDirty(target);
        }

        private void DrawHeader(Rect rect) {
            EditorGUI.LabelField (rect, "Dog Breeds List:", EditorStyles.boldLabel);
        }
    
        private void AddElementCallback(ReorderableList reorderableList) {
            DogBreedConfig.breedList.Add(new DogBreedsData());
            EditorUtility.SetDirty(target);
        }

        private void RemoveElementCallback(ReorderableList reorderableList) {
            DogBreedConfig.breedList.RemoveAt(reorderableList.index);
            EditorUtility.SetDirty(target);
        }
        
        private void DrawElement(Rect rect, int index, bool active, bool focused)
        {
            var item = DogBreedConfig.breedList[index];

            EditorGUI.BeginChangeCheck();
            item.breedType = (DogBreeds)EditorGUI.EnumPopup(new Rect(rect.x, rect.y, rect.width * 0.45f - 18, rect.height), item.breedType);
            item.breedName = EditorGUI.TextField(new Rect(rect.width * 0.45f + 18, rect.y, rect.width * 0.4f, rect.height), item.breedName);

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(target);
            }

        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            
            // Actually draw the list in the inspector
            _list.DoLayoutList();

            var dogBreedConfig = (DogBreedConfig)target;
            
            EditorGUILayout.Space();

            _beedsTextAssetFile = (TextAsset)EditorGUILayout.ObjectField("CSV Config:", _beedsTextAssetFile, typeof(TextAsset), false);

            if (_beedsTextAssetFile != null) {
                if (GUILayout.Button("Parse Text Asset", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {

                    var breedsList = GetBreedsList(_beedsTextAssetFile);
                
                    dogBreedConfig.breedList.Clear();
                
                    foreach (var breed in breedsList) {
                        Debug.Log($"Breed: {breed}");
                        var breedType = (DogBreeds)Enum.Parse(typeof(DogBreeds), breed);
                        var breedName = breed.Replace('_', ' ').ToLower();
                        dogBreedConfig.breedList.Add(new DogBreedsData {breedType = breedType, breedName = breedName});
                    }
                }
            } else {
                EditorGUILayout.HelpBox("If you want to create breeds list dynamicaly, please assign *.csv config above!", MessageType.Warning);
            }

            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }

        private List<string> GetBreedsList(TextAsset breedsList) {
            var path = AssetDatabase.GetAssetPath(breedsList);
            var data = File.ReadAllText(path);
            var parsedData = data.Split(',').ToList();
            return parsedData;
        }
    }
}
