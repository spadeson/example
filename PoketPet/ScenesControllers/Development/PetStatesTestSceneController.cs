using Core.ScenesController;
using PoketPet.Global;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsBuilder;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using PoketPet.UI.PetStatesViewer;
using PoketPet.Waypoints.Managers;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.ScenesControllers.Development {
    public class PetStatesTestSceneController : BasicSceneController {
        
        [Header("Pets Library"), SerializeField]
        private PetsLibraryConfig petsLibraryConfig;
        
        [Header("Pets Builder:"), SerializeField]
        private PetsBuilder petsBuilder;

        [Header("Pets Builder:"), SerializeField]
        private WaypointsManager _waypointsManager;

        private IPetMainController _petMainController; 
        
        protected override void ExecuteSceneController() {
            base.ExecuteSceneController();
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI);
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.MAIN_SCREEN_UI);
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.TASK_DISPLAY_UI);
            
            petsBuilder = new GameObject("PetBuilder", typeof(PetsBuilder)).GetComponent<PetsBuilder>();
            petsBuilder.Initialize(petsLibraryConfig);
            
            _petMainController = petsBuilder.BuildPet();
            _petMainController.SetMovementWaypoints(_waypointsManager.GetAllWaypoints());

            var petStatesViewerData = new PetStatesViewerUIWidgetData {PetMainController = _petMainController};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PET_STATES_VIEWER_UI, petStatesViewerData);
        }
    }
}
