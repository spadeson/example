using PoketPet.Global;
using UnityEditor;
using UnityEngine;

namespace PoketPet.ScenesControllers.Development.Editor {
    [CustomEditor(typeof(TaskAdviserTestsSceneController))]
    public class TaskAdviserTestsSceneControllerEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var taskAdviserTestsSceneController = (TaskAdviserTestsSceneController)target;

            if (GUILayout.Button($"Adviser - {GlobalVariables.UsersPetRoleKeys.OWNER} - {GlobalVariables.TaskTypes.FEED}", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskAdviserTestsSceneController.ShowAdviserWidget(GlobalVariables.UsersPetRoleKeys.OWNER, GlobalVariables.TaskTypes.FEED);
            }
            
            if (GUILayout.Button($"Adviser - {GlobalVariables.UsersPetRoleKeys.WATCHER} - {GlobalVariables.TaskTypes.FEED}", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskAdviserTestsSceneController.ShowAdviserWidget(GlobalVariables.UsersPetRoleKeys.WATCHER, GlobalVariables.TaskTypes.FEED);
            }
            
            if (GUILayout.Button($"Adviser - {GlobalVariables.UsersPetRoleKeys.OWNER} - {GlobalVariables.TaskTypes.WALK}", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskAdviserTestsSceneController.ShowAdviserWidget(GlobalVariables.UsersPetRoleKeys.OWNER, GlobalVariables.TaskTypes.WALK);
            }
            
            if (GUILayout.Button($"Adviser - {GlobalVariables.UsersPetRoleKeys.WATCHER} - {GlobalVariables.TaskTypes.WALK}", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskAdviserTestsSceneController.ShowAdviserWidget(GlobalVariables.UsersPetRoleKeys.WATCHER, GlobalVariables.TaskTypes.WALK);
            }
        }
    }
}
