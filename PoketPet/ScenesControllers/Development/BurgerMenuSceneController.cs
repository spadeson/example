using Core.ScenesController;
using PoketPet.Global;
using VIS.CoreMobile;

namespace PoketPet.ScenesControllers.Development {
    public class BurgerMenuSceneController : BasicSceneController {
        protected override void ExecuteSceneController() {
            base.ExecuteSceneController();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.BURGER_MENU_UI, animate: true);
        }
    }
}