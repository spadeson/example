﻿using System;
using Core.ScenesController;
using UnityEngine;

namespace PoketPet.ScenesControllers.Development {
    public class TimeFormatTestSceneController : BasicSceneController {

        private const string STANDARD_DATE_TIME_FORMAT = "hh:mm tt";
        private const string MILITARY_TIME_FORMAT = "HH:mm";
        
        [Space]
        [SerializeField] private int hoursValue;
        [SerializeField] private int minutesValue;
        
        [SerializeField] private bool useMilitaryTime;

        protected override void ExecuteSceneController() {
            base.ExecuteSceneController();

            var newHoursValue  = useMilitaryTime ? hoursValue : hoursValue + 12;
            var dateTimeFormat = useMilitaryTime ? MILITARY_TIME_FORMAT : STANDARD_DATE_TIME_FORMAT;
            var newDateTime    = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, newHoursValue, minutesValue, DateTime.Now.Second);
            
            Debug.Log($"{newDateTime.ToString(dateTimeFormat)}");
        }
    }
}
