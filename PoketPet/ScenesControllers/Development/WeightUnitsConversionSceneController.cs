using Core.ScenesController;
using Core.Utilities;
using UnityEngine;

namespace PoketPet.ScenesControllers.Development {
    public class WeightUnitsConversionSceneController : BasicSceneController {
        protected override void ExecuteSceneController() {
            base.ExecuteSceneController();

            ConvertIntegers(1);
            ConvertFloats(2.5f);
            ConvertLongs(33333453636);
        }

        private void ConvertIntegers(int value) {

            var pounds    = UnitsUtilities.KilogramToPounds(value);
            var kilograms = UnitsUtilities.PoundsToKilograms(pounds);

            Debug.Log($"Value:{value}, Pounds: {pounds}, Kilograms: {kilograms}");
        }
        
        private void ConvertFloats(float value) {

            var pounds    = UnitsUtilities.KilogramToPounds(value);
            var kilograms = UnitsUtilities.PoundsToKilograms(pounds);
            
            Debug.Log($"Value:{value}, Pounds: {pounds}, Kilograms: {kilograms}");
        }
        
        private void ConvertLongs(long value) {

            var pounds    = UnitsUtilities.KilogramToPounds(value);
            var kilograms = UnitsUtilities.PoundsToKilograms(pounds);
            
            Debug.Log($"Value:{value}, Pounds: {pounds}, Kilograms: {kilograms}");
        }
    }
}
