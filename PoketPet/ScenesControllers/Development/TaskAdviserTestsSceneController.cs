using Core.ScenesController;
using PoketPet.Global;
using PoketPet.UI.TaskAdviser;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.ScenesControllers.Development {
    public class TaskAdviserTestsSceneController : BasicSceneController {
        protected override void ExecuteSceneController() {
            base.ExecuteSceneController();
        }

        public void ShowAdviserWidget(string userRole, string taskType) {
            ShowAdviserWidgetWithData(userRole, taskType);
        }

        private void ShowAdviserWidgetWithData(string userRole, string taskType) {
            var adviserWidgetData = new TaskAdviserUIWidgetData{UserRole = userRole, TaskType = taskType};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.TASK_ADVISER_UI, adviserWidgetData);
        }
    }
}
