﻿using System.Collections.Generic;
using Core.ScenesController;
using PoketPet.Camera;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Network.API.RESTModels.Tasks;
using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsBuilder;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using PoketPet.Task;
using PoketPet.UI.TaskDisplay;
using PoketPet.User;
using PoketPet.Waypoints.Managers;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.ScenesControllers.Development {
    public class PetAnimationsTestSceneController : BasicSceneController {
    
        [SerializeField] private readonly GameObject pet;
    
        [SerializeField] private bool createCameraDynamically;

        [Header("Pets Library"), SerializeField]
        private PetsLibraryConfig petsLibraryConfig;

        [Header("Camera Config"), SerializeField]
        private CameraConfig cameraConfig;

        [Header("Camera Controller:"), SerializeField]
        private CamerasController camerasController;

        [Header("Pets Builder:"), SerializeField]
        private PetsBuilder petsBuilder;

        [Header("Waypoints Manager:"), SerializeField]
        private WaypointsManager waypointsManager;
        
        private IPetMainController _petMainController;

        protected override void ExecuteSceneController() {
            
            base.ExecuteSceneController();
        
            petsBuilder = new GameObject("PetBuilder", typeof(PetsBuilder)).GetComponent<PetsBuilder>();
            petsBuilder.Initialize(petsLibraryConfig);

            //Init CameraController
            camerasController = CamerasController.Instance;

            //Create Main Screen
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.MAIN_SCREEN_UI);

            //Create Waypoints
            if (waypointsManager == null) {
            
                waypointsManager = new GameObject("WayPointsManager", typeof(WaypointsManager)).GetComponent<WaypointsManager>();
            
                if (waypointsManager.waypointsList.Count == 0) {
                    waypointsManager.CreateWayPoints(Vector3.zero, 8, 5f);
                }
            }

            if (UserProfileManager.CurrentUser != null && PetsProfileManager.CurrentPet == null) {
                PetsProfileManager.CreateNewPetModel();
                _petMainController = petsBuilder.BuildPet(PetsProfileManager.CurrentPet);
            } else {
                var localPetDataModel = new PetResponseData{breed = "bulldog"};
                _petMainController = petsBuilder.BuildPet(localPetDataModel);
            }

            _petMainController.SetMovementWaypoints(waypointsManager.waypointsList);
        
            //_petMainController.petMovementController.BeginPatrolling();
        
            camerasController.SetCameraLookAtTarget(_petMainController.Transform);
        
            CamerasController.Instance.SetCameraLookAtTarget(_petMainController.Transform);
        
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioMusicKeys.GAME_MENU_MUSIC, true);

            CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.PRELOADER_UI);
        }
        
        private void GetTasksByPetId(string petId) {
            var userId = UserProfileManager.CurrentUser.id;

            Debug.Log($"User Id: {userId}, Pet Id: {petId}");
            
            var requestData = new RequestData<GetTaskByPetIdRequestData>(userId, new GetTaskByPetIdRequestData(userId, petId), "get_tasks_by_pet");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("flows", requestData);
            operation.OnDataReceived += GetTasksByPetIdHandler;
        }
        
        private void GetTasksByPetIdHandler(string data) {
            
            Debug.Log($"Get Tasks JSON Response: {data}");
            
            var response = JsonUtility.FromJson<ResponseData<List<TaskResponseData>>>(data);

            foreach (var task in response.data) {
                Debug.Log($"Task Id: {task.id}");
            }
            
            var tasks = TaskManager.Tasks;
           
            var taskDisplayWidgetData = new TaskDisplayUIWidgetData {taskDataModels = tasks};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.TASK_DISPLAY_UI, taskDisplayWidgetData);
            
            //TaskManager.CreateTasksListByServerResponse(response.data);
        }
    }
}
