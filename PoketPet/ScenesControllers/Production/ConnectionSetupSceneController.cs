using Core.ScenesController;
using PoketPet.Global;
using PoketPet.UI.ConnectionSetup;
using VIS.CoreMobile;

namespace PoketPet.ScenesControllers.Production {
    public class ConnectionSetupSceneController : BasicSceneController {
        protected override void ExecuteSceneController() {
            base.ExecuteSceneController();

            ShowConnectionOptions();   
        }
        
        private void ShowConnectionOptions() {
            var connectionSetupUIWidgetData = new ConnectionSetupUIWidgetData {connectionsNetworkLibrary = CoreMobile.Instance.CoreMobileConfig.NetworkManagerConfig.ConnectionConfigsLibrary};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.CONNECTION_SETUP_UI, connectionSetupUIWidgetData, true);
        }
    }
}
