using Core.ScenesController;
using PoketPet.Camera;
using PoketPet.CutScenesManager;
using PoketPet.Global;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsBuilder;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using PoketPet.Tutorial.Interfaces;
using PoketPet.Tutorial.Onboarding;
using PoketPet.Tutorial.Onboarding.Data;
using PoketPet.UI.NewTutorial01;
using PoketPet.UI.NewTutorial04;
using PoketPet.UI.NewTutorial05;
using PoketPet.UI.NewTutorial06;
using PoketPet.UI.NewTutorial08;
using PoketPet.UI.NewTutorial09;
using PoketPet.UI.NewTutorial10;
using PoketPet.UI.NewTutorial11;
using PoketPet.UI.NewTutorial13;
using PoketPet.UI.TutoralFakeCamera;
using PoketPet.UI.TutorialGeneric;
using PoketPet.UI.TutorialTakePhoto;
using PoketPet.UI.TutorialTaskBest;
using UnityEngine;
using UnityEngine.Timeline;
using VIS.CoreMobile.UISystem;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.UiKeys;
using static PoketPet.Global.GlobalVariables.ScenesKeys;
using static PoketPet.Global.GlobalVariables.TutorialKeys;

namespace PoketPet.ScenesControllers {
    public class TutorialSceneController : BasicSceneController {

        [SerializeField]
        private SignalReceiver _signalReceiver;

        [Header("Pet Collar")]
        [SerializeField] private Transform petCollar;

        [Header("Pets Library"), SerializeField]
        private PetsLibraryConfig petsLibraryConfig;
        
        private Widget _currentOpenedWidget;
        
        private PetBuilderTutorial _petsBuilder;
        
        private IPetMainController _petMainController;

        private ITutorialManager _tutorialManager;

        #region MonoBehaviour

        private void OnDestroy() {
            if(_tutorialManager != null) _tutorialManager.OnTutorialInit -= OnTutorialInitHandler;
        }

        #endregion

        #region BasicSceneController

        protected override void ExecuteSceneController() {
            
            base.ExecuteSceneController();

            CamerasController.Instance.ActivateCameraById(0);
            
            _petsBuilder = new GameObject("PetBuilder", typeof(PetBuilderTutorial)).GetComponent<PetBuilderTutorial>();
            _petsBuilder.Initialize(petsLibraryConfig);
            
            _petMainController = _petsBuilder.BuildPet(null);

            _petMainController.Transform.position = new Vector3(2, 0, -2f);
            _petMainController.Transform.rotation = Quaternion.Euler(0, 180, 0);
            
            CamerasController.Instance.SetCameraLookAtTarget(_petMainController.Transform, 0);
            
            CutSceneManager.Instance.SetCurrentPet(_petMainController.GameObject);
            CutSceneManager.Instance.OnCutSceneCompleted += OnCutSceneFinished;

            //Create new instance for the tutorial manager
            _tutorialManager = new OnboardingTutorialManager();
            
            _tutorialManager.OnTutorialInit += OnTutorialInitHandler;
            _tutorialManager.Initialize();
        }
        
        #endregion

        #region Public Methods

        public void SetPetAsCameraTarget() {
            CamerasController.Instance.SetCameraLookAtTarget(_petMainController.Transform);
        }

        public void OnCutSceneFinished() {
            CamerasController.Instance.DeactivateAllCameras(0);
            CamerasController.Instance.ActivateCameraById(0);
            _petMainController.Transform.position = Vector3.zero;
            CamerasController.Instance.SetCameraLookAtTarget(_petMainController.Transform);
            CamerasController.Instance.SetActiveCameraPosition(_petMainController.Transform.position + new Vector3(-0.6f, 1, -1.3f));
        }

        public void TakeAssetInTheTeeth() {
            var assetOffset = new Vector3(-0.069f, -0.0312f,0);
            _petMainController.TakeAssetInMouth(petCollar.gameObject, assetOffset, true, 0.35f);
        }

        #endregion

        #region Private Methods

        private void OnTutorialInitHandler() {
            CoreMobile.Instance.UIManager.DismissWidgetByType(PRELOADER_UI);
            _tutorialManager.OnTutorialInit -= OnTutorialInitHandler;
            _tutorialManager.StartTutorial();
            ShowTutorialWidget();
        }

        private void ShowTutorialWidget() {
            
            if (_tutorialManager.IsTutorialFinished()) {
                CoreMobile.Instance.LocalDataManager.SaveLocalDataBoolean(TUTORIAL_COMPLETE, true);
                CoreMobile.Instance.SceneManager.LoadSceneByKeyFromConfig(AUTHENTICATION_SCENE);
                return;
            }
            
            WidgetData widgetData = null;
            
            var tutorialData = _tutorialManager.GetNextTutorialStepModel();
            
            Debug.Log($"Tutorial: {tutorialData.type}, {tutorialData.widget}");

            if (tutorialData.type == CUTSCENE_TYPE) {
                CutSceneManager.Instance.OnCutSceneCompleted += InstanceOnCutSceneCompleted;
                CutSceneManager.Instance.PlayCutScene(tutorialData.widget);
                return;
            }

            if (tutorialData.type == ANIMATION_TYPE) {
                
                return;
            }

            if (tutorialData.type == MULTI_POPUP_TYPE) {
                widgetData = new NewTutorialMultiPopupWidgetData {
                    tutorialStepData = (TutorialMultiStepModelData)tutorialData
                };
            }

            if (tutorialData.type == POPUP_TYPE) {

                switch (tutorialData.widget) {
                    case NEWTUTORIAL04UIWIDGET_UI:
                        widgetData = new NewTutorial04UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case NEWTUTORIAL06UIWIDGET_UI:
                        widgetData = new NewTutorial06UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case TUTORIAL_TAKE_PHOTO_UI:
                        widgetData = new TutorialTakePhotoUIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case NEWTUTORIAL07UIWIDGET_UI:
                        widgetData = new NewTutorial07UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case NEWTUTORIAL08UIWIDGET_UI:
                        widgetData = new NewTutorial08UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case NEWTUTORIAL09UIWIDGET_UI:
                        widgetData = new NewTutorial09UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case NEWTUTORIAL10UIWIDGET_UI:
                        widgetData = new NewTutorial10UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    case NEWTUTORIAL13UIWIDGET_UI:
                        widgetData = new NewTutorial13UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    default:
                        widgetData = new TutorialGenericUIWidgetData {
                            tutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                }
            } else if (tutorialData.type == TASK_TYPE) {

                switch (tutorialData.widget) {
                    case NEWTUTORIAL05UIWIDGET_UI:
                        widgetData = new NewTutorial05UIWidgetData {
                            TutorialStepModelData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    
                    case NEWTUTORIAL11UIWIDGET_UI:
                        widgetData = new NewTutorial11UIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    
                    case TUTORIAL_TASK_BEST_UI:
                        widgetData = new TutorialTaskBestUIWidgetData() {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                }
            }

            _currentOpenedWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(tutorialData.widget, widgetData);
            _currentOpenedWidget.OnDismissed += OnOpenedWidgetDismissHandler;
        }

        private void InstanceOnCutSceneCompleted() {
            CutSceneManager.Instance.OnCutSceneCompleted -= InstanceOnCutSceneCompleted;
            _tutorialManager.CompleteTutorialStep();
            ShowTutorialWidget();
        }

        private void OnOpenedWidgetDismissHandler(Widget widget) {
            
            if (_currentOpenedWidget != null) {
                _currentOpenedWidget.OnDismissed -= OnOpenedWidgetDismissHandler;
            }
            
            _tutorialManager.CompleteTutorialStep();
            
            ShowTutorialWidget();
        }

        #endregion
    }
}
