using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using Core.ScenesController;
using Firebase.Auth;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.FCM;
using PoketPet.Network.API.RESTModels.Login;
using PoketPet.Network.SocketBinds.Connection;
using PoketPet.Settings;
using PoketPet.UI.FirebaseEmailLogin;
using PoketPet.UI.NotificationStatus;
using PoketPet.UI.Preloader;
using PoketPet.UI.Progress;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.ScenesControllers {
    public class AuthenticationSceneController : BasicSceneController {

        private Widget _progressUiWidget;
        private Widget _notificationUiWidget;

        private string _systemUniqueId;
        private bool _isSocketReconnected;

        #region MonoBehaviour

        void Awake() {
            _systemUniqueId = SystemInfo.deviceUniqueIdentifier;
            Debug.Log($"Device Unique Identifier: {_systemUniqueId}");
        }

        private void OnDisable() {
            if (CoreMobile.Instance == null || CoreMobile.Instance.FirebaseManager.AuthenticationManager == null) return;
            
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnFirebaseTokenRecieved -= AuthenticationManagerOnFirebaseTokenRecieved;
            
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnLoginFailed -= AuthenticationManagerOnLoginFailed;
            
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnLoginCompleted -= AuthenticationManagerOnLoginCompleted;
            
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnNewUserCreationStart -= OnNewUserCreationStartHandler;
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnNewUserCreationCompleted -= OnNewUserCreationCompletedHandler;
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnNewUserCreationFailed -= OnNewUserCreationFailedHandler;
        }

        #endregion

        #region BasicSceneController

        protected override void ExecuteSceneController() {
            
            base.ExecuteSceneController();

            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.DEVELOPER_CONSOLE_UI);

            //Initialize Settings
            SettingsManager.Initialize();
            
            //Subscribe for event Firebase Token Received
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnFirebaseTokenRecieved += AuthenticationManagerOnFirebaseTokenRecieved;
            
            //Subscribe for event Login Failed
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnLoginFailed += AuthenticationManagerOnLoginFailed;
            
            //Subscribe for event Login Success
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnLoginCompleted += AuthenticationManagerOnLoginCompleted;
            
            //Subscribe for event User Creation Failed
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnNewUserCreationStart += OnNewUserCreationStartHandler;
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnNewUserCreationCompleted += OnNewUserCreationCompletedHandler;
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnNewUserCreationFailed += OnNewUserCreationFailedHandler;

            if (CoreMobile.Instance.FirebaseManager.AuthenticationManager.Auth.CurrentUser == null) {
                ShowFirebaseLogin();
            } else {
                LoginWithExistingUser();
            }
        }

        private void AuthenticationManagerOnLoginCompleted(FirebaseUser firebaseUser) {
            Debug.Log($"AuthenticationManagerOnLoginCompleted: {firebaseUser.UserId}");
        }

        private void AuthenticationManagerOnLoginFailed(Exception exception) {
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.FIREBASE_LOGIN_UI, true);

            var errorNotificationWidgetData = new NotificationStatusUIWidgetData {Title = "Login Error", Message = exception.Message};
            _notificationUiWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, errorNotificationWidgetData, true);
            _notificationUiWidget.OnDismissed += OnNotificationUiWidgetDismissed;
        }

        private void AuthenticationManagerOnFirebaseTokenRecieved(string firebaseToken) {
            Debug.Log($"TOKEN: {firebaseToken}");
            CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.FIREBASE_LOGIN_UI);
            RequestForUser(firebaseToken);
        }

        private void OnNotificationUiWidgetDismissed(Widget widget) {
            widget.OnDismissed -= OnNotificationUiWidgetDismissed;
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.FIREBASE_LOGIN_UI, true);
        }

        private void OnNewUserCreationStartHandler() {
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.FIREBASE_CREATE_ACCOUNT_UI, true);
            
            var progressUiWidgetData = new ProgressUIWidgetData{title = "Creating new user", description = "New user creation in progress..."};
            _progressUiWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PROGRESS_UI, progressUiWidgetData, true);
        }

        #endregion

        #region LocalData

        private void InitializeLocalData() {
            
        }

        #endregion

        #region Authentication

        private void LoginWithExistingUser() {
            RequestForUser(CoreMobile.Instance.FirebaseManager.AuthenticationManager.FirebaseToken);
        }
        
        private void ShowFirebaseLogin() {
            var loginData = new FirebaseEmailLoginUIWidgetData();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.FIREBASE_LOGIN_UI, loginData, true);
        }
        
        private void RequestForUser(string firebaseToken) {
            
            var requestData = new RequestData<LoginRequestData>(new LoginRequestData(firebaseToken), "login");
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("login", requestData);

            operation.OnDataReceived += RequestForUserHandler;
            operation.OnErrorReceived += error => {

                var notificationWidgetData = new NotificationStatusUIWidgetData {
                        Title = "Error",
                        Message = error
                };
                        
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationWidgetData, true);
            };
        }

        private void RequestForUserHandler(string data) {

            Debug.Log($"{data}");
            var response = JsonUtility.FromJson<ResponseData<UserModel>>(data);
            UserProfileManager.LoginWithServerResponse(response.data);
            Debug.Log($"User Id: {response.data.id}");

            //Add FCM token for current user
            AddFCMForCurrentUser(UserProfileManager.CurrentUser.id);
            
            CoreMobile.Instance.SocketNetworkManager.Connect();
            ConnectionStatusSocketBind.Instance.OnConnect += OnSocketConnectedHandler;
        }

        private void OnSocketConnectedHandler(object o) {
            ConnectionStatusSocketBind.Instance.OnConnect -= OnSocketConnectedHandler;
            LoginSocket();
        }
        
        private void LoginSocket() {
            LoginSocketBind.Instance.Login(UserProfileManager.CurrentUser.id, _systemUniqueId);
            LoginSocketBind.Instance.OnCompleteAction += () => {
                if (_isSocketReconnected) return;
                _isSocketReconnected = true;
                ShowPreloader();
            };
            LoginSocketBind.Instance.OnKickAction += LoginSocket;
        }
        
        private void ShowPreloader() {
            LoginSocketBind.Instance.OnCompleteAction -= ShowPreloader;
            LoginSocketBind.Instance.OnKickAction -= LoginSocket;

            CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.FIREBASE_CREATE_ACCOUNT_UI);
            CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI);
            
            var loadingOperation = CoreMobile.Instance.SceneManager.LoadSceneByKeyFromConfigAsync(GlobalVariables.ScenesKeys.GAME_SCENE);
            
            var preloaderWidgetData = new PreloaderUIWidgetData {
                useFakeProgress = true,
                fakeProgressDuration = 2f,
                operation = loadingOperation
            };
            
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PRELOADER_UI, preloaderWidgetData);
        }
        
        private void OnNewUserCreationCompletedHandler(FirebaseUser firebaseUser) {
            if (_progressUiWidget != null) {
                _progressUiWidget.Dismiss();
            }
                
            var notificationWidgetData = new NotificationStatusUIWidgetData {Title = "User Created", Message = "New User was created successful.\nYou'll receive confirmation e-mail.\nPlease verify you e-mail and you can login with you credentials."};
            _notificationUiWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationWidgetData);
        }

        private void OnNewUserCreationFailedHandler(Exception exception) {
            
            if (_progressUiWidget != null) {
                _progressUiWidget.Dismiss();
            }
                
            var notificationWidgetData = new NotificationStatusUIWidgetData {Title = "Error", Message = exception.Message};
            var notificationWidget     = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationWidgetData);
            notificationWidget.OnDismissed += widget => {
                CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.FIREBASE_CREATE_ACCOUNT_UI, true);
            };
        }

        #endregion

        #region FCMToken

        private void AddFCMForCurrentUser(string userId) {
            
            if (string.IsNullOrEmpty(CoreMobile.Instance.FirebaseManager.CloudMessagingManager.FirebaseToken)) return;
            
            var addFcmData = new RequestData<FCMTokenData> {
                    from = userId,
                    data = new FCMTokenData {
                            token = CoreMobile.Instance.FirebaseManager.CloudMessagingManager.FirebaseToken
                    },
                    action = "add_token"
            };

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("fcm", addFcmData);
            operation.OnDataReceived += AddFCMForCurrentUserHandler;
        }
        
        private void AddFCMForCurrentUserHandler(string data) {
            Debug.Log($"FSM JSON Response: {data}");
        }

        #endregion
    }
}
