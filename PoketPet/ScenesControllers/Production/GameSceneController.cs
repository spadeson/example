using System;
using Core.ScenesController;
using PoketPet.Camera;
using PoketPet.Family;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.DynamicLinks;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Controllers.Main;
using PoketPet.Pets.PetsBuilder;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Pets.PetsSystem.PetsLibrary;
using PoketPet.Schedule;
using PoketPet.Task;
using PoketPet.Tutorial.FirstRun;
using PoketPet.Tutorial.Interfaces;
using PoketPet.Tutorial.Onboarding.Data;
using PoketPet.UI.FirstRunNickName;
using PoketPet.UI.FirstRunTaskScheduleExplanation;
using PoketPet.UI.PetSchedule;
using PoketPet.User;
using PoketPet.Waypoints.Managers;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.ScenesControllers.Production {
    public class GameSceneController : BasicSceneController {
        [SerializeField] private GameObject pet;

        [SerializeField] private bool createCameraDynamically;

        [Header("Pets Library"), SerializeField]
        private PetsLibraryConfig petsLibraryConfig;

        [Header("Pets Builder:"), SerializeField]
        private PetsBuilder petsBuilder;

        [Header("Waypoints Manager:"), SerializeField]
        private WaypointsManager waypointsManager;
        
        private IPetMainController _petMainController;

        private ITutorialManager _tutorialManager;
        
        private Widget _currentOpenedWidget;

        #region BasicSceneController

        protected override void ExecuteSceneController() {
            
            Debug.Log("GameSceneController.ExecuteSceneController");
            
            base.ExecuteSceneController();

            petsBuilder = new GameObject("PetBuilder", typeof(PetsBuilder)).GetComponent<PetsBuilder>();
            petsBuilder.Initialize(petsLibraryConfig);

            CamerasController.Instance.ActivateCameraById(0);

            //Create Waypoints
            if (waypointsManager == null) {
                waypointsManager = new GameObject("WayPointsManager", typeof(WaypointsManager)).GetComponent<WaypointsManager>();

                if (waypointsManager.waypointsList.Count == 0) {
                    waypointsManager.CreateWayPoints(Vector3.zero, 8, 5f);
                }
            }
            
            UserProfileManager.AddSocketListeners();
            PetsProfileManager.AddSocketListeners();
            
            if (UserProfileManager.CurrentUser != null) {
                
                PetsProfileManager.OnPetReceived += CreateMainScreen;

                if (UserProfileManager.CurrentUser.pets != null && UserProfileManager.CurrentUser.pets.Count > 0) {
                    PetsProfileManager.GetPetById(UserProfileManager.CurrentUser.pets[0].id);
                } else {
                    PetsProfileManager.OnPetAddedToUser += OnNewPetAddedHandler;
                    CoreMobile.Instance.UIManager.CreateUiWidgetWithData(ADD_PET_UI);

                    var inviteDynamicLinkData = CoreMobile.Instance.FirebaseManager.DynamicLinksManager.GetInviteDynamicLinkData();
                    var waitForInviteDynamicLinkData = CoreMobile.Instance.FirebaseManager.DynamicLinksManager.GetWaitForInviteDynamicLinkData();

                    if (inviteDynamicLinkData != null && inviteDynamicLinkData.mail == UserProfileManager.CurrentUser.email) {
                        var requestData = new RequestData<DynamicLinkDataInvite>(UserProfileManager.CurrentUser.id, inviteDynamicLinkData, "dynamic_link_data");
                        var operation   = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("wait_invite", requestData);
                    }
                    
                    if (waitForInviteDynamicLinkData != null) {
                        var requestData = new RequestData<DynamicLinkDataWaitForInvite>(UserProfileManager.CurrentUser.id, waitForInviteDynamicLinkData, "dynamic_link_data");
                        var operation   = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("wait_invite", requestData);
                    }
                }

                CoreMobile.Instance.FirebaseManager.AuthenticationManager.OnLogout += () => {
                    
                    PetsProfileManager.OnPetReceived -= CreateMainScreen;
                    PetsProfileManager.OnPetAddedToUser -= OnNewPetAddedHandler;
                    
                    CoreMobile.Instance.AudioManager.StopPlayingSound(GlobalVariables.AudioMusicKeys.GAME_MENU_MUSIC);
                    
                    CoreMobile.Instance.SocketNetworkManager.Disconnect();

                    CoreMobile.Instance.UIManager.DismissAllWidgets();
                    CoreMobile.Instance.SceneManager.LoadSceneByKeyFromConfig(GlobalVariables.ScenesKeys.CONNECTION_SCENE);
                };
            }

            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioMusicKeys.GAME_MENU_MUSIC, true);

            CoreMobile.Instance.UIManager.DismissWidgetByType(PRELOADER_UI);
        }
        
        private void OnNewPetAddedHandler() {
            PetsProfileManager.OnPetAddedToUser -= OnNewPetAddedHandler;
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(TASK_COMPLETED_UI);
        }

        private void CreateMainScreen(PetResponseData petsDataModel) {
            if (UserProfileManager.CurrentUser.nickname == string.Empty) {
                
                _tutorialManager = new FirstRunTutorialManager();
                
                _tutorialManager.OnTutorialInit += OnTutorialInitHandler;
                _tutorialManager.Initialize();
                
                var widgetData = new FirstRunNickNameUIWidgetData();
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(FIRST_RUN_NICKNAME_UI, widgetData);
            }

            
            PetsProfileManager.OnPetReceived -= CreateMainScreen;
            
            FamilyProfileManager.Init();
            FamilyProfileManager.GetUsersByPetId(UserProfileManager.CurrentUser.id, PetsProfileManager.CurrentPet.id);
            TaskManager.GetTasksByPetId(PetsProfileManager.CurrentPet.id);
            ScheduleManager.AddSocketListeners();
            ScheduleManager.GetScheduleByPetId(PetsProfileManager.CurrentPet.id);

            CoreMobile.Instance.UIManager.DismissWidgetByType(ADD_PET_UI);
            CoreMobile.Instance.UIManager.DismissWidgetByType(CREATE_NEW_PET_UI);
            CoreMobile.Instance.UIManager.DismissWidgetByType(MESSAGES_UI);

            _petMainController = petsBuilder.BuildPet(petsDataModel);
            _petMainController.SetMovementWaypoints(waypointsManager.waypointsList);

            //petController.petMovementController.BeginPatrolling();

            CamerasController.Instance.SetCameraLookAtTarget(_petMainController.Transform);

            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(MAIN_SCREEN_UI);

            //Create Task Manager
            TaskManager.AddSocketListeners();
        }
        
        private void OnTutorialInitHandler() {
            _tutorialManager.OnTutorialInit -= OnTutorialInitHandler;
            _tutorialManager.StartTutorial();
            ShowTutorialWidget();
        }
        
        private void ShowTutorialWidget() {

            WidgetData widgetData = null;
            
            var tutorialData = _tutorialManager.GetNextTutorialStepModel();
            
            Debug.Log($"Tutorial: {tutorialData.type}, {tutorialData.widget}");

            // if (tutorialData.type == GlobalVariables.TutorialKeys.CUTSCENE_TYPE) {
            //     CutSceneManager.Instance.OnCutSceneCompleted += InstanceOnCutSceneCompleted;
            //     CutSceneManager.Instance.PlayCutScene(tutorialData.widget);
            //     return;
            // }

            // if (tutorialData.type == GlobalVariables.TutorialKeys.ANIMATION_TYPE) {
            //     
            //     return;
            // }

            // if (tutorialData.type == GlobalVariables.TutorialKeys.MULTI_POPUP_TYPE) {
            //     widgetData = new NewTutorialMultiPopupWidgetData {
            //         tutorialStepData = (TutorialMultiStepModelData)tutorialData
            //     };
            // }

            if (tutorialData.type == GlobalVariables.TutorialKeys.POPUP_TYPE) {

                switch (tutorialData.widget) {
                    case FIRST_RUN_NICKNAME_UI:
                        widgetData = new FirstRunNickNameUIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    
                    case FIRST_RUN_TASK_SCHEDULE_EXPLANATION_UI:
                        widgetData = new FirstRunTaskScheduleExplanationUIWidgetData {
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                    
                    case PET_SCHEDULE_UI:
                        widgetData = new PetScheduleUIWidgetData {
                            isFirstRun = true,
                            TutorialSingleStepData = (TutorialSingleStepModelData)tutorialData
                        };
                        break;
                }
            }

            _currentOpenedWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(tutorialData.widget, widgetData);
            _currentOpenedWidget.OnDismissed += OnOpenedWidgetDismissHandler;
        }
        
        private void OnOpenedWidgetDismissHandler(Widget widget) {
            
            if (_currentOpenedWidget != null) {
                _currentOpenedWidget.OnDismissed -= OnOpenedWidgetDismissHandler;
            }
            
            _tutorialManager.CompleteTutorialStep();
            
            ShowTutorialWidget();
        }

        #endregion
    }
}