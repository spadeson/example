using System;
using PoketPet.Games.Controllers;

namespace PoketPet.Games {
    
    [Serializable]
    public class GameDataModel {
        public string type;
        public int level;
        public IGameController controller;
    }
}