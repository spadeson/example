using System;
using System.Collections.Generic;
using PoketPet.Games.Controllers;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Game;
using PoketPet.Network.API.SocketModels.Game;
using PoketPet.Network.SocketBinds.Games;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.UI.GameResult;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Games {
    
    public static class GameManager {

        public static GameDataModel CurrentGame { get; private set; }
        public static event Action OnGameAwaiting;
        public static event Action OnGameCreateRequestSent;
        private static List<GameDataModel> _awaitingGamesList;

        public static void SetCurrentGame(string gameType) {
            CurrentGame = _awaitingGamesList.Find(item => item.type == gameType);
        }
        
        public static GameDataModel GetGameAwaitingByType(string gameType) {
            return _awaitingGamesList.Find(item => item.type == gameType);
        }

        public static void AddSocketListeners() {
            GamesAwaitingSocketBind.Instance.OnUpdateAction += RequestGameAwaiting;
            GameCompleteSocketBind.Instance.OnGameWinAction += OnGameCreatedHandler;
            GameCompleteSocketBind.Instance.OnGameLoseAction += OnGameCreatedHandler;
        }

        private static void OnGameCreatedHandler(bool success, GameCompleteData data) {
            if (data.to.id != UserProfileManager.CurrentUser.id) return;
            var widgetData = new GameResultUIWidgetData() {
                isSuccess = success,
                scores = data.to.bonus
            };
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.GAME_RESULT_UI, widgetData);

        }

        public static void RequestGameAwaiting() {
            var userId = UserProfileManager.CurrentUser.id;
            var data = new GamesAwaitingCreationRequestData() {petID = PetsProfileManager.CurrentPet.id};
            Debug.Log("RequestGameAwaiting");
            var requestData = new RequestData<GamesAwaitingCreationRequestData> {from = userId, data = data, action = "get_games_awaiting"};

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("games", requestData);
            operation.OnDataReceived += RequestGameAwaitingHandler;
        }

        private static void RequestGameAwaitingHandler(string data) {
            Debug.Log($"Get Games JSON Response: {data}");
            
            var response = JsonUtility.FromJson<ResponseData<List<GameResponseData>>>(data);

            CreateGamesListByServerResponse(response.data);
        }

        private static void CreateGamesListByServerResponse(List<GameResponseData> messagesResponse) {
            _awaitingGamesList = new List<GameDataModel>();
            foreach (var responseData in messagesResponse) {
                _awaitingGamesList.Add(new GameDataModel {
                    type = responseData.type,
                    level = responseData.level,
                    // TODO : create controller by available types
                    controller = new MoodGameController()
                });
                
            }
            OnGameAwaiting?.Invoke();
            Debug.Log("Games List Updated");
        }
        
        public static void CreateGame(WWWForm requestData) {
            OnGameCreateRequestSent?.Invoke();
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("gamesForm", requestData);
            operation.OnDataReceived += CheckStatusHandler;
        }

        public static void CompleteGame(RequestData<CompleteGameRequestData> requestData) {

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("messages", requestData);
            operation.OnDataReceived += CheckStatusHandler;
        }
        
        private static void CheckStatusHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
            
            }else {
                // TODO: Release Error Status 
            }
        }
    }
}