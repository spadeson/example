﻿using System;

[Serializable]
public class AssetCatalogKeyPair {
    public string assetId;
    public string assetName;
}
