﻿using System.Globalization;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace PoketPet.AssetsCatalog {
    public class AssetsCatalogManagerEditorWindow : EditorWindow {
    
        private GUIStyle _labelGuiStyle;
        private string _baseFolder;
        private TextAsset _assetsCatalogFile;
        private TextInfo _textInfo;

        private const string ANIMATIONS_SUBFODLER = "Animations";
        private const string MODELS_SUBFODLER = "Models";
        private const string MATERIALS_SUBFODLER = "Materials";
        private const string PREFABS_SUBFODLER = "Prefabs";
        private const string TEXTURES_SUBFODLER = "Textures";
    
        [MenuItem("Utilities/Assets/Assets Catalog Manager")]
        public static void Init() {
            var assetsCatalogManagerEditorWindow = (AssetsCatalogManagerEditorWindow)EditorWindow.GetWindow(typeof(AssetsCatalogManagerEditorWindow));
            assetsCatalogManagerEditorWindow.Show();
        }

        private void OnFocus() {
        
            _textInfo = new CultureInfo("en-US",false).TextInfo;

            _labelGuiStyle = new GUIStyle();

            _labelGuiStyle.fontSize  = 24;
            _labelGuiStyle.alignment = TextAnchor.MiddleCenter;
            _labelGuiStyle.fontStyle = FontStyle.Bold;
            _labelGuiStyle.margin    = new RectOffset(32, 32, 32, 32);
            _labelGuiStyle.padding   = new RectOffset(16, 16, 16, 16);

            _baseFolder = Path.Combine("Assets", "Models");
            _baseFolder = Path.Combine(_baseFolder, "Props");
        }

        private void OnGUI() {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Assets Catalog Manager", _labelGuiStyle);
        
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Base Assets Catalog", EditorStyles.boldLabel);
        
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(_baseFolder);
        
            EditorGUILayout.Space();
            _assetsCatalogFile = (TextAsset)EditorGUILayout.ObjectField("Assets Catalog File", _assetsCatalogFile, typeof(TextAsset), false);

            if (_assetsCatalogFile != null) {
                if (GUILayout.Button("Create Assets Folders", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                    ParseAssetsCatalogFile(_assetsCatalogFile);
                }
            }

        }

        private void ParseAssetsCatalogFile(TextAsset assetFile) {
            if(assetFile == null) return;

            var    stringReader = new StringReader(assetFile.text);
            string line;

            while ((line = stringReader.ReadLine()) != null) {

                var splittedLine = line.Split(',');
            
                var assetId   = splittedLine[0];
                var assetName = splittedLine[1];

                var newFolderName = $"{assetId}-{_textInfo.ToTitleCase(assetName)}";

                var newFolderPath = Path.Combine(_baseFolder, newFolderName);

                if (Directory.Exists(newFolderPath)) {
                    Debug.LogWarning($"{newFolderName} already exists!");
                } else {
                    AssetDatabase.CreateFolder(_baseFolder, newFolderName);
                    AssetDatabase.Refresh();
                }

                CreateSubFolder(newFolderPath, ANIMATIONS_SUBFODLER);
                CreateSubFolder(newFolderPath, MODELS_SUBFODLER);
                CreateSubFolder(newFolderPath, MATERIALS_SUBFODLER);
                CreateSubFolder(newFolderPath, PREFABS_SUBFODLER);
                CreateSubFolder(newFolderPath, TEXTURES_SUBFODLER);
            }
        }

        private void CreateSubFolder(string parentFolderPath, string subfolderName) {
            var subFolderPath = Path.Combine(parentFolderPath, subfolderName);
            if(Directory.Exists(subFolderPath)) return;
            AssetDatabase.CreateFolder(parentFolderPath, subfolderName);
            AssetDatabase.Refresh();
        }
    }
}
