﻿using UnityEditor;
using System.Text.RegularExpressions;

namespace PoketPet.AssetsCatalog.Editor {
    public class AssetPetsImporter : AssetPostprocessor {
        
        private string pattern = @"\d+$";
        private void OnPreprocessAnimation() {
            
            if(!assetPath.Contains("Pet")) return;

            var modelImporter = assetImporter as ModelImporter;
            modelImporter.animationType = ModelImporterAnimationType.Generic;
            
            var clipAnimations = modelImporter.defaultClipAnimations;
            
            foreach (var clipAnimation in clipAnimations) {
                
                var newClipAnimationName = clipAnimation.name.ToLower();
                var splittedAnimationName = newClipAnimationName.Split('_');
                
                var lastPart = splittedAnimationName[splittedAnimationName.Length - 1];

                var regex = new Regex(pattern);
                
                var match = regex.Match(lastPart);
                
                if (match.Success) {

                    var letters = lastPart.Substring(0, lastPart.Length - match.Length);
                    var numbers = lastPart.Substring(lastPart.Length - match.Length, match.Length);

                    var final = letters.Length > 0 ? $"{letters}_{numbers}" : $"{letters}{numbers}";

                    splittedAnimationName[splittedAnimationName.Length - 1] = final;
                }

                var name = $"{splittedAnimationName[0]}@";
                
                for (var i = 1; i < splittedAnimationName.Length; i++) {
                    if (splittedAnimationName.Length > i + 1) {
                        name += splittedAnimationName[i] + "_";
                    } else {
                        name += splittedAnimationName[i];
                    }
                }
                
                clipAnimation.name = name;
                clipAnimation.loop = true;
                clipAnimation.loopTime = true;
            }
            
            modelImporter.clipAnimations = clipAnimations;
        }
    }
}
