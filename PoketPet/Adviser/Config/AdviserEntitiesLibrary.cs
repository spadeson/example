using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PoketPet.Adviser.Config {
    [CreateAssetMenu(fileName = "AdviserEntitiesLibrary", menuName = "PoketPet/Adviser/AdviserEntitiesLibrary", order = 0)]
    public class AdviserEntitiesLibrary : ScriptableObject {
        public List<AdviserEntityConfig> adviserEntityConfigs = new List<AdviserEntityConfig>();

        public AdviserEntityConfig GetAdviserConfigByUserRoleAndTaskType(string userRole, string taskType) {
            var query = adviserEntityConfigs.Where(c => c.userRole == userRole && c.taskType == taskType).ToList();
            return query.First();
        }
    }
}
