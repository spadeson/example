using UnityEngine;

namespace PoketPet.Adviser.Config {
    [CreateAssetMenu(fileName = "AdviserConfig", menuName = "PoketPet/Adviser/AdviserConfig", order = 0)]
    public class AdviserEntityConfig : ScriptableObject {
        public string userRole;
        public string taskType;
        [MultilineAttribute]
        public string textContent;
        public GameObject visual;
    }
}
