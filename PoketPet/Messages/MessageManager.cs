using System;
using System.Collections.Generic;
using System.Linq;
using PoketPet.Network.API.REST.Messages;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Messages;
using PoketPet.Network.SocketBinds.Messages;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Messages {

    public static class MessageManager {
        
        public static List<MessageModel> Messages { get; private set; }
        public static event Action OnUpdate;

        public static void AddSocketListeners() {
            MessageTransitionSocketBind.Instance.OnAddAction += GetMessagesByUserId;
            MessageTransitionSocketBind.Instance.OnRemoveAction += id => {
                var value = Messages.Find(item => item.id == id);
                Messages.Remove(value);
                OnUpdate?.Invoke();
            };
        }

        public static int CountUnreadMessages() {
            var userId = UserProfileManager.CurrentUser.id;
            return Messages.Count(message => message.opened.IndexOf(userId) == -1);
        }

        public static void ReadMessageById(string messageId) {
            var userId = UserProfileManager.CurrentUser.id;
            var requestData = new RequestData<OpenMessageRequestData>(userId, new OpenMessageRequestData(){msgID = messageId}, "open_message");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("messages", requestData);
            operation.OnDataReceived += CheckStatusHandler;
            
            var value = Messages.Find(item => item.id == messageId);
            value.opened.Add(userId);
            OnUpdate?.Invoke();
        }
        
        public static void CloseMessageById(string messageId, bool confirm) {
            var userId = UserProfileManager.CurrentUser.id;
            var data   = new CloseMessageRequestData {msgID = messageId, value = confirm};

            var requestData = new RequestData<CloseMessageRequestData>(userId, data, "close_msg") {from = userId};

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("messages", requestData);
            operation.OnDataReceived += CheckStatusHandler;
        }
        
        public static void GetMessagesByUserId() {
            var userId = UserProfileManager.CurrentUser.id;
            var requestData = new RequestData<GetMessagesRequestData>(userId, new GetMessagesRequestData(), "get_messages");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("messages", requestData);
            operation.OnDataReceived += GetMessagesByUserIdHandler;
        }

        private static void GetMessagesByUserIdHandler(string data) {
            Debug.Log($"Get Messages JSON Response: {data}");
            
            var response = JsonUtility.FromJson<ResponseData<List<MessageModel>>>(data);
            CreateMessagesListByServerResponse(response.data);
        }

        private static void CreateMessagesListByServerResponse(List<MessageModel> messagesResponse) {
            Messages = messagesResponse;
            OnUpdate?.Invoke();
            Debug.Log("Messages List Updated");
        }
        
        private static void GetMessageById(string messageId) {
            
            var userId = UserProfileManager.CurrentUser.id;

            Debug.Log($"Get message: {messageId}, for User:{userId}");
            
            var requestData = new RequestData<GetMessageByIdRequestData>(userId, new GetMessageByIdRequestData(messageId), "get_message");
            
            var getMessageByIdOperation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("messages", requestData);
            getMessageByIdOperation.OnDataReceived += GetMessageByIdHandler;
        }

        private static void GetMessageByIdHandler(string data) {
            Debug.Log($"Get message JSON response: {data}");

            var responseData = JsonUtility.FromJson<ResponseData<MessageModel>>(data).data;
            var value = Messages.Find(item => item.id == responseData.id);
            Messages.Remove(value);
            Messages.Add(responseData);

            OnUpdate?.Invoke();
        }

        private static void CheckStatusHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
            
            }else {
                // TODO: Release Error Status 
            }
        }
    }
}