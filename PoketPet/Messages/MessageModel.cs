using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.REST.Messages {
    
    [Serializable]
    public class MessageModel {
        public string id;
        public string subtype;
        public string tag;
        public List<string> opened;
        public string from;
        public string to;
        public string img;
        public int create;
        public int end_time;
        public List<float> notification_times;
        public string notification_message;
        public string type;
        public List<string> pets;
        public string description;
        public DynamicData dynamic_data;
    }
    
    [Serializable]
    public class DynamicData {
        public string key;
        public string userID;
        public int bonus;
        public List<string> permissions;
        public Game game;
    }
    
    [Serializable]
    public class Game {
        public string type;
        public int game_level;
        public int user_level;
    }
}