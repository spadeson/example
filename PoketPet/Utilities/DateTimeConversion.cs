using System;

namespace PoketPet.Utilities {
    public static class DateTimeConversion {
        
        /// <summary>
        /// Unix Epoch.
        /// </summary>
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        
        /// <summary>
        /// Converts DotNet DataTime into Backend Timestamp in Hours.
        /// </summary>
        /// <param name="customDateTime">Custom DateTime.</param>
        /// <returns>Timestamp in Hours.</returns>
        public static int DateTimeToTimestamp(DateTime customDateTime) {
            var timestamp      = customDateTime.ToUniversalTime().Subtract(Epoch).TotalSeconds;
            var hoursTimestamp = timestamp / 60;
            return (int)hoursTimestamp;
        }
        
        /// <summary>
        /// Converts Backend Timestamp in Hours into DotNet DataTime.
        /// </summary>
        /// <param name="customTimestamp">Custom timestamp. PoketPet backend stores it in Hours.</param>
        /// <returns>DotNetDatTime.</returns>
        public static DateTime TimestampToDateTime(long customTimestamp) {
            var timeStampInSeconds = customTimestamp * 60;
            return Epoch.AddSeconds(timeStampInSeconds).ToLocalTime();
        }
        
        /// <summary>
        /// Converts Backend Timestamp in Hours into DotNet DataTime.
        /// </summary>
        /// <param name="customTimestamp">Custom timestamp. PoketPet backend stores it in Hours.</param>
        /// <returns></returns>
        public static DateTime TimestampToDateTime(int customTimestamp) {
            var timeStampInSeconds = customTimestamp * 60;
            return Epoch.AddSeconds(timeStampInSeconds).ToLocalTime();
        }
    }
}
