using UnityEngine;
using UnityEngine.Playables;

namespace PoketPet.Locations.Food {
    public class PetFoodControlAsset : PlayableAsset {

        public PetFoodControlBehaviour template;
        
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
            var playable = ScriptPlayable<PetFoodControlBehaviour>.Create(graph, template);
            return playable;
        }
    }
}
