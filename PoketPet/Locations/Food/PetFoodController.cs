using System;
using UnityEngine;

namespace PoketPet.Locations.Food {
    [ExecuteInEditMode]
    public class PetFoodController : MonoBehaviour {
        
        [SerializeField] private GameObject[] foodElements;
        public void SetFoodAmount(float petBowFill) {

            var elementsToActivate = Mathf.RoundToInt(foodElements.Length * petBowFill);

            for (var i = 0; i < foodElements.Length; i++) {
                foodElements[i].SetActive(false);
            }
            
            for (var i = 0; i < elementsToActivate; i++) {
                foodElements[i].SetActive(true);
            }
        }
    }
}
