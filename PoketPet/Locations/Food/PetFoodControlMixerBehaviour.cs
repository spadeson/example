using PoketPet.Locations.Food;
using UnityEngine.Playables;

public class PetFoodControlMixerBehaviour : PlayableBehaviour {
    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData) {
        PetFoodController trackBinding   = playerData as PetFoodController;
        float             finalAmount = 0f;

        if (!trackBinding) return;

        int inputCount = playable.GetInputCount(); //get the number of all clips on this track

        for (int i = 0; i < inputCount; i++) {
            float                                   inputWeight   = playable.GetInputWeight(i);
            ScriptPlayable<PetFoodControlBehaviour> inputPlayable = (ScriptPlayable<PetFoodControlBehaviour>) playable.GetInput(i);
            PetFoodControlBehaviour                 input         = inputPlayable.GetBehaviour();

            // Use the above variables to process each frame of this playable.
            finalAmount += input.fillAmount * inputWeight;
        }

        //assign the result to the bound object
        trackBinding.SetFoodAmount(finalAmount);
    }
}
