using UnityEngine;
using UnityEngine.Playables;

namespace PoketPet.Locations.Food {
    [System.Serializable]
    public class PetFoodControlBehaviour : PlayableBehaviour {
        public float fillAmount;
    }
}