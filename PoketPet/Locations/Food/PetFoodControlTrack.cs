using PoketPet.Locations.Food;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackClipType(typeof(PetFoodControlAsset))]
[TrackBindingType(typeof(PetFoodController))]
public class PetFoodControlTrack : TrackAsset {
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount) {
        return ScriptPlayable<PetFoodControlMixerBehaviour>.Create(graph, inputCount);
    }
}

