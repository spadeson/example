using UnityEngine.Playables;

namespace PoketPet.Locations.Doors {
    [System.Serializable]
    public class DoorControlBehaviour : PlayableBehaviour {
        public float openAmount;
    }
}
