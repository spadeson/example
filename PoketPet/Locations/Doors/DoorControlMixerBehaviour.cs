using PoketPet.Locations.Doors;
using UnityEngine.Playables;

public class DoorControlMixerBehaviour : PlayableBehaviour {
    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData) {
        var trackBinding = playerData as OpeningDoorsController;
        var finalAmount  = 0f;

        if (!trackBinding) return;

        var inputCount = playable.GetInputCount(); //get the number of all clips on this track

        for (var i = 0; i < inputCount; i++) {
            var inputWeight   = playable.GetInputWeight(i);
            var inputPlayable = (ScriptPlayable<DoorControlBehaviour>) playable.GetInput(i);
            var input         = inputPlayable.GetBehaviour();

            // Use the above variables to process each frame of this playable.
            finalAmount += input.openAmount * inputWeight;
        }

        //assign the result to the bound object
        trackBinding.OpenDoor(finalAmount);
    }
}

