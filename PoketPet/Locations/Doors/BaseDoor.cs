﻿using UnityEngine;

namespace PoketPet.Locations.Doors {
    public abstract class BaseDoor : MonoBehaviour {
        public abstract void OpenDoor(float value);
    }
}