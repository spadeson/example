﻿using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Locations.Doors {
    [ExecuteInEditMode]
    public class OpeningDoorsController : MonoBehaviour , IDoorsController {
        
        
        [SerializeField] private List<BaseDoor> doorsList  = new List<BaseDoor>();
        public void OpenDoor(float value) {
            for (int i = 0; i < doorsList.Count; i++) {
                doorsList[i].OpenDoor(value);
            }
        }
    }
}