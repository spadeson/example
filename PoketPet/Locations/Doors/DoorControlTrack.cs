using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace PoketPet.Locations.Doors {
    
    [TrackClipType(typeof(DoorControlAsset))]
    [TrackBindingType(typeof(OpeningDoorsController))]
    public class DoorControlTrack : TrackAsset {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount) {
            return ScriptPlayable<DoorControlMixerBehaviour>.Create(graph, inputCount);
        }
    }
}
