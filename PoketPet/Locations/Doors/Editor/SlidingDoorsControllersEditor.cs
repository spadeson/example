﻿using UnityEditor;

namespace PoketPet.Locations.Doors.Editor {
    [CustomEditor(typeof(SlidingDoorsController))]
    public class SlidingDoorsControllersEditor : UnityEditor.Editor {
        private float openPercent;
        public override void OnInspectorGUI() {
            
            base.OnInspectorGUI();
            
            var doorsController = (SlidingDoorsController)target;

            openPercent = EditorGUILayout.Slider("Open percent", openPercent, 0, 1);
            doorsController.OpenDoor(openPercent);
        }
    }
}