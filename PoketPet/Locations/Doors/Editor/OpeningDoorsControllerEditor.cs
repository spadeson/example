﻿using UnityEditor;

namespace PoketPet.Locations.Doors.Editor {
    [CustomEditor(typeof(OpeningDoorsController))]
    public class OpeningDoorsControllerEditor : UnityEditor.Editor {
        private float openPercent;
        public override void OnInspectorGUI() {
            
            base.OnInspectorGUI();
            
            var doorsController = (OpeningDoorsController)target;

            openPercent = EditorGUILayout.Slider("Open percent", openPercent, 0, 1);
            doorsController.OpenDoor(openPercent);
        }
    }
}