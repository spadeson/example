﻿namespace PoketPet.Locations.Doors {
    public interface IDoorsController {
        void OpenDoor(float value);
    }
}