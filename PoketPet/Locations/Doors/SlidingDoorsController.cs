﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Locations.Doors {
    public class SlidingDoorsController : MonoBehaviour, IDoorsController {
        [SerializeField] private List<BaseDoor> doorsList  = new List<BaseDoor>();
        public void OpenDoor(float value) {
            for (int i = 0; i < doorsList.Count; i++) {
                doorsList[i].OpenDoor(value);
            }
        }

        private void OnTriggerEnter(Collider other) {
            if (other.tag == "Pet") {
                OpenDoor(1);
            }
        }

        private void OnTriggerExit(Collider other) {
            if (other.tag == "Pet") {
                OpenDoor(0);
            }
            
        }
    }
}