﻿using UnityEngine;

namespace PoketPet.Locations.Doors {
    public class SlidingDoor :  BaseDoor {

        [SerializeField] private Vector3 initialPosition;
        [SerializeField] private Vector3 endPosition;
        
        public override void OpenDoor(float value) {
            transform.localPosition = Vector3.Lerp(initialPosition, endPosition, value);
        }
    }
}