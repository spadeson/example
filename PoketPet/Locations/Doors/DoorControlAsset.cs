using UnityEngine;
using UnityEngine.Playables;

namespace PoketPet.Locations.Doors {
    public class DoorControlAsset : PlayableAsset {
        
        public DoorControlBehaviour template;
        
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
            var playable = ScriptPlayable<DoorControlBehaviour>.Create(graph, template);
            return playable;
        }
    }
}
