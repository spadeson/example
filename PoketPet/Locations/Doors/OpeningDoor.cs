﻿using UnityEngine;

namespace PoketPet.Locations.Doors {
    public class OpeningDoor : BaseDoor {
        [SerializeField] private Vector3 initialRotation;
        [SerializeField] private Vector3 endRotation;

        public override void OpenDoor(float value) {
            transform.localRotation =
                Quaternion.Lerp(Quaternion.Euler(initialRotation), Quaternion.Euler(endRotation), value);
        }
    }
}