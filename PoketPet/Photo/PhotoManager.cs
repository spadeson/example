using System;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Files;
using PoketPet.User;
using UnityEditor;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Photo {
    
    public static class PhotoManager {
        
        private static readonly int MaxSize = 512;
        
        private static readonly string PhotoSavePath = Application.temporaryCachePath;
        
        private static string LastPhotoPath;

        private static string imageData = "";
        
        private static readonly Dictionary<string, Sprite> Avatars = new Dictionary<string, Sprite>();

        public static event Action OnPhotoUploaded ;
        public static event Action<Sprite> OnPhotoDownloaded ;

        public static void TakePhoto([CanBeNull] Action<Sprite> callback, int maxSize = -1, NativeCamera.PreferredCamera preferredCamera = NativeCamera.PreferredCamera.Default) {

            #if !UNITY_EDITOR
            var permission = NativeCamera.TakePicture(path => {
                
                LastPhotoPath = path;
                
                Debug.Log("Image path: " + LastPhotoPath);

                if (string.IsNullOrEmpty(LastPhotoPath)) return;

                //OnTextureDownloadedSuccess call delegate and create Sprite form texture 
                CoreMobile.Instance.NetworkManager.ImagesDownloader.DownloaderTexture("file://" + LastPhotoPath,texture => {
                    Debug.Log("Texture Downloaded Success");
                    var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
                    callback?.Invoke(sprite);
                });
                
            }, maxSize, false, preferredCamera);
            
            Debug.Log("Permission result: " + permission);
            
            #else
            
            LastPhotoPath = EditorUtility.OpenFilePanelWithFilters("Set Avatar", Environment.GetFolderPath(Environment.SpecialFolder.Desktop), new[]{"Image files", "png,jpg,jpeg"});
            
            Debug.Log("File path: " + LastPhotoPath);

            if (string.IsNullOrEmpty(LastPhotoPath)) return;

            var bytes = File.ReadAllBytes(LastPhotoPath);
            
            var sprite = CreatePhoto(bytes);
            
            callback?.Invoke(sprite);

            #endif
        }

        public static void GetLastPhoto(Action<Sprite> callback) {
            Debug.Log("Image path: " + LastPhotoPath);

            if (LastPhotoPath == null) return;
            
            //OnTextureDownloadedSuccess call delegate and create Sprite form texture 
            CoreMobile.Instance.NetworkManager.ImagesDownloader.DownloaderTexture("file://" + LastPhotoPath,texture => {
                Debug.Log("Texture Downloaded Success");
                var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
                callback?.Invoke(sprite);
            }); 
        }
        
        public static void PickPhotoFromGallery([CanBeNull] Action<Sprite> callback) {
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery( ( path ) =>
            {
                Debug.Log("Image path: " + path);
                if (path == null) return;

                //OnTextureDownloadedSuccess call delegate and create Sprite form texture 
                CoreMobile.Instance.NetworkManager.ImagesDownloader.DownloaderTexture("file://" + path,texture => {
                    Debug.Log("Texture Downloaded Success");
                    var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
                    callback?.Invoke(sprite);
                });
            }, "Select a PNG image", "image/png" );

            Debug.Log( "Permission result: " + permission );
        }
        
        public static void UploadPhoto(Texture2D texture) {
            Debug.Log("Upload Photo Request");
            var userId = UserProfileManager.CurrentUser.id;
            
            var requestData = new WWWForm();
            requestData.AddBinaryData("img", texture.EncodeToPNG());
            requestData.AddField("from", userId);
            requestData.AddField("action", "save_file");

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("filesForm", requestData);
            operation.OnDataReceived += result => {
                Debug.Log($"Upload Handler JSON Response: {result}");
                OnPhotoUploaded?.Invoke();
            };
        }
        
        public static void GetPhotoById(string fileId, Action<byte[]> callbackActionHandler) {
            Debug.Log($"GetPhotoById: Id - {fileId}");
            var userId = UserProfileManager.CurrentUser.id;
            var requestData = new RequestData<GetFileByIdRequestData>(userId, new GetFileByIdRequestData {id = fileId}, "get_by_id");
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("files", requestData);
            operation.OnDataReceivedBytes += callbackActionHandler;
        }
        
        public static Sprite CreatePhoto(byte[] data) {
            var texture = new Texture2D(2, 2);
            texture.LoadImage( data );
            return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        }

        /// <summary>
        /// Get users avatar as Unity Sprite from string avatar id.
        /// </summary>
        /// <param name="id">Avatar Id</param>
        /// <param name="handler">Callback function. Returns Unity Sprite object.</param>
        public static void GetAvatarById(string id, Action<Sprite> handler) {
            
            if (string.IsNullOrEmpty(id)) return;
            
            if (Avatars.ContainsKey(id)) {
                handler.Invoke(Avatars[id]);
            } else {
                GetPhotoById(id, bytes => {
                    var image = CreatePhoto(bytes);
                    Avatars[id] = image;
                    handler.Invoke(image);
                });
            }
        }
    }
}