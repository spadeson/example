using System;
using PoketPet.Tutorial.Onboarding.Data;

namespace PoketPet.Tutorial.Interfaces {
    public interface ITutorialManager {
        
        event Action OnTutorialInit;

        /// <summary>
        /// Initialize the Tutorial Manager. 
        /// </summary>
        void                      Initialize();

        /// <summary>
        /// Increases the current tutorial step for one. 
        /// </summary>
        void CompleteTutorialStep();
        
        /// <summary>
        /// Get's the next tutorial step data model.
        /// </summary>
        /// <returns>Tutorial Step Base Model Data.</returns>
        TutorialStepBaseModelData GetNextTutorialStepModel();
        
        /// <summary>
        /// Checks is the tutorial has been finished.
        /// </summary>
        /// <returns>Will return true if current tutorial step equals tutorial steps.</returns>
        bool                      IsTutorialFinished();
        
        /// <summary>
        /// Starts the tutorial.
        /// </summary>
        void                      StartTutorial();
        
        /// <summary>
        /// Switch to the next tutorial node.
        /// </summary>
        /// <param name="type">Tutorial type.</param>
        void                      MoveToNextTutorialNode(string type);
        
        /// <summary>
        /// Reset tutorial to the initial state.
        /// </summary>
        void                      ResetTutorial();
    }
}
