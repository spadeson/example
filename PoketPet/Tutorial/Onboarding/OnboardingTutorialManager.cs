using System;
using System.Collections.Generic;
using Core.Utilities;
using Firebase.Analytics;
using PoketPet.Tutorial.Interfaces;
using PoketPet.Tutorial.Onboarding.Data;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.LocalData;
using static PoketPet.Global.GlobalVariables.TutorialKeys;

namespace PoketPet.Tutorial.Onboarding {
    public class OnboardingTutorialManager : ITutorialManager {
        public event Action OnTutorialInit;

        private int _currentStep;

        private List<TutorialStepBaseModelData> _tutorialStepList;
        private OnboardingTutorialModelData _tutorialData;

        private ILocalDataManager LocalDataManager => CoreMobile.Instance.LocalDataManager;

        private string _currentType;

        #region Public Methods

        /// <inheritdoc/>
        public void Initialize() {
            var tutorialData = CoreMobile.Instance.FirebaseManager.FirebaseRemoteConfigManager.GetRemoteStringValue("tutorial_new");
            _tutorialData = JsonUtility.FromJson<OnboardingTutorialModelData>(tutorialData);
            OnTutorialInit?.Invoke();
        }

        /// <inheritdoc/>
        public void CompleteTutorialStep() {
            LocalDataManager.SaveLocalDataString(TUTORIAL_TYPE, _currentType);
            LocalDataManager.SaveLocalDataInt(TUTORIAL_STEP, _currentStep);
            _currentStep++;
        }

        /// <inheritdoc/>
        public TutorialStepBaseModelData GetNextTutorialStepModel() {

            if (_currentStep < GetCurrentNodeSteps()) {
                LocalDataManager.SaveLocalDataInt(TUTORIAL_STEP, _currentStep);
                SendAnalytic();
                return _tutorialStepList[_currentStep];
            }
            
            switch (_currentType) {
                case INTRO:
                    SetTutorialByType(AGE);
                    break;
                case AGE:
                    var nextTutorialType = UserAge < 18 ? CHILD : ADULT;
                    SetTutorialByType(nextTutorialType);
                    break;
            }
            
            _currentStep = 0;
            
            SendAnalytic();
            
            return _tutorialStepList[_currentStep];
        }

        /// <inheritdoc/>
        public bool IsTutorialFinished() {
            Debug.Log($"IsTutorialFinished: {_currentStep} - {GetCurrentNodeSteps()}");
            return (_currentType == CHILD || _currentType == ADULT) && _currentStep >= GetCurrentNodeSteps();
        }

        /// <inheritdoc/>
        public void StartTutorial() {
            
            CoreMobile.Instance.AnalyticsManager.LogEvent(FirebaseAnalytics.EventTutorialBegin);

            if (LocalDataManager.IsDataExists(TUTORIAL_TYPE)) {
                SetTutorialByType(LocalDataManager.GetLocalDataString(TUTORIAL_TYPE));
                _currentStep = LocalDataManager.GetLocalDataInt(TUTORIAL_STEP);
                _currentType = LocalDataManager.GetLocalDataString(TUTORIAL_TYPE);
            } else {
                MoveToNextTutorialNode(INTRO);
                _currentStep = 0;
                _currentType = INTRO;
            }
        }

        /// <inheritdoc/>
        public void MoveToNextTutorialNode(string type) {
            SetTutorialByType(type);
            _currentStep = 0;
            LocalDataManager.SaveLocalDataInt(TUTORIAL_STEP, _currentStep);
            LocalDataManager.SaveLocalDataString(TUTORIAL_TYPE, type);
        }

        /// <inheritdoc/>
        public void ResetTutorial() {
            LocalDataManager.SaveLocalDataBoolean(TUTORIAL_COMPLETE, false);
            LocalDataManager.SaveLocalDataString(TUTORIAL_TYPE, INTRO.ToLower());
            LocalDataManager.SaveLocalDataInt(TUTORIAL_STEP, 0);
        }

        #endregion

        #region Private Methods

        private void SetTutorialByType(string type) {
            _currentType = type;
            switch (type) {
                case INTRO:
                    _tutorialStepList = _tutorialData.GetIntroSteps();
                    break;
                case AGE:
                    _tutorialStepList = _tutorialData.GetAgeSteps();
                    break;
                case CHILD:
                    _tutorialStepList = _tutorialData.GetChildSteps();
                    break;
                case ADULT:
                    _tutorialStepList = _tutorialData.GetAdultSteps();
                    break;
            }
        }

        private void SendAnalytic() {
            
            //Set the numbers of parameters for the Firebase analytics.
            Parameter[] parameters = {
                    new Parameter("type", _currentType),
                    new Parameter("step", _currentStep.ToString("00")),
                    new Parameter("timestamp",  TimeUtil.GetTimestamp()) 
            };
            
            //Sends Firebase analytics event
            CoreMobile.Instance.AnalyticsManager.LogEvent("tutorial_progress_event",  parameters);
        }

        private int GetCurrentNodeSteps() {
            return _tutorialStepList.Count;
        }

        #endregion
    }
}
