using System;

namespace PoketPet.Tutorial.Onboarding.Data {
    [Serializable]
    public class TutorialMultiSubStepModelData {
        public string title;
        public string description;
        public string icon;
    }
}
