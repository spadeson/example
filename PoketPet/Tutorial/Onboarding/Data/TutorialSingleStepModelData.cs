using System;

namespace PoketPet.Tutorial.Onboarding.Data {
    
    [Serializable]
    public class TutorialSingleStepModelData : TutorialStepBaseModelData {
        public string title;
        public string description;
        public string icon;
    }
}