using System;
using System.Collections.Generic;
using System.Linq;

namespace PoketPet.Tutorial.Onboarding.Data {
    
    [Serializable]
    public class OnboardingTutorialModelData {
        public List<TutorialMultiStepModelData> intro;
        public List<TutorialSingleStepModelData> age;
        public List<TutorialSingleStepModelData> adult;
        public List<TutorialSingleStepModelData> child;

        public List<TutorialStepBaseModelData> GetIntroSteps() {
            return intro.Cast<TutorialStepBaseModelData>().ToList();
        }
        
        public List<TutorialStepBaseModelData> GetAgeSteps() {
            return age.Cast<TutorialStepBaseModelData>().ToList();
        }
        
        public List<TutorialStepBaseModelData> GetAdultSteps() {
            return adult.Cast<TutorialStepBaseModelData>().ToList();
        }
        
        public List<TutorialStepBaseModelData> GetChildSteps() {
            return child.Cast<TutorialStepBaseModelData>().ToList();
        }
    }
}