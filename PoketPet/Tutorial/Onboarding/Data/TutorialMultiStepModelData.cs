using System;

namespace PoketPet.Tutorial.Onboarding.Data {
    [Serializable]
    public class TutorialMultiStepModelData : TutorialStepBaseModelData {
        public TutorialMultiSubStepModelData[] content;
    }
}
