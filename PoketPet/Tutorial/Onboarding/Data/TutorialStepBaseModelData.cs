using System;

namespace PoketPet.Tutorial.Onboarding.Data {
    
    [Serializable]
    public class TutorialStepBaseModelData {
        public string widget;
        public string type;
    }
}

