using System;
using System.Collections.Generic;
using System.Linq;
using PoketPet.Tutorial.Onboarding.Data;

namespace PoketPet.Tutorial.FirstRun.Data {
    [Serializable]
    public class FirstRunTutorialModelData {
        public List<TutorialSingleStepModelData> nickname;
        public List<TutorialMultiStepModelData> petSchedule;
        
        public List<TutorialStepBaseModelData> GetNameSteps() {
            return nickname.Cast<TutorialStepBaseModelData>().ToList();
        }
        
        public List<TutorialStepBaseModelData> GetPetScheduleSteps() {
            return petSchedule.Cast<TutorialStepBaseModelData>().ToList();
        }
    }
}
