using System;
using System.Collections.Generic;
using PoketPet.Global;
using PoketPet.Tutorial.FirstRun.Data;
using PoketPet.Tutorial.Interfaces;
using PoketPet.Tutorial.Onboarding.Data;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Tutorial.FirstRun {
    public class FirstRunTutorialManager : ITutorialManager {
        
        public event Action OnTutorialInit;
        
        private static FirstRunTutorialModelData _tutorialData;
        private List<TutorialStepBaseModelData> _tutorialStepList;
        
        private int _currentStep;
        private string _currentType;
        
        /// <inheritdoc/>
        public void Initialize() {
            var tutorialData = CoreMobile.Instance.FirebaseManager.FirebaseRemoteConfigManager.GetRemoteStringValue("first_run");
            Debug.Log($"FirstRunTutorialData: {tutorialData}");
            _tutorialData = JsonUtility.FromJson<FirstRunTutorialModelData>(tutorialData);
            OnTutorialInit?.Invoke();
        }

        /// <inheritdoc/>
        public void CompleteTutorialStep() {
            _currentStep++;
        }

        /// <inheritdoc/>
        public TutorialStepBaseModelData GetNextTutorialStepModel() {
            if (_currentStep < GetCurrentNodeSteps()) {
                return _tutorialStepList[_currentStep];
            }
            
            switch (_currentType) {
                case GlobalVariables.TutorialKeys.NAME:
                    SetTutorialByType(GlobalVariables.TutorialKeys.PET_SCHEDULE);
                    break;
            }
            
            return _tutorialStepList[_currentStep];
        }

        /// <inheritdoc/>
        public bool IsTutorialFinished() {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public void StartTutorial() {
            MoveToNextTutorialNode(GlobalVariables.TutorialKeys.NAME);
            _currentStep = 0;
            _currentType = GlobalVariables.TutorialKeys.NAME;
        }

        /// <inheritdoc/>
        public void MoveToNextTutorialNode(string type) {
            SetTutorialByType(type);
        }

        /// <inheritdoc/>
        public void ResetTutorial() {
            throw new NotImplementedException();
        }
        
        #region Private Methods

        private void SetTutorialByType(string type) {
            _currentType = type;
            switch (type) {
                case GlobalVariables.TutorialKeys.NAME:
                    _tutorialStepList = _tutorialData.GetNameSteps();
                    break;
                case GlobalVariables.TutorialKeys.PET_SCHEDULE:
                    _tutorialStepList = _tutorialData.GetPetScheduleSteps();
                    break;
            }
        }
        
        private int GetCurrentNodeSteps() {
            return _tutorialStepList.Count;
        }
        
        #endregion
    }
}
