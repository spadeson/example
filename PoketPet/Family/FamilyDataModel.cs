using System;
using System.Collections.Generic;
using PoketPet.User;

namespace PoketPet.Family
{
    [Serializable]
    public class FamilyDataModel
    {
        public string petId;
        public List<UserModel> users;
    }
}