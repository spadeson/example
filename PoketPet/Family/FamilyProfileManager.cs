using System;
using System.Collections.Generic;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Users;
using PoketPet.Network.SocketBinds.Avatars;
using PoketPet.Network.SocketBinds.Users;
using PoketPet.User;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Photo;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Family {
    public static class FamilyProfileManager {
        public static event Action OnUsersListByPetIdUpdated;
        
        private static List<UserModel> _familyList = new List<UserModel>();
        private static Dictionary<string, FamilyDataModel> _familiesDictionary = new Dictionary<string, FamilyDataModel>();

        /// <summary>
        /// Initialization method.
        /// </summary>
        public static void Init() {
            _familiesDictionary = new Dictionary<string, FamilyDataModel>();
            ConvertUsersListToFamily(new List<UserModel>());
            AddSocketListeners();
        }

        public static void UpdateUserModel(string petId, UserModel user) {
            var family = _familiesDictionary[petId].users;
            var value = family.Find(item => item.id == user.id);
            
            if (value == null) return;
            family[family.IndexOf(value)] = user;
            OnUsersListByPetIdUpdated?.Invoke();
        }
        
        public static void GetUsersByPetId(string userId, string petId) {
            var requestData = new RequestData<GetUsersByPetIdRequestData>(userId, new GetUsersByPetIdRequestData(petId), "get_users_by_pet");
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("friends", requestData);
            operation.OnDataReceived += GetUsersByPetIdHandler;
        }
        
        private static void GetUsersByPetIdHandler(string data) {
            Debug.Log($"Get Users By Pet JSON Response : {data}");
            var response = JsonUtility.FromJson<ResponseData<List<UserModel>>>(data);
            _familyList = response.data;
            ConvertUsersListToFamily(response.data);
            LoadUsersAvatars();
            OnUsersListByPetIdUpdated?.Invoke();
        }

        public static Dictionary<string, FamilyDataModel> GetFamilies() {
            return _familiesDictionary;
        }

        public static UserModel GetFamilyMemberById(string id) {
            return _familyList.Find(item => item.id == id);
        }

        public static List<UserModel> GetFamiliesAsList() {
            return _familyList;
        }

        public static void DeleteFamily(string petId) {
            _familiesDictionary.Remove(petId);
        }

        private static void AddSocketListeners() {

            UserModelSocketBind.Instance.OnUpdateByUserAction += model => {
                if (model.id == UserProfileManager.CurrentUser.id) return;
                
                var value = _familyList.Find(item => item.id == model.id);
                _familyList.Remove(value);
                _familyList.Add(model);
                PhotoManager.GetAvatarById(model.avatar, sprite => { });
            };
            AvatarUpdateSocketBind.Instance.OnUserAvatarUpdatedAction += (id, avatar) => {
                if (id == UserProfileManager.CurrentUser.id) return;
                
                var familyMember = GetFamilyMemberById(id);
                familyMember.avatar = avatar;
            };
        }

        private static void ConvertUsersListToFamily(List<UserModel> userList) {
            var petId = PetsProfileManager.CurrentPet.id;
            _familiesDictionary[petId] = new FamilyDataModel {petId = petId, users = userList};
        }

        private static void LoadUsersAvatars() {
            var petId = PetsProfileManager.CurrentPet.id;
            foreach (var user in _familiesDictionary[petId].users) {
                PhotoManager.GetAvatarById(user.avatar, sprite => { });
            }
        }
    }
}