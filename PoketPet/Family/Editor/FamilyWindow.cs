using System.Collections.Generic;
using PoketPet.Family;
using PoketPet.Global;
using UnityEngine;
using UnityEditor;

public class FamilyWindow : EditorWindow {
    //private GUID userID;
    //    private string userID;
    //    private string familyName;
    private List<string> familyMembers;
    private FamilyDataModel _familyDataModel = null;


    [MenuItem("Window/Family Editor")]
    public static void showWindow() {
        var window = (FamilyWindow) GetWindow(typeof(FamilyWindow));
        window.Show();
    }

    private void OnFocus() {
        getFamilyProfile();
    }

    private void OnGUI() {
        GUILayout.Label("Family Editor", EditorStyles.boldLabel);

        if (_familyDataModel == null) return;

        //EditorGUILayout.LabelField("Family ID",  _familyDataModel.familyID);

        //_familyDataModel.familyName = EditorGUILayout.TextField("Family name", _familyDataModel.familyName);


        GUILayout.Space(20);
        if (GUILayout.Button("Save Family Profile", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
            UpdateFamilyProfile(_familyDataModel);
        }
    }

    private void getFamilyProfile() {
        var stringData = string.Empty;

        //Get PlayerPrefs
        if (PlayerPrefs.HasKey(GlobalVariables.LocalDataKeys.FAMILY_PROFILE_KEY)) {
            stringData = PlayerPrefs.GetString(GlobalVariables.LocalDataKeys.FAMILY_PROFILE_KEY);
        }

        if (!string.IsNullOrEmpty(stringData)) {
            _familyDataModel = JsonUtility.FromJson<FamilyDataModel>(stringData);
            Debug.Log("!!!");
        }
    }

    private void UpdateFamilyProfile(FamilyDataModel data) {
        var serializedData = JsonUtility.ToJson(data);
        if (PlayerPrefs.HasKey(GlobalVariables.LocalDataKeys.FAMILY_PROFILE_KEY)) {
            PlayerPrefs.DeleteKey(GlobalVariables.LocalDataKeys.FAMILY_PROFILE_KEY);
        }

        PlayerPrefs.SetString(GlobalVariables.LocalDataKeys.FAMILY_PROFILE_KEY, serializedData);
        PlayerPrefs.Save();
    }
}