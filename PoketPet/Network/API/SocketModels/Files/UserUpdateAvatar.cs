using System;

namespace PoketPet.Network.API.SocketModels.Files {
    
    [Serializable] 
    public class UserUpdateAvatar {
        public string avatar;
        public string id;
    }
}