using System;

namespace PoketPet.Network.API.SocketModels.Ping {
    [Serializable]
    public class StatusPing {
        public long ts;
    }
}