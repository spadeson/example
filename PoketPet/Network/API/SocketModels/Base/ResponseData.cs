using System;

namespace PoketPet.Network.API.SocketModels.Base {
    
    [Serializable]
    public class ResponseData<T>
    {
        public T data;
        public string action;
        public string status;
        public int time;
        public ErrorModel error;

    }
}