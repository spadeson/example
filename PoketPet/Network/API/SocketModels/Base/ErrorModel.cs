using System;

namespace PoketPet.Network.API.SocketModels.Base {
    
    [Serializable]
    public class ErrorModel {
        public string error_code;
        public string error;
    }
}