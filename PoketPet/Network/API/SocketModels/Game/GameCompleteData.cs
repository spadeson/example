using System;

namespace PoketPet.Network.API.SocketModels.Game {
    
    [Serializable]
    public class GameCompleteData {
        public UserGameComplete to;
        public UserGameComplete from;
    }
    [Serializable]
    public class UserGameComplete {
        public string id;
        public int value;
        public int bonus;
        public int score;
        public int level;
        public int bonus_exp;
    }
}