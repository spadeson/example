using System;

namespace PoketPet.Network.API.SocketModels.Users {
    
    [Serializable]
    public class UserScoreData {
        public string userID;
        public int bonus_score;
        public int score;
        public string action;
        public string target;
    }
}