using System;
using PoketPet.User;

namespace PoketPet.Network.API.SocketModels.Users {
    
    [Serializable] 
    public class UserUpdateData {
        public string userID;
        public UserModel vo;
    }
}