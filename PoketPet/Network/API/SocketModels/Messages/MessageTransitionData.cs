using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.SocketModels.Messages {
    
    [Serializable]
    public class MessageTransitionData {
        public string type;
        public string msgID;
        public List<string> pets;
    }
}