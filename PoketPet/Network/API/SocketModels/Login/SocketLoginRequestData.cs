using System;
using UnityEngine;

namespace PoketPet.Network.API.SocketModels.Login {
    
    [Serializable]
    public class SocketLoginRequestData {
        [SerializeField]public string id;
        [SerializeField]public string deviceId;
    }
}