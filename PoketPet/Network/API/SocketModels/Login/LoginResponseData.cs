using System;

namespace PoketPet.Network.API.SocketModels.Login {
    
    [Serializable]
    public class LoginResponseData {
        public string key;
        public string message;
    }
}