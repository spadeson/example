using System;

namespace PoketPet.Network.API.SocketModels.Tasks {
    
    [Serializable]
    public class TaskModelData {
        public string userID;
        public string taskID;
        public string petID;
    }
}