using System;

namespace PoketPet.Network.API.SocketModels.Tasks {
    
    [Serializable]
    public class TaskConfirmData {
        public string type;
        public string msgID;
        public string pets;
    }
}