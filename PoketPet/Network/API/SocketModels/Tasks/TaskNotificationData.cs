using System;

namespace PoketPet.Network.API.SocketModels.Tasks {
    
    [Serializable]
    public class TaskNotificationData {
        public string notification;
        public string taskID;
        public string petID;
    }
}