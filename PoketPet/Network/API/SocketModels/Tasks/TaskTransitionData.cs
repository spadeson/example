using System;

namespace PoketPet.Network.API.SocketModels.Tasks {
    
    [Serializable]
    public class TaskTransitionData {
        public string who;
        public string taskID;
        public string petID;
    }
}