using System;
using PoketPet.Network.API.RESTModels.Pets;

namespace PoketPet.Network.API.SocketModels.Pets {
    
    [Serializable]
    public class PetUpdateData {
        public PetResponseData vo;
    }
}