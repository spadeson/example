using System;

namespace PoketPet.Network.API.SocketModels.Pets {
    
    [Serializable]
    public class PetWatcherData {
        public string permission;
        public string userID;
        public string petID;
    }
}