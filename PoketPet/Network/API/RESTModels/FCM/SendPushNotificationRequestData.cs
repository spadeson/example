using System;

namespace PoketPet.Network.API.RESTModels.FCM {
    
    [Serializable]
    public class SendPushNotificationRequestData {
        public string to;
        public string title;
        public string body;
    }
}