using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.FCM {
    [Serializable]
    public class FCMSendPushRequestModel : IRequestData {
        public string to;
        public string title;
        public string body;
        public object data;
    }
}
