using System;

namespace PoketPet.Network.API.RESTModels.FCM {
    
    [Serializable]
    public class FCMTokenData {
        public string token;
    }
}