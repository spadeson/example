using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.FCM {
    [Serializable]
    public class FCMAddTokenRequestData : IRequestData { }
}
