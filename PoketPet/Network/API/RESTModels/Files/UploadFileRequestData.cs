using System;

namespace PoketPet.Network.API.RESTModels.Files {
    
    [Serializable]
    public class UploadFileRequestData {
        public string name;
        public string file;
    }
}