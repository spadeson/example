using System;

namespace PoketPet.Network.API.RESTModels.Files {
    
    [Serializable]
    public class GetFileByIdRequestData {
        public string id;
    }
}