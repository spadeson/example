using PoketPet.Network.API.RESTModels.Base;
using UnityEngine;

namespace PoketPet.Network.API.RESTModels.Files {
    
    public class SetUserAvatarData : IFormRequestData {

        public string from;
        public byte[] image;
        public string action;
        
        public WWWForm GetForm() {
            var form = new WWWForm();
            form.AddField("from", from);
            form.AddBinaryData("img", image);
            form.AddField("action", action);
            return form;
        }
    }
}