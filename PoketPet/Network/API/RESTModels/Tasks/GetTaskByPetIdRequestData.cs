using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class GetTaskByPetIdRequestData {
        public string from;
        public string petID;
        
        public GetTaskByPetIdRequestData(string from, string petId) {
            
            this.from = from;
            this.petID = petId;
        }
    }
}