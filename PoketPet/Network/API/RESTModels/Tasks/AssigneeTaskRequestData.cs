using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class AssigneeTaskRequestData {
        public string userID;
        public string taskID;
        
        public AssigneeTaskRequestData(string id, string user) {
            this.userID = user;
            this.taskID = id;
        }
    }
}