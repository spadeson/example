using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    [Serializable]
    public class TaskLifeFlow {
        public int create;
        public int notification;
        public int best;
        public int late;
        public int expire;
    }
}