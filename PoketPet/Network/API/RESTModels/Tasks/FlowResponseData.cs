using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.RESTModels.Tasks {

    [Serializable]
    public class FlowResponseData {
        public string type;
        public int day_shift_time;
        public int duration;
        public string repeat_type;
        public int age_category;
        public string status;
        public string custom_status;
        public FlowRecommended recommended;
        public List<int> every_day;
        public List<int> week;
        public List<int> dates;
        public List<int> month;
        public List<int> year;
        public List<History> history;
        public FlowTaskLife task_life;
        public string id;
        public List<string> completedStatus;
        public List<DateTime> localEveryDayTime = new List<DateTime>();
    }

    [Serializable]
    public class FlowRecommended {
        public List<int> every_day;
        public List<int> week;
        public List<int> dates;
        public int frequency;
    }

    [Serializable]
    public class FlowTaskLife {
        public FlowTaskLifeItem create;
        public FlowTaskLifeItem notification;
        public FlowTaskLifeItem best;
        public FlowTaskLifeItem late;
        public FlowTaskLifeItem expire;
    }

    [Serializable]
    public class FlowTaskLifeItem {
        public int time;
        public string next;
        public string msg;
        public bool push_msg;
        public FlowTaskLifeItemResult close;
        public FlowTaskLifeItemResult fail;
        public bool transition;
    }
    
    [Serializable]
    public class FlowTaskLifeItemResult {
        public FlowTaskLifeItemResultUser user;
        public List<float> pet;
    }
    
    [Serializable]
    public class FlowTaskLifeItemResultUser {
        public float loyalty;
        public float score;
    }
    
    [Serializable]
    public class History {
        public string userID;
        public float time;
        public string assignee;
    }
}