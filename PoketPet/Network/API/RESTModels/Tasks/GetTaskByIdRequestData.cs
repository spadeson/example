using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class GetTaskByIdRequestData {
        public string taskID;
        
        public GetTaskByIdRequestData(string id)
        {
            this.taskID = id;
        }
    }

}