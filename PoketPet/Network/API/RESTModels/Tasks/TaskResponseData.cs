using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class TaskResponseData {
        public TaskMeta meta;
        public string type;
        public string id;
        public string petID;
        public string assignee;
        public string assignee_flow;
        public string owner_confirm;
        public string owner_confirm_flow;
        public string locked;
        public string creator;
        public string close_link;
        public string close_link_value;

        public TaskLifeFlow life_flow;
    }
}