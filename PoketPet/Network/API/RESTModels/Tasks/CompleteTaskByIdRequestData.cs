using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class CompleteTaskByIdRequestData {
        public string taskID;
        
        public CompleteTaskByIdRequestData(string id)
        {
            this.taskID = id;
        }
    }
}