using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class GetFlowsByPetIdRequestData {
        public string petID;
    }
}