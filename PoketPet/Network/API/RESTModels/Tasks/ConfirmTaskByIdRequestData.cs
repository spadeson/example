using PoketPet.Network.API.RESTModels.Base;
using UnityEngine;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    public class ConfirmTaskByIdRequestData : IFormRequestData {
            
        public string action;
        public string from;
        public string taskID;
        public byte[] image;
        
        public WWWForm GetForm() {
            var form = new WWWForm();
            form.AddField("action", action);
            form.AddField("from", from);
            form.AddField("taskID", taskID);
            form.AddBinaryData("img", image);
            
            return form;
        }
    }
}