using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    [Serializable]
    public class  TaskMeta {
        public string title;
        public string description;
        public string type;
    }
}