using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    [Serializable]
    public class GetFlowByIdRequestData {
        public string taskID;
    }
}