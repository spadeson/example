using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class GetTasksByPetIdResponseData {
        public List<string> keys;
        public List<TaskResponseData> values;
    }
}