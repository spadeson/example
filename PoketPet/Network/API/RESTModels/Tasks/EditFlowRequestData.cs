using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class EditFlowRequestData {
        public string taskID;
        public List<int> flow;
    }
}