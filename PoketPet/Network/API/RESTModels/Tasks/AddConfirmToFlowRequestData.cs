using System;

namespace PoketPet.Network.API.RESTModels.Tasks {
    
    [Serializable]
    public class AddConfirmToFlowRequestData {
        public string taskID;
    }
}