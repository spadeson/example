using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.DynamicLinks {
    [Serializable]
    public class DynamicLinkDataWaitForInvite : IRequestData {
        public string mail;
        public string type;
        public string from;
        public string pro;
        public string time;
    }
}

