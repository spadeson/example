using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.DynamicLinks {
    [Serializable]
    public class DynamicLinkDataInvite : IRequestData {
        public string mail;
        public string type;
        public string petID;
        public string from;
        public string permissions;
        public string pro;
        public string time;
    }
}
