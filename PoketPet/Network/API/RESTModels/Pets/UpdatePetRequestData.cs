using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.Pets {
    [Serializable]
    public class UpdatePetRequestData : IRequestData {
        public string id;
        public string nickname;
        public string type;
        public string breed;
        public int birth_day;
        public string gender;
        public float weight;

        public UpdatePetRequestData(string id, string nickname, string type, string breed, int birthDay, string gender, int weight) {
            this.id = id;
            this.nickname = nickname;
            this.type = type;
            this.breed = breed;
            birth_day = birthDay;
            this.gender = gender;
            this.weight = weight;
        }
        
        public UpdatePetRequestData(PetResponseData petsDataModel) {
            id = petsDataModel.id;
            nickname = petsDataModel.nickname;
            type = petsDataModel.type;
            breed = petsDataModel.breed;
            birth_day = (int)petsDataModel.birth_day;
            gender = petsDataModel.gender;
            weight = petsDataModel.weight;
        }
    }
}
