using PoketPet.Network.API.RESTModels.Base;
using UnityEngine;

namespace PoketPet.Network.API.RESTModels.Pets {
    
    public class SetPetAvatarData : IFormRequestData {

        public string from;
        public byte[] image;
        public string action;
        public string petId;
        
        public WWWForm GetForm() {
            var form = new WWWForm();
            form.AddField("from", from);
            form.AddBinaryData("img", image);
            form.AddField("action", action);
            form.AddField("petID", petId);
            return form;
        }
    }
}