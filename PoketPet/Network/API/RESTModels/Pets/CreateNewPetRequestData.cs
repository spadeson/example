using System;

namespace PoketPet.Network.API.RESTModels.Pets {
    
    [Serializable]
    public class CreateNewPetRequestData {
        public string nickname;
        public string type;
        public string breed;
        public long birth_day;
        public string gender;
        public float weight;
    }
}