using System;

namespace PoketPet.Network.API.RESTModels.Pets {
    
    [Serializable]
    public class GetPetByIdRequestData {
        public string petID;
        
        public GetPetByIdRequestData(string id)
        {
            this.petID = id;
        }
    }
}