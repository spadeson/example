using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.RESTModels.Pets {
    
    [Serializable]
    public class PetResponseData {
        
        public string nickname;
        public string type;
        public string breed;
        public long birth_day;
        public string age_category;
        public string gender;
        public string body_condition;
        public float weight;
        public int best_weight;
        public string id;
        public List<PetUser> users;
        public PetCategories categories;
        public string avatar;
    }
    
    [Serializable]
    public class PetUser {
        public string id;
        public List<string> status;
        public float loyalty;
    }

    [Serializable]
    public class PetCategories {
        public PetCategoryNeeds needs;
        public PetCategoryHealth health;
        public PetCategoryActivity activity;
        public PetCategorySocial social;
    }

    [Serializable]
    public class PetCategorySocial {
        public string value;
        public PetCategorySocialSub sub;
    }

    [Serializable]
    public class PetCategoryActivity {
        public string value;
        public PetCategoryActivitySub sub;
    }

    [Serializable]
    public class PetCategoryHealth {
        public string value;
        public PetCategoryHealthSub sub;
    }

    [Serializable]
    public class PetCategoryNeeds {
        public string value;
        public PetCategoryNeedsSub sub;
    }
    
    [Serializable]
    public class PetCategoryActivitySub {
        public float play;
    }
    
    [Serializable]
    public class PetCategoryHealthSub {
        public float parasites;
        public float healthy;
        public float nutrition;
    }
    
    [Serializable]
    public class PetCategoryNeedsSub {
        public float food;
        public float toilet;
        public float walking;
    }
    
    [Serializable]
    public class PetCategorySocialSub {
        public float socialisation;
    }
}