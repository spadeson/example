using System;

namespace PoketPet.Network.API.RESTModels.Settings {
    [Serializable]
    public class SettingsDataModel {
        public string time_format;
        public string weight_units;
        public string localisation;
        public bool push_active;
    }
}
