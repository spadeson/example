using System;
using Core.NetworkSubsystem.Data.Responce;

namespace PoketPet.Network.API.RESTModels.Base
{

    [Serializable]
    public class ResponseData<T> : IResponseData
    {
        public T data;
        public string status;
        public ErrorModel error;

    }
}