﻿using System;

namespace PoketPet.Network.API.RESTModels.Base {
    
    [Serializable]
    public class ErrorModel {
        public string error_code;
        public string error;
    }
}