using UnityEngine;

namespace PoketPet.Network.API.RESTModels.Base {
    public interface IFormRequestData {
        WWWForm GetForm();
    }
}