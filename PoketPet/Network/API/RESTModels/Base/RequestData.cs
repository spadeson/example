using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.Base
{
    [Serializable]
    public class RequestData<T> : IRequestData {
        
        public string from;
        public T data;
        public string action;

        public RequestData()
        {
        }

        public RequestData(T data, string action)
        {
            this.data = data;
            this.action = action;
        }
        
        public RequestData(string from, T data, string action)
        {
            this.from = from;
            this.data = data;
            this.action = action;
        }
    }
}