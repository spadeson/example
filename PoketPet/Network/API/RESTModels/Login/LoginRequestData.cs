using System;
using Core.NetworkSubsystem.Data.Request;

namespace PoketPet.Network.API.RESTModels.Login
{
    [Serializable]
    public class LoginRequestData : IRequestData
    {
        public string token;

        public LoginRequestData()
        {
        }

        public LoginRequestData(string token)
        {
            this.token = token;
        }
    }
}