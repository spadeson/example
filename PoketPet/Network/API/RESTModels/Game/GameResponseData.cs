using System;

namespace PoketPet.Network.API.RESTModels.Game {
    
    [Serializable]
    public class GameResponseData {
        public string type;
        public string petID;
        public int level;
    }
}