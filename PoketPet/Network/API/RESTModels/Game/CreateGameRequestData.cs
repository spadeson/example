using PoketPet.Network.API.RESTModels.Base;
using UnityEngine;

namespace PoketPet.Network.API.RESTModels.Game {
    
    public class CreateGameRequestData : IFormRequestData {

        public string from;
        public string petId;
        public string type;
        public int value;
        public byte[] image;
        public string action;
        
        public WWWForm GetForm() {
            var form = new WWWForm();
            form.AddField("from", from);
            form.AddField("petID", petId);
            form.AddField("type", type);
            form.AddField("value", value);
            form.AddBinaryData("img", image);
            form.AddField("action", action);
            return form;
        }
    }
}