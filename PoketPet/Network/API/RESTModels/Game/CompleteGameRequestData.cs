using System;

namespace PoketPet.Network.API.RESTModels.Game {
    
    [Serializable]
    public class CompleteGameRequestData {
        public string msgID;
        public int value;
    }
}