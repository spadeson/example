using System;

namespace PoketPet.Network.API.RESTModels.Game {
    
    [Serializable]
    public class GamesAwaitingCreationRequestData {
        public string petID;
    }
}