using System;

namespace PoketPet.Network.API.RESTModels.Users {
    
    [Serializable]
    public class GetUserByIdRequestData {
        public string userID;
    }
}