using System;

namespace PoketPet.Network.API.RESTModels.Users {
    
    [Serializable]
    public class GetUsersByPetIdRequestData {
        public string petID;
        
        public GetUsersByPetIdRequestData(string id)
        {
            this.petID = id;
        }
    }
}