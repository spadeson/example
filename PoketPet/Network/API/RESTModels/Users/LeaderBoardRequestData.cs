using System;

namespace PoketPet.Network.API.RESTModels.Users {
    
    [Serializable]
    public class LeaderBoardRequestData {
        public string petID;
    }
}