using System;
using System.Collections.Generic;

namespace PoketPet.Network.API.RESTModels.Invites {
    
    [Serializable]
    public class InviteUserRequestData {
        public string from;
        public string to;
        public string petID;
        public List<string> permissions;
    }
}