using System;

namespace PoketPet.Network.API.RESTModels.Invites {
    [Serializable]
    public class WaitForInviteByEmailRequest {
        public string to;

        public WaitForInviteByEmailRequest(string email) {
            to = email;
        }
    }
}
