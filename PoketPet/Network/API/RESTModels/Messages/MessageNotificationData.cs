using System;

namespace PoketPet.Network.API.RESTModels.Messages {
    
    [Serializable]
    public class MessageNotificationData {
        public string userID;
        public int bonus;
    }
}