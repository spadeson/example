using System;

namespace PoketPet.Network.API.RESTModels.Messages {
    
    [Serializable]
    public class GetMessageByIdRequestData {
        
        public string msgID;

        public GetMessageByIdRequestData(string messageId) {
            msgID = messageId;
        }
    }
}