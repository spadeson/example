using System;

namespace PoketPet.Network.API.RESTModels.Messages {
    
    [Serializable]
    public class CloseMessageRequestData {
        public string msgID;
        public bool value;
    }
}