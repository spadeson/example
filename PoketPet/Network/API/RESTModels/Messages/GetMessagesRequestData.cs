using System;

namespace PoketPet.Network.API.RESTModels.Messages {
    
    [Serializable]
    public class GetMessagesRequestData {
        public string petID;
    }
}