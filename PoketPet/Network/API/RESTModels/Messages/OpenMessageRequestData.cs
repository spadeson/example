using System;

namespace PoketPet.Network.API.RESTModels.Messages {
    
    [Serializable]
    public class OpenMessageRequestData {
        public string msgID;
    }
}