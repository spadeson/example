﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Network.Dispatchers {
    public class Dispatcher : MonoBehaviour {
        public static Dispatcher Instance { get; private set; }
        public List<Action> PendingActions { get; } = new List<Action>();
        
        private void Awake() {
            if (Instance != null && Instance != this) {
                Destroy(gameObject);
            } else {
                Instance = this;
            }

            DontDestroyOnLoad(gameObject);
        }

        private void Update() {
            InvokePending();
        }

        public void Invoke(Action fn) {
            lock (PendingActions) {
                PendingActions.Add(fn);
            }
        }

        private void InvokePending() {
            lock (PendingActions) {
                foreach (var action in PendingActions) {
                    action();
                    Debug.Log($"{action.GetHashCode()}");
                }

                PendingActions.Clear();
            }
        }
    }
}