using System;
using PoketPet.Family;
using PoketPet.Messages;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Network.SocketBinds.Connection;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Schedule;
using PoketPet.Task;
using PoketPet.User;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Network {

    public class NetworkConnectionManager : MonoBehaviour {
        //public static NetworkConnectionManager Instance { get; private set; }
        
        private string _systemUniqueId;
        
        void Awake() {
            _systemUniqueId = SystemInfo.deviceUniqueIdentifier;

            ConnectionStatusSocketBind.Instance.OnConnect += OnSocketConnectedHandler;
            ConnectionStatusSocketBind.Instance.OnDisconnect += OnSocketDisconnectedHandler;
        }

        private void OnDestroy() {
            ConnectionStatusSocketBind.Instance.OnConnect -= OnSocketConnectedHandler;
            ConnectionStatusSocketBind.Instance.OnDisconnect -= OnSocketDisconnectedHandler;
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus) {
                CoreMobile.Instance.SocketNetworkManager.Disconnect();
            }
            else {
                CoreMobile.Instance.SocketNetworkManager.Connect();
            }
        }

        private void OnSocketDisconnectedHandler(object o) {
            Debug.Log($"socket disconnect timestamp: {DateTime.Now.ToLongTimeString()}");
            PlayerPrefs.SetString("socket_disconnect_timestamp", DateTime.Now.ToLongTimeString());
            PlayerPrefs.Save();
        }

        private void OnSocketConnectedHandler(object o) {
            LoginSocket();
        }
           
        private void LoginSocket() {
            LoginSocketBind.Instance.Login(UserProfileManager.CurrentUser.id, _systemUniqueId);
            LoginSocketBind.Instance.OnCompleteAction -= RestoreData;
            LoginSocketBind.Instance.OnCompleteAction += RestoreData;
            LoginSocketBind.Instance.OnKickAction -= LoginSocket;
            LoginSocketBind.Instance.OnKickAction += LoginSocket;
        }
        
        private static void RestoreData() {
            Debug.Log("RestoreData");
            UserProfileManager.OnUserDataModelUpdated += OnCurrentUserUpdated;
            UserProfileManager.GetCurrentUser();
            MessageManager.GetMessagesByUserId();
        }

        private static void OnCurrentUserUpdated() {
            Debug.Log($"OnCurrentUserUpdated. Pets: {UserProfileManager.CurrentUser.pets}");
            UserProfileManager.OnUserDataModelUpdated -= OnCurrentUserUpdated;
            if (UserProfileManager.CurrentUser.pets != null && UserProfileManager.CurrentUser.pets.Count > 0) {
                PetsProfileManager.GetPetById(UserProfileManager.CurrentUser.pets[0].id);
                PetsProfileManager.OnPetReceived += OnPetReceivedUpdated;
            }
            else {
                //TODO: NO PET - NO VIEW
            }
        }
        
        private static void OnPetReceivedUpdated(PetResponseData pet) {
            PetsProfileManager.OnPetReceived -= OnPetReceivedUpdated;
            FamilyProfileManager.GetUsersByPetId(UserProfileManager.CurrentUser.id, PetsProfileManager.CurrentPet.id);
            TaskManager.GetTasksByPetId(PetsProfileManager.CurrentPet.id);
            ScheduleManager.GetScheduleByPetId(pet.id);
        }
    }
}