using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.API.SocketModels.Users;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Users {
    public class UserScoreSocketBind : ISocketBind {
        private static UserScoreSocketBind _instance;
        public static UserScoreSocketBind Instance => _instance ?? (_instance = Init());

        private static UserScoreSocketBind Init() {
            var bind = new UserScoreSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(USER_SCORE_EVENT, bind);
            return bind;
        }
        
        public event Action<UserScoreData> OnUpdateAction;

        public void SetSocket(Socket socket) {
            socket.On(USER_SCORE_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<UserScoreData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {USER_SCORE_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case  UPDATE:
                            OnUpdateAction?.Invoke(response.data);
                            break;
                    }
                });
            });
        }
    }
}