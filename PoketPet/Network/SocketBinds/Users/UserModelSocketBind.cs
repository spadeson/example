using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.API.SocketModels.Users;
using PoketPet.Network.Dispatchers;
using PoketPet.User;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Users {
    public class UserModelSocketBind : ISocketBind {
        private static UserModelSocketBind _instance;
        public static UserModelSocketBind Instance => _instance ?? (_instance = Init());

        private static UserModelSocketBind Init() {
            var bind = new UserModelSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(USER_MODEL_EVENT, bind);
            return bind;
        }
        
        public event Action<UserModel> OnUpdateByUserAction;

        public void SetSocket(Socket socket) {
            socket.On(USER_MODEL_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<UserUpdateData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {USER_MODEL_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case  USER_UPDATE:
                            OnUpdateByUserAction?.Invoke(response.data.vo);
                            break;
                    }
                });
            });
        }
    }
}