using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Game;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Games {
    public class GameCompleteSocketBind : ISocketBind {
        private static GameCompleteSocketBind _instance;
        public static GameCompleteSocketBind Instance => _instance ?? (_instance = Init());

        private static GameCompleteSocketBind Init() {
            var bind = new GameCompleteSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(GAME_COMPLETE, bind);
            return bind;
        }
        
        public event Action<bool, GameCompleteData> OnGameWinAction;
        public event Action<bool, GameCompleteData> OnGameLoseAction;

        public void SetSocket(Socket socket) {
            socket.On(GAME_COMPLETE, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<GameCompleteData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {GAME_COMPLETE} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case WIN:
                            OnGameWinAction?.Invoke(true, response.data);
                            break;
                        case LOSE:
                            OnGameLoseAction?.Invoke(false, response.data);
                            break;
                    }
                });
            });
        }
    }
}