using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Games {
    public class GamesAwaitingSocketBind : ISocketBind {
        private static GamesAwaitingSocketBind _instance;
        public static GamesAwaitingSocketBind Instance => _instance ?? (_instance = Init());

        private static GamesAwaitingSocketBind Init() {
            var bind = new GamesAwaitingSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(GAMES_AWAITING_EVENT, bind);
            return bind;
        }
        
        public event Action OnUpdateAction;

        public void SetSocket(Socket socket) {
            socket.On(GAMES_AWAITING_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<string>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {GAMES_AWAITING_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case UPDATE:
                            OnUpdateAction?.Invoke();
                            break;
                    }
                });
            });
        }
    }
}