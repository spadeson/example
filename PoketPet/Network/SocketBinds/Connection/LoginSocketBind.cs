using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Files;
using PoketPet.Network.API.SocketModels.Login;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using PoketPet.User;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Connection {
    public class LoginSocketBind : ISocketBind {
        private static LoginSocketBind _instance;
        public static LoginSocketBind Instance => _instance ?? (_instance = Init());

        private static LoginSocketBind Init() {
            var bind = new LoginSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(LOGIN_EVENT, bind);
            return bind;
        }

        public event Action OnKickAction;
        public event Action OnCompleteAction;

        private Socket _socket;

        public void SetSocket(Socket socket) {
            _socket = socket;
            
            socket.On("login", (loginData) => {
                var response = JsonUtility.FromJson<LoginResponseData>(loginData.ToString());
                Debug.Log($"<color=orange>socket login {response.key}</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.key) {
                        case "kick":
                            Debug.Log($"<color=orange>socket login kick {response.message} {DateTime.Now}reconnect</color>");
                            OnKickAction?.Invoke();
                            break;
                        case "ko":
                            Debug.Log($"<color=orange>socket login ko message {response.message} {DateTime.Now}</color>");
                            break;
                        case "ok":
                            //if (_isSocketReconnected) break;
                            //_isSocketReconnected = true;
                            OnCompleteAction?.Invoke();
                            break;
                    }
                });
            });
        }

        public void Login(string userId, string systemUniqueId) {
            var loginRequestData = new SocketLoginRequestData {
                id = userId,
                deviceId = systemUniqueId
            };
            var serializedData = JsonUtility.ToJson(loginRequestData);
            Debug.Log($"<color=orange>socket connect {serializedData}</color>");
            _socket.Emit("login", serializedData);
        }
    }
}