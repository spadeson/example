using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Files;
using PoketPet.Network.API.SocketModels.Login;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using PoketPet.User;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Connection {
    public class ConnectionStatusSocketBind : ISocketBind {
        private static ConnectionStatusSocketBind _instance;
        public static ConnectionStatusSocketBind Instance => _instance ?? (_instance = Init());

        private static ConnectionStatusSocketBind Init() {
            var bind = new ConnectionStatusSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(CONNECTIONS_EVENT, bind);
            return bind;
        }
        
        public event Action<object> OnDisconnect;
        public event Action<object> OnConnect;

        public void SetSocket(Socket socket) {
            socket.On(Socket.EVENT_CONNECT, (e) => {
                Debug.Log($"<color=orange>socket connect: {e}</color>");
                Dispatcher.Instance.Invoke(() => { OnConnect?.Invoke(e); });
            });
            socket.On(Socket.EVENT_DISCONNECT, (e) => {
                Debug.Log($"<color=orange>socket disconnect: {e} {DateTime.Now}</color>");
                Dispatcher.Instance.Invoke(() => { OnDisconnect?.Invoke(e); });
            });
            socket.On(Socket.EVENT_ERROR, (e) => {
                Debug.Log($"socket event error: {e}");
            });
            socket.On(Socket.EVENT_MESSAGE, (e) => {
                Debug.Log($"socket event message: {e}");
            });
            socket.On(Socket.EVENT_CONNECT_ERROR, (e) => {
                Debug.Log($"socket connect error: {e}");
            });
            socket.On(Socket.EVENT_CONNECT_TIMEOUT, (e) => {
                Debug.Log($"socket connect timeout: {e}");
            });
            socket.On(Socket.EVENT_RECONNECT, (e) => {
                Debug.Log($"socket connect timeout: {e}");
            });
            socket.On(Socket.EVENT_RECONNECT_ERROR, (e) => {
                Debug.Log($"socket reconnect error: {e}");
            });
            socket.On(Socket.EVENT_RECONNECT_FAILED, (e) => {
                Debug.Log($"socket reconnect failed: {e}");
            });
            socket.On(Socket.EVENT_RECONNECT_ATTEMPT, (e) => {
                Debug.Log($"socket reconnect attempt: {e}");
            });
            socket.On(Socket.EVENT_RECONNECTING, (e) => {
                Debug.Log($"socket reconnecting: {e}");
            });
        }
    }
}