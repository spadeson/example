using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Messages;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Messages {
    public class MessageTransitionSocketBind : ISocketBind {
        private static MessageTransitionSocketBind _instance;
        public static MessageTransitionSocketBind Instance => _instance ?? (_instance = Init());

        private static MessageTransitionSocketBind Init() {
            var bind = new MessageTransitionSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(MESSAGE_TRANSITION_EVENT, bind);
            return bind;
        }
        
        public event Action OnAddAction;
        public event Action<string> OnRemoveAction;
        
        public void SetSocket(Socket socket) {
            socket.On(MESSAGE_TRANSITION_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<MessageTransitionData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {MESSAGE_TRANSITION_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case ADD:
                            OnAddAction?.Invoke();
                            break;
                        case REMOVE:
                            OnRemoveAction?.Invoke(response.data.msgID);
                            break;
                    }
                });
            });
        }
    }
}