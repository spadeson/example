using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Command;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Commands {
    public class CommandSocketBind : ISocketBind {
        private static CommandSocketBind _instance;
        public static CommandSocketBind Instance => _instance ?? (_instance = Init());

        private static CommandSocketBind Init() {
            var bind = new CommandSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(COMMAND_EVENT, bind);
            return bind;
        }
        
        public event Action OnLogoutAction;

        public void SetSocket(Socket socket) {
            socket.On(COMMAND_EVENT, (data) => {
                var response = JsonUtility.FromJson<ResponseData<CommandData>>(data.ToString());
                Debug.Log($"<color=orange>socket update tasks with {COMMAND_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case  LOGOUT:
                            OnLogoutAction?.Invoke();
                            break;
                    }
                });
            });
        }
    }
}