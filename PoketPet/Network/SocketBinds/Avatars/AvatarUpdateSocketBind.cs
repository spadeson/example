using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Files;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Avatars {
    public class AvatarUpdateSocketBind : ISocketBind {
        private static AvatarUpdateSocketBind _instance;
        public static AvatarUpdateSocketBind Instance => _instance ?? (_instance = Init());

        private static AvatarUpdateSocketBind Init() {
            var bind = new AvatarUpdateSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(UPDATE_AVATAR_EVENT, bind);
            return bind;
        }
        
        public event Action<string, string> OnPetAvatarUpdatedAction;
        public event Action<string, string> OnUserAvatarUpdatedAction;

        public void SetSocket(Socket socket) {
            socket.On(UPDATE_AVATAR_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<UserUpdateAvatar>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {UPDATE_AVATAR_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case  USER:
                            Debug.Log($"user avatar {response.data.avatar}");
                            OnUserAvatarUpdatedAction?.Invoke(response.data.id, response.data.avatar);
                            break;
                        case  PET:
                            Debug.Log($"pet avatar {response.data.avatar}");
                            OnPetAvatarUpdatedAction?.Invoke(response.data.id, response.data.avatar);
                            break;
                    }
                });
            });
        }
    }
}