using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.API.SocketModels.Tasks;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Tasks {
    public class TaskModelSocketBind : ISocketBind {
        private static TaskModelSocketBind _instance;
        public static TaskModelSocketBind Instance => _instance ?? (_instance = Init());

        private static TaskModelSocketBind Init() {
            var bind = new TaskModelSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(TASK_MODEL_EVENT, bind);
            return bind;
        }
        
        public event Action OnHistoryUpdatedAction;
        public event Action<string, string> OnAssigneeAction;
        public event Action<string> OnUpdateByUserAction;

        public void SetSocket(Socket socket) {
            socket.On(TASK_MODEL_EVENT, (data) => {
                var response = JsonUtility.FromJson<ResponseData<TaskModelData>>(data.ToString());
                Debug.Log($"<color=orange>socket update tasks with {TASK_MODEL_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case HISTORY:
                            OnHistoryUpdatedAction?.Invoke();
                            break;
                        case ASSIGNEE:
                            OnAssigneeAction?.Invoke(response.data.taskID, response.data.userID);
                            break;
                        case USER_UPDATE:
                            OnUpdateByUserAction?.Invoke(response.data.taskID);
                            break;
                    }
                });
            });
        }
    }
}