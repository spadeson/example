using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.API.SocketModels.Tasks;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Tasks {
    public class TaskFlowSocketBind : ISocketBind {
        private static TaskFlowSocketBind _instance;
        public static TaskFlowSocketBind Instance => _instance ?? (_instance = Init());

        private static TaskFlowSocketBind Init() {
            var bind = new TaskFlowSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(TASK_FLOW_EVENT, bind);
            return bind;
        }
        
        public event Action<string> OnUpdateByUserAction;

        public void SetSocket(Socket socket) {
            socket.On(TASK_FLOW_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<TaskModelData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update schedule with {TASK_FLOW_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case USER_UPDATE:
                            OnUpdateByUserAction?.Invoke(response.data.taskID);
                            break;
                    }
                });
            });
        }
    }
}