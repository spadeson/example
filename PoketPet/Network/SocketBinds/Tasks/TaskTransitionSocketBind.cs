using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.API.SocketModels.Tasks;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Tasks {
    public class TaskTransitionSocketBind : ISocketBind {
        private static TaskTransitionSocketBind _instance;
        public static TaskTransitionSocketBind Instance => _instance ?? (_instance = Init());

        private static TaskTransitionSocketBind Init() {
            var bind = new TaskTransitionSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(TASK_TRANSITION_EVENT, bind);
            return bind;
        }
        
        public event Action<string> OnAddAction;
        public event Action<string> OnRemoveAction;

        public void SetSocket(Socket socket) {
            socket.On(TASK_TRANSITION_EVENT, (data) => {
                var response = JsonUtility.FromJson<ResponseData<TaskTransitionData>>(data.ToString());
                Debug.Log($"<color=orange>socket update tasks with {TASK_TRANSITION_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case ADD:
                            OnAddAction?.Invoke(response.data.taskID);
                            break;
                        case REMOVE:
                            OnRemoveAction?.Invoke(response.data.taskID);
                            break;
                    }
                });
            });
        }
    }
}