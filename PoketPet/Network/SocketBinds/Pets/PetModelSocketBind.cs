using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Pets {
    public class PetModelSocketBind : ISocketBind {
        private static PetModelSocketBind _instance;
        public static PetModelSocketBind Instance => _instance ?? (_instance = Init());

        private static PetModelSocketBind Init() {
            var bind = new PetModelSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(PET_UPDATE_EVENT, bind);
            return bind;
        }
        
        public event Action<PetResponseData> OnUpdateByUserAction;

        public void SetSocket(Socket socket) {
            socket.On(PET_UPDATE_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<PetUpdateData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {PET_UPDATE_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case  USER_UPDATE:
                            OnUpdateByUserAction?.Invoke(response.data.vo);
                            break;
                    }
                });
            });
        }
    }
}