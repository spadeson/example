using System;
using Core.NetworkSubsystem.Managers.SocketSubsystem;
using PoketPet.Network.API.SocketModels.Base;
using PoketPet.Network.API.SocketModels.Pets;
using PoketPet.Network.Dispatchers;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SocketKeys;

namespace PoketPet.Network.SocketBinds.Pets {
    public class PetWatcherSocketBind : ISocketBind {

        private static PetWatcherSocketBind _instance;
        public static PetWatcherSocketBind Instance => _instance ?? (_instance = Init());

        private static PetWatcherSocketBind Init() {
            var connector = new PetWatcherSocketBind();
            CoreMobile.Instance.SocketNetworkManager.SetBind(PET_WATCHER_EVENT, connector);
            return connector;
        }
        
        public event Action<PetWatcherData> OnAddAction;
        public event Action OnRemoveAction;
        
        public void SetSocket(Socket socket) {
            socket.On(PET_WATCHER_EVENT, (modelData) => {
                var response = JsonUtility.FromJson<ResponseData<PetWatcherData>>(modelData.ToString());
                Debug.Log($"<color=orange>socket update massages with {PET_WATCHER_EVENT} {response.action} action</color>");
                Dispatcher.Instance.Invoke(() => {
                    switch (response.action) {
                        case  ADD:
                            OnAddAction?.Invoke(response.data);
                            break;
                        case  REMOVE:
                            OnRemoveAction?.Invoke();
                            break;
                    }
                });
            });
        }
    }
}