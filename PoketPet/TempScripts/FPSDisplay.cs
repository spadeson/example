﻿using UnityEngine;
using System.Collections;

public class FPSDisplay : MonoBehaviour
{
    // Attach this to any object to make a frames/second indicator.
    //
    // It calculates frames/second over each updateInterval,
    // so the display does not keep changing wildly.
    //
    // It is also fairly accurate at very low FPS counts (<10).
    // We do this not by simply counting frames per interval, but
    // by accumulating FPS for each frame. This way we end up with
    // corstartRect overall FPS even if the interval renders something like
    // 5.5 frames.

    public Rect startRect = new Rect(10, 10, 75, 50); // The rect the window is initially displayed at.
    public bool updateColor = true; // Do you want the color to change if the FPS gets low
    public float frequency = 0.5F; // The update frequency of the fps

    private float accum = 0f; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private Color color = Color.white; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )
    private string sFPS = "0"; // The fps formatted into a string.
    private GUIStyle style; // The style the text will be displayed at, based en defaultSkin.label.

    void Start()
    {
        StartCoroutine(FPS());
    }

    void Update()
    {
        accum += Time.timeScale / Time.deltaTime;
        ++frames;
    }

    IEnumerator FPS()
    {
        // Infinite loop executed every "frenquency" secondes.
        while (true)
        {
            // Update the FPS
            float fps = accum / frames;
            sFPS = Mathf.RoundToInt(fps).ToString();

            //Update the color
            color = (fps >= 30) ? Color.green : ((fps > 10) ? Color.red : Color.yellow);

            accum = 0.0F;
            frames = 0;

            yield return new WaitForSeconds(frequency);
        }
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;
        startRect = new Rect(0, 0, w, h * 2 / 100);

        // Copy the default label skin, change the color and the alignement
        if (style == null)
        {
            style = new GUIStyle();
            style.alignment = TextAnchor.UpperRight;
            style.fontSize = Screen.height * 4 / 100;
            style.fontStyle = FontStyle.Bold;
        }

        style.normal.textColor = updateColor ? color : Color.white;
        GUI.Label(startRect, sFPS, style);
    }

}