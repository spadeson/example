﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DogsSwitcherWidget : MonoBehaviour {
    [SerializeField] private Button _prevButton;
    [SerializeField] private Button _nextButton;
    
    [SerializeField] List<GameObject> _dogs = new List<GameObject>();
    [SerializeField] private GameObject _activeDog;

    private int _activeDogIndex;

    void Start() {
        _prevButton.onClick.AddListener(OnPreviousButtonClick);
        _nextButton.onClick.AddListener(OnNextButtonClick);
        
        AcivateDog(0);
    }

    private void OnDestroy() {
        _prevButton.onClick.RemoveListener(OnPreviousButtonClick);
        _nextButton.onClick.RemoveListener(OnNextButtonClick);
    }


    private void OnPreviousButtonClick() {
        _activeDogIndex--;
        _activeDogIndex = Mathf.Clamp(_activeDogIndex, 0, _dogs.Count - 1);
        AcivateDog(_activeDogIndex);
    }
    
    private void OnNextButtonClick() {
        _activeDogIndex++;
        _activeDogIndex = Mathf.Clamp(_activeDogIndex, 0, _dogs.Count - 1);
        AcivateDog(_activeDogIndex);
    }

    private void AcivateDog(int index) {
        _activeDog.SetActive(false);
        _activeDog = _dogs[index];
        _activeDog.SetActive(true);
    }
}
