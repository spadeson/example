using UnityEngine;
using System.Collections;

public class BagCamera : MonoBehaviour {
	public Vector3 move;
	public Transform target;
	public float distance = 10;
	public float maxDist=2;
	public float minDist=1;
	Vector3 mPos;

	public float xSpeed = 250;
	public float ySpeed = 120;

	public float yMinLimit = -20;
	public float yMaxLimit = 80;

	public float wantedRotationAngleX = 0;
	public float wantedRotationAngleY = 0;
	public float curRotationAngleX = 0;
	public float curRotationAngleY = 0;
	public float maxRotAngle = 10;
	
//	public Transform[] bugParts;
//	public int partNum;
//	public int[] curMat;
//	public Material[] partMaterials;
//	
//	public Material selectionMaterial;
//	public LayerMask mask;
	Vector3 mClickTemp;
	
	float pp;
	float oldDist;
		Quaternion rotation;
		Vector3 position;
//	public float rotateTime = 3;
//	float napram;
	
	//Color colMugannia;
	//float curMugTime;
	//float MugTime = 2;
	//public bool isMug;
	//bool isGUI;
	//public GUISkin guiSkin;
	
	
	//public Texture2D[] guiPics;
	//GUIStyle style = new GUIStyle();
	//Texture2D[] blackProzorist; 
	//public int clepkiMat;
	
	// Use this for initialization
	void Start () {
//		blackProzorist = new Texture2D[10];
			
//		for(int i=0;i<blackProzorist.Length-1;i++)
//		{
//			blackProzorist[i] = new Texture2D(1, 1);
//	        blackProzorist[i].SetPixel(1, 1, new Color32(0,0,0,(byte)(i*25)));
//	        blackProzorist[i].Apply();
//		} 
		
//		curRotateTime = rotateTime;
//		curMat = new int[3];
//		isMug=false;
//		BrowserCommunicator.TestCommunication();
//		BrowserCommunicator.AddButton();
//		HelperFunctions.AddCube();

	}
	
	void Update () 
	{
		float delta; // when width 768 s ui 248 delta = 1
					 // 
		delta = 1 - Screen.width / 768;
		
		distance -= Input.GetAxis("Mouse ScrollWheel");
		
		if(distance>maxDist)
			distance=maxDist;
		if(distance<minDist)
			distance=minDist;
		//distance = 3.3f;
		
		#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE 
		if(Input.GetMouseButtonDown(0))
		{
			mClickTemp = Input.mousePosition;
		}
		
		if(Input.GetMouseButtonUp(0))
		{
			if(mClickTemp == Input.mousePosition)
			{
				//CheckpointClick();
			}
			
//			if(mClickTemp.x > Input.mousePosition.x)
//				napram = -1;
//			else
//			if(mClickTemp.x < Input.mousePosition.x)
//				napram = 1;
//			else
//				napram = 0;
//
//			napram = 0;
		}
		
		if(Input.GetMouseButton(0))
		{
			if(Input.GetAxis("Mouse X") * xSpeed * 0.02f > Input.GetAxis("Mouse X") * 200 * 0.02f)
			{
		        wantedRotationAngleX += Input.GetAxis("Mouse X") * 200 * 0.02f;
//				Debug.Log((Input.GetAxis("Mouse X") * xSpeed * 0.02f).ToString());
			}
			else 
			if(Input.GetAxis("Mouse X") * xSpeed * 0.02f < Input.GetAxis("Mouse X") * -200 * 0.02f)
			{
		        wantedRotationAngleX += Input.GetAxis("Mouse X") * 200 * 0.02f;
			}
			else
				wantedRotationAngleX += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
			
	        wantedRotationAngleY -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
			
			
			if(mPos != Input.mousePosition)
			{
//				curRotateTime = rotateTime;
				mPos = Input.mousePosition;
			}
		}
		else
		{
//			wantedRotationAngleX += napram * 0.2f;
//	        wantedRotationAngleY -= 1;
			
		}
		
//		Debug.Log((Input.GetAxis("Mouse X") * xSpeed * 0.02f).ToString());

		curRotationAngleX = Mathf.LerpAngle(curRotationAngleX, wantedRotationAngleX, Time.deltaTime*2);	
		curRotationAngleY = Mathf.LerpAngle(curRotationAngleY, wantedRotationAngleY, Time.deltaTime);
		
		if(maxRotAngle>wantedRotationAngleX)
		{
			maxRotAngle=wantedRotationAngleX;
		}
		
//		if(Input.GetMouseButtonUp(0))
//			Debug.Log(curRotationAngleX.ToString() + " / " + wantedRotationAngleX.ToString()+ " / " +
//				((float)(wantedRotationAngleX - curRotationAngleX)).ToString());
		
 		curRotationAngleY = ClampAngle(curRotationAngleY, yMinLimit, yMaxLimit);
 		wantedRotationAngleY = ClampAngle(wantedRotationAngleY, yMinLimit-10, yMaxLimit+10);
 		//curRotationAngleX = ClampAngle(curRotationAngleX, yMinLimit, yMaxLimit);
	 		       
        rotation = Quaternion.Euler(curRotationAngleY, curRotationAngleX, 0);
        position = rotation * (new Vector3(0.0f, 0.0f, -distance)+ new Vector3(move.x+delta*move.x/2,move.y, move.z)*distance) + target.position ;
	        
        transform.rotation = rotation;
        transform.position = position;
		#endif
		
		#if UNITY_ANDROID || UNITY_IPHONE
		if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
		{
			mPos=Input.touches[0].position;
		}
//		if(Input.GetMouseButtonDown(0))
//		{
//			mClickTemp = Input.mousePosition;
//		}
		
//		if(Input.GetMouseButtonUp(0))
//		{
//			if(mClickTemp == Input.mousePosition)
//			{
//				//CheckpointClick();
//			}
//		}
		
		if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended)
		{
			mPos = Input.touches[0].position;
		}
		
		if(Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
		{
			if(mPos != new Vector3(Input.touches[0].position.x,Input.touches[0].position.y,0))//.x,Input.touches[0].position.y,0))
			{
				wantedRotationAngleX -= (mPos.x-Input.touches[0].position.x)/2;
				if( Mathf.Abs((mPos.x-Input.touches[0].position.x)/2)>30)
				{
					wantedRotationAngleX -= 30;
					pp = (mPos.x-Input.touches[0].position.x)/2;
				}
				wantedRotationAngleY += (mPos.y-Input.touches[0].position.y)/2;
				mPos = Input.touches[0].position;//.x,Input.touches[0].position.y,0);
			}
		}
		
//		if(Input.GetMouseButton(0))
//		{
//			if(Input.GetAxis("Mouse X") * xSpeed * 0.02f > Input.GetAxis("Mouse X") * 200 * 0.02f)
//			{
//		        wantedRotationAngleX += Input.GetAxis("Mouse X") * 200 * 0.02f;
//			}
//			else 
//			if(Input.GetAxis("Mouse X") * xSpeed * 0.02f < Input.GetAxis("Mouse X") * -200 * 0.02f)
//			{
//		        wantedRotationAngleX += Input.GetAxis("Mouse X") * 200 * 0.02f;
//			}
//			else
//				wantedRotationAngleX += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
//			
//	        wantedRotationAngleY -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
//			
//			
//			if(mPos != Input.mousePosition)
//			{
//				mPos = Input.mousePosition;
//			}
//		}
		
		if(Input.touchCount == 2 && Input.touches[1].phase == TouchPhase.Began)
		{
			oldDist = Vector3.Distance(Input.touches[0].position, Input.touches[1].position);
		}
		
		if(Input.touchCount == 2 && Input.touches[1].phase == TouchPhase.Moved)
		{
			float di;
			di = Vector3.Distance(Input.touches[0].position, Input.touches[1].position);
			
			distance -= (di-oldDist)/100;
			

			oldDist = di;
			
		}
		
				if(distance>maxDist)
					distance=maxDist;
				if(distance<minDist)
					distance=minDist;		
		
		curRotationAngleX = Mathf.LerpAngle(curRotationAngleX, wantedRotationAngleX, Time.deltaTime*2);	
		curRotationAngleY = Mathf.LerpAngle(curRotationAngleY, wantedRotationAngleY, Time.deltaTime);
		
		if(maxRotAngle>wantedRotationAngleX)
		{
			maxRotAngle=wantedRotationAngleX;
		}
		
		
 		curRotationAngleY = ClampAngle(curRotationAngleY, yMinLimit, yMaxLimit);
 		wantedRotationAngleY = ClampAngle(wantedRotationAngleY, yMinLimit-10, yMaxLimit+10);
	 		       
        rotation = Quaternion.Euler(curRotationAngleY, curRotationAngleX, 0);
        position = rotation * (new Vector3(0.0f, 0.0f, -distance)+ new Vector3(move.x+delta*move.x/2,move.y, move.z)*distance) + target.position ;
	        
        transform.rotation = rotation;
        transform.position = position;
		#endif
	}	
	
	void OnGUI() {
/*		GUI.skin = guiSkin;
		
 		style.normal.background = blackProzorist[5];
		//GUI.Box(new Rect(100, 100, 100, 100), );
		
				
		if(GUI.Button(new Rect(10,Screen.height/2-65*3,139,64),guiPics[1]))
		{
			bugParts[partNum].renderer.material = partMaterials[curMat[partNum]];
//			bugParts[2].renderer.material = selectionMaterial;
			bugParts[2].renderer.material.color = colMugannia;
			partNum=2;
			isMug = true;
			isGUI = true;
//			Application.ExternalCall( "add", "Button" );			
//			BrowserCommunicator.AddButton();
		}
		
		if(GUI.Button(new Rect(10,Screen.height/2-65*2,139,64),guiPics[3]))
		{
			bugParts[partNum].renderer.material = partMaterials[curMat[partNum]];
//			bugParts[1].renderer.material = selectionMaterial;
			bugParts[1].renderer.material.color = colMugannia;
			partNum=1;
			isMug = true;
			isGUI = true;
//			Application.ExternalCall( "SayHello", "Part 2" );			
//			BrowserCommunicator.WritePart2();
		}

		if(GUI.Button(new Rect(10,Screen.height/2-65,139,64),guiPics[5]))
		{
			bugParts[partNum].renderer.material = partMaterials[curMat[partNum]];
//			bugParts[0].renderer.material = selectionMaterial;
			bugParts[0].renderer.material.color = colMugannia;
			partNum=0;
			isMug = true;
			isGUI = true;
//			Application.ExternalCall( "SayHello", "Part 1" );	// для виклику функції з браузера 		
//			BrowserCommunicator.TestCommunication();
		}

		if(partNum != 2)
			GUI.Box(new Rect(10,Screen.height/2-65*3,139,64),new GUIContent(""), style); 
		if(partNum != 1)
			GUI.Box(new Rect(10,Screen.height/2-65*2,139,64),new GUIContent(""), style); 
		if(partNum != 0)
			GUI.Box(new Rect(10,Screen.height/2-65,139,64),new GUIContent(""), style); 


		if(GUI.Button(new Rect(Screen.width-145,Screen.height/2-65*3,139,64),guiPics[7]))
		{
			bugParts[partNum].renderer.material = partMaterials[0];
			curMat[partNum] = 0;
		}

		if(GUI.Button(new Rect(Screen.width-145,Screen.height/2-65,139,64),guiPics[11]))
		{
			bugParts[partNum].renderer.material = partMaterials[1];
			curMat[partNum] = 1;
		}

		if(GUI.Button(new Rect(Screen.width-145,Screen.height/2-65*2,139,64),guiPics[9]))
		{
			bugParts[partNum].renderer.material = partMaterials[2];
			curMat[partNum] = 2;
//			isMug = true;
//			isGUI = true;
		}

		if(curMat[partNum] != 0)
			GUI.Box(new Rect(Screen.width-145,Screen.height/2-65*3,139,64),new GUIContent(""), style); 
		if(curMat[partNum] != 1)
			GUI.Box(new Rect(Screen.width-145,Screen.height/2-65,139,64),new GUIContent(""), style); 
		if(curMat[partNum] != 2)
			GUI.Box(new Rect(Screen.width-145,Screen.height/2-65*2,139,64),new GUIContent(""), style); 

		if(GUI.Button(new Rect(Screen.width-145,Screen.height/2+64,139,32),guiPics[13]))
		{
			bugParts[3].renderer.material = partMaterials[3];
			clepkiMat=0;
			//curMat[partNum] = 2;			
			//bugParts[3].renderer.material = partMaterials[4];
		}		

		if(GUI.Button(new Rect(Screen.width-145,Screen.height/2+96,139,32),guiPics[15]))
		{
			bugParts[3].renderer.material = partMaterials[4];
			clepkiMat = 1;
			//bugParts[3].renderer.material = partMaterials[4];
		}		
		
		if(clepkiMat != 0)
			GUI.Box(new Rect(Screen.width-145,Screen.height/2+64,139,32),new GUIContent(""), style); 
		if(clepkiMat != 1)
			GUI.Box(new Rect(Screen.width-145,Screen.height/2+96,139,32),new GUIContent(""), style); 
	*/	
	}
	
	
	
	float ClampAngle (float angle, float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp(angle, min, max);
	}
	
//	void CheckpointClick()
//	{
//		RaycastHit hit;
//		Camera cam = Camera.main;
//		
//		if (Physics.Raycast	(cam.ScreenPointToRay (Input.mousePosition), out hit, Mathf.Infinity, mask))
//		{
//			Transform hitedObj = hit.collider.GetComponent<Transform>();
////			Debug.Log(hitedObj.name);
//			for(int i=0;i<bugParts.Length;i++)
//			{
//				if(hitedObj == bugParts[i])
//				{
////					Debug.Log(i.ToString());
//
//					bugParts[partNum].renderer.material = partMaterials[curMat[partNum]];
//					bugParts[i].renderer.material.color = colMugannia;
//					partNum=i;
//					isMug = true;
////					isGUI = false;
//				}
//			}
//		}
//		else
//		{
//			/*isMug = false;
//			if(!isGUI)
//			{
//				bugParts[partNum].renderer.material = partMaterials[curMat[partNum]];
//				isMug = false;
//			}
//			else
//				isMug = true;
//			isGUI = false;*/
//		}
//	}
	
//	public static void ChangeMaterial()
//    {
//		int num = 1;
//		bugParts[partNum].renderer.material = partMaterials[num];
//		curMat[partNum] = num;
//    }	
}
