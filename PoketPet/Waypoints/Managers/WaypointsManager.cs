using System.Collections.Generic;
using PoketPet.Pets.PetNavigation.Utilities;
using UnityEngine;

namespace PoketPet.Waypoints.Managers {
    public class WaypointsManager : MonoBehaviour {

        public List<Transform> waypointsList = new List<Transform>();

        public void CreateWayPoints(Vector3 center, int pointsCount, float radius = 1f) {
            WaypointsSpawner.CreateWayPoints(center, pointsCount, radius);
            waypointsList = WaypointsSpawner.WaypointsList;
        }

        public List<Transform> GetAllWaypoints() {
            return waypointsList;
        }

        public Transform GetWayPoint(int index) {
            return waypointsList[index];
        }
    }
}