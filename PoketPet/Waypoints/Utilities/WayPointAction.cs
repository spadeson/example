namespace PoketPet.Pets.PetNavigation.Utilities
{
    public enum WayPointAction
    {
        MOVE_NEXT_POINT = 0,
        RUN_NEXT_POINT = 1,
        STAND_AND_THINK = 2
        
    }
}