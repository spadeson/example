using System;
using System.Collections.Generic;
using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using UnityEngine;

namespace PoketPet.Pets.PetNavigation.Utilities
{
    [RequireComponent(typeof(SphereCollider))]
    public class WayPointTrigger : MonoBehaviour
    {
        private SphereCollider _sphereCollider;

        public List<WayPointAction> ActionsList = new List<WayPointAction>();

        private void OnEnable()
        {
            _sphereCollider = GetComponent<SphereCollider>();
            _sphereCollider.isTrigger = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log($"Triggered: {other.name}");

            var petController = other.gameObject.GetComponent<PetMainController>();
            if (petController == null) {
                return;
            }
        }
    }
    
}