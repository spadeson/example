using System;
using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.Pets.PetNavigation.Utilities
{
    public static class WaypointsSpawner
    {
        public static List<Transform> WaypointsList; 
        public static void CreateWayPoints(Vector3 center, int pointsCount, float radius = 1f )
        {
            WaypointsList = new List<Transform>();
            
            for (var i = 0; i < pointsCount; i++)
            {
                var theta = (i * Mathf.PI * 2f) / pointsCount;
                
                var posX = Mathf.Cos(theta) * radius + center.x;
                var posZ = Mathf.Sin(theta) * radius + center.z;
                
                var newPos = new Vector3(posX, 0, posZ);
                
                var go = new GameObject( $"Way-Point-{(i + 1):00}");
                
                go.transform.position = newPos;
                
                WaypointsList.Add(go.transform);
            }
        }
    }
}