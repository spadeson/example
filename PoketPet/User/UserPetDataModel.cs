using System;
using System.Collections.Generic;

namespace PoketPet.User {
    [Serializable]
    public class UserPetDataModel {
        public string id;
        public List<string> type;
        public List<string> tasks;
    }
}