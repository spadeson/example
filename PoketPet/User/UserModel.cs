using System;
using System.Collections.Generic;
using PoketPet.Utilities;

namespace PoketPet.User {
    
    [Serializable]
    public class UserModel {
        public string nickname;
        public string email;
        public string phone;
        public int birth_day;
        public string gender;
        public long registration;
        public List<UserPet> pets;
        public string id;
        public string avatar;
        public int store;
        public string fcm;
        public string device;
        public UserSettings settings;

        public string FamiliesRole => UserUtilities.GetUsersFamilyRole(this);
        public int Age => DateTime.Now.Year - DateTimeConversion.TimestampToDateTime(birth_day).Year;
        public string RoleByCurrentPet => UserUtilities.GetUserRoleByCurrentPet();
    }

    [Serializable]
    public class UserPet {
        public string id;
        public List<string> status;
    }
    [Serializable]
    public class UserSettings {
        public string time_format;
        public string weight_units;
        public string localisation;
        public bool push_active;
    }

}