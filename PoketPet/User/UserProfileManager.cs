﻿using System;
using System.Collections.Generic;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Users;
using PoketPet.Network.SocketBinds.Avatars;
using PoketPet.Network.SocketBinds.Commands;
using PoketPet.Network.SocketBinds.Users;
using PoketPet.Utilities;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.User {
    public static class UserProfileManager {
        public static event Action<string> OnWeightUnitsChanged;
        public static event Action<string> OnTimeFormatChanged;
        public static event Action OnUserDataModelUpdated;
        public static event Action OnUserAvatarUpdated;
        public static UserModel CurrentUser { get; private set; }
        private static int BirthdayDate { get; set; }

        /// <summary>
        /// Create new user profile data model and set it as current.
        /// </summary>
        public static void CreteNewUser() {
            CurrentUser = new UserModel {birth_day = BirthdayDate};
            Debug.Log($"Created new user: {CurrentUser.id}");
        }

        /// <summary>
        /// Login with user profile.
        /// </summary>
        /// <param name="userModel">User Data Model</param>
        public static void LoginWithProfile(UserModel userModel) {
            CurrentUser = userModel;
        }
    
        /// <summary>
        /// Login with server response.
        /// </summary>
        /// <param name="data">Login Data from Response</param>
        public static void LoginWithServerResponse(UserModel data) {

            CurrentUser = data;

            OnUserDataModelUpdated?.Invoke();
        
            Debug.Log("LoginWithServerResponse");
        }

        /// <summary>
        /// Sets user data model as current user.
        /// </summary>
        /// <param name="data">User Data Model</param>
        /// <param name="saveInLocalData">Flag should be user data model also saved in local storage.</param>
        public static void UpdateUserProfile(UserModel data, bool saveInLocalData = true) {
            CurrentUser = data;
            if(!saveInLocalData) return;
            var serializedUserProfile = JsonUtility.ToJson(CurrentUser);
            CoreMobile.Instance.LocalDataManager.SaveLocalDataString(GlobalVariables.LocalDataKeys.USER_PROFILE_KEY, serializedUserProfile);
        }

        /// <summary>
        /// Set Pet to current user.
        /// </summary>
        /// <param name="petID">Pet ID</param>
        public static void SetPet(string petID) {
            //CurrentUser.petIDs.Add(petID);
        }

        /// <summary>
        /// Returns entered user birthdate in case when user model wasn't actually created.
        /// </summary>
        /// <returns>Users birthdate.</returns>
        public static long GetPotentialUserBirthday() {
            return BirthdayDate;
        }

        /// <summary>
        /// Set Converted DateTime into BirthdayDate property. It will user just after created user profile data.
        /// </summary>
        /// <param name="birthdate">DateTime.</param>
        public static void SetUserBirthday(DateTime birthdate) {
            BirthdayDate = DateTimeConversion.DateTimeToTimestamp(birthdate);
        
            if (CurrentUser != null) {
                CurrentUser.birth_day = BirthdayDate;
            }
        }

        public static void AddPetToUser(string petId, string permission) {
            CurrentUser.pets.Add(new UserPet{id = petId, status = new List<string>{permission}});
        }

        public static void AddSocketListeners() {
            UserModelSocketBind.Instance.OnUpdateByUserAction += model => {
                if (model.id != CurrentUser.id) return;
                
                CurrentUser = model;
                OnUserDataModelUpdated?.Invoke();
            };

            UserScoreSocketBind.Instance.OnUpdateAction += data => {
                Debug.Log($"action: {data.action}, score: {data.score}");
            };

            AvatarUpdateSocketBind.Instance.OnUserAvatarUpdatedAction += (id, avatar) => {
                if (id != CurrentUser.id) return;
                
                CurrentUser.avatar = avatar;
                OnUserAvatarUpdated?.Invoke();
            };

            CommandSocketBind.Instance.OnLogoutAction += () => {
                CoreMobile.Instance.FirebaseManager.AuthenticationManager.Logout();
            };
        }

        public static void GetUserById(string id, Action<string> handler) {
            var requestData = new RequestData<GetUserByIdRequestData>(new GetUserByIdRequestData(){userID = id}, "get");

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("users", requestData);
            operation.OnDataReceived += handler;
        }
        public static void UpdateUser() {
            var userUpdateRequest = new RequestData<UserModel> {from = CurrentUser.id, data = CurrentUser, action = "update"};

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("users", userUpdateRequest);
            operation.OnDataReceived += UpdateUserHandler;
        }

        public static void SetWeightUnits(string weightUnits) {
            CurrentUser.settings.weight_units = weightUnits;
            OnWeightUnitsChanged?.Invoke(CurrentUser.settings.weight_units);
        }
        
        public static void SetTimeFormat(string timeFormat) {
            CurrentUser.settings.time_format = timeFormat;
            OnTimeFormatChanged?.Invoke(CurrentUser.settings.time_format);
        }
        public static void GetCurrentUser() {
            var requestData = new RequestData<GetUserByIdRequestData>(new GetUserByIdRequestData(){userID = CurrentUser.id}, "get");

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("users", requestData);
            operation.OnDataReceived += GetCurrentUserHandler;
        }
        private static void GetCurrentUserHandler(string data) {
            var response = JsonUtility.FromJson<ResponseData<UserModel>>(data);
            CurrentUser = response.data;
            OnUserDataModelUpdated?.Invoke();
        }

        private static void UpdateUserHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
                OnUserDataModelUpdated?.Invoke();
            }else {
                // TODO: Release Error Status 
            }
        }
    }
}