using PoketPet.Pets.PetsSystem.Manager;
using UnityEngine;
using static PoketPet.Global.GlobalVariables.UsersFamilyRoleKeys;
using static PoketPet.Global.GlobalVariables.UsersPetRoleKeys;

namespace PoketPet.User {
    
    public static class UserUtilities {

        public static string GetUsersFamilyRole(UserModel user) {
            switch (user.gender) {
                case MALE: {
                    return user.Age > 18 ? MALE : BOY;
                    break;
                }
                case FEMALE:
                    return user.Age > 18 ? FEMALE : GIRL;
                    break;
                default:
                    return UNKNOWN;
            }
        }

        public static string GetUserRoleByCurrentPet() {
            var petId = PetsProfileManager.CurrentPet.id;
            var userPetsList = UserProfileManager.CurrentUser.pets;
            var listStatuses = userPetsList.Find(item => item.id == petId);
            
            return listStatuses.status.Contains(OWNER) ? OWNER :
                listStatuses.status.Contains(WATCHER) ? WATCHER : "";
        }
    }
}