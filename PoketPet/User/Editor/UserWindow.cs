using System;
using System.Collections.Generic;
using PoketPet.Settings;
using PoketPet.Utilities;
using UnityEditor;
using UnityEngine;

namespace PoketPet.User.Editor {
    public class UserWindow : UnityEditor.EditorWindow {

        [MenuItem("Window/PoketPet/Users/User Model Viwer")]
        public static void ShowWindow() {
            var window = (UserWindow) GetWindow(typeof(UserWindow));
            window.Show();
        }

        private void OnGUI() {
            GUILayout.Label("User Viewer", EditorStyles.boldLabel);
            
            if (UserProfileManager.CurrentUser == null) {
                EditorGUILayout.HelpBox("Sorry, not set current user data model!", MessageType.Warning);
            } else {
                //Display basic user's data 
                DisplayBasicUsersData();

                EditorGUILayout.Space();

                //Display User Settings
                DisplayUserSettings();
            }
        }

        private void DisplayBasicUsersData() {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("Basic User Info", EditorStyles.boldLabel);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.LabelField("ID", UserProfileManager.CurrentUser.id);
            EditorGUILayout.LabelField("Device ID", UserProfileManager.CurrentUser.device);
            EditorGUILayout.LabelField("Nickname", UserProfileManager.CurrentUser.nickname);
            EditorGUILayout.LabelField("E-mail", UserProfileManager.CurrentUser.email);
            EditorGUILayout.LabelField("Mobile", UserProfileManager.CurrentUser.phone);
            EditorGUILayout.LabelField($"Birthday: M:{DateTimeConversion.TimestampToDateTime(UserProfileManager.CurrentUser.birth_day):MM/dd/yyyy}");
            
            EditorGUILayout.EndVertical();
        }
        
        private void DisplayUserSettings() {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.LabelField("User Settings", EditorStyles.boldLabel);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.LabelField($"Sounds Volume: {SettingsManager.GetSoundsVolume()}");
            EditorGUILayout.LabelField($"Push Notifications: {UserProfileManager.CurrentUser.settings.push_active}");
            EditorGUILayout.LabelField($"Weight Units: {UserProfileManager.CurrentUser.settings.weight_units}");
            EditorGUILayout.LabelField($"Time Format: {UserProfileManager.CurrentUser.settings.time_format}");

            EditorGUILayout.EndVertical();
        }
    }
}
