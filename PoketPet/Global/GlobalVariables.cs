namespace PoketPet.Global {
    public static class GlobalVariables {
        public static class AudioMusicKeys {
            public const string GAME_MENU_MUSIC = "GAME_MENU_MUSIC";
        }

        public static class AudioPetKeys {
            public const string DOG_SNIFFING_SFX = "DOG_SNIFFING_SFX";
            public const string DOG_BARKING_SFX = "DOG_BARKING_SFX";
        }

        public static class AudioUiKeys {
            public const string BAGE_NOTIFICATION_SFX = "BAGE_NOTIFICATION_SFX";
            public const string COMPLETED_SFX = "COMPLETED_SFX";
            public const string CLOSE_BUTTON_SFX = "CLOSE_BUTTON_SFX";
            public const string SWIPE_IN_SFX = "SWIPE_IN_SFX";
            public const string SWIPE_OUT_SFX = "SWIPE_OUT_SFX";
            public const string BUTTON_CLICK_SFX = "BUTTON_CLICK_SFX";
            public const string POPUP_SHOW_SFX = "POPUP_SHOW_SFX";
            public const string TUTORIAL_TASK_COMPLETED_SFX = "TUTORIAL_TASK_COMPLETED_SFX";
            public const string TUTORIAL_PAGE_TURN_SFX = "TUTORIAL_PAGE_TURN_SFX";
            public const string IPHONE_SHUTTER_SFX = "IPHONE_SHUTTER_SFX";
            public const string TUTORIAL_SCORE_COUNTER_SFX = "TUTORIAL_SCORE_COUNTER_SFX";

        }

        public static class AuthenticationKeys {
            public const string EMAIL = "email";
            public const string PASSWORD = "password";
        }

        public static class LocalDataKeys {
            public const string USER_PROFILE_KEY = "player";
            public const string PET_PROFILE_KEY = "pet_profile";
            public const string FAMILY_PROFILE_KEY = "family_profile";

            //Settings
            public const string MUSIC_VOLUME = "musicVolume";
            public const string SOUNDS_VOLUME = "soundsVolume";
            public const string ENABLED_PUSH_NOTIFICATIONS = "enabledPushNotifications";
            
            public const string STANDARD_TIME = "standard";
            public const string MILITARY_TIME = "military";
            
            public const string KILOGRAM_WEIGHT_UNITS = "kg";
            public const string POUNDS_WEIGHT_UNITS = "lbs";

            public const string FIRST_TIME_VET_CHAT = "first_time_vet_chat";
        }

        public static class ScenesKeys {
            public const string CONNECTION_SCENE = "CONNECTION_SCENE";
            public const string AUTHENTICATION_SCENE = "AUTHENTICATION_SCENE";
            public const string TUTORIAL_SCENE = "TUTORIAL_SCENE";
            public const string GAME_SCENE = "GAME_SCENE";
            public const string PET_STATES_VIEWER_SCENE = "PET_STATES_VIEWER_SCENE";
        }

        public static class UiKeys {
            public const string FIREBASE_LOGIN_UI = "FIREBASE_LOGIN_UI";
            public const string FIREBASE_CREATE_ACCOUNT_UI = "FIREBASE_CREATE_ACCOUNT_UI";
            public const string TASK_DISPLAY_UI = "TASK_DISPLAY_UI";
            public const string FAMILY_MANAGEMENT_UI = "FAMILY_MANAGEMENT_UI";
            public const string USER_PROFILE_UI = "USER_PROFILE_UI";
            public const string NOTIFICATION_STATUS_UI = "NOTIFICATION_STATUS_UI";
            public const string CONFIRMATION_POPUP_UI = "CONFIRMATION_POPUP_UI";
            public const string PRELOADER_UI = "PRELOADER_UI";
            public const string DEVELOPER_MODE_UI = "DEVELOPER_MODE_UI";
            public const string BURGER_MENU_UI = "BURGER_MENU_UI";
            public const string MAIN_SCREEN_UI = "MAIN_SCREEN_UI";
            public const string SETTINGS_UI = "SETTINGS_UI";
            public const string CONNECTION_SETUP_UI = "CONNECTION_SETUP_UI";
            public const string MESSAGES_UI = "MESSAGES_UI";
            public const string INVITE_CREATION_UI = "INVITE_CREATION_UI";
            public const string INVITE_CONFIRM_UI = "INVITE_CONFIRM_UI";
            public const string ABNORMAL_REPORT_UI = "ABNORMAL_REPORT_UI";
            public const string BREED_SELECTION_UI = "BREED_SELECTION_UI";
            public const string DATE_PICKER_UI = "DATE_PICKER_UI";
            public const string ADD_PET_UI = "ADD_PET_UI";
            public const string PET_SCHEDULE_UI = "PET_SCHEDULE_UI";
            public const string PET_SCHEDULE_EDITOR_UI = "PET_SCHEDULE_EDITOR_UI";
            public const string PET_SCHEDULE_CREATOR_UI = "PET_SCHEDULE_CREATOR_UI";
            public const string PET_PROFILE_UI = "PET_PROFILE_UI";
            public const string REQUEST_FOR_INVITE_UI = "REQUEST_FOR_INVITE_UI";
            public const string GAME_PHOTO_DESCRIPTION_UI = "GAME_PET_MOOD_DESCRIPTION_UI";
            public const string GAME_PHOTO_UI = "GAME_PHOTO_UI";
            public const string GAME_RESULT_UI = "GAME_RESULT_UI";
            public const string PET_PARAMETER_UI = "PET_PARAMETER_UI";
            public const string TASK_COMPLETED_UI = "TASK_COMPLETED_UI";
            public const string SUPPORT_UI = "SUPPORT_UI";
            public const string PROGRESS_UI = "PROGRESS_UI";
            public const string TUTORIAL_GENERIC_UI = "TUTORIAL_GENERIC_UI";
            public const string TUTORIAL_USER_AGE_UI = "TUTORIAL_USER_AGE_UI";
            public const string TUTORIAL_PET_NAME_UI = "TUTORIAL_PET_NAME_UI";
            public const string TUTORIAL_TASK_UI = "TUTORIAL_TASK_UI";
            public const string TUTORIAL_TASK_BEST_UI = "TUTORIAL_TASK_BEST_UI";
            public const string TUTORIAL_TAKE_PHOTO_UI = "TUTORIAL_TAKE_PHOTO_UI";
            public const string TUTORIAL_FAKE_CAMERA_UI = "TUTORIAL_FAKE_CAMERA_UI";
            public const string TUTORIAL_SEND_PHOTO_UI = "TUTORIAL_SEND_PHOTO_UI";
            public const string FLASHLIGHT_UI = "FLASHLIGHT_UI";
            public const string EMAIL_INPUT_UI = "EMAIL_INPUT_UI";
            public const string CREATE_NEW_PET_UI = "CREATE_NEW_PET_UI";
            
            public const string TASK_ADVISER_UI = "TASK_ADVISER_UI";

            public const string TASK_COMPLETED_TAKE_PHOTO_UI = "TASK_COMPLETED_TAKE_PHOTO_UI";
            public const string TASK_COMPLETED_SEND_PHOTO_UI = "TASK_COMPLETED_SEND_PHOTO_UI";
            public const string TASK_COMPLETED_OWNER_APPROVE_UI = "TASK_COMPLETED_OWNER_APPROVE_UI";
            public const string TASK_COMPLETED_RESULT_UI = "TASK_COMPLETED_RESULT_UI";

            public const string EXPLANATION_TOOLTIP_UI = "EXPLANATION_TOOLTIP_UI";

            //Veterinary Chat UI Widgets
            public const string VET_CHAT_UI = "VET_CHAT_UI";
            public const string VET_CHAT_SEEKING_UI = "VET_CHAT_SEEKING_UI";
            public const string VET_CHAT_RESULT_UI = "VET_CHAT_RESULT_UI";
            public const string VET_CHAT_TUTORIAL_UI = "VET_CHAT_TUTORIAL_UI";
            
            public const string LEADERBOARD_UI = "LEADERBOARD_UI";
            
            //New Tutorial UI Widgets
            public const string NEWTUTORIAL01UIWIDGET_UI = "NEWTUTORIAL01UIWIDGET_UI";
            public const string NEWTUTORIAL02UIWIDGET_UI = "NEWTUTORIAL02UIWIDGET_UI";
            public const string NEWTUTORIAL03UIWIDGET_UI = "NEWTUTORIAL03UIWIDGET_UI";
            public const string NEWTUTORIAL04UIWIDGET_UI = "NEWTUTORIAL04UIWIDGET_UI";
            public const string NEWTUTORIAL05UIWIDGET_UI = "NEWTUTORIAL05UIWIDGET_UI";
            public const string NEWTUTORIAL06UIWIDGET_UI = "NEWTUTORIAL06UIWIDGET_UI";
            public const string NEWTUTORIAL07UIWIDGET_UI = "NEWTUTORIAL07UIWIDGET_UI";
            public const string NEWTUTORIAL08UIWIDGET_UI = "NEWTUTORIAL08UIWIDGET_UI";
            public const string NEWTUTORIAL09UIWIDGET_UI = "NEWTUTORIAL09UIWIDGET_UI";
            public const string NEWTUTORIAL10UIWIDGET_UI = "NEWTUTORIAL10UIWIDGET_UI";
            public const string NEWTUTORIAL11UIWIDGET_UI = "NEWTUTORIAL11UIWIDGET_UI";
            public const string NEWTUTORIAL12UIWIDGET_UI = "NEWTUTORIAL12UIWIDGET_UI";
            public const string NEWTUTORIAL13UIWIDGET_UI = "NEWTUTORIAL13UIWIDGET_UI";

            //First Run UI Widget
            public const string FIRST_RUN_NICKNAME_UI = "FIRST_RUN_NICKNAME_UI";
            public const string FIRST_RUN_TASK_SCHEDULE_EXPLANATION_UI = "FIRST_RUN_TASK_SCHEDULE_EXPLANATION_UI";
            public const string FIRST_RUN_GAME_INVITES_UI = "FIRST_RUN_GAME_INVITES_UI";
            public const string FIRST_RUN_NOTIFICATION_UI = "FIRST_RUN_NOTIFICATION_UI";
            public const string FIRST_RUN_GAME_ONBOARDING_UI = "FIRST_RUN_GAME_ONBOARDING_UI";
            
            public const string PET_STATES_VIEWER_UI = "PET_STATES_VIEWER_UI";
            
            public const string DEVELOPER_CONSOLE_UI = "DEVELOPER_CONSOLE_UI";
        }
        
        public static class SocketKeys {
            public const string CONNECTIONS_EVENT = "connections";
            public const string COMMAND_EVENT = "command";
            
            public const string LOGIN_EVENT = "login";
            
            public const string GAMES_AWAITING_EVENT = "games_awaiting";
            public const string GAME_COMPLETE = "game_complete";
            
            public const string MESSAGE_TRANSITION_EVENT = "message_transition";
            
            public const string PET_WATCHER_EVENT = "pet_watcher";
            public const string PET_UPDATE_EVENT = "pet_update";
            
            public const string USER_MODEL_EVENT = "user_model";
            public const string USER_SCORE_EVENT = "user_score";

            public const string UPDATE_AVATAR_EVENT = "update_avatar";
            
            public const string TASK_CONFIRM_EVENT = "task_confirm";
            public const string TASK_MODEL_EVENT = "task_model";
            public const string TASK_NOTIFICATION_EVENT = "task_notification";
            public const string TASK_TRANSITION_EVENT = "task_transition";
            
            public const string TASK_FLOW_EVENT = "task_flow";
            
            public const string LOGOUT = "logout";
            public const string NOTIFICATION = "notification";
            public const string HISTORY = "history";
            public const string ASSIGNEE = "assignee";
            public const string OWNER_CONFIRM = "owner_confirm";
            public const string USER_UPDATE = "user_update";
            public const string UPDATE = "update";
            public const string ADD = "add";
            public const string REMOVE = "remove";
            public const string WIN = "win";
            public const string LOSE = "lose";
            public const string USER = "user";
            public const string PET = "pet";
        }
        
        public static class GameTypesKeys {
            public const string GAME_EMOTIONS = "game_001";
        }
        
        public static class TutorialKeys {
            
            //Tutorial Steps Types
            public const string CUTSCENE_TYPE = "cutscene";
            public const string ANIMATION_TYPE = "animation";
            public const string MULTI_POPUP_TYPE = "multi-popup";
            public const string POPUP_TYPE = "popup";
            public const string TASK_TYPE = "task";

            //Onboadring Keys
            public const string TUTORIAL_PATH = "tutorial";
            public const string TUTORIAL_NEW_PATH = "tutorial_new";

            public const string INTRO = "intro";
            public const string AGE = "age";
            public const string ADULT = "adult";
            public const string CHILD = "child";

            public const string TUTORIAL_TYPE = "tutorial_type";
            public const string TUTORIAL_STEP = "tutorial_step";
            public const string TUTORIAL_COMPLETE = "tutorial_complete";

            public static int UserAge { get; set; }
            
            //First Run Keys
            public const string NAME = "name";
            public const string PET_SCHEDULE = "pet_schedule";
        }
        
        public static class SpriteIconsGroupKeys {
            public const string ADVISER_ICONS = "adviser";
            public const string MESSAGES_ICONS = "messages";
            public const string TASKS_ICONS = "tasks";
            public const string TASKS_STATUS = "status";
            public const string ASSIGNEE_ICONS = "assignee";
        }
        
        public static class UsersPetRoleKeys {
            public const string OWNER = "owner";
            public const string WATCHER = "watcher";
        }
        
        public static class UsersFamilyRoleKeys {
            public const string MALE = "male";
            public const string FEMALE = "female";
            public const string BOY = "boy";
            public const string GIRL = "girl";
            public const string UNKNOWN = "unknown";
        }
        
        public static class AssigneeIconsKeys {
            public const string DONE = "done";
            public const string ADVISOR = "advisor";
        }
        
        public static class TaskTypes {
            public const string FEED = "feed";
            public const string PEE = "pee";
            public const string WALK = "walk";
        }
        
        public static class ScheduleKeys {
            public const string STATUS_UNUSED = "unused";
            public const string STATUS_COMPLETED = "completed";
            public const string STATUS_EXPIRED = "expired";
        }
        
        public static class PetGenders {
            public const string MALE = "male";
            public const string FEMALE = "female";
        }
        
        public static class GamePetMoodContent {
            public const string MOOD_RESPONDENT_SUCCESS = "Congratulations!\n\nYour choice same as\nanother player.";
            public const string MOOD_RESPONDENT_FALIED = "Bad luck, Try more!\n\nYour choice different\nto the another player.";
            public const string MOOD_SUCCESS = "Congratulations!\n\nAnother player choice\nsame as you did!";
            public const string MOOD_FALIED = "Bad luck, Try more!\n\nAnother player choice\ndifferent than you did.";
        }
    }
}