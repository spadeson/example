using System;
using UnityEngine.SocialPlatforms.Impl;

namespace PoketPet.LeaderBoard {
    
    [Serializable]
    public class LeaderBoardItemModel {
        public string id;
        public string nickname;
        public string score;
        public string avatar;
    }
}