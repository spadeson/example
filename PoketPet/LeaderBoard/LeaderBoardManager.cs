using System;
using System.Collections.Generic;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Users;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.LeaderBoard {
    
    public static class LeaderBoardManager {

        public static event Action OnUpdate;
        
        public static List<LeaderBoardItemModel> LeaderBoardItems;

        public static void GetLeaderBoard() {

            var requestData = new RequestData<LeaderBoardRequestData>(UserProfileManager.CurrentUser.id, new LeaderBoardRequestData{
                petID = PetsProfileManager.CurrentPet.id
            }, "get_leaderboard_by_pet");
        
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("friends", requestData);
            operation.OnDataReceived += GetLeaderBoardHandler;
        }

        private static void GetLeaderBoardHandler(string data) {
            Debug.Log($"Get LeaderBoard JSON Response: {data}");

            var response = JsonUtility.FromJson<ResponseData<List<LeaderBoardItemModel>>>(data);
            LeaderBoardItems = response.data;
            OnUpdate?.Invoke();
        }
    }
}