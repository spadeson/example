﻿using UnityEngine;

namespace PoketPet.EventsSubsystem {
    [CreateAssetMenu(fileName = "EventActionConfig", menuName = "PoketPet/Events/EventActionConfig", order = 0)]
    public class EventActionConfig : ScriptableObject {
        public EventActionTypes actionType;
        public string targetTag;
        public string animatorLayerName;
        public string animatorLayerTrigger;
        public string soundEffect;
        public bool soundLooped;
        public bool additive;
        public float duration;
        public string pickableItemTag;
        public string pickableItemBoneTag;
        public Vector3 pickableItemOffsetPosition;
        public Vector3 pickableItemOffsetRotation;
        public Vector3 pickableItemOffsetScale;
    }
}