﻿using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.EventsSubsystem {
    [CreateAssetMenu(fileName = "EventConfig", menuName = "PoketPet/Events/EventConfig", order = 0)]
    public class EventConfig : ScriptableObject {
        public string eventId;
        public EventTypes eventType;

        public List<EventActionConfig> eventActionsList = new List<EventActionConfig>();
    }
}