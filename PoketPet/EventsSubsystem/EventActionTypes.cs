﻿namespace PoketPet.EventsSubsystem {
    public enum EventActionTypes {
        GO_TO_POINT = 0,
        PLAY_ANIMATION = 1,
        GO_TO_WITH_ANIMATION = 2,
        TAKE_ITEM = 3
    }
}