﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.EventsSubsystem {
    public class EventsManager : MonoBehaviour {

        public static EventsManager Instance;
        
        public List<EventConfig> eatEventsConfigsList = new List<EventConfig>();
        public List<EventConfig> walkEventConfigsList = new List<EventConfig>();
        
        private EventConfig _currentEventDataModel;

        private IEventAction _currentEventAction;

        private PetMainController _petMainController;

        private void Awake() {
            Instance = this;
        }
        
        #if UNITY_EDITOR

        private void Update() {
            
            if (Input.GetKeyDown(KeyCode.F1)) {
                ExecuteEvent(EventTypes.EAT, 0);
            }
            
            if (Input.GetKeyDown(KeyCode.F2)) {
                ExecuteEvent(EventTypes.EAT, 1);
            }
            
            if (Input.GetKeyDown(KeyCode.F3)) {
                ExecuteEvent(EventTypes.EAT, 2);
            }
            
            if (Input.GetKeyDown(KeyCode.F4)) {
                ExecuteEvent(EventTypes.WALK, 0);
            }
            
            if (Input.GetKeyDown(KeyCode.F5)) {
                ExecuteEvent(EventTypes.WALK, 1);
            }
            
            if (Input.GetKeyDown(KeyCode.F6)) {
                ExecuteEvent(EventTypes.WALK, 2);
            }
        }

        #endif

        public void ExecuteEvent(EventConfig eventDataModel) {
            _petMainController = FindObjectOfType<PetMainController>();
            StartCoroutine(PlayEventsActions_Co(eventDataModel));
        }
        
        public void ExecuteEvent(EventTypes eventTypes, int eventIndex) {
            
            List<EventConfig> eventConfigs;
            
            switch (eventTypes) {
                case EventTypes.EAT:
                    eventConfigs = eatEventsConfigsList;        
                    break;
                case EventTypes.WALK:
                    eventConfigs = walkEventConfigsList;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(eventTypes), eventTypes, null);
            }

            var eventDataModel = eventConfigs[eventIndex];
            
            _petMainController = FindObjectOfType<PetMainController>();
            
            StartCoroutine(PlayEventsActions_Co(eventDataModel));
        }

        IEnumerator PlayEventsActions_Co(EventConfig eventDataModel) {

            _currentEventDataModel = eventDataModel;

            foreach (var eventActionData in _currentEventDataModel.eventActionsList) {
                yield return PlayAction_Co(eventActionData.actionType, eventActionData);
            }
        }

        IEnumerator PlayAction_Co(EventActionTypes type, EventActionConfig actionConfig) {
            
            Debug.Log($"<color=magenta>Play Event Action: {type}</color>");
            
            switch (type) {
                case EventActionTypes.GO_TO_POINT:
                    _currentEventAction = new EventActionTypeGoToPoint();
                    break;
                case EventActionTypes.PLAY_ANIMATION:
                    _currentEventAction = new EventActionPlayAnimation();
                    break;
                case EventActionTypes.GO_TO_WITH_ANIMATION:
                    _currentEventAction = new EventActionGoToWithAnimation();
                    break;
                case EventActionTypes.TAKE_ITEM:
                    _currentEventAction = new EventActionPickItem();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _currentEventAction.StartExecution();
            
            _currentEventAction.ExecuteEventAction(actionConfig, _petMainController);

            if (type == EventActionTypes.PLAY_ANIMATION || type == EventActionTypes.TAKE_ITEM) {
                yield return new WaitForSeconds(actionConfig.duration);
            } else {
                yield return new WaitUntil(() => _currentEventAction.ActionCompleted);
            }
            
            _petMainController.petAnimatorController.ResetAllLayers(1f);

            _currentEventAction = null;

            _petMainController.petMovementController.BeginPatrolling();
        }
    }
}