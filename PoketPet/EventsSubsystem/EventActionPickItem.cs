﻿using PoketPet.EventsSubsystem;
using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using UnityEngine;
using VIS.CoreMobile;

namespace DefaultNamespace {
    public class EventActionPickItem: IEventAction {
        [SerializeField] private Transform pickItemTransform;
        [SerializeField] private Transform rootBoneTransform;
        public bool ActionCompleted { get; set; }
        public void StartExecution() {
            ActionCompleted = false;
        }

        public void ExecuteEventAction(EventActionConfig actionConfig, PetMainController petMainController) {
            petMainController.petMovementController.StopCharacter();
            
            petMainController.petAnimatorController.SetLayerWeight(actionConfig.animatorLayerName, 1f);
            petMainController.petAnimatorController.SetTrigger(actionConfig.animatorLayerTrigger);
            
            if (pickItemTransform == null) {
                pickItemTransform = GameObject.Find(actionConfig.pickableItemTag).transform;
            }
            if (rootBoneTransform == null) {
                rootBoneTransform = GameObject.Find(actionConfig.pickableItemBoneTag).transform;
            }
            
            Debug.Log($"<color=red>pickItemTransform: {pickItemTransform.name}</color>");
            Debug.Log($"<color=red>rootBoneTransform: {rootBoneTransform.name}</color>");
            
            pickItemTransform.SetParent(rootBoneTransform, false);
            pickItemTransform.position = actionConfig.pickableItemOffsetPosition;
            pickItemTransform.rotation = Quaternion.Euler(actionConfig.pickableItemOffsetRotation);
            pickItemTransform.localScale = actionConfig.pickableItemOffsetScale;
            
            if (!string.IsNullOrEmpty(actionConfig.soundEffect)) {
                CoreMobile.Instance.AudioManager.PlaySoundClip(actionConfig.soundEffect, actionConfig.soundLooped, 0.75f);
            }
            
            ActionCompleted = true;
        }
    }
}