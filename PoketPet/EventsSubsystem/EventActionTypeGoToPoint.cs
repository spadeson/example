﻿using System;
using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using UnityEngine;
using Object = UnityEngine.Object;

namespace PoketPet.EventsSubsystem {
    [Serializable]
    public class EventActionTypeGoToPoint : IEventAction {
        
        [SerializeField] public Transform targetTransform;

        public bool ActionCompleted { get; set; }

        public void StartExecution() {
            ActionCompleted = false;
        }

        public void ExecuteEventAction(EventActionConfig actionConfig, PetMainController petMainController) {
            if (targetTransform == null) {
                targetTransform = GameObject.Find(actionConfig.targetTag).transform;
            }
            
            Debug.Log($"Target Transform: {targetTransform.position}");

            petMainController.petMovementController.OnCompletedMovement += () => {
                ActionCompleted = true;
            };
            
            petMainController.petMovementController.MoveCharacterToTarget(targetTransform.position);
        }
    }
}