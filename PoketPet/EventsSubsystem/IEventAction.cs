using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;

namespace PoketPet.EventsSubsystem {
    public interface IEventAction {
        bool ActionCompleted { get; set; }
        void StartExecution();
        void ExecuteEventAction(EventActionConfig actionConfig, PetMainController petMainController);
    }
}
