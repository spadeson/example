using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.EventsSubsystem {
    public class EventActionGoToWithAnimation : IEventAction {
        public bool ActionCompleted { get; set; }
        
        [SerializeField] public Transform targetTransform;
        
        public void StartExecution() {
            ActionCompleted = false;
        }

        public void ExecuteEventAction(EventActionConfig actionConfig, PetMainController petMainController) {
            
            if (targetTransform == null) {
                targetTransform = GameObject.Find(actionConfig.targetTag).transform;
            }
            
            Debug.Log($"<color=magenta>Target Transform: {targetTransform.position}</color>");

            if (!string.IsNullOrEmpty(actionConfig.soundEffect)) {
                CoreMobile.Instance.AudioManager.PlaySoundClip(actionConfig.soundEffect, actionConfig.soundLooped, 0.75f);
            }
            
            petMainController.petMovementController.OnCompletedMovement += () => {
                ActionCompleted = true;
            };
            
            petMainController.petMovementController.MoveCharacterToTarget(targetTransform.position);
            
            petMainController.petAnimatorController.SetLayerWeight(actionConfig.animatorLayerName, 1f);
            petMainController.petAnimatorController.SetTrigger(actionConfig.animatorLayerTrigger);
        }
    }
}
