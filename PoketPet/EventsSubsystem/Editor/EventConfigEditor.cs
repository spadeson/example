using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PoketPet.EventsSubsystem.Editor {
    [CustomEditor(typeof(EventConfig))]
    public class EventConfigEditor : UnityEditor.Editor {

        private EventActionConfig _newEventActionConfig;
        private EventActionTypes _newEventActionType;
        
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            
            serializedObject.Update();

            var eventConfig = (EventConfig)target;
            
            EditorGUILayout.Space();

            if (eventConfig.eventActionsList != null) {
                
                foreach (var eventActionData in eventConfig.eventActionsList) {
                    
                    if(eventActionData == null) continue;
                
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                
                    eventActionData.actionType = (EventActionTypes)EditorGUILayout.EnumPopup("Type: ", eventActionData.actionType);
                
                    EditorGUILayout.EndVertical();
                }
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}
