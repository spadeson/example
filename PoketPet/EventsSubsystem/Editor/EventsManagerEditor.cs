﻿using UnityEditor;
using UnityEngine;

namespace PoketPet.EventsSubsystem.Editor {
    [CustomEditor(typeof(EventsManager))]
    public class EventsManagerEditor : UnityEditor.Editor {
        private EventTypes _eventType;
        private int _eventID;
        public override void OnInspectorGUI() {
            
            base.OnInspectorGUI();
            
            var eventsManager = (EventsManager)target;
            
            EditorGUILayout.Space();

            _eventType = (EventTypes)EditorGUILayout.EnumPopup("EventType", _eventType);
            _eventID = EditorGUILayout.IntSlider("EventID", _eventID, 0, 2);

            if (GUILayout.Button("Execute Event", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                eventsManager.ExecuteEvent(_eventType, _eventID);
            }
        }
    }
}