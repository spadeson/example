using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.Main;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.EventsSubsystem {
    public class EventActionPlayAnimation : IEventAction {
     
        public bool ActionCompleted { get; set; }
        public void StartExecution() {
            ActionCompleted = false;
        }

        public void ExecuteEventAction(EventActionConfig actionConfig, PetMainController petMainController) {
            
            petMainController.petMovementController.StopCharacter();
            
            petMainController.petAnimatorController.SetLayerWeight(actionConfig.animatorLayerName, 1f);
            petMainController.petAnimatorController.SetTrigger(actionConfig.animatorLayerTrigger);

            if (!string.IsNullOrEmpty(actionConfig.soundEffect)) {
                CoreMobile.Instance.AudioManager.PlaySoundClip(actionConfig.soundEffect, actionConfig.soundLooped, 0.75f);
                
            }
            
        }
    }
}
