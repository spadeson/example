using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Object = UnityEngine.Object;

namespace PoketPet.CutScenesManager {
    [RequireComponent(typeof(PlayableDirector))]
    public class CutSceneManager : MonoBehaviour {
        
        public static CutSceneManager Instance;
        
        public event Action OnCutSceneStarted;
        public event Action OnCutScenePaused;
        public event Action OnCutSceneCompleted;

        [SerializeField] private List<TimelineAsset> cutSceneConfigs;

        [SerializeField] private PlayableDirector playableDirector;

        private GameObject _currentPet;

        #region MonoBehaviour

        private void Awake() {
            Instance = this;
            
            playableDirector = GetComponent<PlayableDirector>();
            
            playableDirector.played += PlayableDirectorPlayHandle;
            playableDirector.played += PlayableDirectorPauseHandle;
            playableDirector.stopped += PlayableDirectorStoppedHandle;
        }

        private void OnDestroy() {
            playableDirector.played -= PlayableDirectorPlayHandle;
            playableDirector.played -= PlayableDirectorPauseHandle;
            playableDirector.stopped -= PlayableDirectorStoppedHandle;
        }

        #endregion

        #region Public Methods

        public void PlayCutScene(string cutSceneKey) {
            var timeline = cutSceneConfigs.Find(c => c.name.Contains(cutSceneKey));
            playableDirector.playableAsset = timeline;
            SetBinding(timeline, "PetTranslation", _currentPet);
            SetBinding(timeline, "PetAnimations", _currentPet);
            playableDirector.Play(timeline);
        }
        
        public void PlayCutScene(int cutSceneId) {
            var timeline = cutSceneConfigs[cutSceneId];
            playableDirector.playableAsset = timeline;
            playableDirector.Play(timeline);
        }

        public void SetCurrentPet(GameObject pet) {
            _currentPet = pet;
        }

        #endregion

        private void PlayableDirectorPlayHandle(PlayableDirector director) {
            OnCutSceneStarted?.Invoke();
        }
        
        private void PlayableDirectorPauseHandle(PlayableDirector director) {
            OnCutScenePaused?.Invoke();
        }

        private void PlayableDirectorStoppedHandle(PlayableDirector director) {
            OnCutSceneCompleted?.Invoke();
        }

        private void SetBinding(TimelineAsset timelineAsset, string trackName, Object objectToBind) {
            var track = timelineAsset.GetOutputTracks().FirstOrDefault(t => t.name == trackName);
            playableDirector.SetGenericBinding(track, objectToBind);
        }
    }
}
