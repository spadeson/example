using UnityEditor;
using UnityEngine;

namespace PoketPet.CutScenesManager.Editor {
    [CustomEditor(typeof(CutSceneManager))]
    public class CutSceneManagerEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var cutSceneManager = (CutSceneManager)target;

            if (GUILayout.Button("Play")) {
                cutSceneManager.PlayCutScene(0);
            }
        }
    }
}
