using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Settings;
using PoketPet.User;
using VIS.CoreMobile;

namespace PoketPet.Settings {
    public static class SettingsManager {

        private const float DEFAULT_SOUNDS_VOLUME = 0.5f;

        public static void Initialize() {
            //Initializes sounds volume
            var soundsVolume = CoreMobile.Instance.LocalDataManager.IsDataExists(GlobalVariables.LocalDataKeys.SOUNDS_VOLUME) ? CoreMobile.Instance.LocalDataManager.GetLocalDataFloat(GlobalVariables.LocalDataKeys.SOUNDS_VOLUME) : DEFAULT_SOUNDS_VOLUME;
            CoreMobile.Instance.LocalDataManager.SaveLocalDataFloat(GlobalVariables.LocalDataKeys.SOUNDS_VOLUME, soundsVolume);
            CoreMobile.Instance.AudioManager.SetSoundsVolume(soundsVolume);
        }

        #region Sounds

        public static float GetSoundsVolume() {
            var soundsVolume = CoreMobile.Instance.LocalDataManager.IsDataExists(GlobalVariables.LocalDataKeys.SOUNDS_VOLUME) ? CoreMobile.Instance.LocalDataManager.GetLocalDataFloat(GlobalVariables.LocalDataKeys.SOUNDS_VOLUME) : DEFAULT_SOUNDS_VOLUME;  
            return soundsVolume;
        }
        
        public static void SetSoundsVolume(float value) {
            CoreMobile.Instance.AudioManager.SetSoundsVolume(value);
            CoreMobile.Instance.LocalDataManager.SaveLocalDataFloat(GlobalVariables.LocalDataKeys.SOUNDS_VOLUME, value);
        }

        #endregion

        #region WeightUnits

        public static string GetWeightUnits() {
            return UserProfileManager.CurrentUser.settings.weight_units;
        }

        public static void SetWeightUnits(string weightUnits) {
            UserProfileManager.SetWeightUnits(weightUnits);
        }

        #endregion

        #region TimeFormat

        public static string GetTimeFormat() {
            return UserProfileManager.CurrentUser.settings.time_format;
        }

        public static void SetTimeFormat(string timeFormat) {
            UserProfileManager.CurrentUser.settings.time_format = timeFormat;
        }

        #endregion

        #region Push Notification

        public static bool GetPushNotificationStatus() {
            return UserProfileManager.CurrentUser.settings.push_active;
        }

        public static void EnableFCMToken() {
            CoreMobile.Instance.FirebaseManager.CloudMessagingManager.FCMTokenEnable();
            UserProfileManager.CurrentUser.settings.push_active = true;
        }
        
        public static void DisableFCMToken() {
            CoreMobile.Instance.FirebaseManager.CloudMessagingManager.FCMTokenDisable();
            UserProfileManager.CurrentUser.settings.push_active = false;
        }

        #endregion
        
        public static void SaveToBackend() {
            var settingsRequest = new RequestData<SettingsDataModel> {
                from = UserProfileManager.CurrentUser.id,
                data = new SettingsDataModel {
                    time_format = UserProfileManager.CurrentUser.settings.time_format,
                    weight_units = UserProfileManager.CurrentUser.settings.weight_units,
                    localisation = "en",
                    push_active = UserProfileManager.CurrentUser.settings.push_active
                }, action = "update_settings"
            };
            CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("settings", settingsRequest);
        }
    }
}
