﻿using System;
using System.Collections.Generic;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Tasks;
using PoketPet.Network.SocketBinds.Tasks;
using PoketPet.UI.TaskDisplay;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.Task {
    public class TaskManager : MonoBehaviour {

        public static List<TaskResponseData> Tasks = new List<TaskResponseData>();

        public List<TaskResponseData> pendingTasks = new List<TaskResponseData>();
        public List<TaskResponseData> activeTasks = new List<TaskResponseData>();
        public List<TaskResponseData> completedTasks = new List<TaskResponseData>();

        public static event Action OnUpdate;
        public static event Action<TaskResponseData> OnRemove;

        public static void AddSocketListeners() {
            TaskModelSocketBind.Instance.OnUpdateByUserAction += GetTaskById;
            TaskModelSocketBind.Instance.OnAssigneeAction += (taskId, userId) => {
                var value = Tasks.Find(item => item.id == taskId);
                value.assignee = userId;
                OnUpdate?.Invoke();
            };
            TaskTransitionSocketBind.Instance.OnAddAction += GetTaskById;
            TaskTransitionSocketBind.Instance.OnRemoveAction += id => {
                var value = Tasks.Find(item => item.id == id);
                Tasks.Remove(value);
                OnRemove?.Invoke(value); 
            };
        }

        public  static void CompleteTaskWithConfirm(string taskId, Sprite sprite) {
            var requestData = new ConfirmTaskByIdRequestData() {
                    from = UserProfileManager.CurrentUser.id,
                    taskID = taskId,
                    image = sprite.texture.EncodeToPNG(),
                    action = "close_confirm_task"
            };
        
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("messagesForm", requestData.GetForm());
            operation.OnDataReceived += CheckStatusHandler;
        }

        public  static void CompleteTask(string id) {
            var userId = UserProfileManager.CurrentUser.id;

            Debug.Log($"User Id: {userId}, task Id: {id}");
            
            var requestData = new RequestData<CompleteTaskByIdRequestData>(userId, new CompleteTaskByIdRequestData(id), "close");
        
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("tasks", requestData);
            operation.OnDataReceived += CheckStatusHandler;
        }
    
        public  static void AddConfirmToFlow(string taskId) {
            var userId = UserProfileManager.CurrentUser.id;

            Debug.Log($"User Id: {userId}, task Id: {taskId}");
            
            var requestData = new RequestData<AddConfirmToFlowRequestData> {
                    @from = userId, 
                    data = new AddConfirmToFlowRequestData {taskID = taskId}, 
                    action = "add_confirm_flow"
            };
        
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("friends", requestData);
            operation.OnDataReceived += CheckStatusHandler;
        }

        public  static void TransferTask() { }
    
        public static void AssignTask(string taskId, string toUserId)
        {
            var userId = UserProfileManager.CurrentUser.id;

            var requestData = new RequestData<AssigneeTaskRequestData>(userId, new AssigneeTaskRequestData(taskId, toUserId), "assign_task");
            
            var friendsRequestOperation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("friends", requestData);
            friendsRequestOperation.OnDataReceived += CheckStatusHandler;
            Debug.Log($"!!! assign by user {userId} task id {taskId} to user with id {toUserId}");
        }
    
        private  static void GetTaskById(string id) {
            var userId = UserProfileManager.CurrentUser.id;

            Debug.Log($"User Id: {userId}, task Id: {id}");
            
            var requestData = new RequestData<GetTaskByIdRequestData>(userId, new GetTaskByIdRequestData(id), "get");
        
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("tasks", requestData);
            operation.OnDataReceived += GetTaskByIdHandler;
        }

        private  static void GetTaskByIdHandler(string data) {
            Debug.Log($"Get Tasks JSON Response: {data}");
            
            var taskResponseData = JsonUtility.FromJson<ResponseData<TaskResponseData>>(data).data;

            var value = Tasks.Find(item => item.id == taskResponseData.id);
            Tasks.Remove(value);
            Tasks.Add(taskResponseData);
        
            OnUpdate?.Invoke();
        }
    
        public static void GetTasksByPetId(string petId) {
            
            var userId = UserProfileManager.CurrentUser.id;

            Debug.Log($"User Id: {userId}, Pet Id: {petId}");

            var requestData = new RequestData<GetTaskByPetIdRequestData>(userId, new GetTaskByPetIdRequestData(userId, petId), "get_tasks_by_pet");

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("flows", requestData);
            operation.OnDataReceived += GetTasksByPetIdHandler;
        }

        private static void GetTasksByPetIdHandler(string data) {
            Debug.Log($"Get Tasks JSON Response: {data}");

            var response = JsonUtility.FromJson<ResponseData<List<TaskResponseData>>>(data);

            foreach (var task in response.data) {
                Debug.Log($"Task Id: {task.id}");
            }

            //CoreMobile.Instance.NetworkManager.RESTNetworkManager.OnDataReceived -= GetTasksByPetIdHandler;

            var taskDisplayWidgetData = new TaskDisplayUIWidgetData {taskDataModels = TaskManager.Tasks};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.TASK_DISPLAY_UI, taskDisplayWidgetData);

            CreateTasksListByServerResponse(response.data);
        }
    
        private static void CreateTasksListByServerResponse(List<TaskResponseData> tasksResponse) {
            Tasks = tasksResponse;
            /*foreach (var task in tasksResponse) {
                Tasks.Add(new TaskResponseData {
                        id = task.id,
                        petID = task.petID,
                        type = task.type,
                        assignee = task.assignee,
                        meta = new TaskMeta {
                                title = task.meta.title,
                                description = task.meta.description,
                                type = task.meta.type
                        },
                        life_flow = new TaskLifeFlow {
                                create = task.life_flow.create,
                                notification = task.life_flow.notification,
                                best = task.life_flow.best,
                                expire = task.life_flow.expire,
                                late = task.life_flow.late
                        }
                });
            }*/
        
            Debug.Log("Task List Updated");
            OnUpdate?.Invoke();
        }
    

        private  static void CheckStatusHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
            
            }else {
                // TODO: Release Error Status 
            }
        }
    }
}