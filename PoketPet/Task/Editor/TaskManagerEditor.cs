﻿using PoketPet.Task;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TaskManager))]
public class TaskManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var taskManager = (TaskManager) target;
        
        EditorGUILayout.Space();

        #region Active Tasks
        
        EditorGUILayout.LabelField("Active Tasks", EditorStyles.boldLabel);

        for (int i = 0; i < taskManager.activeTasks.Count; i++)
        {
            var task = taskManager.activeTasks[i];
            
            EditorGUILayout.LabelField(task.id);
            
            EditorGUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Stop Task", GUILayout.Width(96), GUILayout.Height(24)))
            {
                //taskManager.TransferTask();
            }
            
            if (GUILayout.Button("Complete Task", GUILayout.Width(96), GUILayout.Height(24)))
            {
                
            }
            
            EditorGUILayout.EndHorizontal();
        }
        
        #endregion
        
        EditorGUILayout.Space();

        #region Pending Tasks
        
        EditorGUILayout.LabelField("Pending Tasks", EditorStyles.boldLabel);

        for (int i = 0; i < taskManager.pendingTasks.Count; i++)
        {
            var task = taskManager.pendingTasks[i];
            
            EditorGUILayout.LabelField("Task ID:", task.id);
            EditorGUILayout.LabelField("Timer ID:", task.id);
            
            EditorGUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Activate Task", GUILayout.Width(96), GUILayout.Height(24)))
            {
               // taskManager.TransferTask();
            }
            
            if (GUILayout.Button("Complete Task", GUILayout.Width(96), GUILayout.Height(24)))
            {
                
            }
            
            EditorGUILayout.EndHorizontal();
        }
        
        #endregion
        
        EditorGUILayout.Space();

        #region Completed Tasks
        
        EditorGUILayout.LabelField("Copmleted Tasks", EditorStyles.boldLabel);

        for (int i = 0; i < taskManager.completedTasks.Count; i++)
        {
            var task = taskManager.completedTasks[i];
            
            EditorGUILayout.LabelField(task.id);
            
            EditorGUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Activate Task", GUILayout.Width(96), GUILayout.Height(24)))
            {
               // taskManager.TransferTask();
            }
          
            EditorGUILayout.EndHorizontal();
        }
        
        #endregion
    }
}
