using System;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace PoketPet.Camera {
    [ExecuteInEditMode]
    public class CamerasController : MonoBehaviour {
        
        [SerializeField] private List<CinemachineVirtualCamera> virtualCamerasList;

        public static CamerasController Instance;

        private Transform _targetTransform;

        [SerializeField] private CinemachineVirtualCamera currentCamera;

        private void Awake() {
            Instance = this;
            DeactivateAllCameras(0);
            currentCamera = virtualCamerasList[0];
        }

        public void SetCameraLookAtTarget(Transform target) {
            _targetTransform = target;
            foreach (var virtualCamera in virtualCamerasList) {
                virtualCamera.m_LookAt = _targetTransform;
            }
        }
        
        public void SetCameraLookAtTarget(Transform target, int cameraId) {
            _targetTransform = target;
            var virtualCamera = virtualCamerasList[cameraId];
            virtualCamera.m_LookAt = _targetTransform;
        }

        public void ActivateCameraById(int id) {
            DeactivateAllCameras(id);
            virtualCamerasList[id].gameObject.SetActive(true);
            currentCamera = virtualCamerasList[id];
            virtualCamerasList[id].m_LookAt =_targetTransform;
        }

        public void DeActivateCameraById(int id) {
            virtualCamerasList[id].gameObject.SetActive(false);
        }

        public void DeactivateAllCameras() {
            for (var i = 0; i < virtualCamerasList.Count; i++) {
                virtualCamerasList[i].gameObject.SetActive(false);
            }
        }

        public void DeactivateAllCameras(int exceptionId) {
            for (var i = 0; i < virtualCamerasList.Count; i++) {
                if (i == exceptionId) continue;
                virtualCamerasList[i].gameObject.SetActive(false);
            }
        }

        public void GetVirtualCameras() {
            virtualCamerasList = new List<CinemachineVirtualCamera>();
            virtualCamerasList.AddRange(FindObjectsOfType<CinemachineVirtualCamera>());
            virtualCamerasList.Sort( (cam1, cam2) => string.Compare(cam1.gameObject.name, cam2.gameObject.name, StringComparison.Ordinal));
        }

        public int GetCamerasCount() {
            return virtualCamerasList.Count;
        }

        public void SetActiveCameraPosition(Vector3 position) {
            currentCamera.transform.position = position;
        }
    }
}
