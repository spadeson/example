using System;
using UnityEngine;

namespace PoketPet.Camera {
    [RequireComponent(typeof(BoxCollider))]
    public class CameraTrigger : MonoBehaviour {
        [Header("Trigger Collider"), SerializeField] private BoxCollider trigger;

        [Header("Target Camera Id"), SerializeField] private int targetCameraId;

        private void Start() {
            
            if (trigger == null) {
                trigger = GetComponent<BoxCollider>();
            }

            trigger.isTrigger = true;

            targetCameraId = transform.GetSiblingIndex();
        }

        private void OnTriggerEnter(Collider other) {
            if(!other.gameObject.CompareTag("Pet")) return;
            //Debug.Log($"{gameObject.name}, was triggered by: {other.name}");
            CamerasController.Instance.ActivateCameraById(targetCameraId);
        }
    }
}
