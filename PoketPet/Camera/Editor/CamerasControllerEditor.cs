using UnityEditor;
using UnityEngine;

namespace PoketPet.Camera.Editor {
    [CustomEditor(typeof(CamerasController))]
    public class CamerasControllerEditor : UnityEditor.Editor {
        
        private int _cameraId;
        
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var camerasManager = (CamerasController)target;
            
            if (GUILayout.Button("Find Cameras", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                camerasManager.GetVirtualCameras();
            }

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            EditorGUILayout.Space();
            _cameraId = EditorGUILayout.IntSlider("Camera ID:", _cameraId, 0, camerasManager.GetCamerasCount() - 1);
            
            if (GUILayout.Button("Active Camera by ID", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                camerasManager.ActivateCameraById(_cameraId);
            }
            
            EditorGUILayout.EndVertical();
        }
    }
}
