using UnityEditor;
using UnityEngine;

namespace PoketPet.Camera.Editor {
    public class CameraTriggerGizmoDrawer {
        [DrawGizmo(GizmoType.Active | GizmoType.InSelectionHierarchy)]
        static void DrawGizmoForCameraTrigger(CameraTrigger cameraTrigger, GizmoType gizmoType) {
            Gizmos.DrawIcon(cameraTrigger.transform.position, "camera-trigger.png", true);
        }
    }
}