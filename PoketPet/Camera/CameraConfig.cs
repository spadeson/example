using System;
using UnityEngine;

namespace PoketPet.Camera {
    [Serializable, CreateAssetMenu(fileName = "New Camera Config", menuName = "VIS/Camera/Camera Config")]
    public class CameraConfig : ScriptableObject {
        public Vector3 move;
        public float distance;
        public float maxDist;
        public float minDist;
        public Vector3 mPos;

        public float pitchSpeed;
        public float yawSpeed;

        public float yawMinLimit;
        public float yawMaxLimit;

        public Vector3 startPosition;
        public Vector3 startRotation;

        public LayerMask LayerMask;
    }
}