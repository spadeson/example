using UnityEngine;

namespace PoketPet.Camera {

    public static class CameraUtilities {
        
        /// <summary>
        /// Clamps rotation angle and pevent roataion zero issue. 
        /// </summary>
        /// <param name="angle">Current Angle.</param>
        /// <param name="min">Minimal Angle.</param>
        /// <param name="max">Maximum Angle.</param>
        /// <returns></returns>
        public static float ClampAngle(float angle, float min, float max) {
            if (angle < -360) angle += 360;
            if (angle > 360) angle  -= 360;
            return Mathf.Clamp(angle, min, max);
        }   
    }
}
