using System;
using System.Collections.Generic;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Tasks;
using PoketPet.Network.API.RESTModels.Utils;
using PoketPet.Network.SocketBinds.Tasks;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.ScheduleKeys;

namespace PoketPet.Schedule {
    
    public static class ScheduleManager {
        
        private static List<FlowResponseData> Flows { get; set; }
        public static List<List<TaskScheduleModel>> Tasks { get; private set; }
        public static event Action OnUpdate;
        public static List<string> TaskTypeList { get; private set; }
        
        #region Public Methods

        public static FlowResponseData GetFlowByTaskType(string type) {
            return Flows.Find(item => item.type == type);
        }
        public static void AddSocketListeners() {
            TaskFlowSocketBind.Instance.OnUpdateByUserAction += GetFlowById;
        }
        /// <summary>
        /// Get tasks schedule list by pet. Rest request.
        /// </summary>
        /// <param name="id">Pet id.</param>
        public static void GetScheduleByPetId(string id) {
            var userId = UserProfileManager.CurrentUser.id;
            var requestData = new RequestData<GetFlowsByPetIdRequestData>(userId, new GetFlowsByPetIdRequestData(){petID = id}, "get_flows_by_pet");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("flows", requestData);
            operation.OnDataReceived += GetScheduleByPetIdHandler;
        }

        /// <summary>
        /// Edit task schedule. Rest request.
        /// </summary>
        /// <param name="id">Task id.</param>
        /// <param name="schedule">Schedule task time</param>
        public static void ChangeScheduleTask(string id, List<int> schedule) {
            var userId = UserProfileManager.CurrentUser.id;
            var editFlowRequestData = new EditFlowRequestData() {taskID = id, flow = schedule};
            var requestData = new RequestData<EditFlowRequestData>(userId, editFlowRequestData,"set_new_flow");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("flows", requestData);
            operation.OnDataReceived += CheckStatusHandler;
        }
        /// <summary>
        /// Delete task schedule. Rest request.
        /// </summary>
        /// <param name="id">Task id.</param>
        public static void DeleteScheduleTask(string id) {
            
        }
        #endregion
        
        #region Private Methods

        private static void GetScheduleByPetIdHandler(string data) {
            
            Debug.Log($"Get Flows JSON Response: {data}");
            
            var response = JsonUtility.FromJson<ResponseData<List<FlowResponseData>>>(data);

            foreach (var item in response.data) {
                Debug.Log($"Flow Id: {item.id}");
            }
            
            Flows = response.data;
            Debug.Log("Schedule List Updated");
            GetServerTime();
        }
        
        private static void GetFlowById(string id) {
            var userId = UserProfileManager.CurrentUser.id;
            var requestData = new RequestData<GetFlowByIdRequestData>(userId, new GetFlowByIdRequestData(){taskID = id}, "get_pet_flow");
            
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("flows", requestData);
            operation.OnDataReceived += GetFlowByIdHandler;
        }
        
        private static void GetFlowByIdHandler(string data) {
            
            Debug.Log($"Get Flow JSON Response: {data}");
            
            var response = JsonUtility.FromJson<ResponseData<FlowResponseData>>(data);
            for (var i = 0; i < Flows.Count; i++) {
                if (Flows[i].id == response.data.id) {
                    Flows[i] = response.data;
                }
            }

            GetServerTime();
        }

        private static void GetServerTime() {
            var requestData = new RequestData<List<string>>(new List<string>(), "get_server_info") {from = ""};
            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("utils", requestData);
            operation.OnDataReceived += GetServerTimeHandler;
        }
        
        private static void GetServerTimeHandler(string data) {
            
            Debug.Log($"Get Server Time JSON Response: {data}");
            
            var response = JsonUtility.FromJson<ResponseData<ServerInfoResponseData>>(data);
            SearchTaskStatus(response.data.today);
        }

        private static void SearchTaskStatus(int serverTimeStamp) {
            
            
            foreach (var flow in Flows) {
                var taskDuration = flow.duration;
                
                var historyList = flow.history;
                flow.completedStatus = new List<string>();
                
                if (historyList.Count == 0) {
                    Debug.Log("Task status: history is empty");
                }
                for (var i = 0; i < flow.every_day.Count; i++) {
                    flow.completedStatus.Add(STATUS_UNUSED);
                    var taskCreation = flow.every_day[i] + serverTimeStamp;
                    
                    foreach (var historyItem in historyList) {
                        if (taskCreation <= historyItem.time && taskCreation + taskDuration >= historyItem.time) {
                            Debug.Log($"Task status: {historyItem.userID}");
                            flow.completedStatus[i] = historyItem.userID == "server" ? STATUS_EXPIRED : STATUS_COMPLETED;
                            break;
                        }
                        Debug.Log("Task status: unused");
                    }
                }
            }

            CreateTaskLists();
        }

        private static void CreateTaskLists() {
            TaskTypeList = Flows.ConvertAll(o => o.type);
            
            Tasks = new List<List<TaskScheduleModel>>();
            foreach (var flow in Flows) {
                var taskListByType = new List<TaskScheduleModel>();
                for (var i = 0; i < flow.every_day.Count; i++) {
                    var timeSpan = TimeSpan.FromMinutes(flow.every_day[i]);
                    var utcDate = new DateTime(timeSpan.Ticks);
                    var localTime = Core.Utilities.TimeUtil.ConvertUtcToLocal(utcDate);
                    var task = new TaskScheduleModel {
                        Id = flow.id,
                        Type = flow.type,
                        CompletedStatus = flow.completedStatus[i],
                        EveryDayLocalTime = localTime,
                        EveryDayLocal = localTime.Hour * 60 + localTime.Minute,
                        EveryDayUtc = flow.every_day[i],
                        EveryDayList = flow.every_day
                    };
                    taskListByType.Add(task);
                }
                taskListByType.Sort((x, y) => x.EveryDayLocal.CompareTo(y.EveryDayLocal));
                Tasks.Add(taskListByType);
            }

            OnUpdate?.Invoke();
        }
        
        private  static void CheckStatusHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
            
            }else {
                // TODO: Release Error Status 
            }
        }
        #endregion
    }
    

}