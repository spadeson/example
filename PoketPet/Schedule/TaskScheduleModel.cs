using System;
using System.Collections.Generic;

namespace PoketPet.Schedule {
    public class TaskScheduleModel {
        public string Id;
        public string CompletedStatus;
        public string Type;
        public DateTime EveryDayLocalTime;
        public int EveryDayLocal;
        public int EveryDayUtc;
        public List<int> EveryDayList;
    }
}