using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunNotification {
    public class FirstRunNotificationUIWidget : BasicPopupUIWidget {

        public event Action OnCloseButtonClick;
        public event Action OnActionButtonClick;

        [Header("Message Text")]
        [SerializeField] private TextMeshProUGUI _messageText;

        private FirstRunNotificationUIWidget _widgetData;
        public override void Create() {
            base.Create();

            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
            actionButton.onClick.AddListener(delegate {
                OnActionButtonClick?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }
        
        public void Initialize(FirstRunNotificationUIWidget widgetData) {
            _widgetData = widgetData;
        }
    }
}