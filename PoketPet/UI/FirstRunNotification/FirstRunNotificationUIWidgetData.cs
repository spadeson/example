using System;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunNotification
{
	[Serializable]
	public class FirstRunNotificationUIWidgetData : WidgetData	{	}
}