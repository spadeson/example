using PoketPet.Global;
using PoketPet.UI.InviteCreation;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunNotification {
    public class FirstRunNotificationUIWidgetController : WidgetControllerWithData<FirstRunNotificationUIWidget, FirstRunNotificationUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void WidgetOnCloseButtonClick() {
            Widget.Dismiss();
        }
        
        private void WidgetOnActionButtonClick() {
            var inviteWidgetData = new InviteCreationUIWidgetData{inviteeEmail = string.Empty};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.INVITE_CREATION_UI, inviteWidgetData, true);
            Widget.Dismiss();
        }
    }
}