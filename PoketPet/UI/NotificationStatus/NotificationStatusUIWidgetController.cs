using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NotificationStatus {
    public class NotificationStatusUIWidgetController : WidgetControllerWithData<NotificationStatusUIWidget, NotificationStatusUIWidgetData> {
        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClicked += CloseButtonClickedHandler;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClicked -= CloseButtonClickedHandler;
        }

        #endregion

        private void CloseButtonClickedHandler() {
            Widget.Dismiss();
        }
    }
}