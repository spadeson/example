using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NotificationStatus {
    [Serializable]
    public class NotificationStatusUIWidgetData : WidgetData {
        public string Title { get; set; }
        public string Message { get; set; }
    }
}