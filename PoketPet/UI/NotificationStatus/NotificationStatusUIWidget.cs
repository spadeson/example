using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.NotificationStatus {
    public class NotificationStatusUIWidget : BasicPopupUIWidget {
        public event Action OnCloseButtonClicked;

        [Header("Text Fields")]
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI messageText;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(NotificationStatusUIWidgetData widgetData) {
            titleText.text = widgetData.Title;
            messageText.text = widgetData.Message;
            closeButton.onClick.AddListener(OnCloseButtonClickedHandler);
            actionButton.onClick.AddListener(OnCloseButtonClickedHandler);
        }

        private void OnCloseButtonClickedHandler() {
            closeButton.onClick.RemoveAllListeners();
            actionButton.onClick.RemoveAllListeners();
            closeButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.65f, 3, 0.5f).OnComplete(() => { OnCloseButtonClicked?.Invoke(); });
        }
    }
}
