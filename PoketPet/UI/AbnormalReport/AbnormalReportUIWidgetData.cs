using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.AbnormalReport {
    [Serializable]
    public class AbnormalReportUIWidgetData : WidgetData {
        public AbnormalReportType abnormalReportType;
        public string abnormalReportMessage;
    }
}