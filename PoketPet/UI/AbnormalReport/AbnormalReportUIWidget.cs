using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.AbnormalReport {
    public class AbnormalReportUIWidget : BasicPopupUIWidget {
        public event Action OnCloseButtonClick;
        public event Action<AbnormalReportUIWidgetData> OnReportButtonClick;
        public event Action<AbnormalReportType> OnTakePhotoButtonClick;

        [Header("Take Photo Button:"), SerializeField]
        private Button takePhotoButton;

        [Header("Abnormal Message:"), SerializeField]
        private TMP_InputField abnormalMessageTextField;

        [Header("Photo:"), SerializeField] private CanvasGroup photoContainer;
        
        private AbnormalReportUIWidgetData _widgetData;

        public override void Create() {
            base.Create();
            
            ToggleImageThumbnail(false);

            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
            
            actionButton.onClick.AddListener(delegate {
                OnReportButtonClick?.Invoke(_widgetData);
            });

            takePhotoButton.onClick.AddListener(delegate {
                OnTakePhotoButtonClick?.Invoke(_widgetData.abnormalReportType);
            });
            
            abnormalMessageTextField.onEndEdit.AddListener(delegate(string value) { _widgetData.abnormalReportMessage = value; });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(AbnormalReportUIWidgetData widgetData) {
            _widgetData = widgetData;
            
            switch (_widgetData.abnormalReportType) {
                case AbnormalReportType.TAKE_FROM_CAMERA:
                    takePhotoButton.transform.GetChild(0).gameObject.SetActive(true);
                    takePhotoButton.transform.GetChild(1).gameObject.SetActive(false);
                    break;
                case AbnormalReportType.TAKE_FROM_GALLERY:
                    takePhotoButton.transform.GetChild(0).gameObject.SetActive(false);
                    takePhotoButton.transform.GetChild(1).gameObject.SetActive(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SetThumbnailImage(Sprite sprite) {
            ToggleImageThumbnail(sprite != null);
        }

        private void ToggleImageThumbnail(bool value) {
            photoContainer.alpha = value ? 1 : 0;
        }
    }
}