namespace PoketPet.UI.AbnormalReport {
    public enum AbnormalReportType {
        TAKE_FROM_CAMERA = 0,
        TAKE_FROM_GALLERY = 1
    }
}
