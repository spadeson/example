using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.AbnormalReport {
    public class AbnormalReportUIWidgetController : WidgetControllerWithData<AbnormalReportUIWidget, AbnormalReportUIWidgetData> {

        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += OnCloseButtonClickHandler;
            Widget.OnTakePhotoButtonClick += OnTakePhotoButtonClickHandler;
            Widget.OnReportButtonClick += OnOnReportButtonClickHandler;
            
            Widget.Initialize(WidgetData);
        }
        
        protected override void OnWidgetActivated(Widget widget) {
            
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= OnCloseButtonClickHandler;
            Widget.OnTakePhotoButtonClick -= OnTakePhotoButtonClickHandler;
            Widget.OnReportButtonClick -= OnOnReportButtonClickHandler;
        }

        #endregion

        #region Private Methods

        private void OnCloseButtonClickHandler() {
            Widget.Dismiss();
        }

        private void OnTakePhotoButtonClickHandler(AbnormalReportType reportType) {

            switch (WidgetData.abnormalReportType) {
                case AbnormalReportType.TAKE_FROM_CAMERA:
                    //TODO Open camera with native camera plugin.
                    break;
                case AbnormalReportType.TAKE_FROM_GALLERY:
                    //TODO Open Gallery with native Gallery plugin.
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnOnReportButtonClickHandler(AbnormalReportUIWidgetData widgetData) {
            //TODO Send Network Request for Report abnormal
        }

        #endregion
    }
}