using PoketPet.Pets.PetsSystem.Manager;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.BreedSelection {
    public class BreedSelectionUIWidgetController : WidgetControllerWithData<BreedSelectionUIWidget, BreedSelectionUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.Initialize(WidgetData);
            Widget.SetBreedSilently(WidgetData.selectedBreed);

            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.OnConfirmButtonClick += WidgetOnConfirmButtonClick;
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        private void WidgetOnConfirmButtonClick(BreedSelectionUIWidgetData data) {
            Widget.Dismiss();
        }

        private void WidgetOnCloseButtonClick() {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
            Widget.OnConfirmButtonClick -= WidgetOnConfirmButtonClick;
            Widget.Dismiss();
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}