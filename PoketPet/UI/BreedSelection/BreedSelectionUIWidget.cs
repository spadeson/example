using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using PoketPet.Pets.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.BreedSelection {
    public class BreedSelectionUIWidget : BaseUIWidget {
        public event Action OnCloseButtonClick;
        public event Action<BreedSelectionUIWidgetData> OnConfirmButtonClick;

        [Header("Buttons")]
        [SerializeField] private Button closeButton;
        [SerializeField] private Button confirmButton;

        [Header("Text")]
        [SerializeField] private TextMeshProUGUI currentBreed;

        [Header("Container"), SerializeField] private RectTransform container;

        [Header("Breed UI Prefab"), SerializeField]
        private GameObject breedUiPrefab;

        [Header("Filter Input Field"), SerializeField]
        private TMP_InputField filterInputField;

        private BreedSelectionUIWidgetData _widgetData;
        
        private string _regexPattern = @"[a-zA-Z\s]";

        #region BaseUIWidget

        public override void Create() {
            base.Create();
            
            closeButton.onClick.AddListener(OnCloseButtonClickHandler);
            confirmButton.onClick.AddListener(OnConfirmButtonClickHandler);

            filterInputField.onValueChanged.AddListener(value => {
                Debug.Log($"Value: {value}, match: {Regex.IsMatch(value, _regexPattern)}");
                if (string.IsNullOrEmpty(value)) {
                    DeleteAllBreedButtons();
                    confirmButton.interactable = false;
                } else if (Regex.IsMatch(value, _regexPattern)){
                    DeleteAllBreedButtons();
                    confirmButton.interactable = false;
                    var filteredBreedsList = FilterBreeds(value);
                    CreateBreedButtons(filteredBreedsList);
                }
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        #endregion BaseUIWidget

        #region PublicMethods

        public void Initialize(BreedSelectionUIWidgetData widgetData) {
            _widgetData = widgetData;
            currentBreed.text = $"Current Breed: {_widgetData.selectedBreed}";
            confirmButton.interactable = !string.IsNullOrEmpty(_widgetData.selectedBreed);
            CreateBreedButtons(_widgetData.dogBreedConfig.breedList);
        }

        public void SetBreedSilently(string selectedBreed) {
            filterInputField.SetTextWithoutNotify(selectedBreed);
            _widgetData.selectedBreed = selectedBreed;
        }

        #endregion

        #region PrivateMethods

        private void OnConfirmButtonClickHandler() {
            OnConfirmButtonClick?.Invoke(_widgetData);
        }

        private void OnCloseButtonClickHandler() {
            OnCloseButtonClick?.Invoke();
        }

        private void DeleteAllBreedButtons() {
            for (var i = 0; i < container.childCount; i++) {
                var uiElement = container.GetChild(i);
                uiElement.GetComponent<BreedSelectionUIElement>().Dismiss();
                Destroy(uiElement.gameObject);
            }
        }

        private void CreateBreedButtons(List<DogBreedsData> dataList) {
            for (var i = 0; i < dataList.Count; i++) {
                var breedConfig      = dataList[i];
                var newBreedButtonGo = Instantiate(breedUiPrefab, container, false);
                var newBreedButton   = newBreedButtonGo.GetComponent<BreedSelectionUIElement>();
                newBreedButton.Initialize(breedConfig);
                newBreedButton.OnButtonClicked += breedData => {
                    filterInputField.SetTextWithoutNotify(breedData.breedName);
                    _widgetData.selectedBreed = breedData.breedName;
                    currentBreed.text = $"Current Breed: {_widgetData.selectedBreed}";
                    confirmButton.interactable = !string.IsNullOrEmpty(_widgetData.selectedBreed);
                };
            }
        }

        private List<DogBreedsData> FilterBreeds(string value) {
            return _widgetData.dogBreedConfig.breedList.Where(b => b.breedName.Contains(value.ToLower())).ToList();
        }

        #endregion
    }
}