using System;
using PoketPet.Pets.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.BreedSelection {
    [Serializable]
    public class BreedSelectionUIWidgetData : WidgetData {
        public DogBreedConfig dogBreedConfig;
        public string selectedBreed { get; set; }
    }
}
