using System;
using PoketPet.Pets.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace PoketPet.UI.BreedSelection {
    public class BreedSelectionUIElement : MonoBehaviour, ISelectHandler, IDeselectHandler {
        
        [SerializeField] private TextMeshProUGUI breedNameText;
        [SerializeField] private Button button;

        public event Action<DogBreedsData> OnButtonClicked;

        private DogBreedsData _breedsData;
        private Color _defaultLabelColor;

        public void Initialize(DogBreedsData data) {
            _breedsData = data;

            if (button == null) {
                button = GetComponent<Button>();
            }
            button.onClick.AddListener(OnEditButtonClickedHandler);

            if (breedNameText == null) {
                breedNameText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            }

            _defaultLabelColor = breedNameText.color;

            breedNameText.text = data.breedName;
        }

        public void Dismiss() {
            button.onClick.RemoveAllListeners();
        }

        private void OnEditButtonClickedHandler() {
            OnButtonClicked?.Invoke(_breedsData);
        }
        
        public void OnSelect(BaseEventData eventData) {
            Debug.Log($"Selected: {name}");
            breedNameText.color = Color.white;
        }

        public void OnDeselect(BaseEventData eventData) {
            Debug.Log($"Deselected: {name}");
            breedNameText.color = _defaultLabelColor;
        }
    }
}
