using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChatResult {
    public class VetChatResultUIWidgetController : WidgetControllerWithData<VetChatResultUIWidget, VetChatResultUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.Initialize();
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
        }

        private void WidgetOnCloseButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.VET_CHAT_UI);
            Widget.Dismiss();
        }
    }
}