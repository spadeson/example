using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChatResult {
    public class VetChatResultUIWidget : BaseUIWidget {
        
        public event Action OnCloseButtonClick;
        
        [Header("Title")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Close Button")]
        [SerializeField] private Button closeButton;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            closeButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize() {
            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke(); 
            });
        }
    }
}