using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial11 {
    [Serializable]
    public class NewTutorial11UIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}
