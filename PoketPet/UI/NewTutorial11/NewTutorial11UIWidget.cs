using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using PoketPet.UI.Tooltip;
using PoketPet.UI.TutorialTask;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial11 {
    public class NewTutorial11UIWidget : BaseUIWidget {
        
        public event Action OnTaskReleased;
        public event Action OnActionButtonClick;
        
        [Header("Tooltip")]
        [SerializeField] private TooltipUIWidget tooltipUIWidget;

        [Header("Finger Pointer")]
        [SerializeField] private Image fingerPointerImage;
        
        [Header("Arrow Image")]
        [SerializeField] private Image arrowImage;

        [Header("Task UI Element")]
        [SerializeField] private TutorialTaskUIElementDragable taskUIElementDragable;

        [Header("Assignee Elements")]
        [SerializeField] private List<TutorialTaskUIAssignee> tutorialTaskUiAssignee = new List<TutorialTaskUIAssignee>();

        private Sequence _pointerSequence;
        private Sequence _completionSequence;

        private NewTutorial11UIWidgetData _widgetData;
        
        private const float START_TASK_POSITION = -622f;
        private const float FINAL_TASK_POSITION = -120f;
        
        private const float TASK_COMPLETE_THRESHOLD = 200f;
        
        private Vector2 _initialTaskElementPosition;

        #region MonoBehaviour

        private void OnEnable() {
            taskUIElementDragable.Initialize();

            foreach (var taskAssignee in tutorialTaskUiAssignee) {
                taskAssignee.Initialize();
                taskAssignee.transform.DOScale(0, 0);
            }

            fingerPointerImage.DOFade(0, 0);
            arrowImage.DOFade(0, 0);

            taskUIElementDragable.RectTransform.DOScale(0, 0);

            taskUIElementDragable.OnElementCLick += OnElementDragableCLickHandler;
            taskUIElementDragable.OnElementDrag += OnElementDragableDragHandler;
            taskUIElementDragable.OnElementRelease += OnElementDragableReleaseHandler;
            
            _initialTaskElementPosition = taskUIElementDragable.RectTransform.anchoredPosition;
        }

        #endregion

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
            ShowAnimations();
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            _pointerSequence?.Kill();
            _completionSequence?.Kill();

            taskUIElementDragable.OnElementCLick -= OnElementDragableCLickHandler;
            taskUIElementDragable.OnElementDrag -= OnElementDragableDragHandler;
            taskUIElementDragable.OnElementRelease -= OnElementDragableReleaseHandler;
            
            base.Dismiss();
        }

        #endregion

        #region Public Methods

        public void Initialize(NewTutorial11UIWidgetData widgetData) {
            _widgetData = widgetData;

            tooltipUIWidget.OnTooltipClosed += OnTooltipCloseHandler;
            tooltipUIWidget.Initialize(_widgetData.TutorialSingleStepData.title, _widgetData.TutorialSingleStepData.description);
        }

        #endregion

        #region Private Methods

        private void ShowAnimations() {
            tooltipUIWidget.Show();
        }
        
        private void OnTooltipCloseHandler() {
            tooltipUIWidget.OnTooltipClosed -= OnTooltipCloseHandler;
            tooltipUIWidget.Hide();
            PlayLoopedAnimation();
        }
        
        private void PlayLoopedAnimation() {
            
            var showTaskSequence = DOTween.Sequence();
            showTaskSequence.Append(taskUIElementDragable.transform.DOScale(1f, 0.5f).SetEase(Ease.OutSine));

            foreach (var taskAssignee in tutorialTaskUiAssignee) {
                showTaskSequence.Join(taskAssignee.transform.DOScale(1, 0).From(0f).SetEase(Ease.OutSine));
            }

            _pointerSequence = DOTween.Sequence();
            _pointerSequence.PrependInterval(0.5f);
            _pointerSequence.Append(taskUIElementDragable.RectTransform.DOScale(1f, 0.5f).SetEase(Ease.OutBounce));
            
            _pointerSequence.AppendInterval(0.5f);
            _pointerSequence.Append(fingerPointerImage.DOFade(1, 0.35f).SetEase(Ease.Flash));
            _pointerSequence.Join(arrowImage.DOFade(0.65f, 0.5f).SetEase(Ease.Flash));
            
            _pointerSequence.AppendInterval(0.35f);
            _pointerSequence.Append(fingerPointerImage.rectTransform.DOScale(0.75f, 0.25f).SetEase(Ease.OutSine));
            _pointerSequence.Append(taskUIElementDragable.RectTransform.DOAnchorPosY(FINAL_TASK_POSITION, 1f).SetEase(Ease.OutSine));
            _pointerSequence.Join(fingerPointerImage.rectTransform.DOAnchorPosY(FINAL_TASK_POSITION, 1f).SetEase(Ease.OutSine));
            
            _pointerSequence.Join(arrowImage.rectTransform.DOAnchorPosY(FINAL_TASK_POSITION, 0.75f).SetEase(Ease.OutSine));
            _pointerSequence.Join(arrowImage.rectTransform.DOScale(2, 0.75f).SetEase(Ease.OutSine));
            _pointerSequence.Append(arrowImage.DOFade(0f, 0.25f).SetEase(Ease.Flash));
            
            
            for (var i = 0; i < tutorialTaskUiAssignee.Count; i++) {
                _pointerSequence.Join(tutorialTaskUiAssignee[i].transform.DOScale(1f, 0.5f).SetEase(Ease.OutBounce));
            }
            
            _pointerSequence.Join(tutorialTaskUiAssignee[0].RectTransform.DOScale(2f, 0.35f).SetEase(Ease.OutSine).SetDelay(0.5f));
            
            _pointerSequence.AppendInterval(0.25f);
            _pointerSequence.Append(fingerPointerImage.DOFade(0f, 0.35f).SetEase(Ease.Flash));
            _pointerSequence.Join(fingerPointerImage.rectTransform.DOScale(1f, 0.35f).SetEase(Ease.OutSine));
            
            _pointerSequence.AppendInterval(0.25f);
            _pointerSequence.Append(taskUIElementDragable.RectTransform.DOScale(0f, 0.35f).SetEase(Ease.OutSine));
            for (var i = 0; i < tutorialTaskUiAssignee.Count; i++) {
                _pointerSequence.Join(tutorialTaskUiAssignee[i].transform.DOScale(0f, 0.35f).SetEase(Ease.OutSine));
            }

            _pointerSequence.AppendInterval(0.5f);

            _pointerSequence.SetLoops(-1, LoopType.Restart);
        }

        private void OnElementDragableCLickHandler(Vector2 position) {
            _pointerSequence?.Kill(true);
            fingerPointerImage.DOFade(0, 0.1f).SetEase(Ease.Flash);
        }

        private void OnElementDragableDragHandler(Vector2 position) {
            for (var i = 0; i < tutorialTaskUiAssignee.Count; i++) {
                var distance = Vector2.Distance(tutorialTaskUiAssignee[i].RectTransform.anchoredPosition, position);
                tutorialTaskUiAssignee[i].SetScale(distance);
            }
        }
        
        private void OnElementDragableReleaseHandler(Vector2 position) {
            
            taskUIElementDragable.transform.DOScale(1f, 0.15f).SetEase(Ease.OutSine);

            var assigneeElement = GetClosestAssignee(tutorialTaskUiAssignee, position);
            var distance= Vector2.Distance(assigneeElement.RectTransform.anchoredPosition, position);

            if (assigneeElement == null || distance > TASK_COMPLETE_THRESHOLD) {
                taskUIElementDragable.RectTransform.DOAnchorPos(_initialTaskElementPosition, 0.25f).SetEase(Ease.OutSine);
                return;
            }
            
            OnTaskReleased?.Invoke();

            _completionSequence = DOTween.Sequence();
            _completionSequence.Append(taskUIElementDragable.RectTransform.DOScale(1f, 0.25f).SetEase(Ease.OutSine));
            _completionSequence.Join(taskUIElementDragable.RectTransform.DOAnchorPos(position, 0.05f).SetEase(Ease.OutSine));

            for (var i = 0; i < tutorialTaskUiAssignee.Count; i++) {
                _completionSequence.Join(tutorialTaskUiAssignee[i].RectTransform.DOScale(1f, 0.25f).SetEase(Ease.OutSine));
            }

            _completionSequence.AppendInterval(0.5f);

            for (var i = 0; i < tutorialTaskUiAssignee.Count; i++) {
                _completionSequence.Join(tutorialTaskUiAssignee[i].RectTransform.DOAnchorPos(position, 0.1f).SetEase(Ease.OutSine));
                _completionSequence.Join(tutorialTaskUiAssignee[i].RectTransform.DOScale(0f, 0.2f).SetEase(Ease.OutSine));
            }

            _completionSequence.Join(taskUIElementDragable.RectTransform.DOScale(0f, 0.25f).SetEase(Ease.OutSine));

            _completionSequence.OnComplete(() => { OnActionButtonClick?.Invoke(); });
        }

        private TutorialTaskUIAssignee GetClosestAssignee(List<TutorialTaskUIAssignee> assignees, Vector2 position) {
            var distances = new List<float>();

            for (var i = 0; i < assignees.Count; i++) {
                var assigneePosition = tutorialTaskUiAssignee[i].GetAnchoredPosition();
                var distance         = Vector2.Distance(assigneePosition, position);

                distances.Add(distance);
            }

            var closestDistance     = distances.Min();
            var closestElementIndex = distances.IndexOf(closestDistance);

            return assignees[closestElementIndex];
        }

        #endregion
    }
}
