using System.Text.RegularExpressions;
using PoketPet.User;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunNickName {
    public class FirstRunNickNameUIWidgetController : WidgetControllerWithData<FirstRunNickNameUIWidget, FirstRunNickNameUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnInputFieldValueChanged += OnInputFieldValueChangedHandler;
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            UserProfileManager.OnUserDataModelUpdated -= OnUserDataModelUpdatedHandler;
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void OnInputFieldValueChangedHandler(string value) {
            WidgetData.Nickname = value;
            Widget.SetInputFieldError(!Regex.IsMatch(value, @"^[A-Za-z0-9].{2,15}$"));
        }

        private void WidgetOnActionButtonClick(string nickname) {
            UserProfileManager.CurrentUser.nickname = nickname;
            UserProfileManager.OnUserDataModelUpdated += OnUserDataModelUpdatedHandler;
            UserProfileManager.UpdateUser();
        }

        private void OnUserDataModelUpdatedHandler() {
            if (UserProfileManager.CurrentUser.nickname == string.Empty) return;
            Widget.Dismiss();
        }
    }
}