using System;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunNickName {
    [Serializable]
    public class FirstRunNickNameUIWidgetData : WidgetData {
        public string Nickname { get; set; }
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}