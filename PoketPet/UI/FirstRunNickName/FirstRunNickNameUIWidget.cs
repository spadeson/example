using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.FirstRunNickName {
    public class FirstRunNickNameUIWidget : BasicPopupUIWidget {

        [Header("Input Field")]
        [SerializeField] private TMP_InputField nicknameInputField;
        [SerializeField] private TextMeshProUGUI nicknameStatusField;
        public event Action<string> OnActionButtonClick;
        public event Action<string> OnInputFieldValueChanged;

        private FirstRunNickNameUIWidgetData _widgetData;
        
        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            nicknameInputField.onValueChanged.RemoveAllListeners();
            actionButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize(FirstRunNickNameUIWidgetData widgetData) {

            _widgetData = widgetData;

            actionButton.interactable = false;
            nicknameInputField.onValueChanged.AddListener(delegate(string nickname) {
                OnInputFieldValueChanged?.Invoke(nickname);
            });
            
            actionButton.onClick.AddListener(delegate {
                OnActionButtonClick?.Invoke(_widgetData.Nickname);
            });
            
            nicknameStatusField.text = string.Empty;
        }
        
        public void SetInputFieldError(bool anError) {
            nicknameStatusField.text = anError ? "Minimal 2 and maximum 16 chars length." : string.Empty;
            actionButton.interactable = !anError;
        }
    }
}
