using PoketPet.UI.FirstRunGameOnboarding;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.UI.FirstRunGameInvites
{
	public class FirstRunGameInvitesUIWidgetController : WidgetControllerWithData<FirstRunGameInvitesUIWidget, FirstRunGameInvitesUIWidgetData>	{

		protected override void OnWidgetCreated(Widget widget) {			
			Widget.OnActionButtonClick += WidgetOnActionButtonClick;
			Widget.Initialize(WidgetData);
		}

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) {
			Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
		}
		
		private void WidgetOnActionButtonClick() {
			var widgetData = new FirstRunGameOnboardingUIWidgetData {
				isFirstRun = true
			};
			CoreMobile.Instance.UIManager.CreateUiWidgetWithData(FIRST_RUN_GAME_ONBOARDING_UI, widgetData);
			Widget.Dismiss();
		}
	}
}