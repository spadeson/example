using System;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ExplanationTooltip
{
	public class ExplanationTooltipUIWidget : BaseUIWidget	{

		public override void Create() {
			base.Create();
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			base.Dismiss();
		}

	}
}