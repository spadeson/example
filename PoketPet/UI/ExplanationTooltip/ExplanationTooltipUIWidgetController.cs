using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ExplanationTooltip
{
	public class ExplanationTooltipUIWidgetController : WidgetControllerWithData<ExplanationTooltipUIWidget, ExplanationTooltipUIWidgetData>	{

		protected override void OnWidgetCreated(Widget widget) { }

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) { }
	}
}