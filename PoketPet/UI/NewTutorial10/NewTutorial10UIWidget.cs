using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial10 {
    public class NewTutorial10UIWidget : BasicPopupUIWidget {
        public event Action OnActionButtonClick;

        [Header("Circle Image")]
        [SerializeField] private Image circleImage;
        
        [Header("Task Fill Image")]
        [SerializeField] private Image taskFillImage;
        
        [Header("Glow Sector Image")]
        [SerializeField] private Image glowSectorImage;

        private NewTutorial10UIWidgetData _widgetData;
        
        #region MonoBehaviour

        private void OnEnable() {

            circleImage.DOFade(0, 0);

            taskFillImage.DOFillAmount(0, 0);

            glowSectorImage.DOFade(0, 0);
        }

        #endregion

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            ShowAnimation();
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            HideAnimation();
        }

        #endregion

        #region PublicMethods

        public void Initialize(NewTutorial10UIWidgetData widgetData) {

            _widgetData = widgetData;

            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                actionButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 4, 0.5f).OnComplete(() => {
                    OnActionButtonClick?.Invoke();
                });
            });
        }

        #endregion

        #region PrivateMethods

        private void ShowAnimation() {
            var showSequence = DOTween.Sequence();
            showSequence.AppendInterval(0.25f);
            showSequence.Join(circleImage.DOFade(1f, 0.5f).SetEase(Ease.Flash));
            showSequence.AppendInterval(0.15f);

            var loopSequence = DOTween.Sequence();
            loopSequence.PrependInterval(0.5f);
            loopSequence.Append(taskFillImage.DOFillAmount(0.65f, 1f));
            loopSequence.Append(glowSectorImage.DOFade(1, 0.5f));
            loopSequence.AppendInterval(1.5f);
            loopSequence.SetLoops(-1);
        }
        
        private void HideAnimation() {
            var hideSequence = DOTween.Sequence();
            hideSequence.AppendInterval(0.2f);
            hideSequence.OnComplete(() => {
                DOTween.KillAll(true);
                base.Dismiss();
            });
        }

        #endregion
    }
}