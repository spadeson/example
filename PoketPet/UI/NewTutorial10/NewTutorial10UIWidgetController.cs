using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial10 {
    public class NewTutorial10UIWidgetController : WidgetControllerWithData<NewTutorial10UIWidget, NewTutorial10UIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void WidgetOnActionButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            Widget.Dismiss();
        }
    }
}
