using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialScores {
    public class TutorialScoresUIWidgetController : WidgetControllerWithData<TutorialScoresUIWidget, TutorialScoresUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
