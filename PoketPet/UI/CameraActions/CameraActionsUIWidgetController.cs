using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.CameraActions {
    public class CameraActionsUIWidgetController : WidgetControllerWithData<CameraActionsUIWidget, CameraActionsUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}