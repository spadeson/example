using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial01.Points {
    public class TutorialPointUiElement : MonoBehaviour {
        [SerializeField] private Sprite fillSprite;
        [SerializeField] private Sprite emptySprite;
        
        [SerializeField] private Image pointImage;

        public void SetActive(bool setActive) {
            pointImage.sprite = setActive ? fillSprite : emptySprite;
        }
    }
}