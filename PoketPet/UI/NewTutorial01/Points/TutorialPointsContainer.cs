using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.UI.NewTutorial01.Points {
    public class TutorialPointsContainer : MonoBehaviour {
        
        [SerializeField] private RectTransform pointerUiElementsContainer;
        [SerializeField] private GameObject pointerUiElementPrefab;
        [SerializeField] private GameObject arrowUiElementPrefab;

        private List<TutorialPointUiElement> _pointsList = new List<TutorialPointUiElement>();

        private int _pointsCount;

        public void Initialize(int pointsCount) {
            _pointsCount = pointsCount;
        }

        public void CreatePoints() {
            for (var i = 0; i < _pointsCount; i++) {
                CreatePoint();
            }
            
            AppendArrow();
        }

        public void CreatePoint() {
            var pointUiElementGo = Instantiate(pointerUiElementPrefab, pointerUiElementsContainer, false);
            var pointUiElement   = pointUiElementGo.GetComponent<TutorialPointUiElement>();
            pointUiElement.SetActive(false);
            _pointsList.Add(pointUiElement);
        }

        public void RemovePoints() {
            
            _pointsList.Clear();
            
            for (var i = 0; i < pointerUiElementsContainer.childCount; i++) {
                Destroy(pointerUiElementsContainer.GetChild(i).gameObject);
            }
        }

        public void ActivatePoint(int pointIndex) {
            for (var i = 0; i < _pointsList.Count; i++) {
                _pointsList[i].SetActive(false);    
            }
            
            _pointsList[pointIndex].SetActive(true);
        }

        private void AppendArrow() {
            var arrowUiElement = Instantiate(arrowUiElementPrefab, pointerUiElementsContainer, false);
        }
    }
}
