using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PoketPet.UI.NewTutorial01 {
    public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler {

        public event Action<int> OnSwipedNewPage;
        public event Action OnCompletedPages;

        private Vector3 _panelLocation;
        private float _percentThreshold = 0.25f;
        private float easing = 0.35f;
        
        public RectTransform container;

        private int _totalPages;
        private int _currentPage;

        private void Start() {

            _panelLocation = container.anchoredPosition;

            _currentPage = 0;
            _totalPages = container.childCount - 1;
        }

        public void OnDrag(PointerEventData eventData) {
            var difference = eventData.pressPosition.x - eventData.position.x;
            var percentage = (eventData.pressPosition.x - eventData.position.x) / Screen.width;
            container.anchoredPosition = _panelLocation - new Vector3(difference, 0, 0);
        }

        public void OnEndDrag(PointerEventData eventData) {
            
            var percentage = (eventData.pressPosition.x - eventData.position.x) / Screen.width;

            if (Mathf.Abs(percentage) >= _percentThreshold) {
                
                var newLocation = _panelLocation;

                if (percentage > 0 && _currentPage < _totalPages) {
                    _currentPage++;
                    newLocation += new Vector3(-Screen.width, 0, 0);
                } else if (percentage > 0 && _currentPage == _totalPages) {
                    OnCompletedPages?.Invoke();
                } else if (percentage < 0 && _currentPage > 0) {
                    _currentPage--;
                    newLocation += new Vector3(Screen.width, 0, 0);
                }

                StartCoroutine(SmoothMove(container.anchoredPosition, newLocation, easing));
                
                _panelLocation = newLocation;
            } else {
                StartCoroutine(SmoothMove(container.anchoredPosition, _panelLocation, easing));
            }
        }

        private IEnumerator SmoothMove(Vector3 transformPosition, Vector3 newLocation, float seconds) {
            var time = 0f;
            while (time < 1.0f) {
                time += Time.deltaTime / seconds;
                container.anchoredPosition = Vector2.Lerp(transformPosition, newLocation, Mathf.SmoothStep(0f, 1f, time));
                yield return null;
            }
            
            OnSwipedNewPage?.Invoke(_currentPage);
        }
    }
}
