using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using UnityEngine;

namespace PoketPet.UI.NewTutorial01.MultiContainer {
    public interface ITutorialMultiPopupContainer {
        RectTransform RectTransform { get; }

        void Initialize(TutorialMultiSubStepModelData data);

        void SetActive(bool value, bool animated = false, float animationSpeed = 0.5f);
    }
}
