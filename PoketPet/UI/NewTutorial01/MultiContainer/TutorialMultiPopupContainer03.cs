using DG.Tweening;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.NewTutorial01.MultiContainer {
    public class TutorialMultiPopupContainer03 : MonoBehaviour, ITutorialMultiPopupContainer {
        [Header("Canvas Group")]
        [SerializeField] private CanvasGroup canvasGroup;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        [Header("Pet Image")]
        [SerializeField] private RectTransform petImage;

        [Header("Features Container")]
        [SerializeField] private RectTransform featuresContainer;

        public RectTransform RectTransform => _rectTransform;

        private RectTransform _rectTransform;

        private TutorialMultiSubStepModelData _data;

        private Sequence _showSequence;

        public void Initialize(TutorialMultiSubStepModelData data) {
            _data = data;

            descriptionText.text = _data.description;

            if (canvasGroup == null) {
                canvasGroup = GetComponent<CanvasGroup>();
            }

            _rectTransform = GetComponent<RectTransform>();
        }

        public void SetActive(bool value, bool animated = false, float animationSpeed = 0.5f) {
            
            _showSequence?.Kill(true);

            petImage.DOScale(0, 0);

            for (var i = 0; i < featuresContainer.childCount; i++) {
                var featureElement = featuresContainer.GetChild(i);
                featureElement.DOScale(0f, 0f);
            }

            if (!animated) {
                canvasGroup.alpha = value ? 1f : 0f;
                canvasGroup.blocksRaycasts = value;
                canvasGroup.interactable = value;
            } else {
                _showSequence = DOTween.Sequence();
                _showSequence.Append(canvasGroup.DOFade(value ? 1f : 0f, animationSpeed).SetEase(Ease.InOutSine));

                _showSequence.Join(petImage.DOScale(value ? 1f : 0f, animationSpeed * 0.5f).SetDelay(0.075f).SetEase(Ease.OutSine));

                for (var i = 0; i < featuresContainer.childCount; i++) {
                    var featureElement           = featuresContainer.GetChild(i);
                    _showSequence.Join(featureElement.DOScale(value ? 1f : 0f, animationSpeed / featuresContainer.childCount).SetEase(Ease.OutSine).SetDelay(0.05f * i));
                }

                _showSequence.OnComplete(() => {
                    canvasGroup.blocksRaycasts = value;
                    canvasGroup.interactable = value;
                });
            }
        }
    }
}