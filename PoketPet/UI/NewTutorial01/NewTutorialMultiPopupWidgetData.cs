using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial01 {
    [Serializable]
    public class NewTutorialMultiPopupWidgetData : WidgetData {
        public TutorialMultiStepModelData tutorialStepData;
    }
}
