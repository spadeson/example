using PoketPet.Global;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding;
using UnityEngine;
using UnityEngine.EventSystems;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial01 {
    public class NewTutorial01UIWidgetController : WidgetControllerWithData<NewTutorial01UIWidget, NewTutorialMultiPopupWidgetData> {
        
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActivatedContainer += WidgetOnActivatedContainer;
            Widget.OnCompletedPages += WidgetOnCompletedPages;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActivatedContainer -= WidgetOnActivatedContainer;
            Widget.OnCompletedPages -= WidgetOnCompletedPages;
        }

        private void WidgetOnCompletedPages() {
            Widget.Dismiss();
        }

        private void WidgetOnActivatedContainer() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.TUTORIAL_PAGE_TURN_SFX);
        }
    }
}