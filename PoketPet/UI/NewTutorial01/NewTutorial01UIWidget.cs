using System;
using DG.Tweening;
using PoketPet.UI.NewTutorial01.MultiContainer;
using PoketPet.UI.NewTutorial01.Points;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial01 {
    public class NewTutorial01UIWidget : BaseUIWidget {
        public event Action OnActivatedContainer;
        public event Action OnCompletedPages;

        [Header("Swipe Animation Container")]
        [SerializeField] private PointerAnimation pointerAnimation;

        [Header("Page Swiper")]
        [SerializeField] private PageSwiper pageSwiper;

        [Header("Points")]
        [SerializeField] private TutorialPointsContainer tutorialPointsContainer;

        private ITutorialMultiPopupContainer[] _multiPopupContainersList;
        
        private NewTutorialMultiPopupWidgetData _tutorialGenericUiWidgetData;

        public override void Create() {
            base.Create();
            pointerAnimation.Activate();
        }

        public override void Dismiss() {
            pageSwiper.OnSwipedNewPage -= OnSwipedNewPageHandler;
            pageSwiper.OnCompletedPages -= OnCompletedPagesHandler;
            HideAndDismissAnimatedWidget();
        }

        public void Initialize(NewTutorialMultiPopupWidgetData tutorialGenericUiWidgetData) {
            _tutorialGenericUiWidgetData = tutorialGenericUiWidgetData;

            tutorialPointsContainer.Initialize(_tutorialGenericUiWidgetData.tutorialStepData.content.Length);
            tutorialPointsContainer.RemovePoints();
            
            tutorialPointsContainer.CreatePoints();
            tutorialPointsContainer.ActivatePoint(0);

            _multiPopupContainersList = pageSwiper.container.GetComponentsInChildren<ITutorialMultiPopupContainer>();

            for (var i = 0; i < _tutorialGenericUiWidgetData.tutorialStepData.content.Length; i++) {
                _multiPopupContainersList[i].Initialize(_tutorialGenericUiWidgetData.tutorialStepData.content[i]);
                _multiPopupContainersList[i].RectTransform.anchoredPosition = new Vector2(Screen.width * i, _multiPopupContainersList[i].RectTransform.anchoredPosition.y);
                if (i > 0) {
                    _multiPopupContainersList[i].SetActive(false);
                }
            }

            pageSwiper.OnSwipedNewPage += OnSwipedNewPageHandler;
            pageSwiper.OnCompletedPages += OnCompletedPagesHandler;
        }

        private void ActivateContainerByIndex(int index) {
            
            for (var i = 0; i < _tutorialGenericUiWidgetData.tutorialStepData.content.Length; i++) {
                _multiPopupContainersList[i].SetActive(false, true, 0.35f);
            }
            
            _multiPopupContainersList[index].SetActive(true, true);

            OnActivatedContainer?.Invoke();
        }

        private void OnSwipedNewPageHandler(int pageNumber) {
            ActivateContainerByIndex(pageNumber);
            tutorialPointsContainer.ActivatePoint(pageNumber);
            
            if(!pointerAnimation.IsActive) return;
            pointerAnimation.Dismiss();
        }
        
        private void OnCompletedPagesHandler() {
            OnCompletedPages?.Invoke();
        }
        
        private void HideAndDismissAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(transform.DOMoveX(-Screen.width, 0.5f).SetEase(Ease.OutSine));
            sequence.Join(canvasGroup.DOFade(0, 0.5f).SetEase(Ease.OutSine));
            sequence.OnComplete(() => {
                base.Dismiss();
            });
        }
    }
}