using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial01 {
    public class PointerAnimation : MonoBehaviour {

        public bool IsActive { get; set; }

        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Image pointer;
        [SerializeField] private Image arrow;
        private Sequence _pointerSequence;
        private float _pointerAnimationLength = 1f;

        public void Activate() {

            IsActive = true;
            
            canvasGroup.DOFade(1f, 0.35f).SetEase(Ease.OutSine);
            
            _pointerSequence = DOTween.Sequence();
            _pointerSequence.Append(pointer.rectTransform.DOAnchorPosX(-320f, _pointerAnimationLength).From(new Vector2(128f, pointer.rectTransform.anchoredPosition.y)).SetEase(Ease.OutSine));
            _pointerSequence.Join(pointer.DOFade(0, _pointerAnimationLength).SetEase(Ease.OutSine));
            _pointerSequence.Join(arrow.rectTransform.DOAnchorPosX(-220f, _pointerAnimationLength * 0.75f).From(new Vector2(220, arrow.rectTransform.anchoredPosition.y)).SetEase(Ease.OutSine));
            _pointerSequence.Join(arrow.DOFade(0, _pointerAnimationLength                         * 0.5f).From(1f).SetEase(Ease.OutSine));
            _pointerSequence.AppendInterval(_pointerAnimationLength);
            _pointerSequence.SetLoops(-1, LoopType.Restart);
        }

        public void Dismiss() {
            IsActive = false;
            _pointerSequence.Kill();
            canvasGroup.DOFade(0f, 0.15f).SetEase(Ease.OutSine);
        }
    }
}
