using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using static PoketPet.Global.GlobalVariables.GamePetMoodContent;

namespace PoketPet.UI.GameResult {
    public class GameResultUIWidget : BasicPopupUIWidget {
        public event Action OnCloseButtonClick;

        [Header("Game Result Text:"), SerializeField]
        private TextMeshProUGUI headerText;

        [Header("Game Result Scores:"), SerializeField]
        private TextMeshProUGUI scoresText;

        public void Initialize(GameResultUIWidgetData widgetData) {
            
            closeButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });

            actionButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });

            if (widgetData.isRespondent) {
                headerText.text = widgetData.isSuccess ? MOOD_RESPONDENT_SUCCESS : MOOD_RESPONDENT_FALIED;
            } else {
                headerText.text = widgetData.isSuccess ? MOOD_SUCCESS : MOOD_FALIED;
            }

            scoresText.text = $"+{widgetData.scores}";
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }
    }
}