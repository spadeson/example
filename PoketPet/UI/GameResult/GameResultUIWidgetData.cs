using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.GameResult {
    [Serializable]
    public class GameResultUIWidgetData : WidgetData {
        public bool isSuccess;
        public bool isRespondent;
        public int scores;
    }
}