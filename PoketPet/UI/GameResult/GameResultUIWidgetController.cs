using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.GameResult {
    public class GameResultUIWidgetController : WidgetControllerWithData<GameResultUIWidget, GameResultUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += OnCloseButtonClickHandler;
        }

        protected override void OnWidgetActivated(Widget widget) {
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }


        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= OnCloseButtonClickHandler;
        }

        private void OnCloseButtonClickHandler() {
            Widget.Dismiss();
        }
    }
}
