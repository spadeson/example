using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedTakePhoto {
    [Serializable]
    public class TaskCompletedTakePhotoUIWidgetData : WidgetData {
        public string taskId;
    }
}