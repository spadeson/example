using PoketPet.Photo;
using PoketPet.UI.TaskCompletedSendPhoto;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.UI.TaskCompletedTakePhoto {
    public class TaskCompletedTakePhotoUIWidgetController : WidgetControllerWithData<TaskCompletedTakePhotoUIWidget, TaskCompletedTakePhotoUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClickHandler;
            Widget.OnActionButtonClick += WidgetOnActionButtonClickHandler;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClickHandler;
            Widget.OnActionButtonClick -= WidgetOnActionButtonClickHandler;
        }
        
        private void WidgetOnCloseButtonClickHandler() {
            Widget.Dismiss();
        }
        
        private void WidgetOnActionButtonClickHandler() {
            PhotoManager.TakePhoto(sprite => {
                var widgetData = new TaskCompletedSendPhotoUIWidgetData{photo = sprite, taskId = WidgetData.taskId};
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(TASK_COMPLETED_SEND_PHOTO_UI, widgetData, true);
            }, 512, NativeCamera.PreferredCamera.Front);
            Widget.Dismiss();
        }
    }
}
