using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using PoketPet.UI.TaskCompletedSendPhoto;

namespace PoketPet.UI.TaskCompletedTakePhoto {
    public class TaskCompletedTakePhotoUIWidget : BasicPopupUIWidget {

        public event Action OnCloseButtonClick;
        public event Action OnActionButtonClick;
        
        private TaskCompletedTakePhotoUIWidgetData _widgetData;
        
        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(TaskCompletedTakePhotoUIWidgetData widgetData) {
            _widgetData = widgetData;
            
            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
            
            actionButton.onClick.AddListener(delegate {
                OnActionButtonClick?.Invoke();
            });
        }
    }
}