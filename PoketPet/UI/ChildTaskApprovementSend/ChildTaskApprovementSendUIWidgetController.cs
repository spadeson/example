using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ChildTaskApprovementSend {
    public class ChildTaskApprovementSendUIWidgetController : WidgetControllerWithData<ChildTaskApprovementSendUIWidget, ChildTaskApprovementSendUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
