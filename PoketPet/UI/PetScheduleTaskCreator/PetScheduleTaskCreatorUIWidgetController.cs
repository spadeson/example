using System;
using System.Globalization;
using PoketPet.Global;
using PoketPet.Schedule;
using PoketPet.UI.PetScheduleTaskCreator;
using PoketPet.UI.Progress;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetScheduleTaskEditor {
    public class PetScheduleTaskCreatorUIWidgetController : WidgetControllerWithData<PetScheduleTaskCreatorUIWidget, PetScheduleTaskCreatorUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClickHandler;
            Widget.OnConfirmButtonClick += WidgetOnConfirmButtonClickHandler;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClickHandler;
            Widget.OnConfirmButtonClick -= WidgetOnConfirmButtonClickHandler;
        }

        private void WidgetOnConfirmButtonClickHandler(DateTime newTime) {

            var utcDate = Core.Utilities.TimeUtil.ConvertLocalToUtc(newTime);
            var value = ScheduleManager.GetFlowByTaskType(WidgetData.TaskType);
            value.every_day.Add(utcDate.Hour * 60 + utcDate.Minute);
            ScheduleManager.ChangeScheduleTask(value.id, value.every_day);
            
            var progressUiWidgetData = new ProgressUIWidgetData {title = "Update Tasks", description = ""};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PROGRESS_UI, progressUiWidgetData);
            Widget.Dismiss();
        }

        private void WidgetOnCloseButtonClickHandler() {
            Widget.Dismiss();
        }
    }
}