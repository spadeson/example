using System;
using System.Collections.Generic;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetScheduleTaskCreator
{
	[Serializable]
	public class PetScheduleTaskCreatorUIWidgetData : WidgetData {
		public string TaskType { get; set; }
		public List<int> TaskSchedule { get; set; }
	}
}