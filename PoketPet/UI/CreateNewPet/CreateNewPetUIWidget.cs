using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using PoketPet.UI.PetParameter;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.CreateNewPet {
    public class CreateNewPetUIWidget : BasicPopupUIWidget {
        [Header("Button:")]
        [SerializeField] private Button applyButton;

        [Header("Birth:")]
        [SerializeField] private Button birthDayButton;

        [SerializeField] private TextMeshProUGUI birthDayText;
        [SerializeField] private Button birthMonthButton;
        [SerializeField] private TextMeshProUGUI birthMonthText;
        [SerializeField] private Button birthYearButton;
        [SerializeField] private TextMeshProUGUI birthYearText;

        [Header("Input Fields")]
        [SerializeField] private TMP_InputField petNameInputField;

        [SerializeField] private TMP_InputField petWeightInputField;

        [Header("Toggles:")]
        [SerializeField] private Toggle weightToggle;

        [SerializeField] private Toggle genderToggle;
        public event Action OnCloseButtonClick;
        public event Action OnApplyButtonClick;
        public event Action OnBirthButtonClick;
        public event Action<PetParameterTypes, string> OnInputFieldValueChanged;
        public event Action<bool> OnWeightToggleChanged;
        public event Action<bool> OnGenderToggleValueChanged;

        private bool isNameError = true;
        private bool isWeightError = true;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            petNameInputField.onValueChanged.RemoveAllListeners();
            petWeightInputField.onValueChanged.RemoveAllListeners();
            
            weightToggle.onValueChanged.RemoveAllListeners();
            genderToggle.onValueChanged.RemoveAllListeners();
            
            birthDayButton.onClick.RemoveAllListeners();
            birthMonthButton.onClick.RemoveAllListeners();
            birthYearButton.onClick.RemoveAllListeners();
            
            base.Dismiss();
        }

        public void Initialize(CreateNewPetUIWidgetData widgetData) {
            
            applyButton.interactable = false;

            petNameInputField.onValueChanged.AddListener((delegate(string value) { OnInputFieldValueChanged?.Invoke(PetParameterTypes.NICKNAME, value); }));
            petWeightInputField.onValueChanged.AddListener((delegate(string value) { OnInputFieldValueChanged?.Invoke(PetParameterTypes.WEIGHT, value); }));
            
            weightToggle.onValueChanged.AddListener(delegate(bool flag) { OnWeightToggleChanged?.Invoke(flag); });
            genderToggle.onValueChanged.AddListener(delegate(bool flag) { OnGenderToggleValueChanged?.Invoke(flag); });

            birthDayButton.onClick.AddListener(delegate { OnBirthButtonClick?.Invoke(); });
            birthMonthButton.onClick.AddListener(delegate { OnBirthButtonClick?.Invoke(); });
            birthYearButton.onClick.AddListener(delegate { OnBirthButtonClick?.Invoke(); });

            birthDayText.text = DateTime.Now.Day.ToString();
            birthMonthText.text = DateTime.Now.Month.ToString();
            birthYearText.text = DateTime.Now.Year.ToString();
            
            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });

            applyButton.onClick.AddListener(delegate {
                applyButton.onClick.RemoveAllListeners();
                applyButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.15f, 4, 0.5f).OnStart(() => { }).OnComplete(() => { OnApplyButtonClick?.Invoke(); });
            });
        }

        public void UpdateWeightFieldValue(string value) {
            petWeightInputField.text = value;
        }

        public void SetInputFieldError(PetParameterTypes type, bool anError) {
            switch (type) {
                case PetParameterTypes.NICKNAME:
                    isNameError = anError;
                    break;
                case PetParameterTypes.WEIGHT:
                    isWeightError = anError;
                    break;
            }

            applyButton.interactable = 
                !string.IsNullOrEmpty(petNameInputField.text) &&
                !string.IsNullOrEmpty(petWeightInputField.text) &&
                !isNameError && !isWeightError;
        }

        public void UpdateBirthDate(DateTime date) {
            birthDayText.text = date.Day.ToString();
            birthMonthText.text = date.Month.ToString();
            birthYearText.text = date.Year.ToString();
        }
    }
}