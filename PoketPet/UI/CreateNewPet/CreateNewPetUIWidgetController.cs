using System;
using System.Text.RegularExpressions;
using PoketPet.Global;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Settings;
using PoketPet.UI.DataPicker;
using PoketPet.UI.PetParameter;
using PoketPet.Utilities;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.PetGenders;

namespace PoketPet.UI.CreateNewPet {
    public class CreateNewPetUIWidgetController : WidgetControllerWithData<CreateNewPetUIWidget, CreateNewPetUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            
            WidgetData.gender = FEMALE;
            WidgetData.birth = DateTime.Now;
            WidgetData.weightUnits = GlobalVariables.LocalDataKeys.POUNDS_WEIGHT_UNITS;

            Widget.OnInputFieldValueChanged += OnInputFieldValueChangedHandler;
            Widget.OnWeightToggleChanged += OnWeightToggleChangedHandler;
            Widget.OnGenderToggleValueChanged += OnGenderToggleChangedHandler;
            Widget.OnBirthButtonClick += OnBirthButtonClickHandler;
            Widget.OnApplyButtonClick += OnApplyButtonClickHandler;
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.Initialize(WidgetData);
            PetsProfileManager.CreateNewPetModel();
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnInputFieldValueChanged -= OnInputFieldValueChangedHandler;
            Widget.OnWeightToggleChanged -= OnWeightToggleChangedHandler;
            Widget.OnGenderToggleValueChanged -= OnGenderToggleChangedHandler;
            Widget.OnBirthButtonClick -= OnBirthButtonClickHandler;
            Widget.OnApplyButtonClick -= OnApplyButtonClickHandler;
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
        }

        private void OnInputFieldValueChangedHandler(PetParameterTypes type, string value) {
            switch (type) {
                case PetParameterTypes.NICKNAME:
                    WidgetData.name = value;
                    Widget.SetInputFieldError(type, !Regex.IsMatch(value, @"[A-Za-z]+"));
                    break;
                case PetParameterTypes.WEIGHT:
                    float.TryParse(value, out var weight);
                    WidgetData.weight = WidgetData.weightUnits == GlobalVariables.LocalDataKeys.KILOGRAM_WEIGHT_UNITS ? weight : Core.Utilities.UnitsUtilities.PoundsToKilograms(weight);
                    Widget.SetInputFieldError(type, WidgetData.weight > 70 || WidgetData.weight < 1);
                    break;
            }
        }

        private void OnGenderToggleChangedHandler(bool flag) {
            WidgetData.gender = flag ? "male" : "female";
        }

        private void OnWeightToggleChangedHandler(bool flag) {
            WidgetData.weightUnits = flag ? GlobalVariables.LocalDataKeys.KILOGRAM_WEIGHT_UNITS : GlobalVariables.LocalDataKeys.POUNDS_WEIGHT_UNITS;
            
            if (WidgetData.weight <= 0) return;
            var value = flag ? WidgetData.weight : Core.Utilities.UnitsUtilities.KilogramToPounds(WidgetData.weight);
            
            Widget.UpdateWeightFieldValue(value.ToString());
            
            SettingsManager.SetWeightUnits(WidgetData.weightUnits);
        }

        private void OnBirthButtonClickHandler() {
			var datePickerUiWidgetData = new DataPickerUIWidgetData(new DateTime(
				WidgetData.birth.Year,
				WidgetData.birth.Month,
				WidgetData.birth.Day));
			datePickerUiWidgetData.OnPickedData += date => {
				Debug.Log("datePickerUiWidgetData.OnPickedData");
				WidgetData.birth = date;
				if (DateTime.Compare(date, DateTime.Now) > 0) {
					date = DateTime.Now;
				}
				Widget.UpdateBirthDate(date);	
			};
			
			CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.DATE_PICKER_UI, datePickerUiWidgetData);
		}

        private void OnApplyButtonClickHandler() {
            
            //Set Firebase user properties
            CoreMobile.Instance.AnalyticsManager.SetUserProperty("units_system", WidgetData.weightUnits);
            CoreMobile.Instance.AnalyticsManager.SetUserProperty("pet_gender", WidgetData.gender);
            CoreMobile.Instance.AnalyticsManager.SetUserProperty("pet_name", WidgetData.name);

            //Play click button sound
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);

            //Create new pet data model
            var pet = PetsProfileManager.CurrentPet;
            pet.nickname = WidgetData.name;
            pet.weight = WidgetData.weight;
            pet.gender = WidgetData.gender;
            pet.birth_day = DateTimeConversion.DateTimeToTimestamp(WidgetData.birth);

            //Create new pet
            PetsProfileManager.CreateNewPet();
            
            SettingsManager.SaveToBackend();
        }

        private void WidgetOnCloseButtonClick() {
            Widget.Dismiss();
        }
    }
}