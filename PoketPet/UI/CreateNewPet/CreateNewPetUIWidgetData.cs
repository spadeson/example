using System;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.CreateNewPet
{
	[Serializable]
	public class CreateNewPetUIWidgetData : WidgetData {
		public string name;
		public float weight;
		public string weightUnits;
		public string gender;
		public DateTime birth;
	}
}