using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.FullScreenPopupUIWidget;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PoketPet.UI.UserProfile {
    public class UserProfileUIWidget : FullScreenPopupUIWidget {
        public event Action OnCloseButton;
        public event Action OnEditBirthDayButtonClick;
        public event Action<UserProfileUIWidgetData> OnSaveButton;
        public event Action OnLogoutButtonClick;
        public event Action OnAvatarButtonClick;

        public event Action<string> OnCopyUserIdButtonClick;

        [Header("Logout Button"), SerializeField] private Button logoutButton;

        [Header("Avatar Image:")]
        [SerializeField] private Image userAvatarImage;
        
        [Header("Avatar Button:")]
        [SerializeField] private Button userAvatarButton;
        
        [Header("UserID")]
        [SerializeField] private TextMeshProUGUI userId;
        [SerializeField] private Button copyUserIdButton;

        [Header("Form")]
        [SerializeField] private TMP_InputField nameField;
        [SerializeField] private TMP_InputField birthDateField;
        [SerializeField] private TMP_InputField emailField;

        public UserProfileUIWidgetData userProfileUiWidgetData;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            
            closeButton.onClick.RemoveAllListeners();
            actionButton.onClick.RemoveAllListeners();
            logoutButton.onClick.RemoveAllListeners();
            
            userAvatarButton.onClick.RemoveAllListeners();
            
            copyUserIdButton.onClick.RemoveAllListeners();
            
            nameField.onEndEdit.RemoveAllListeners();
            birthDateField.onSelect.RemoveAllListeners();
            emailField.onEndEdit.RemoveAllListeners();
            
            base.Dismiss();
        }

        public void Initialize(UserProfileUIWidgetData userProfileData) {
            
            userProfileUiWidgetData = userProfileData;

            closeButton.onClick.AddListener(CloseButtonClickHandler);
            actionButton.onClick.AddListener(SaveButtonClickedHandler);
            logoutButton.onClick.AddListener(LogoutButtonClickHandler);
            
            userAvatarButton.onClick.AddListener(UserAvatarButtonClickHandler);
            
            copyUserIdButton.onClick.AddListener(OnCopyUserIdButtonClickHandler);

            userId.text = userProfileUiWidgetData.UserDataModel.id;
            
            nameField.SetTextWithoutNotify(userProfileUiWidgetData.UserDataModel.nickname);
            birthDateField.text = new DateTime(userProfileUiWidgetData.UserDataModel.registration).ToString("dd MM yyyy - HH:mm:ss");
            emailField.SetTextWithoutNotify(userProfileUiWidgetData.UserDataModel.email);

            nameField.onEndEdit.AddListener(delegate(string nickName) { userProfileUiWidgetData.UserDataModel.nickname = nickName; });
            birthDateField.onSelect.AddListener(delegate(string birthdate) { OnEditBirthDayButtonClick?.Invoke(); });
            emailField.onEndEdit.AddListener(delegate(string email) { userProfileUiWidgetData.UserDataModel.email = email; });
        }

        private void OnCopyUserIdButtonClickHandler() {
            OnCopyUserIdButtonClick?.Invoke(userProfileUiWidgetData.UserDataModel.id);
        }

        public void UpdateWidgetFields(UserModel userModel) {
            
            userProfileUiWidgetData.UserDataModel = userModel;
            
            nameField.SetTextWithoutNotify(userProfileUiWidgetData.UserDataModel.nickname);
            emailField.SetTextWithoutNotify(userProfileUiWidgetData.UserDataModel.email);
        }

        public void SetBirthdayDate(DateTime dateTime) {
            birthDateField.text = dateTime.ToString("MM / dd / yyyy");
        }

        public void SetAvatarImage(Sprite sprite) {
            userAvatarImage.sprite = sprite;
        }

        private void UserAvatarButtonClickHandler() {
            Debug.Log("User Avatar Button Clicked");
            OnAvatarButtonClick?.Invoke();
        }
        
        private void CloseButtonClickHandler() {
            closeButton.onClick.RemoveAllListeners();
            closeButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnCloseButton?.Invoke();
                EventSystem.current.SetSelectedGameObject(null);
            });
        }
        
        private void LogoutButtonClickHandler() {
            logoutButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnLogoutButtonClick?.Invoke();
                EventSystem.current.SetSelectedGameObject(null);
            });
        }

        private void SaveButtonClickedHandler() {
            actionButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnSaveButton?.Invoke(userProfileUiWidgetData);
                EventSystem.current.SetSelectedGameObject(null);
            });
        }

        # if UNITY_EDITOR
        public void ToggleFocus() {
            if (nameField.isFocused) {
                EventSystem.current.SetSelectedGameObject(emailField.gameObject, null);
                emailField.OnPointerClick(new PointerEventData(EventSystem.current));
            } else if (emailField.isFocused) {
                EventSystem.current.SetSelectedGameObject(nameField.gameObject, null);
                nameField.OnPointerClick(new PointerEventData(EventSystem.current));
            }
        }
        #endif
    }
}
