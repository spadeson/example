using System;
using PoketPet.User;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.UserProfile
{
    [Serializable]
    public class UserProfileUIWidgetData : WidgetData
    {
        public UserModel UserDataModel;
    }
}