using System;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Files;
using PoketPet.Photo;
using PoketPet.UI.DataPicker;
using PoketPet.User;
using PoketPet.Utilities;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.UserProfile {
    public class UserProfileUIWidgetController : WidgetControllerWithData<UserProfileUIWidget, UserProfileUIWidgetData> {

        #region WidgetControllerWithData

        protected override void OnWidgetCreated(Widget widget) {

            Widget.OnCopyUserIdButtonClick += WidgetOnCopyUserIdButtonClick;
            Widget.OnCloseButton += WidgetOnCloseButtonClickedHandler;
            Widget.OnEditBirthDayButtonClick += WidgetOnOnEditBirthDayButtonClick;
            Widget.OnLogoutButtonClick += WidgetOnLogoutButtonClick;
            Widget.OnSaveButton += WidgetOnSaveButtonClickedHandler;

            Widget.OnAvatarButtonClick += WidgetOnAvatarButtonClickHandler;

            UserProfileManager.OnUserAvatarUpdated += UserAvatarUpdatedHandler;
            UserAvatarUpdatedHandler();

            UserProfileManager.OnUserDataModelUpdated += () => { Widget.UpdateWidgetFields(UserProfileManager.CurrentUser); };

            #if UNITY_EDITOR
            CoreMobile.Instance.GesturesManager.OnTabButtonPressed += () => { Widget.ToggleFocus(); };
            #endif
            
            Widget.Initialize(WidgetData);
            Widget.SetBirthdayDate(DateTimeConversion.TimestampToDateTime(WidgetData.UserDataModel.birth_day));
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
            
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCopyUserIdButtonClick -= WidgetOnCopyUserIdButtonClick;
            Widget.OnCloseButton -= WidgetOnCloseButtonClickedHandler;
            Widget.OnEditBirthDayButtonClick -= WidgetOnOnEditBirthDayButtonClick;
            Widget.OnLogoutButtonClick -= WidgetOnLogoutButtonClick;
            Widget.OnSaveButton -= WidgetOnSaveButtonClickedHandler;
            UserProfileManager.OnUserAvatarUpdated -= UserAvatarUpdatedHandler;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        #endregion

        #region Private Methods

        private void WidgetOnCopyUserIdButtonClick(string userId) {
            var textEditor = new TextEditor {text = userId};
            textEditor.SelectAll();
            textEditor.Copy();
        }
        
        private void WidgetOnCloseButtonClickedHandler() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.Dismiss();
        }
        
        private void WidgetOnOnEditBirthDayButtonClick() {
            var dataPickerUiWidgetData = new DataPickerUIWidgetData(DateTimeConversion.TimestampToDateTime(WidgetData.UserDataModel.birth_day));
            dataPickerUiWidgetData.OnPickedData += OnPickedDataHandler;
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.DATE_PICKER_UI, dataPickerUiWidgetData);
        }
        
        private void WidgetOnLogoutButtonClick() {
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.Logout();
        }
        
        private void WidgetOnAvatarButtonClickHandler() {
            PhotoManager.TakePhoto(sprite => {
                SaveImageByRest(sprite.texture);
            }, 512, NativeCamera.PreferredCamera.Front);
        }
        
        private void SaveImageByRest(Texture2D texture) {
            var requestData = new SetUserAvatarData() {from = UserProfileManager.CurrentUser.id, image = texture.EncodeToPNG(), action = "set_avatar"};
            var operation   = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("usersForm", requestData.GetForm());
            operation.OnDataReceived += OnUserAvatarUpdateHandler;
        }
        
        private static void OnUserAvatarUpdateHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
            } else {
                // TODO: Release Error Status 
            }
        }
        
        private void OnPickedDataHandler(DateTime dateTime) {
            Debug.Log($"Obtained Birthday Data: {dateTime.Month}, {dateTime.Day}, {dateTime.Year}");
            UserProfileManager.SetUserBirthday(dateTime);
            Widget.SetBirthdayDate(DateTimeConversion.TimestampToDateTime(WidgetData.UserDataModel.birth_day));
        }

        private void WidgetOnSaveButtonClickedHandler(UserProfileUIWidgetData userProfileUiWidgetData) {
            
            userProfileUiWidgetData.UserDataModel.gender = userProfileUiWidgetData.UserDataModel.gender.ToLower();
            
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);

            var userUpdateRequest = new RequestData<UserModel> {from = UserProfileManager.CurrentUser.id, data = userProfileUiWidgetData.UserDataModel, action = "update"};

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("users", userUpdateRequest);
            operation.OnDataReceived += UserDataHandler;
        }
        
        private void UserAvatarUpdatedHandler() {
            PhotoManager.GetAvatarById(UserProfileManager.CurrentUser.avatar, sprite => {
                if (Widget.enabled) {
                    Widget.SetAvatarImage(sprite);
                }
            });
        }
        
        private void UserDataHandler(string data) {
            var response = JsonUtility.FromJson<ResponseData<UserModel>>(data);
            UserProfileManager.LoginWithServerResponse(response.data);
        }
        
        #endregion
    }
}