using System;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunGameOnboarding
{
	[Serializable]
	public class FirstRunGameOnboardingUIWidgetData : WidgetData {
		public bool isFirstRun;
	}
}