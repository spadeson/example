using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunGameOnboarding {
    public class FirstRunGameOnboardingUIWidgetController : WidgetControllerWithData<FirstRunGameOnboardingUIWidget, FirstRunGameOnboardingUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void WidgetOnActionButtonClick() {
            Widget.Dismiss();
        }
    }
}
