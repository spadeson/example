using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunGameOnboarding {
    public class FirstRunGameOnboardingUIWidget : BasicPopupUIWidget {

        public event Action OnActionButtonClick;
        
        public override void Create() {
            base.Create();
            actionButton.onClick.AddListener(delegate {
                OnActionButtonClick?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            actionButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }
    }
}