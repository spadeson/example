using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialGeneric {
    public class TutorialGenericUIWidgetController : WidgetControllerWithData<TutorialGenericUIWidget, TutorialGenericUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }
        
        private void WidgetOnActionButtonClick() {
            Widget.Dismiss();
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}