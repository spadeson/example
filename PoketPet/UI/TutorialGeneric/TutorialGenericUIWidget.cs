using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialGeneric {
    public class TutorialGenericUIWidget : BaseUIWidget {
        
        public event Action OnActionButtonClick;
        
        [Header("Shroud Image")]
        [SerializeField] private Image shroudImage;

        [Header("Container")]
        [SerializeField] private RectTransform container;
        
        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;
        
        [Header("Icon Image")]
        [SerializeField] private Image iconImage;
        
        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;
        
        [Header("Action Button Label")]
        [SerializeField] private TextMeshProUGUI actionButtonLabel;

        [Header("Action Button")]
        [SerializeField] private Button actionButton;

        #region BaseUIWidget

        public override void Activate(bool animated) {
            Debug.Log("Activate");
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            HideAnimatedWidget();
        }

        #endregion

        private void OnEnable() {
            SetInitialStates();
        }


        public void Initialize(TutorialGenericUIWidgetData widgetData) {
            Debug.Log("Initialize");
            actionButton.onClick.AddListener(ActionButtonClickHandler);
            iconImage.sprite = CoreMobile.Instance.SpritesIconsManager.GetSpriteIconByKey("adviser", widgetData.tutorialSingleStepData.icon);
            titleText.text = widgetData.tutorialSingleStepData.title;
            descriptionText.text = widgetData.tutorialSingleStepData.description;
            
            ShowAnimatedWidget();
        }

        private void ActionButtonClickHandler() {
            actionButton.onClick.RemoveAllListeners();
            actionButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnActionButtonClick?.Invoke();
            });
        }

        private void SetInitialStates() {
            shroudImage.DOFade(0, 0);
            
            container.DOScale(0, 0f);
            
            iconImage.rectTransform.DOScale(0, 0);
            iconImage.DOFade(0, 0);

            descriptionText.rectTransform.DOScale(0, 0);
        }
        
        private void ShowAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(canvasGroup.DOFade(1, 0));
            sequence.Join(shroudImage.DOFade(0.75f, 0.35f).SetEase(Ease.OutFlash));
            sequence.Join(container.DOScale(1f, 0.35f).SetEase(Ease.OutSine));
            sequence.Append(iconImage.rectTransform.DOScale(1f, 0.45f).From(0).SetEase(Ease.OutSine));
            sequence.Join(iconImage.DOFade(1f, 0.45f).From(0).SetEase(Ease.Flash));
            sequence.Join(descriptionText.rectTransform.DOScale(1, 0.45f).SetEase(Ease.Flash));
        }

        private void HideAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(0, 0.15f).SetEase(Ease.InSine));
            sequence.Append(shroudImage.DOFade(0, 0.25f).SetEase(Ease.OutFlash));
            sequence.OnComplete(() => { base.Dismiss(); });
        }
    }
}