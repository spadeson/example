using TMPro;
using UnityEngine;

namespace PoketPet.UI.TaskAdviser {
    public class TaskAdviserPresenter : MonoBehaviour {
        [SerializeField] private TextMeshProUGUI descriptionText;

        public void Initialize(string textData) {
            descriptionText.text = textData;
        }
    }
}
