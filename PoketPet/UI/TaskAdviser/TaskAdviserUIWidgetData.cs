using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskAdviser {
    [Serializable]
    public class TaskAdviserUIWidgetData : WidgetData {
        public string TaskType { get; set; }
        public string UserRole { get; set; }
    }
}