using PoketPet.Adviser.Config;
using PoketPet.Global;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskAdviser {
    public class TaskAdviserUIWidgetController : WidgetControllerWithData<TaskAdviserUIWidget, TaskAdviserUIWidgetData> {

        [SerializeField] private AdviserEntitiesLibrary adviserEntitiesLibrary;
        
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;

            var adviserConfig = adviserEntitiesLibrary.GetAdviserConfigByUserRoleAndTaskType(WidgetData.UserRole, WidgetData.TaskType);
            
            Widget.Initialize(adviserConfig);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void WidgetOnActionButtonClick() {
            //CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            Widget.Dismiss();
        }
    }
}
