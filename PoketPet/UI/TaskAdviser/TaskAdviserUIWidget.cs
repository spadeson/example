using System;
using DG.Tweening;
using PoketPet.Adviser.Config;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskAdviser {
    public class TaskAdviserUIWidget : BasicPopupUIWidget {
        
        public event Action OnActionButtonClick;
        
        [Header("Adviser Elements Container")]
        [SerializeField] private RectTransform adviserElementsContainer; 

        #region MonoBehaviour

        #endregion

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        #endregion

        #region PublicMethods

        public void Initialize(AdviserEntityConfig widgetData) {

            var newAdviserElement = Instantiate(widgetData.visual, adviserElementsContainer).GetComponent<TaskAdviserPresenter>();
            newAdviserElement.Initialize(widgetData.textContent);
            
            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                actionButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 4, 0.5f).OnComplete(() => {
                    OnActionButtonClick?.Invoke();
                });
            });
        }

        #endregion

        #region PrivateMethods
        

        #endregion
    }
}