using System;
using Core.NetworkSubsystem.Config;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ConnectionSetup {
    [Serializable]
    public class ConnectionSetupUIWidgetData : WidgetData {
        public ConnectionsNetworkLibrary connectionsNetworkLibrary;
        public bool resetTutorial;
    }
}
