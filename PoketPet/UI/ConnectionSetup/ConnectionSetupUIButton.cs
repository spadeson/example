﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.ConnectionSetup {
    public class ConnectionSetupUIButton : MonoBehaviour {
    
        public event Action<int> OnButtonClick;
    
        private int _connectionConfigId;

        [SerializeField] private TextMeshProUGUI buttonLabel;
        [SerializeField] private Button button;
    
        public void Initialize(int connectionId, string connectionName) {
            _connectionConfigId = connectionId;

            if (button == null) {
                button = GetComponent<Button>();
            }

            if (buttonLabel == null) {
                buttonLabel = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            }

            buttonLabel.text = !string.IsNullOrEmpty(connectionName) ? connectionName : $"Connection {connectionId}";

            button.onClick.AddListener(delegate {
                OnButtonClick?.Invoke(_connectionConfigId);
            });
        }
    }
}
