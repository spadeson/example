using PoketPet.Global;
using PoketPet.Tutorial.Onboarding;
using PoketPet.UI.Preloader;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.TutorialKeys;

namespace PoketPet.UI.ConnectionSetup {
    public class ConnectionSetupUIWidgetController : WidgetControllerWithData<ConnectionSetupUIWidget, ConnectionSetupUIWidgetData> {

        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnApplyButtonClicked += OnApplyButtonClickedHandler;
            Widget.OnConnectionButtonClicked += OnConnectionButtonClickedHandler;
            
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) { }

        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnApplyButtonClicked -= OnApplyButtonClickedHandler;
            Widget.OnConnectionButtonClicked -= OnConnectionButtonClickedHandler;
        }

        #endregion

        #region Private Methods

        private void OnApplyButtonClickedHandler(bool resetTutorial) {
            
            if (!CoreMobile.Instance.LocalDataManager.IsDataExists(TUTORIAL_COMPLETE) || resetTutorial) {
                CoreMobile.Instance.LocalDataManager.SaveLocalDataBoolean(TUTORIAL_COMPLETE, false);
                CoreMobile.Instance.LocalDataManager.SaveLocalDataString(TUTORIAL_TYPE, INTRO.ToLower());
                CoreMobile.Instance.LocalDataManager.SaveLocalDataInt(TUTORIAL_STEP, 0);
            }

            if (!CoreMobile.Instance.LocalDataManager.GetLocalDataBoolean(TUTORIAL_COMPLETE)) {
                var tutorialSceneOperation = CoreMobile.Instance.SceneManager.LoadSceneByKeyFromConfigAsync(GlobalVariables.ScenesKeys.TUTORIAL_SCENE);
                
                var preloaderWidgetData = new PreloaderUIWidgetData {
                    operation = tutorialSceneOperation,
                    useFakeProgress = true,
                    fakeProgressDuration = 1f
                };

                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PRELOADER_UI, preloaderWidgetData);
            } else {
                CoreMobile.Instance.SceneManager.LoadSceneByKeyFromConfig(GlobalVariables.ScenesKeys.AUTHENTICATION_SCENE);
            }

            Widget.Dismiss();
        }
        
        private void OnConnectionButtonClickedHandler(int configId) {
            CoreMobile.Instance.NetworkManager.SetConnectionConfig(configId, out var connectionConfig);
            CoreMobile.Instance.SocketNetworkManager.SetConnectionConfig(configId);

            Widget.UpdateConnectionData(connectionConfig);
        }

        #endregion
    }
}
