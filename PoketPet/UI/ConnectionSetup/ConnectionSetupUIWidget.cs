using System;
using Core.NetworkSubsystem.Config;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.ConnectionSetup {
    public class ConnectionSetupUIWidget : BasicPopupUIWidget {

        public event Action<int> OnConnectionButtonClicked;
        public event Action<bool> OnApplyButtonClicked;

        [Header("Connection Button Prefab")]
        [SerializeField] private GameObject buttonPrefab;

        [Header("Connection Buttons Container")]
        [SerializeField] private RectTransform buttonsContainer;

        [Header("Connection Data Text Label")]
        [SerializeField] private TextMeshProUGUI hostDataText;

        [Header("Tutorial Toggle")]
        [SerializeField] private Toggle tutorialToggle;

        private ConnectionSetupUIWidgetData _widgetData;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(ConnectionSetupUIWidgetData widgetData) {

            _widgetData = widgetData;
            
            CleanConnectionButtons();

            for (var index = 0; index < _widgetData.connectionsNetworkLibrary.ConnectionConfigsList.Count; index++) {
                var config             = _widgetData.connectionsNetworkLibrary.ConnectionConfigsList[index];
                var connectionButtonGo = Instantiate(buttonPrefab, buttonsContainer, false);
                var connectionButton   = connectionButtonGo.GetComponent<ConnectionSetupUIButton>();
                connectionButton.Initialize(index, config.networkConnectionConfigModel.ConnectionName);
                connectionButton.OnButtonClick += id => { OnConnectionButtonClicked?.Invoke(id); };
            }

            actionButton.onClick.AddListener(delegate { OnApplyButtonClicked?.Invoke(_widgetData.resetTutorial); });

            _widgetData.resetTutorial = tutorialToggle.isOn;

            tutorialToggle.onValueChanged.AddListener(delegate(bool flag) { _widgetData.resetTutorial = flag; });
        }

        public void UpdateConnectionData(ConnectionConfig connectionConfig) {
            hostDataText.text = $"Protocol:\t{connectionConfig.networkConnectionConfigModel.Protocol}";
            hostDataText.text += $"\nHost:\t{connectionConfig.networkConnectionConfigModel.Host}";
            hostDataText.text += $"\nPort:\t\t{connectionConfig.networkConnectionConfigModel.Port}";
        }

        private void CleanConnectionButtons() {
            for (var i = 0; i < buttonsContainer.childCount; i++) {
                Destroy(buttonsContainer.GetChild(i).gameObject);
            }
        }
    }
}
