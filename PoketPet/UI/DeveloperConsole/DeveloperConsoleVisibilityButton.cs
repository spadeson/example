using System;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.DeveloperConsole {
    public class DeveloperConsoleVisibilityButton : Button {
        
        public event Action OnButtonClick;

        [SerializeField] private Image buttonIcon;

        public Sprite visibleIcon;
        public Sprite hiddenIcon;

        public void Initialize(bool isVisible) {
            SetButtonIcon(isVisible);
            onClick.AddListener(delegate {
                OnButtonClick?.Invoke();
            });
        }

        public void ToggleButton(bool isVisible) {
            SetButtonIcon(isVisible);
        }

        private void SetButtonIcon(bool value) {
            buttonIcon.sprite = value ? visibleIcon : hiddenIcon;
        }
    }
}
