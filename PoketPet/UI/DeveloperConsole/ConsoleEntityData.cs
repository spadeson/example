using System;
using UnityEngine;

namespace PoketPet.UI.DeveloperConsole {
    [Serializable]
    public class ConsoleEntityData {
        public ConsoleMessageTypes messageType;
        public DateTime timestamp;
        public string message;
    }
}
