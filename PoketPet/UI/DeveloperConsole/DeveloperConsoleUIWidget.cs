using System;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DeveloperConsole {
    public class DeveloperConsoleUIWidget : BaseUIWidget {
        
        public event Action<string> OnCopyButtonClick;

        [SerializeField] private ConsoleEntityPresenter consoleEntityPresenter;
        [SerializeField] private Transform container;

        [Header("Content Canvas Group")]
        [SerializeField] private CanvasGroup _canvasGroup;
        
        [Header("Clear Button")]
        [SerializeField] private Button clearConsoleButton;
        
        [Header("Visibility Button")]
        [SerializeField] private DeveloperConsoleVisibilityButton visibilityConsoleButton;

        private bool _isVisible;
        
        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            clearConsoleButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize() {
            _isVisible = true;
            clearConsoleButton.onClick.AddListener(ClearConsole);
            visibilityConsoleButton.onClick.AddListener(ToggleVisibility);
            visibilityConsoleButton.Initialize(_isVisible);
        }

        public void CreateNewConsoleEntity(ConsoleEntityData consoleEntityData) {
            var consoleEntity = Instantiate(consoleEntityPresenter, container, false);
            consoleEntity.Initialize(consoleEntityData);
            consoleEntity.OnCopyButtonClick += ConsoleEntityOnCopyButtonClick;
        }

        private void ConsoleEntityOnCopyButtonClick(string data) {
            OnCopyButtonClick?.Invoke(data);
        }

        private void ClearConsole() {
            for (var i = 0; i < container.childCount; i++) {
                Destroy(container.GetChild(i).gameObject);
            }
        }
        
        private void ToggleVisibility() {
            _isVisible = !_isVisible;
            _canvasGroup.alpha = _isVisible ? 1f : 0f;
            _canvasGroup.interactable = _isVisible;
            _canvasGroup.blocksRaycasts = _isVisible;
        }
    }
}