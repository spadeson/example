using System;
using Core.NetworkSubsystem.Managers.REST;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DeveloperConsole {
    public class DeveloperConsoleUIWidgetController : WidgetControllerWithData<DeveloperConsoleUIWidget, DeveloperConsoleUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {

            Widget.OnCopyButtonClick += WidgetOnCopyButtonClick;

            OperationSchema.NotifyConsoleOnRequest += (operation, data) => {
                var newConsoleEntity = new ConsoleEntityData {messageType = ConsoleMessageTypes.INFO, timestamp = DateTime.Now, message = $"REST REQUEST: {operation.name} - Data:{data}"};
                Widget.CreateNewConsoleEntity(newConsoleEntity);
            };
            
            OperationSchema.NotifyConsoleOnDataReceived += (operation, data) => {
                var newConsoleEntity = new ConsoleEntityData {messageType = ConsoleMessageTypes.INFO, timestamp = DateTime.Now, message = $"REST RESPONSE: {operation.name} - Data:{data}"};
                Widget.CreateNewConsoleEntity(newConsoleEntity);
            };
            
            OperationSchema.NotifyConsoleOnErrorReceived += (operation, data) => {
                var newConsoleEntity = new ConsoleEntityData {messageType = ConsoleMessageTypes.ERROR, timestamp = DateTime.Now, message = $"REST ERROR: {operation.name} - Data:{data}"};
                Widget.CreateNewConsoleEntity(newConsoleEntity);
            };
            
            Widget.Initialize();
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCopyButtonClick -= WidgetOnCopyButtonClick;
        }
        
        private void WidgetOnCopyButtonClick(string data) {
            var textEditor = new TextEditor {text = data};
            textEditor.SelectAll();
            textEditor.Copy();
        }
    }
}