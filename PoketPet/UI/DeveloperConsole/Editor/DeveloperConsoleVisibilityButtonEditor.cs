using UnityEditor;

namespace PoketPet.UI.DeveloperConsole.Editor {
    [CustomEditor(typeof(DeveloperConsoleVisibilityButton))]
    public class DeveloperConsoleVisibilityButtonEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var visibilityButton = (DeveloperConsoleVisibilityButton)target;
        }
    }
}
