using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.DeveloperConsole {
    public class ConsoleEntityPresenter : MonoBehaviour {

        public event Action<string> OnCopyButtonClick;
        
        [Header("Timestamp")]
        [SerializeField] private TextMeshProUGUI timestamp;
        
        [Header("Message")]
        [SerializeField] private TextMeshProUGUI message;

        [Header("Copy Button")]
        [SerializeField] private Button copyButton;

        private ConsoleEntityData _data;

        public void Initialize(ConsoleEntityData data) {
            _data = data;
            timestamp.text = _data.timestamp.ToString("u");
            message.text = _data.message;
            copyButton.onClick.AddListener(delegate {
                OnCopyButtonClick?.Invoke(_data.message);
            });
        }
    }
}
