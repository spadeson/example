using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskFlow {
    public class TaskFlowUIWidgetController : WidgetControllerWithData<TaskFlowUIWidget, TaskFlowUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
            throw new System.NotImplementedException();
        }

        protected override void OnWidgetDismissed(Widget widget) {
            throw new System.NotImplementedException();
        }
    }
}
