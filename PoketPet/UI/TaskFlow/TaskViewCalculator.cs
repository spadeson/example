using System;
using System.Collections.Generic;
using PoketPet.UI.TaskDisplay;
using UnityEngine;

namespace PoketPet.UI.TaskFlow {
    public static class TaskViewCalculator {
        
        public static List<float> sizesList;

        public static bool IsBlockedEdge(int index, int listCont, float toTaskAngDif) {
            return (index == 0 && toTaskAngDif < 35 && toTaskAngDif > 0) ||
                   (index == listCont - 1 && toTaskAngDif > -35 && toTaskAngDif < 0);
        }
        
        public static Vector2 GetNewCenter(Vector2 cachedHoldPos, List<TaskUIElementAssignee> assigneeList, float diameter, int dir, int index) {
            var curAng1 = CalcAngle(cachedHoldPos, ((RectTransform) assigneeList[index].transform).anchoredPosition);
            var curAng2 = CalcAngle(cachedHoldPos, ((RectTransform) assigneeList[index + dir].transform).anchoredPosition);

            var curCenter1 = CalcPointOnCircleADegR(diameter, curAng1);
            var curCenter2 = CalcPointOnCircleADegR(diameter, curAng2);

            var dist = CalcDistance(curCenter1, curCenter2);

            var minDist = (diameter * sizesList[index] + diameter * sizesList[index + dir]) / 1.95;

            var partDist = (float)Math.Round(Math.Abs(minDist - dist));
            if (Math.Round(partDist) > 10) {
                dir = dist > minDist ? dir :  dir * -1;
                curAng2 += (float) Math.Abs(dist - minDist) / 5 * dir;
            }

            //calc new position of icon and set it
            return CalcPointOnCircleADegR(diameter, 360 - (curAng2 - 90));
        }

        public static float CalcDistance(Vector2 v1, Vector2 v2) {
            var res = (float) Math.Sqrt(Math.Pow((v2.x - v1.x), 2) + Math.Pow((v2.y - v1.y), 2));
            return res;
        }

        public static float CalcAngle(Vector2 v1, Vector2 v2) {
            var res = (float) (Math.Atan2(v2.y - v1.y, v2.x - v1.x) / Math.PI * 180);
            return res;
        }

        public static float AngDifference(float ang1, float ang2) {
            var res = Math.Min((ang1 - ang2 + 360) % 360, (ang2 - ang1 + 360) % 360);
            return res;
        }
        
        public static Vector2 CalcPointOnCircleADegR(float radius, float ang)
        {
            var ang2 = (float) (ang * Math.PI / 180);
            var xPos = Math.Sin(ang2) * radius;
            var yPos = Math.Cos(ang2) * radius;
            return new Vector2((float)xPos,(float)yPos);
        }

        public static float AngDiffAbs(float ang1, float ang2)
        {
            var dif = (float)(ang1 - ang2) % 360;
            if (dif < 0) dif = dif + 360;
            if (dif > 180) dif = 360 - dif;
            return dif;
        }
        
        public static float AngDiff(float ang1, float ang2)
        {
            var dif = (float)(ang1 - ang2) % 360;
            if (dif > 180) dif = 360 - dif;
            if (dif < -180) dif = 360 + dif;
            return dif;
        }
    }
}