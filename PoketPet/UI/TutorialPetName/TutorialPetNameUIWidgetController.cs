using System;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialPetName {
    public class TutorialPetNameUIWidgetController : WidgetControllerWithData<TutorialPetNameUIWidget, TutorialPetNameUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {

        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
        
        private void WidgetOnActionButtonClick() {
            Widget.Dismiss();
        }
    }
}
