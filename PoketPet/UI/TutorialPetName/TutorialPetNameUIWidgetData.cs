using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialPetName {
    [Serializable]
    public class TutorialPetNameUIWidgetData : WidgetData {
        
        public TutorialSingleStepModelData tutorialSingleStepData;
        public string petName;
    }
}
