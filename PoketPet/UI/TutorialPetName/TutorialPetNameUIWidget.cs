using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialPetName {
    public class TutorialPetNameUIWidget : BaseUIWidget {
        
        public event Action OnActionButtonClick;

        [Header("Shroud Image")]
        [SerializeField] private Image shroudImage;

        [Header("Container")]
        [SerializeField] private RectTransform container;

        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Icon Image")]
        [SerializeField] private Image iconImage;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;
        
        [Header("Nickname Input Field")]
        [SerializeField] private TMP_InputField nicknameInputField;

        [Header("Action Button Label")]
        [SerializeField] private TextMeshProUGUI actionButtonLabel;

        [Header("Action Button")]
        [SerializeField] private Button actionButton;

        private TutorialPetNameUIWidgetData _widgetData;

        #region BaseUIWidget

        public override void Activate(bool animated) {
            nicknameInputField.onValueChanged.AddListener(NicknameInputFieldValueHandler);
            ShowAnimatedWidget();
        }

        private void NicknameInputFieldValueHandler(string value) {
            _widgetData.petName = value;
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            HideAnimatedWidget();
        }

        #endregion

        private void OnEnable() {
            SetInitialStates();
        }


        public void Initialize(TutorialPetNameUIWidgetData widgetData) {
            _widgetData = widgetData;
            actionButton.onClick.AddListener(ActionButtonClickHandler);
        }

        private void ActionButtonClickHandler() {
            actionButton.onClick.RemoveAllListeners();
            actionButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => { OnActionButtonClick?.Invoke(); });
        }

        private void SetInitialStates() {
            shroudImage.DOFade(0, 0);
            container.DOScale(0, 0f);
        }

        private void ShowAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(canvasGroup.DOFade(1, 0));
            sequence.Join(shroudImage.DOFade(0.75f, 0.25f).SetEase(Ease.OutFlash));
            sequence.Join(container.DOScale(1f, 0.2f).SetEase(Ease.OutBounce));
            sequence.OnComplete(() => { base.Activate(false); });
        }

        private void HideAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(0, 0.15f).SetEase(Ease.InBounce));
            sequence.Append(shroudImage.DOFade(0, 0.25f).SetEase(Ease.OutFlash));
            sequence.OnComplete(() => { base.Dismiss(); });
        }
    }
}