using UnityEngine;

namespace PoketPet.UI.TutorialTask {
    public class TutorialTaskUIAssignee : MonoBehaviour {
        
        public RectTransform RectTransform => _rectTransform;
        private RectTransform _rectTransform;

        private const float DEFAULT_SCALE = 1f;
        private const float LARGE_SCALE = 1.5f;

        private const float SCALE_THRESHOLD = 164f;

        public void Initialize() {
            _rectTransform = GetComponent<RectTransform>();
        }

        public Vector2 GetAnchoredPosition() {
            return RectTransform.anchoredPosition;
        }
        
        public Vector2 GetRectPosition() {
            return RectTransform.rect.position;
        }

        public void SetScale(float distance) {
            var lerp = Mathf.Clamp(1f - distance / SCALE_THRESHOLD, 0f, 1f);
            RectTransform.localScale = Vector3.Lerp(Vector3.one * DEFAULT_SCALE, Vector3.one * LARGE_SCALE, lerp);
        }
    }
}
