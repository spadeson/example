using System;
using DG.Tweening;
using PoketPet.UI.Tooltip;
using PoketPet.UI.TutorialTask;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial05 {
    public class NewTutorial05UIWidget : BaseUIWidget {
        public event Action OnTaskReleased;
        public event Action OnActionButtonClick;

        [Header("Tooltip")]
        [SerializeField] private TooltipUIWidget tooltipUIWidget;

        [Header("Finger Pointer")]
        [SerializeField] private Image fingerPointerImage;
        
        [Header("Arrow Image")]
        [SerializeField] private Image arrowImage;

        [Header("Task UI Element")]
        [SerializeField] private TutorialTaskUIElementDragable taskUIElementDragable;
        
        [Header("Assignee Element")]
        [SerializeField] private TutorialTaskUIAssignee taskAssignee;

        private Sequence _pointerSequence;
        private Sequence _completionSequence;
        
        private const float START_TASK_POSITION = -622f;
        private const float FINAL_TASK_POSITION = -120f;
        
        private const float TASK_ASSIGNEE_UPPER_POSITION = 0f;
        private const float TASK_ASSIGNEE_SCALE = 1.75f;

        private const float TASK_COMPLETE_THRESHOLD = 200f;

        private Vector2 _initialTaskElementPosition;

        #region MonoBehaviour

        private void OnEnable() {
            
            taskUIElementDragable.Initialize();
            taskAssignee.Initialize();

            taskUIElementDragable.RectTransform.DOScale(0, 0);
            
            fingerPointerImage.DOFade(0, 0);
            arrowImage.DOFade(0, 0);
            
            taskAssignee.transform.DOScale(0, 0);
            
            taskUIElementDragable.OnElementCLick += OnElementDragableCLickHandler;
            taskUIElementDragable.OnElementDrag += OnElementDragableDragHandler;
            taskUIElementDragable.OnElementRelease += OnElementDragableReleaseHandler;

            _initialTaskElementPosition = taskUIElementDragable.RectTransform.anchoredPosition;
        }

        #endregion

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
            ShowAnimations();
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            _pointerSequence?.Kill();
            _completionSequence?.Kill();
            
            taskUIElementDragable.OnElementCLick -= OnElementDragableCLickHandler;
            taskUIElementDragable.OnElementDrag -= OnElementDragableDragHandler;
            taskUIElementDragable.OnElementRelease -= OnElementDragableReleaseHandler;
            
            base.Dismiss();
        }

        #endregion

        #region Public Methods
        
        public void Initialize(NewTutorial05UIWidgetData widgetData) {
            tooltipUIWidget.OnTooltipClosed += OnTooltipCloseHandler;
            tooltipUIWidget.Initialize(widgetData.TutorialStepModelData.title, widgetData.TutorialStepModelData.description);
        }

        #endregion

        #region Private Methods

        private void ShowAnimations() {
            tooltipUIWidget.Show();
        }

        private void OnTooltipCloseHandler() {
            tooltipUIWidget.OnTooltipClosed -= OnTooltipCloseHandler;
            tooltipUIWidget.Hide();
            PlayLoopedAnimation();
        }

        private void PlayLoopedAnimation() {

            var showTaskSequence = DOTween.Sequence();
            showTaskSequence.Append(taskUIElementDragable.transform.DOScale(1f, 0.5f).SetEase(Ease.OutSine));
            showTaskSequence.Join(taskAssignee.transform.DOScale(1, 0).From(0f).SetEase(Ease.OutSine));
            
            
            _pointerSequence = DOTween.Sequence();
            _pointerSequence.PrependInterval(0.5f);
            _pointerSequence.Append(fingerPointerImage.DOFade(1, 0.5f).SetEase(Ease.Flash));
            _pointerSequence.Join(fingerPointerImage.rectTransform.DOScale(0.75f, 0.5f).SetEase(Ease.OutSine));
            _pointerSequence.Join(arrowImage.DOFade(0.65f, 0.5f).SetEase(Ease.Flash));
            
            _pointerSequence.Append(fingerPointerImage.rectTransform.DOAnchorPosY(FINAL_TASK_POSITION, 1f).SetEase(Ease.OutSine));
            _pointerSequence.Join(arrowImage.rectTransform.DOAnchorPosY(FINAL_TASK_POSITION, 0.75f).SetEase(Ease.OutSine));
            _pointerSequence.Join(arrowImage.rectTransform.DOScale(2, 0.75f).SetEase(Ease.OutSine));
            _pointerSequence.Append(arrowImage.DOFade(0f, 0.25f).SetEase(Ease.Flash));
            
            _pointerSequence.AppendInterval(0.25f);
            _pointerSequence.Append(fingerPointerImage.DOFade(0f, 0.5f).SetEase(Ease.Flash));

            _pointerSequence.AppendInterval(0.25f);
            _pointerSequence.SetLoops(-1, LoopType.Restart);
        }

        private void OnElementDragableCLickHandler(Vector2 pointerPosition) {
            _pointerSequence?.Kill();
            fingerPointerImage.DOFade(0, 0.1f).SetEase(Ease.Flash);
            arrowImage.DOFade(0, 0.1f).SetEase(Ease.Flash);
            taskUIElementDragable.transform.DOScale(1.25f, 0.15f).SetEase(Ease.OutSine);
        }
        
        private void OnElementDragableDragHandler(Vector2 pointerPosition) {
            var assigneePosition = taskAssignee.GetAnchoredPosition();
            var distance         = Vector2.Distance(assigneePosition, pointerPosition);
            taskAssignee.SetScale(distance);
        }
        
        private void OnElementDragableReleaseHandler(Vector2 pointerPosition) {
            
            taskUIElementDragable.transform.DOScale(1f, 0.15f).SetEase(Ease.OutSine);
            
            var assigneePosition = taskAssignee.GetAnchoredPosition();
            var distance         = Vector2.Distance(assigneePosition, pointerPosition);

            if (distance > TASK_COMPLETE_THRESHOLD) {
                taskUIElementDragable.RectTransform.DOAnchorPos(_initialTaskElementPosition, 0.25f).SetEase(Ease.OutSine);
            } else {
                _completionSequence = DOTween.Sequence();
                
                _completionSequence.Append(taskUIElementDragable.RectTransform.DOScale(1f, 0.25f).SetEase(Ease.OutSine));
                _completionSequence.Join(taskAssignee.RectTransform.DOScale(1f, 0.25f).SetEase(Ease.OutSine));
                
                _completionSequence.AppendInterval(0.25f);
                _completionSequence.Join(taskUIElementDragable.RectTransform.DOAnchorPos(new Vector2(0, FINAL_TASK_POSITION), 0.25f).SetEase(Ease.OutSine));
                _completionSequence.Join(taskAssignee.RectTransform.DOAnchorPos(new Vector2(0, FINAL_TASK_POSITION), 0.25f).SetEase(Ease.OutSine));
                
                _completionSequence.AppendInterval(0.15f);
                _completionSequence.Append(taskUIElementDragable.RectTransform.DOScale(0f, 0.25f).SetEase(Ease.OutQuint));
                _completionSequence.Join(taskAssignee.RectTransform.DOScale(0f, 0.25f).SetEase(Ease.OutQuint));
                
                _completionSequence.AppendInterval(0.5f);
                _completionSequence.OnStart(() => { OnTaskReleased?.Invoke(); });
                _completionSequence.OnComplete(() => { OnActionButtonClick?.Invoke(); });
            }
        }

        #endregion
    }
}