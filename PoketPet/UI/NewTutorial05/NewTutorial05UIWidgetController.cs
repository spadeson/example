using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial05 {
    public class NewTutorial05UIWidgetController : WidgetControllerWithData<NewTutorial05UIWidget, NewTutorial05UIWidgetData> {

        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnTaskReleased += WidgetOnTaskReleased;
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnTaskReleased -= WidgetOnTaskReleased;
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        #endregion

        #region Private Methods

        private void WidgetOnActionButtonClick() {
            Widget.Dismiss();
        }
        
        private void WidgetOnTaskReleased() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.TUTORIAL_TASK_COMPLETED_SFX);
        }

        #endregion
    }
}