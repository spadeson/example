using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PoketPet.UI.TutorialTask {
    public class TutorialTaskUIElementDragable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
        public event Action<Vector2> OnElementCLick;
        public event Action<Vector2> OnElementDrag;
        public event Action<Vector2> OnElementRelease;

        [Header("Progress Fill Image")]
        [SerializeField] private Image progressFillImage;

        public RectTransform RectTransform => _rectTransform;
        private RectTransform _rectTransform;

        private float _halfScreenWidth;
        private float _halfScreenHeight;

        private void OnDestroy() {
            DOTween.KillAll();
        }

        public void Initialize() {
            _rectTransform = GetComponent<RectTransform>();
            _halfScreenWidth = Screen.width * 0.5f;
            _halfScreenHeight = Screen.height * 0.5f;
        }

        public void OnBeginDrag(PointerEventData eventData) {
            OnElementCLick?.Invoke(eventData.position);
        }

        public void OnDrag(PointerEventData eventData) {
            var newPosition = new Vector2(eventData.position.x - _halfScreenWidth, eventData.position.y - _halfScreenHeight);
            _rectTransform.anchoredPosition = newPosition;
            OnElementDrag?.Invoke(_rectTransform.anchoredPosition);
        }
        
        public void OnEndDrag(PointerEventData eventData) {
            var newPosition = new Vector2(eventData.position.x - _halfScreenWidth, eventData.position.y - _halfScreenHeight);
            OnElementRelease?.Invoke(newPosition);
        }

        public void SetProgress(float value, float duration) {
            progressFillImage.DOKill();
            DOTween.To(() => progressFillImage.fillAmount, x => progressFillImage.fillAmount = x, 0, 0);
            DOTween.To(() => progressFillImage.fillAmount, x => progressFillImage.fillAmount = x, value, duration);
        }

    }
}
