using System;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial05 {
    public class TutorialTaskUIElementClickable : MonoBehaviour {

        public event Action OnTaskElementButtonClick;
        
        [Header("Button")]
        [SerializeField] private Button taskElementButton;

        #region MonoBehaviour

        private void OnEnable() {
            taskElementButton.onClick.AddListener(delegate {
                OnTaskElementButtonClick?.Invoke(); 
            });
        }

        private void OnDisable() {
            taskElementButton.onClick.RemoveAllListeners();
        }

        #endregion
    }
}
