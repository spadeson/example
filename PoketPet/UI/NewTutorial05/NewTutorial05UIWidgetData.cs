using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial05 {
    [Serializable]
    public class NewTutorial05UIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialStepModelData { get; set; }
    }
}