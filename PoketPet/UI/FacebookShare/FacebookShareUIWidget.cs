using System;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FacebookShare {
    public class FacebookShareUIWidget : BaseUIWidget {
        public event Action OnShareButtonClick;

        [Header("Share Button")]
        [SerializeField] private Button shareButton;

        public override void Create() {
            base.Create();
            shareButton.onClick.AddListener(delegate { OnShareButtonClick?.Invoke(); });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            shareButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }
    }
}