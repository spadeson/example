using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FacebookShare {
    public class FacebookShareUIWidgetController : WidgetControllerWithData<FacebookShareUIWidget, FacebookShareUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            CoreMobile.Instance.FacebookManager.Login();
            Widget.OnShareButtonClick += OnShareButtonClickHandler;
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnShareButtonClick -= OnShareButtonClickHandler;
        }

        private void OnShareButtonClickHandler() {
            CoreMobile.Instance.FacebookManager.Share();
        }
    }
}
