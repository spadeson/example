using System.Collections.Generic;
using PoketPet.Global;
using PoketPet.Messages;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.UI.BreedSelection;
using PoketPet.UI.BurgerMenu;
using PoketPet.UI.CreateNewPet;
using PoketPet.UI.EmailInput;
using PoketPet.UI.Messages;
using PoketPet.UI.PetParameter;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.AddPet {
    public class AddPetUIWidgetController : WidgetControllerWithData<AddPetUIWidget, AddPetUIWidgetData> {
        
        [Header("Breeds Library:")]
        [SerializeField] private DogBreedConfig dogBreedConfig;

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnMessagesButtonClick += WidgetOnMessagesButtonClickHandler;
            Widget.OnBurgerMenuButtonClick += WidgetOnBurgerMenuButtonClickHandler;
            Widget.OnAddNewPetButtonClick += WidgetOnAddNewPetButtonClickHandler;
            Widget.OnReverseInviteButtonClick += WidgetOnReverseInviteButtonClickHandler;

            PetsProfileManager.OnPetUpdated += OnPetUpdateHandler;
            
            MessageManager.AddSocketListeners();
            MessageManager.GetMessagesByUserId();
            MessageManager.OnUpdate += UpdateMessagesBadgeHandler;
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }
        
        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnMessagesButtonClick -= WidgetOnMessagesButtonClickHandler;
            Widget.OnBurgerMenuButtonClick -= WidgetOnBurgerMenuButtonClickHandler;
            Widget.OnAddNewPetButtonClick -= WidgetOnAddNewPetButtonClickHandler;
            Widget.OnReverseInviteButtonClick -= WidgetOnReverseInviteButtonClickHandler;
            
            PetsProfileManager.OnPetUpdated -= OnPetUpdateHandler;
            MessageManager.OnUpdate -= UpdateMessagesBadgeHandler;
        }

        private void UpdateMessagesBadgeHandler() {
            Widget.SetBadgeCount(MessageManager.CountUnreadMessages());
        }

        private void WidgetOnMessagesButtonClickHandler() {
            var messagesUiWidgetData = new MessagesUIWidgetData {messagesDataList = MessageManager.Messages};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.MESSAGES_UI, messagesUiWidgetData);
        }
        
        private void WidgetOnBurgerMenuButtonClickHandler() {
            var burgerMenuWidgetData = new BurgerMenuUIWidgetData {
                burgerMenuButtons = new List<BurgerMenuButtonTypes> {BurgerMenuButtonTypes.USER_PROFILE}
            };
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.BURGER_MENU_UI, burgerMenuWidgetData, true);
        }
        
        private void WidgetOnAddNewPetButtonClickHandler() {
            //var breedSelectionUiWidgetData = new BreedSelectionUIWidgetData {
            //    dogBreedConfig = dogBreedConfig,
            //    selectedBreed = "unknown"
            //};
            
            //CoreMobile.Instance.UIManager.ShowUiWidgetWithData(GlobalVariables.UiKeys.BREED_SELECTION_UI, breedSelectionUiWidgetData);
            
            var widgetData = new CreateNewPetUIWidgetData();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.CREATE_NEW_PET_UI, widgetData, true);
        }
        
        private void WidgetOnReverseInviteButtonClickHandler() {
            var widgetData = new EmailInputUIWidgetData();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.EMAIL_INPUT_UI, widgetData, true);
        }

        private void OnPetUpdateHandler(PetResponseData petsDataModel) {
            Widget.Dismiss();
        }
    }
}
