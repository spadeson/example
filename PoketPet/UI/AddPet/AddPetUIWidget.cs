using System;
using PoketPet.UI.CommonUIElements;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.AddPet {
    public class AddPetUIWidget : BaseUIWidget {

        public event Action OnMessagesButtonClick;
        public event Action OnBurgerMenuButtonClick;
        public event Action OnAddNewPetButtonClick;
        public event Action OnReverseInviteButtonClick;
        
        [Header("Bage Counter:")]
        [SerializeField] private BageUIElement messagesBadge;

        [Header("Buttons:")]
        [SerializeField] private Button messagesButton;
        [SerializeField] private Button burgerMenuButton;
        [SerializeField] private Button addNewPetButton;
        [SerializeField] private Button reverseInviteButton;

        public override void Create() {
            base.Create();
            
            messagesButton.onClick.AddListener(delegate {
                OnMessagesButtonClick?.Invoke();
            });
            
            burgerMenuButton.onClick.AddListener(delegate {
                OnBurgerMenuButtonClick?.Invoke();
            });
            
            addNewPetButton.onClick.AddListener(delegate {
                OnAddNewPetButtonClick?.Invoke();
            });
            
            reverseInviteButton.onClick.AddListener(delegate {
                OnReverseInviteButtonClick?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            messagesButton.onClick.RemoveAllListeners();
            burgerMenuButton.onClick.RemoveAllListeners();
            addNewPetButton.onClick.RemoveAllListeners();
            reverseInviteButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }
        
        public void SetBadgeCount(int value) {
            messagesBadge.SetBageCounter(value);
        }
    }
}
