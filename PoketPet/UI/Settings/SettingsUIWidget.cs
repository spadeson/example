using System;
using PoketPet.Global;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.Settings {
    public class SettingsUIWidget : BasicPopupUIWidget {
        public event Action<SettingsUIWidgetData> OnApplyButtonClicked;
        public event Action<float> OnSoundsVolumeChanged;
        public event Action<float> OnMusicVolumeChanged;
        public event Action OnCloseButtonClicked;

        [Header("Sliders")]
        [SerializeField] private Slider soundsVolumeSlider;

        [Header("Push Notifications Toggle")]
        [SerializeField] private Toggle pushNotificationToggle;
        
        [Header("Weigh Units Toggle")]
        [SerializeField] private Toggle weightUnitsToggle;
        
        [Header("Time Format Toggle")]
        [SerializeField] private Toggle timeFormatToggle;

        private SettingsUIWidgetData _widgetData;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(SettingsUIWidgetData widgetData) {
            _widgetData = widgetData;

            //Set initial UI elements values
            soundsVolumeSlider.SetValueWithoutNotify(_widgetData.soundsVolume);
            pushNotificationToggle.SetIsOnWithoutNotify(_widgetData.enablePushNotifications);
            weightUnitsToggle.SetIsOnWithoutNotify(_widgetData.weightUnits == GlobalVariables.LocalDataKeys.POUNDS_WEIGHT_UNITS);
            timeFormatToggle.SetIsOnWithoutNotify(_widgetData.timeFormat == GlobalVariables.LocalDataKeys.MILITARY_TIME);

            soundsVolumeSlider.onValueChanged.AddListener(delegate(float soundsVolume) {
                _widgetData.soundsVolume = soundsVolume;
                OnSoundsVolumeChanged?.Invoke(soundsVolume);
            });

            pushNotificationToggle.onValueChanged.AddListener(delegate(bool pushNotificationsEnabled) {
                _widgetData.enablePushNotifications = pushNotificationsEnabled;
            });
            
            weightUnitsToggle.onValueChanged.AddListener(delegate(bool weightUnitsPounds) {
                _widgetData.weightUnits = weightUnitsPounds ? GlobalVariables.LocalDataKeys.POUNDS_WEIGHT_UNITS : GlobalVariables.LocalDataKeys.KILOGRAM_WEIGHT_UNITS;
            });
            
            timeFormatToggle.onValueChanged.AddListener(delegate(bool timeFormatMilitary) {
                _widgetData.timeFormat = timeFormatMilitary ? GlobalVariables.LocalDataKeys.MILITARY_TIME : GlobalVariables.LocalDataKeys.STANDARD_TIME;
            });

            actionButton.onClick.AddListener(delegate { OnApplyButtonClicked?.Invoke(_widgetData); });
            
            closeButton.onClick.AddListener(delegate { OnCloseButtonClicked?.Invoke(); });
        }
    }
}