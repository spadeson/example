using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Settings;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Settings {
    public class SettingsUIWidgetController : WidgetControllerWithData<SettingsUIWidget, SettingsUIWidgetData> {

        private float _previousSoundsVolume;
        
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnSoundsVolumeChanged += OnSoundsVolumeChangedHandler;

            Widget.OnCloseButtonClicked += CloseButtonClickedHandler;
            Widget.OnApplyButtonClicked += ApplyButtonClickedHandler;

            _previousSoundsVolume = WidgetData.soundsVolume;
            
            Widget.Initialize(WidgetData);
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClicked -= CloseButtonClickedHandler;
            Widget.OnApplyButtonClicked -= ApplyButtonClickedHandler;
            
            Widget.OnSoundsVolumeChanged -= OnSoundsVolumeChangedHandler;

            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }
        
        private void CloseButtonClickedHandler() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            SettingsManager.SetSoundsVolume(_previousSoundsVolume);
            Widget.Dismiss();
        }

        private void ApplyButtonClickedHandler(SettingsUIWidgetData settingsData) {
            //Set Sounds Volume
            _previousSoundsVolume = settingsData.soundsVolume;
            SettingsManager.SetSoundsVolume(settingsData.soundsVolume);
            
            //Set Push Notifications
            if (settingsData.enablePushNotifications) {
                SettingsManager.EnableFCMToken();
            } else {
                SettingsManager.DisableFCMToken();
            }

            //Set Weight Units
            SettingsManager.SetWeightUnits(settingsData.weightUnits);
            
            //Set Time Format
            SettingsManager.SetTimeFormat(settingsData.timeFormat);
            
            //Save data to the backend
            SettingsManager.SaveToBackend();
        }

        private void OnSoundsVolumeChangedHandler(float soundsVolume) {
            SettingsManager.SetSoundsVolume(soundsVolume);
        }

        private void UpdateNotificationsStatusHandler(string data) {
            var response = JsonUtility.FromJson<ResponseData<object>>(data);
            Debug.Log($"FCM Change Status: {response.data}");
        }
    }
}