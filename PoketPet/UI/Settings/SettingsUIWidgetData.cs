using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Settings {
    [Serializable]
    public class SettingsUIWidgetData : WidgetData {
        public float soundsVolume;
        public string timeFormat;
        public string weightUnits;
        public string localisation;
        public bool enablePushNotifications;
    }
}