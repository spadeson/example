using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.EmailInput {
    [Serializable]
    public class EmailInputUIWidgetData : WidgetData {
        public string Email { get; set; }
    }
}
