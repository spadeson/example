using System;
using Core.ErrorsHandlerSubsystem.Data;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.EmailInput
{
	public class EmailInputUIWidget : BasicPopupUIWidget	{
		
		public event Action OnCloseButtonClick;
		public event Action<string> OnRequestButtonClick;
		
		public event Action<InputFieldsTypes, EmailInputUIWidgetData> OnInputFieldEditEnd;
		
		private EmailInputUIWidgetData _widgetData;

		[Header("E-mail input field")]
		[SerializeField] private TMP_InputField emailInputField;
		
		[Header("Input Fields Status Labels:")]
		[SerializeField] private TextMeshProUGUI emailStatusText;
		
		[Header("Colors:")]
		[SerializeField] private Color normalColor = Color.white;
		[SerializeField] private Color errorColor = Color.red;
		
		private bool _emailFieldError;

		public override void Create() {
			base.Create();

			closeButton.onClick.AddListener(delegate {
				OnCloseButtonClick?.Invoke();
			});
			
			actionButton.onClick.AddListener(delegate {
				OnRequestButtonClick?.Invoke(emailInputField.text);
			});
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			actionButton.onClick.RemoveAllListeners();

			base.Dismiss();
		}
		
		public void Initialize(EmailInputUIWidgetData widgetData) {
			_widgetData = widgetData;
			
			emailStatusText.text = string.Empty;
			
			//Listen for the selection of the E-mail input filed
			emailInputField.onSelect.AddListener(delegate(string email) {
				actionButton.interactable = IsRequiresFieldsSet(email);
			});

			//Listen for the value changed for E-mail input filed
			emailInputField.onValueChanged.AddListener(delegate(string value) {
				_widgetData.Email = value;
				OnInputFieldEditEnd?.Invoke(InputFieldsTypes.EMAIL, _widgetData);
				actionButton.interactable = IsRequiresFieldsSet(value);
			});
            
			//Listen for end edit for input fields
			/*emailInputField.onEndEdit.AddListener(delegate(string value) {
				OnInputFieldEditEnd?.Invoke(InputFieldsTypes.EMAIL, _widgetData);
				actionButton.interactable = IsRequiresFieldsSet(value);
			});*/
			
			actionButton.interactable = IsRequiresFieldsSet(emailInputField.text);
		}
		
		public void SetInputFieldError(InputFieldsTypes type, string error) {

			TMP_InputField  inputField = null;
            TextMeshProUGUI statusText = null;

            switch (type) {
                case InputFieldsTypes.EMAIL:
                    inputField = emailInputField;
                    statusText = emailStatusText;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            if (string.IsNullOrEmpty(error)) {
                inputField.image.color = normalColor;

                statusText.color = normalColor;
                statusText.text = string.Empty;
                
                _emailFieldError = false;

            } else {
                inputField.image.color = errorColor;

                statusText.color = errorColor;
                statusText.text = error;
                
                _emailFieldError = true;
            }
            
            actionButton.interactable = !_emailFieldError;
        }
		
		private bool IsRequiresFieldsSet(string value1) {
			return !string.IsNullOrEmpty(value1) && !string.IsNullOrWhiteSpace(value1) && !_emailFieldError;
		}
	}
}