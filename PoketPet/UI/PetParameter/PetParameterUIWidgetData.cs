using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetParameter {
    [Serializable]
    public class PetParameterUIWidgetData : WidgetData {
        public string petParameterValue;
        public string petParameterTitle;
        public PetParameterTypes petParameterType;
    }
}
