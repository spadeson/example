using UnityEngine;

namespace PoketPet.UI.PetParameter {
    public enum PetParameterTypes : byte {
        NICKNAME = 0,
        GENDER = 1,
        WEIGHT = 2
    }
}
