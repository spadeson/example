using PoketPet.Pets.PetsSystem.Manager;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetParameter {
    public class PetParameterUIWidgetController : WidgetControllerWithData<PetParameterUIWidget, PetParameterUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnApplyButtonClick += WidgetOnApplyButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        private void WidgetOnApplyButtonClick(PetParameterUIWidgetData widgetData) {
            Widget.Dismiss();
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnApplyButtonClick -= WidgetOnApplyButtonClick;
        }
    }
}
