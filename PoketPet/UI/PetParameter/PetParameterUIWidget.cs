using System;
using System.Text.RegularExpressions;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetParameter {
    public class PetParameterUIWidget : BasicPopupUIWidget {
        
        public event Action<PetParameterUIWidgetData> OnApplyButtonClick;

        [Header("Text")]
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI descriptionText;

        [Header("Input Field")]
        [SerializeField] private TMP_InputField petParameterInputField;
        [SerializeField] private TextMeshProUGUI petParameterInputFieldPlaceholder;

        private PetParameterUIWidgetData _widgetData;
        
        private string _genderPattern = @"\b[a-zA-z-_]*";
        private string _weightPattern = @"^[0-9.,]*$";

        public override void Create() {
            base.Create();
            
            actionButton.onClick.AddListener(delegate {
                _widgetData.petParameterValue = petParameterInputField.text;
                OnApplyButtonClick?.Invoke(_widgetData);
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            actionButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize(PetParameterUIWidgetData widgetData) {
            _widgetData = widgetData;
            titleText.text = _widgetData.petParameterTitle;
            descriptionText.text = $"Enter you pet's {_widgetData.petParameterType.ToString().ToLower()} in the field below.";
            petParameterInputFieldPlaceholder.text = _widgetData.petParameterType.ToString().ToUpperInvariant();
            actionButton.interactable = false;
            petParameterInputField.onValueChanged.AddListener(value => {

                switch (_widgetData.petParameterType) {
                    case PetParameterTypes.NICKNAME:
                        actionButton.interactable = !string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value);
                        break;
                    case PetParameterTypes.GENDER:
                        Debug.Log(Regex.Match(value, _genderPattern).Value + ", " + Regex.Match(value, _genderPattern).Success);
                        actionButton.interactable = Regex.Match(value, _genderPattern).Success;
                        break;
                    case PetParameterTypes.WEIGHT:
                        actionButton.interactable = Regex.Match(value, _weightPattern).Success;
                        break;
                    default:
                        actionButton.interactable = !string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value);
                        break;
                }
            });
        }
    }
}
