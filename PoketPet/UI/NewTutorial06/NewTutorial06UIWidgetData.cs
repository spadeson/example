using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial06 {
    [Serializable]
    public class NewTutorial06UIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}