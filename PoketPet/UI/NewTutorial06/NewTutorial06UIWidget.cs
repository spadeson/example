using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial06 {
    public class NewTutorial06UIWidget : BasicPopupUIWidget {
        public event Action OnActionButtonClick;

        [Header("Main Image")]
        [SerializeField] private Image mainImage;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        #region BaseUIWidget

        public override void Create() {
            base.Create();
            mainImage.DOFade(0, 0);
        }

        public override void Activate(bool animated) {
            base.Activate(false);
            mainImage.DOFade(1, 0.35f).SetEase(Ease.Flash);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
            mainImage.DOFade(0, 0.15f).SetEase(Ease.Flash);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        #endregion

        #region Public Methods

        public void Initialize(NewTutorial06UIWidgetData widgetData) {
            descriptionText.text = widgetData.TutorialSingleStepData.description;
            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                actionButton.targetGraphic.rectTransform.DOPunchScale(Vector3.one * 0.25f, 0.35f, 4, 0.5f).OnComplete(() => {
                    OnActionButtonClick?.Invoke(); 
                });
            });
        }

        #endregion
    }
}