using PoketPet.Games;
using PoketPet.Global;
using PoketPet.Photo;
using PoketPet.UI.GamePhoto;
using PoketPet.User;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.GameTypesKeys;

namespace PoketPet.UI.PetMoodGameDescription
{
	public class PetMoodGameDescriptionUIWidgetController : WidgetControllerWithData<PetMoodGameDescriptionUIWidget, PetMoodGameDescriptionUIWidgetData>	{
		
		protected override void OnWidgetCreated(Widget widget) {
			Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
			Widget.OnActionButtonClick += WidgetOnActionButtonClick;
			Widget.Initialize();
		}

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) {
			Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
			Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
		}
		
		private void WidgetOnActionButtonClick() {
			
			CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
			
#if UNITY_IOS
			PhotoManager.TakePhoto((sprite) => {
				GameManager.SetCurrentGame(GAME_EMOTIONS);
				var gamePhotoUiWidgetData = new GamePhotoUIWidgetData() {
					from = UserProfileManager.CurrentUser.id,
					photo = sprite,
					level = 0
				};
				Widget.Dismiss();
				CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.GAME_PHOTO_UI, gamePhotoUiWidgetData); 
			});
#else
            //Make request for texture download
#endif
		}
		
		private void WidgetOnCloseButtonClick() {
			CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
			Widget.Dismiss();
		}
	}
}