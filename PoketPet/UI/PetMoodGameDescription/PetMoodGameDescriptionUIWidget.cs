using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetMoodGameDescription
{
	public class PetMoodGameDescriptionUIWidget : BasicPopupUIWidget	{

		public event Action OnCloseButtonClick;
		public event Action OnActionButtonClick;

		public override void Create() {
			base.Create();
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			closeButton.onClick.RemoveAllListeners();
			actionButton.onClick.RemoveAllListeners();
			base.Dismiss();
		}

		public void Initialize() {
			
			closeButton.onClick.AddListener(delegate {
				closeButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.25f, 3, 0.5f).OnComplete(() => {
					OnCloseButtonClick?.Invoke();
				});
			});
			
			actionButton.onClick.AddListener(delegate {
				actionButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.25f, 3, 0.5f).OnComplete(() => {
					OnActionButtonClick?.Invoke();
				});
			});
		}

	}
}