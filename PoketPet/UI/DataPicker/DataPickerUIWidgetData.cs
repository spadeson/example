using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DataPicker {
    [Serializable]
    public class DataPickerUIWidgetData : WidgetData {

        public bool Closable { get; set; }

        public DateTime birthDate;
        public event Action<DateTime> OnPickedData;

        public DataPickerUIWidgetData(DateTime birthDate, bool closable = true) {
            this.birthDate = birthDate;
            Closable = closable;
        }

        public void SetData(DateTime dateTime) {
            OnPickedData?.Invoke(dateTime);   
        }
    }
}