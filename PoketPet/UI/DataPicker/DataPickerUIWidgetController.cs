using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DataPicker {
    public class DataPickerUIWidgetController : WidgetControllerWithData<DataPickerUIWidget, DataPickerUIWidgetData> {

        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += OnCloseButtonClickHandler;
            Widget.OnConfirmButtonClick += OnConfirmButtonClickHandler;
            
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= OnCloseButtonClickHandler;
            Widget.OnConfirmButtonClick -= OnConfirmButtonClickHandler;
        }

        #endregion

        #region Private Methods

        private void OnConfirmButtonClickHandler(DateTime data) {
            WidgetData.SetData(data);
            Widget.Dismiss();
        }

        private void OnCloseButtonClickHandler() {
            Widget.Dismiss();
        }

        #endregion
    }
}