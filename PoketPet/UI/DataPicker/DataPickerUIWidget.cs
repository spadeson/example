using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.DataPicker {
    public class DataPickerUIWidget : BasicPopupUIWidget {
        public event Action OnCloseButtonClick;
        public event Action<DateTime> OnConfirmButtonClick;

        private DataPickerUIWidgetData _widgetData;
        private DateTime _currentDatTime;
        private DateTime DateTimeNow => DateTime.Now;

        [Header("Month:"), SerializeField] private TextMeshProUGUI monthValue;
        [SerializeField] private Button monthUpButton;
        [SerializeField] private Button monthDownButton;

        [Header("Day:"), SerializeField] private TextMeshProUGUI dayValue;
        [SerializeField] private Button dayUpButton;
        [SerializeField] private Button dayDownButton;

        [Header("Year:"), SerializeField] private TextMeshProUGUI yearValue;
        [SerializeField] private Button yearUpButton;
        [SerializeField] private Button yearDownButton;

        public override void Create() {
            base.Create();

            dayUpButton.onClick.AddListener(delegate {
                var dateTime = _currentDatTime;
                _currentDatTime = dateTime.AddDays(1);
                UpdateDate(_currentDatTime);
            });

            dayDownButton.onClick.AddListener(delegate {
                var dateTime = _currentDatTime;
                _currentDatTime = dateTime.AddDays(-1);
                UpdateDate(_currentDatTime);
            });

            monthUpButton.onClick.AddListener(delegate {
                var dateTime = _currentDatTime;
                _currentDatTime = dateTime.AddMonths(1);
                UpdateDate(_currentDatTime);
            });

            monthDownButton.onClick.AddListener(delegate {
                var dateTime = _currentDatTime;
                _currentDatTime = dateTime.AddMonths(-1);
                UpdateDate(_currentDatTime);
            });

            yearUpButton.onClick.AddListener(delegate {
                if (_currentDatTime.Year >= DateTimeNow.Year) return;
                var dateTime = _currentDatTime;
                _currentDatTime = dateTime.AddYears(1);
                UpdateDate(_currentDatTime);
            });

            yearDownButton.onClick.AddListener(delegate {
                if (_currentDatTime.Year <= 1900) return;
                var dateTime = _currentDatTime;
                _currentDatTime = dateTime.AddYears(-1);
                UpdateDate(_currentDatTime);
            });

            if (_widgetData.Closable) {
                closeButton.interactable = true;
                closeButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });
            } else {
                closeButton.interactable = false;
            }

            actionButton.onClick.AddListener(delegate { OnConfirmButtonClick?.Invoke(_currentDatTime); });
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(DataPickerUIWidgetData widgetData) {
            _widgetData = widgetData;
            _currentDatTime = _widgetData.birthDate;
            UpdateDate(_currentDatTime);
        }

        private void UpdateDate(DateTime dateTime) {
            monthValue.text = dateTime.Month.ToString();
            dayValue.text = dateTime.Day.ToString();
            yearValue.text = dateTime.Year.ToString();
        }
    }
}