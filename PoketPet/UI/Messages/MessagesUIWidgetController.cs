using PoketPet.Global;
using PoketPet.Messages;
using PoketPet.Network.API.REST.Messages;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Photo;
using PoketPet.UI.ConfirmationPopup;
using PoketPet.UI.GamePhoto;
using PoketPet.UI.GameResult;
using PoketPet.UI.InviteConfirm;
using PoketPet.UI.InviteCreation;
using PoketPet.UI.NotificationStatus;
using PoketPet.UI.TaskCompletedOwnerApprove;
using PoketPet.UI.TaskCompletedResult;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.UI.Messages {
    public class MessagesUIWidgetController : WidgetControllerWithData<MessagesUIWidget, MessagesUIWidgetData> {
        
        #region WidgetControllerWithData

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClicked += OnCloseButtonClickedHandler;
            Widget.OnMessageElementButtonClick += WidgetOnMessageElementButtonClick;
            Widget.OnDeleteButtonClick += WidgetOnDeleteButtonClick;
            
            MessageManager.OnUpdate += OnUpdateMessagesList;
            
            Widget.Initialize(WidgetData);
            
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }
        
        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClicked -= OnCloseButtonClickedHandler;
            Widget.OnMessageElementButtonClick -= WidgetOnMessageElementButtonClick;
            Widget.OnDeleteButtonClick -= WidgetOnDeleteButtonClick;
            
            MessageManager.OnUpdate -= OnUpdateMessagesList;
        }

        #endregion

        #region Private Methods

        private void WidgetOnDeleteButtonClick(string messageId, bool close) {

            var selectedMessageId = messageId;
            
            var confirmationUiWidgetData = new ConfirmationPopupUIWidgetData {
                Title = "Delete Message",
                Message = "Are you sure want to delete message?",
                PositiveButtonLabel = "Yes",
                NegativeButtonLabel = "No",
            };

            confirmationUiWidgetData.OnPositiveButtonClicked += () => {
                MessageManager.CloseMessageById(messageId, false);
            };
            
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.CONFIRMATION_POPUP_UI, confirmationUiWidgetData);
        }

        private void WidgetOnMessageElementButtonClick(MessageModel messageData) {
            var uiManager = CoreMobile.Instance.UIManager;
            switch (messageData.type) {
                case "notification":
                    ShowNotificationPopup(messageData.notification_message);
                    //ShowTaskConfirmationResultPopup(true, 40);
                    break;
                case "task":
                    
                    break;
                case "confirm":
                    ShowConfirmationPopup(messageData.from, messageData.img, messageData.id);
                    break;
                case "wait_invite":
                    ShowInvitePopup(messageData.from);
                    break;
                case "invite":
                    ShowInviteConfirmPopup(messageData.from, messageData.id);
                    break;
                case "game":
                    switch (messageData.dynamic_data.key) {
                        case "play" :
                            ShowPlayGamePopup(messageData);
                            break;
                        case "win" :
                            ShowResultGamePopup(true, messageData.dynamic_data.bonus);
                            break;
                        case "lose" :
                            ShowResultGamePopup(false, messageData.dynamic_data.bonus);
                            break;
                    }
                    break;
            }

            MessageManager.ReadMessageById(messageData.id);
        }
        
        private void ShowNotificationPopup(string notification) {
            var widgetData = new NotificationStatusUIWidgetData() {Title = "notification", Message = notification};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(NOTIFICATION_STATUS_UI, widgetData);
        }
        
        private void ShowTaskConfirmationResultPopup(bool isConfirmed, int scores) {
            var widgetData = new TaskCompletedResultUIWidgetData() {isConfirmed = isConfirmed, scores = scores};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(TASK_COMPLETED_RESULT_UI, widgetData);
        }
        
        private void ShowConfirmationPopup(string userId, string photoId, string massageId) {
            PhotoManager.GetPhotoById(photoId, bytes => {
                var photo = PhotoManager.CreatePhoto(bytes);
                var widgetData = new TaskCompletedOwnerApproveUIWidgetData{photo = photo, userId = userId, messageId = massageId};
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(TASK_COMPLETED_OWNER_APPROVE_UI, widgetData);
            });
        }

        private void ShowInvitePopup(string userId) {
            UserProfileManager.GetUserById(userId, data => {
                var response = JsonUtility.FromJson<ResponseData<UserModel>>(data);
                
                var widgetData = new InviteCreationUIWidgetData {inviteeEmail = response.data.email, inviteeType = InviteeType.WATCHER};
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(INVITE_CREATION_UI, widgetData);
            });
        }
        
        private void ShowInviteConfirmPopup(string userId, string messageId) {
            var widgetData = new InviteConfirmUIWidgetData {userId = userId, messageId = messageId};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(INVITE_CONFIRM_UI, widgetData);
        }

        private void ShowPlayGamePopup(MessageModel messageData) {
            PhotoManager.GetPhotoById(messageData.img, bytes => {
                var gamePhotoUiWidgetData = new GamePhotoUIWidgetData() {
                    from = messageData.from,
                    messageId = messageData.id,
                    photo = PhotoManager.CreatePhoto(bytes),
                    level = 0
                };
                        
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GAME_PHOTO_UI, gamePhotoUiWidgetData);
            });
        }
        
        private void ShowResultGamePopup(bool isSuccess, int scores) {
            var widgetData = new GameResultUIWidgetData {
                isSuccess = isSuccess,
                scores = scores
            };
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GAME_RESULT_UI, widgetData);
        }

        private void OnUpdateMessagesList() {
            Widget.UpdateList();
        }
        
        private void OnCloseButtonClickedHandler() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.Dismiss();
        }

        #endregion
    }
}
