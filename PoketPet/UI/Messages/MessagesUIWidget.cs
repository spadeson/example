using System;
using DG.Tweening;
using PoketPet.Messages;
using PoketPet.Network.API.REST.Messages;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;

namespace PoketPet.UI.Messages {
    public class MessagesUIWidget : BasicPopupUIWidget {
        public event Action<string, bool> OnDeleteButtonClick;
        public event Action OnCloseButtonClicked;
        public event Action<MessageModel> OnMessageElementButtonClick;

        [Header("Messages Container")]
        [SerializeField] private RectTransform uiElementsContainer;
        
        [Header("Messages UI Element")]
        [SerializeField] private GameObject messageUiElementPrefab;

        private MessagesUIWidgetData _widgetData;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(MessagesUIWidgetData widgetData) {
            _widgetData = widgetData;

            UpdateList();
            
            closeButton.onClick.AddListener(CloseButtonClickHandler);
        }

        public void UpdateList() {
            RemoveMessagesUiElements();

            foreach (var messageDataModel in _widgetData.messagesDataList) {
                CreateNewMessageUiElement(messageDataModel);
            }
        }

        private void RemoveMessagesUiElements() {
            for (var i = 0; i < uiElementsContainer.childCount; i++) {
                var messageUiElement = uiElementsContainer.GetChild(i);
                messageUiElement.GetComponent<MessagesUIElement>().Dismiss();
                Destroy(messageUiElement.gameObject);
            }
        }

        private void CreateNewMessageUiElement(MessageModel data) {
            var newMessageUiElementGo = Instantiate(messageUiElementPrefab, uiElementsContainer, false);

            var newMessageUiElement = newMessageUiElementGo.GetComponent<MessagesUIElement>();

            newMessageUiElement.Initialize(data);

            newMessageUiElement.OnButtonClick += messageDataModel => {
                OnMessageElementButtonClick?.Invoke(messageDataModel);
            };
            
            newMessageUiElement.OnDeleteButtonClick += messageDataModel => {
                OnDeleteButtonClick?.Invoke(messageDataModel.id, false);
            };
        }

        private void CloseButtonClickHandler() {
            closeButton.onClick.RemoveAllListeners();
            closeButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnCloseButtonClicked?.Invoke();
            });
        }
    }
}