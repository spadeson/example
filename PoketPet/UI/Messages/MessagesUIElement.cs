﻿using System;
using PoketPet.Network.API.REST.Messages;
using PoketPet.User;
using PoketPet.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile;

namespace PoketPet.UI.Messages {
    public class MessagesUIElement : MonoBehaviour {

        public event Action<MessageModel> OnButtonClick;
        public event Action<MessageModel> OnDeleteButtonClick;

        [SerializeField] private Image background;
        [SerializeField] private Image icon;
        [SerializeField] private Button button;
        [SerializeField] private Button deleteButton;
        [SerializeField] private TextMeshProUGUI dateLabel;
        [SerializeField] private TextMeshProUGUI typeLabel;
        
        private MessageModel _data;

        public void Initialize(MessageModel data) {

            _data = data;
            
            if (button == null) {
                button = GetComponent<Button>();
            }

            icon.sprite = CoreMobile.Instance.SpritesIconsManager.GetSpriteIconByKey("messages", _data.type);
            
            dateLabel.text = DateTimeConversion.TimestampToDateTime(_data.create).ToString("MM/dd/yyyy - HH:mm");
            typeLabel.text = _data.type;
            if (_data.opened.IndexOf(UserProfileManager.CurrentUser.id) != -1) {
                MakeRead();
            }
            button.onClick.AddListener(OnButtonClickHandler);
            deleteButton.onClick.AddListener(OnDeleteButtonClickHandler);
        }

        public void Dismiss() {
            button.onClick.RemoveAllListeners();
        }

        private void MakeRead() {
            background.color = Color.gray;
        }

        private void OnButtonClickHandler() {
            OnButtonClick?.Invoke(_data);
        }
        
        private void OnDeleteButtonClickHandler() {
            OnDeleteButtonClick?.Invoke(_data);
        }
    }
}
