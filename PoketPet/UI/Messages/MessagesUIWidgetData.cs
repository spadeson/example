using System;
using System.Collections.Generic;
using PoketPet.Messages;
using PoketPet.Network.API.REST.Messages;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Messages {
    [Serializable]
    public class MessagesUIWidgetData : WidgetData {
        public List<MessageModel> messagesDataList = new List<MessageModel>();
    }
}