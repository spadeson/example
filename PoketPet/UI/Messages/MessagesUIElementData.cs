using System;

namespace PoketPet.UI.Messages {
    [Serializable]
    public class MessagesUIElementData {
        public string messageDate;
        public string messageType;
        public string messageId;
        public string from;
    }
}