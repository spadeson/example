using System;
using UnityEngine;

namespace PoketPet.UI.Messages.Config {
    [Serializable]
    public class MessagesIconData {
        public MessageTypes messageType;
        public Sprite messageIconSprite;
    }
}
