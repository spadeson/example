using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.UI.Messages.Config {
    [CreateAssetMenu(fileName = "MessagesIconsLibrary", menuName = "PoketPet/Messages/Messages Icons Library", order = 0)]
    public class MessagesIconsLibrary : ScriptableObject {
        
        public List<MessagesIconData> messagesIconsList = new List<MessagesIconData>();

        public Sprite GetSpriteIconByMessageType(MessageTypes type) {
            return messagesIconsList.Find(i => i.messageType == type).messageIconSprite;
        }
    }
}
