using System;
using PoketPet.Pets.Controllers.Main;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetStatesViewer {
    [Serializable]
    public class PetStatesViewerUIWidgetData : WidgetData {
        public IPetMainController PetMainController { get; set; }
    }
}