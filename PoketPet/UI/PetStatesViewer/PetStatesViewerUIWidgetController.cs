using PoketPet.Pets.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetStatesViewer
{
	public class PetStatesViewerUIWidgetController : WidgetControllerWithData<PetStatesViewerUIWidget, PetStatesViewerUIWidgetData>	{

		protected override void OnWidgetCreated(Widget widget) {
			Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
			Widget.OnPetStateButtonClick += WidgetOnOnPetStateButtonClick;
			Widget.Initialize();
		}

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) {
			Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
			Widget.OnPetStateButtonClick -= WidgetOnOnPetStateButtonClick;
		}
		
		private void WidgetOnCloseButtonClick() {
			Widget.Dismiss();
		}
		
		private void WidgetOnOnPetStateButtonClick(PetStates petState) {
			WidgetData.PetMainController.SetNewState(petState);
		}
	}
}