using PoketPet.Pets.Data;
using UnityEditor;

namespace PoketPet.UI.PetStatesViewer.Editor {
    [CustomEditor(typeof(PetStateButton))]
    public class PetStateButtonEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var petStateButton = (PetStateButton) target;
            petStateButton.petState = (PetStates)EditorGUILayout.EnumPopup("Pet State:", petStateButton.petState);
        }
    }
}
