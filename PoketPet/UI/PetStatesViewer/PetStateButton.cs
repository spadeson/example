using PoketPet.Pets.Data;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.PetStatesViewer {
    public class PetStateButton : Button {
        [HideInInspector] public PetStates petState;
    }
}
