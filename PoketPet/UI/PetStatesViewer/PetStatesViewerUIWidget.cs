using System;
using PoketPet.Pets.Data;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetStatesViewer
{
	public class PetStatesViewerUIWidget : BaseUIWidget {

		public event Action OnCloseButtonClick;
		public event Action<PetStates> OnPetStateButtonClick;
		
		public event Action OnStayButtonClick;
		public event Action OnSitButtonClick;
		public event Action OnLayButtonClick;
		public event Action OnMovingButtonClick;

		[Header("Close Button")]
		[SerializeField] private Button closeButton;
		
		[Header("States Buttons")]
		[SerializeField] private PetStateButton stayButton;
		[SerializeField] private PetStateButton sitButton;
		[SerializeField] private PetStateButton layButton;
		[SerializeField] private PetStateButton movingButton;

		public override void Create() {
			base.Create();
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			base.Dismiss();
		}

		public void Initialize() {
			closeButton.onClick.AddListener(delegate {
				OnCloseButtonClick?.Invoke();
			});
			
			stayButton.onClick.AddListener(delegate {
				OnPetStateButtonClick?.Invoke(stayButton.petState);
			});
			
			sitButton.onClick.AddListener(delegate {
				OnPetStateButtonClick?.Invoke(sitButton.petState);
			});
			
			layButton.onClick.AddListener(delegate {
				OnPetStateButtonClick?.Invoke(layButton.petState);
			});
			
			movingButton.onClick.AddListener(delegate {
				OnPetStateButtonClick?.Invoke(movingButton.petState);
			});
		}

	}
}