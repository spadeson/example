using System;
using System.Globalization;
using System.Linq;
using PoketPet.Global;
using PoketPet.Schedule;
using PoketPet.User;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;
using TMPro;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SpriteIconsGroupKeys;
using static PoketPet.Global.GlobalVariables.LocalDataKeys;

namespace PoketPet.UI.PetScheduleTaskEditor {
    public class PetScheduleTaskEditorUIWidget : BaseUIWidget {

        public event Action OnCloseButtonClick;
        public event Action OnDeleteButtonClick;
        public event Action<DateTime> OnConfirmButtonClick;

        private PetScheduleTaskEditorUIWidgetData _widgetData;

        [Header("Shroud Image")]
        [SerializeField] private Image shroudImage;
        [SerializeField] private Button shroudButton;

        [Header("Close Button")]
        [SerializeField] private Button closeButton;
        
        [Header("Delete Button")]
        [SerializeField] private Button deleteButton;
        
        [Header("Confirm Button")]
        [SerializeField] private Button confirmButton;
        
        [Header("Task Type Button")]
        [SerializeField] private Button taskTypeButton;

        [Header("Timer Fields")]
        [SerializeField] private TextMeshProUGUI timerInputHoursField;
        [SerializeField] private TextMeshProUGUI timerInputMinutesField;
        
        [Header("Hours Buttons")]
        [SerializeField] private Button hourUpButton;
        [SerializeField] private Button hourDownButton;
        
        [Header("Minute Buttons")]
        [SerializeField] private Button minuteUpButton;
        [SerializeField] private Button minuteDownButton;
        
        [Header("Time Format")]
        [SerializeField] private RectTransform timeMilitaryBackgroundContainer;
        [SerializeField] private RectTransform timeStandardBackgroundContainer;
        [SerializeField] private Button timeFormatButton;
        [SerializeField] private TextMeshProUGUI timeFormatField;
        
        [Header("Task Icon")]
        [SerializeField] private Image taskIcon;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(PetScheduleTaskEditorUIWidgetData widgetData) {
            _widgetData = widgetData;
            var timeFormat = UserProfileManager.CurrentUser.settings.time_format;
            
            UpdateTaskType(widgetData.TaskType);

            var timeSpan = TimeSpan.FromMinutes(_widgetData.TaskTime);
            var utcDate = new DateTime(timeSpan.Ticks);
            var localDate = Core.Utilities.TimeUtil.ConvertUtcToLocal(utcDate).AddYears(1);
            UpdateDate(localDate, timeFormat);

            taskTypeButton.enabled = false;

            hourUpButton.onClick.AddListener(delegate {
                var dateTime = localDate;
                localDate = dateTime.AddHours(1);
                UpdateDate(localDate, timeFormat);
            });

            hourDownButton.onClick.AddListener(delegate {
                var dateTime = localDate;
                localDate = dateTime.AddHours(-1);
                UpdateDate(localDate, timeFormat);
            });
            
            minuteUpButton.onClick.AddListener(delegate {
                var dateTime = localDate;
                localDate = dateTime.AddMinutes(1);
                UpdateDate(localDate, timeFormat);
            });

            minuteDownButton.onClick.AddListener(delegate {
                var dateTime = localDate;
                localDate = dateTime.AddMinutes(-1);
                UpdateDate(localDate, timeFormat);
            });
            
            timeMilitaryBackgroundContainer.gameObject.SetActive(timeFormat == MILITARY_TIME);
            timeStandardBackgroundContainer.gameObject.SetActive(timeFormat == STANDARD_TIME);
            timeFormatButton.onClick.AddListener(delegate {
                var dateTime = localDate;
                localDate = dateTime.AddHours(12);
                UpdateDate(localDate, timeFormat);
            });
            
            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
            
            shroudButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
            
            deleteButton.onClick.AddListener(delegate {
                OnDeleteButtonClick?.Invoke();
            });
            
            confirmButton.onClick.AddListener(delegate {
                OnConfirmButtonClick?.Invoke(localDate);
            });
        }

        private void UpdateDate(DateTime localDate, string timeFormat) {
            timeFormatField.text = localDate.ToString("tt", CultureInfo.CreateSpecificCulture("en-US"));
            timerInputHoursField.text = timeFormat == MILITARY_TIME ? $"{localDate.Hour:D2}" : $"{localDate:hh}";
            timerInputMinutesField.text = $"{localDate.Minute:D2}";
        }
        
        private void UpdateTaskType(string taskType) {
            var taskIconSprite = CoreMobile.Instance.SpritesIconsManager.GetSpriteIconByKey(TASKS_ICONS, taskType);
            taskIcon.sprite = taskIconSprite;
        }
    }
}