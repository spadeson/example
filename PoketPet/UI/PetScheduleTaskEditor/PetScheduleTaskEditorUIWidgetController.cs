using System;
using System.Globalization;
using PoketPet.Global;
using PoketPet.Schedule;
using PoketPet.UI.Progress;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetScheduleTaskEditor {
    public class PetScheduleTaskEditorUIWidgetController : WidgetControllerWithData<PetScheduleTaskEditorUIWidget, PetScheduleTaskEditorUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClickHandler;
            Widget.OnDeleteButtonClick += WidgetOnDeleteButtonClickHandler;
            Widget.OnConfirmButtonClick += WidgetOnConfirmButtonClickHandler;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClickHandler;
            Widget.OnDeleteButtonClick -= WidgetOnDeleteButtonClickHandler;
            Widget.OnConfirmButtonClick -= WidgetOnConfirmButtonClickHandler;
        }

        private void WidgetOnDeleteButtonClickHandler() {
            WidgetData.TaskSchedule.Remove(WidgetData.TaskTime);
            ScheduleManager.ChangeScheduleTask( WidgetData.TaskId, WidgetData.TaskSchedule);
            
            var progressUiWidgetData = new ProgressUIWidgetData {title = "Update Tasks", description = ""};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PROGRESS_UI, progressUiWidgetData);
            Widget.Dismiss();
        }

        private void WidgetOnConfirmButtonClickHandler(DateTime newTime) {
            WidgetData.TaskSchedule.Remove(WidgetData.TaskTime);
            
            var utcDate = Core.Utilities.TimeUtil.ConvertLocalToUtc(newTime);
            
            WidgetData.TaskSchedule.Add(utcDate.Hour * 60 + utcDate.Minute);
            WidgetData.TaskSchedule.Sort();
            ScheduleManager.ChangeScheduleTask(WidgetData.TaskId, WidgetData.TaskSchedule);
            
            var progressUiWidgetData = new ProgressUIWidgetData {title = "Update Tasks", description = ""};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PROGRESS_UI, progressUiWidgetData);
            Widget.Dismiss();
        }

        private void WidgetOnCloseButtonClickHandler() {
            Widget.Dismiss();
        }
    }
}