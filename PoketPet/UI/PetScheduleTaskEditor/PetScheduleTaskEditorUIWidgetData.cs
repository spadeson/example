using System;
using System.Collections.Generic;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetScheduleTaskEditor
{
	[Serializable]
	public class PetScheduleTaskEditorUIWidgetData : WidgetData {
		public string TaskId { get; set; }
		public int TaskTime { get; set; }
		public string TaskType { get; set; }
		public List<int> TaskSchedule { get; set; }
	}
}