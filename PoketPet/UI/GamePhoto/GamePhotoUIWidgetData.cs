using System;
using System.Collections.Generic;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.GamePhoto
{
	[Serializable]
	public class GamePhotoUIWidgetData : WidgetData {
		public string from;
		public string messageId;
		public Sprite photo;
		public int level;
	}
}