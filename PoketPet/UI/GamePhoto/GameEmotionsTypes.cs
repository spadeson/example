namespace PoketPet.UI.GamePhoto {
    public enum GameEmotionsTypes : byte {
        SAD = 0,
        NEUTRAL = 1,
        ANGRY = 2,
        HAPPY = 3
    }
}