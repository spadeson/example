using PoketPet.UI.GamePhoto.Config;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace PoketPet.UI.GamePhoto.Editor {
    [CustomEditor(typeof(GameEmotionsLevel))]
    public class GameEmotionsLevelEditor : UnityEditor.Editor {
        
        private ReorderableList _list;
        
        private GameEmotionsLevel GameEmotionsLevel
        {
            get
            {
                return target as GameEmotionsLevel;
            }
        }
        
        private void OnEnable()
        {
            _list = new ReorderableList(GameEmotionsLevel.emotionConfigs, typeof(GameEmotionData), true, true, true, true);

            _list.elementHeight = EditorGUIUtility.singleLineHeight * 3f;

            _list.onAddCallback += AddElementCallback;
            _list.onRemoveCallback += RemoveElementCallback;
        
            _list.drawHeaderCallback += DrawHeader;
            _list.drawElementCallback += DrawElement;
            _list.onChangedCallback += OnChangedCallback;
        }
        
        private void OnDisable()
        {
            _list.drawElementCallback -= DrawElement;
        }
        
        private void OnChangedCallback(ReorderableList list) {
            EditorUtility.SetDirty(target);
        }

        private void DrawHeader(Rect rect) {
            EditorGUI.LabelField (rect, "Game Emotions List:", EditorStyles.boldLabel);
        }
    
        private void AddElementCallback(ReorderableList reorderableList) {
            GameEmotionsLevel.emotionConfigs.Add(new GameEmotionData());
            EditorUtility.SetDirty(target);
        }

        private void RemoveElementCallback(ReorderableList reorderableList) {
            GameEmotionsLevel.emotionConfigs.RemoveAt(reorderableList.index);
            EditorUtility.SetDirty(target);
        }

        private void DrawElement(Rect rect, int index, bool active, bool focused)
        {
            var item = GameEmotionsLevel.emotionConfigs[index];

            EditorGUI.BeginChangeCheck();
            item.emotionsType = (GameEmotionsTypes)EditorGUI.EnumPopup(new Rect(rect.x, rect.y, rect.width * 0.3f, rect.height), item.emotionsType);
            item.emotionIcon = (Sprite)EditorGUI.ObjectField(new Rect(rect.x + rect.width * 0.3f + 8, rect.y, 48, 48), item.emotionIcon, typeof(Sprite), false);

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(target);
            }

        }
        
        public override void OnInspectorGUI() {            
            base.OnInspectorGUI();

            // Actually draw the list in the inspector
            _list.DoLayoutList();

            EditorGUILayout.Space();

            if (GUILayout.Button("Save Config", GUILayout.ExpandWidth(true), GUILayout.Height(32)))
            {
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}