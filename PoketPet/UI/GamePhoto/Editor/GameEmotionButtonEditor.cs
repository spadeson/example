using UnityEditor;
using UnityEngine;

namespace PoketPet.UI.GamePhoto.Editor {
    [CustomEditor(typeof(GameEmotionButton))]
    public class GameEmotionButtonEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var gameEmotionButton = (GameEmotionButton)target;

            if (GUILayout.Button("Select", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                gameEmotionButton.SelectElement();
            }
        }
    }
}
