using System;
using System.Collections.Generic;
using PoketPet.UI.GamePhoto.Config;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.GamePhoto {
    public class GamePhotoUIWidget : BasicPopupUIWidget {
        public event Action OnCloseButtonClick;
        public event Action OnChooseButtonClick;
        public event Action<GameEmotionsTypes> OnEmotionButtonClick;

        [SerializeField] private RectTransform uiElementsContainer;
        [SerializeField] private GameObject emotionElementPrefab;

        [Header("Photo:"), SerializeField] private Image photo;

        private GameEmotionsLibrary _emotionsLibrary;
        
        private readonly List<GameEmotionButton> _emotionButtonsList = new List<GameEmotionButton>();

        public override void Create() {
            base.Create();

            closeButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });

            actionButton.onClick.AddListener(delegate { OnChooseButtonClick?.Invoke(); });
        }

        public void ActivateChooseButton() {
            actionButton.GetComponent<CanvasGroup>().interactable = true;
        }

        public void Initialize(GamePhotoUIWidgetData widgetData, GameEmotionsLibrary emotionsLibrary) {
            _emotionsLibrary = emotionsLibrary;

            photo.sprite = widgetData.photo;

            _emotionButtonsList.Clear();
            for (var i = 0; i < uiElementsContainer.childCount; i++) {
                var emotionButton = uiElementsContainer.GetChild(i).GetComponent<GameEmotionButton>();
                emotionButton.Dismiss();
                Destroy(emotionButton.gameObject);
            }

            for (var index = 0; index < _emotionsLibrary.gameEmotionsLevels[widgetData.level].emotionConfigs.Count; index++) {
                var data                  = _emotionsLibrary.gameEmotionsLevels[widgetData.level].emotionConfigs[index];
                var newEmotionUiElementGo = Instantiate(emotionElementPrefab, uiElementsContainer, false);
                var newEmotionUiElement   = newEmotionUiElementGo.GetComponent<GameEmotionButton>();
                newEmotionUiElement.Initialize(data, index);
                newEmotionUiElement.DeselectElement();
                newEmotionUiElement.OnButtonClick += (emotionType, idx)  => {
                    DeselectAllButtons();
                    _emotionButtonsList[idx].SelectElement();
                    OnEmotionButtonClick?.Invoke(emotionType);
                };
                _emotionButtonsList.Add(newEmotionUiElement);
            }
        }

        private void DeselectAllButtons() {
            foreach (var gameEmotionButton in _emotionButtonsList) {
                gameEmotionButton.DeselectElement();
            }
        }
    }
}