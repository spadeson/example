using System;
using UnityEngine;

namespace PoketPet.UI.GamePhoto {
    [Serializable]
    public class GameEmotionData {
        public GameEmotionsTypes emotionsType;
        public Sprite emotionIcon;
    }
}