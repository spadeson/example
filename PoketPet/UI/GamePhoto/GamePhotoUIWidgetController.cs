using PoketPet.Games;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Game;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.UI.GamePhoto.Config;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.GamePhoto
{
	public class GamePhotoUIWidgetController : WidgetControllerWithData<GamePhotoUIWidget, GamePhotoUIWidgetData>	{

		[SerializeField] private GameEmotionsLibrary emotionsLibrary;

		private GameEmotionsTypes _chosenEmotion;

		#region WidgetController

		protected override void OnWidgetCreated(Widget widget) {
			Widget.OnCloseButtonClick += OnCloseButtonClickHandler;
			Widget.OnChooseButtonClick += OnChooseButtonClickHandler;
			Widget.OnEmotionButtonClick += OnEmotionButtonClickHandler;
			
			Widget.Initialize(WidgetData, emotionsLibrary);
		}
		
		protected override void OnWidgetActivated(Widget widget) { }
		
		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) {
			Widget.OnCloseButtonClick -= OnCloseButtonClickHandler;
			Widget.OnChooseButtonClick -= OnChooseButtonClickHandler;
			Widget.OnEmotionButtonClick -= OnEmotionButtonClickHandler;
		}

		#endregion

		#region Private Methods

		private void OnEmotionButtonClickHandler(GameEmotionsTypes emotion) {
			_chosenEmotion = emotion;
			Widget.ActivateChooseButton();
		}

		private void OnChooseButtonClickHandler() {
			if (WidgetData.from == UserProfileManager.CurrentUser.id) {
				CreateGame();
			} else {
				CompleteGame();
			}
		}

		private void OnCloseButtonClickHandler() {
			Widget.Dismiss();
		}

		private void CreateGame() {
			var requestData = new CreateGameRequestData {
				from = UserProfileManager.CurrentUser.id,
				petId = PetsProfileManager.CurrentPet.id,
				type = GameManager.CurrentGame.type,
				value = (int)_chosenEmotion,
				image = WidgetData.photo.texture.EncodeToPNG(),
				action = "create_game"
			};
					
			GameManager.CreateGame(requestData.GetForm());
			Widget.Dismiss();
		}

		private void CompleteGame() {
			var userId = UserProfileManager.CurrentUser.id;
			var data   = new CompleteGameRequestData {msgID = WidgetData.messageId, value = (int)_chosenEmotion};

			var requestData = new RequestData<CompleteGameRequestData>(userId, data, "close_msg") {from = userId};
					
			GameManager.CompleteGame(requestData);
			Widget.Dismiss();
		}

		#endregion
	}
}