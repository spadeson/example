using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.UI.GamePhoto.Config {
    [CreateAssetMenu(fileName = "GameEmotionsLevel", menuName = "PoketPet/Game/Emotions/Game Emotions Level", order = 0)]
    public class GameEmotionsLevel : ScriptableObject {
        
        [HideInInspector] public List<GameEmotionData> emotionConfigs = new List<GameEmotionData>();
    }
}