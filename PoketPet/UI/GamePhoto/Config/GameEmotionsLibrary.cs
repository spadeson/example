using System.Collections.Generic;
using UnityEngine;

namespace PoketPet.UI.GamePhoto.Config {
    
    [CreateAssetMenu(fileName = "GameEmotionsLibrary", menuName = "PoketPet/Game/Emotions/Game Emotions Library", order = 0)]
    public class GameEmotionsLibrary : ScriptableObject
    {
        public List<GameEmotionsLevel> gameEmotionsLevels;
    }
}