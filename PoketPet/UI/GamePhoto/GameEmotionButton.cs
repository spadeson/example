﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PoketPet.UI.GamePhoto {
    public class GameEmotionButton : Button {
        
        private GameEmotionData _gameEmotionData;
        public event Action<GameEmotionsTypes, int> OnButtonClick;

        [Header("Sprites")]
        [SerializeField] private Sprite normalBackground;
        [SerializeField] private Sprite activeBackground;

        [Header("Emotion Image")]
        [SerializeField] private Image emotionIcon;
        
        [Header("Background Image")]
        [SerializeField] private Image backgroundImage;

        private RectTransform _rectTransform;

        private int _index;

        public void Initialize(GameEmotionData data, int index) {

            _index = index;

            _rectTransform = GetComponent<RectTransform>();
            
            _gameEmotionData = data;

            emotionIcon.sprite = _gameEmotionData.emotionIcon;

            onClick.AddListener(OnButtonClickHandler);
        }

        public void SelectElement() {
            backgroundImage.sprite = activeBackground;
            emotionIcon.rectTransform.DOShakeScale(0.2f, 0.35f, 7, 0.15f);
            _rectTransform.DOScale(1f, 0.25f).SetEase(Ease.OutSine);
        }
        
        public void DeselectElement() {
            backgroundImage.sprite = normalBackground;
            _rectTransform.DOScale(0.75f, 0.25f).SetEase(Ease.OutSine);
        }

        public void Dismiss() {
            onClick.RemoveAllListeners();
        }

        private void OnButtonClickHandler() {
            OnButtonClick?.Invoke(_gameEmotionData.emotionsType, _index);
        }
    }
}