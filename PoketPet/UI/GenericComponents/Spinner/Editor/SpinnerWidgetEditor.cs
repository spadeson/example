using UnityEditor;
using UnityEngine;

namespace PoketPet.UI.GenericComponents.Spinner.Editor {
    [CustomEditor(typeof(SpinnerWidget))]
    public class SpinnerWidgetEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var spinner = (SpinnerWidget)target; 

            if (GUILayout.Button("Initialize", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                spinner.Initialize();
            }
            
            if (GUILayout.Button("Start Animation", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                spinner.StartAnimation();
            }
            
            if (GUILayout.Button("Stop Animation", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                spinner.StopAnimation();
            }
        }
    }
}
