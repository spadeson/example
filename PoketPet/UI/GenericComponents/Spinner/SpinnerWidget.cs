﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.GenericComponents.Spinner {
    public class SpinnerWidget : MonoBehaviour {
    
        [Header("Pad")]
        [SerializeField] private Image padImage;
    
        [Header("Fingers")]
        [SerializeField] private Image[] fingersImages;

        private const float DEFAULT_OPACITY = 0.6f;
        private const float OPACITY_STEP = 0.15f;
    
        private const float ANIMATION_SPEED = 0.1f;

        private Coroutine _animationCoroutine;

        private int _currentFingerIndex;

        private Dictionary<int, float> _opacityById;
        
        private int[] _fingerIds; 

        public void Initialize() {
            padImage.color = new Color(padImage.color.r, padImage.color.g, padImage.color.b, DEFAULT_OPACITY);
            
            _fingerIds = new[] { 0, 1, 2, 3};

            _opacityById = new Dictionary<int, float> {{0, 0.15f}, {1, 0.3f}, {2, 0.45f}, {3, 0.6f}};

            ApplyOpacityByIndex();
        }

        public void StartAnimation() {
            if (_animationCoroutine != null) {
                StopCoroutine(_animationCoroutine);
            }

            _currentFingerIndex = 0;

            _animationCoroutine = StartCoroutine(Animation_Coroutine());
        }
    
        public void StopAnimation() {
            if (_animationCoroutine != null) {
                StopCoroutine(_animationCoroutine);
            }
        }

        private IEnumerator Animation_Coroutine() {

            while (true) {
                yield return new WaitForSeconds(ANIMATION_SPEED);

                if ( _fingerIds.Length > 1 )
                {
                    var tmp = _fingerIds[_fingerIds.Length - 1];

                    for ( var  i = _fingerIds.Length - 1; i != 0; --i ) _fingerIds[i] = _fingerIds[i -1];

                    _fingerIds[0] = tmp;
                }

                ApplyOpacityByIndex();
            }
        }

        private void ApplyOpacityByIndex() {
            for (var i = 0; i < _fingerIds.Length; i++) {
                var fingerColor = fingersImages[i].color;
                var opacity     = _opacityById[_fingerIds[i]];
                fingersImages[i].color = new Color(fingerColor.r, fingerColor.g, fingerColor.b, opacity);
            }
        }
    }
}