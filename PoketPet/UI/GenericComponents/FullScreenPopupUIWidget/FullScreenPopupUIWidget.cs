using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.GenericComponents.FullScreenPopupUIWidget {
    public class FullScreenPopupUIWidget : BaseUIWidget {
        
        [Header("Container")]
        [SerializeField] private RectTransform container;
        
        [Header("Action Button Label")]
        [SerializeField] private TextMeshProUGUI actionButtonLabel;
        
        [Header("Action Button")]
        [SerializeField] protected Button actionButton;

        [Header("Close Button")]
        [SerializeField] protected Button closeButton;
        
        #region MonoBehaviour

        private void OnEnable() {
            SetInitialStates();
        }

        #endregion
        
        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            if (animated) {
                ShowAnimatedWidget();
            } else {
                container.DOScale(1f, 0f);
                base.Activate(false);
            }
        }

        public override void Deactivate(bool animated) {
            if (animated) {
                HideAnimatedWidget();
            } else {
                base.Deactivate(false);
                container.DOScale(0, 0f);
            }
        }

        public override void Dismiss() {
            HideAndDismissAnimatedWidget();
        }
        
        #endregion
        
        #region Private Methods

        private void SetInitialStates() {
            container.DOScale(0, 0f);
        }

        private void ShowAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(1f, 0.25f).From(0).SetEase(Ease.OutSine));
            sequence.OnComplete(() => {
                base.Activate(false);
            });
        }

        private void HideAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(0, 0.2f).SetEase(Ease.InSine));
            sequence.OnComplete(() => {
                base.Deactivate(false);
            });
        }
        
        private void HideAndDismissAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(0, 0.2f).SetEase(Ease.InSine));
            sequence.OnComplete(() => {
                base.Dismiss();
            });
        }

        #endregion
    }
}
