using TMPro;
using TMPro.EditorUtilities;
using UnityEditor;

namespace PoketPet.UI.GenericComponents.InputFields.Editor {
    [CustomEditor(typeof(InputFiledValidatable))]
    public class InputFiledValidatableEditor : TMP_InputFieldEditor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var inputFiledValidatable = (InputFiledValidatable)target;
            inputFiledValidatable.statusTextLabel = EditorGUILayout.ObjectField("Status Text:", inputFiledValidatable.statusTextLabel, typeof(TextMeshProUGUI), true) as TextMeshProUGUI;
        }
    }
}
