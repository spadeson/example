using System;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.GenericComponents.InputFields {

    public class InputFiledValidatable : TMP_InputField {
        
        public bool IsValid;

        public TextMeshProUGUI statusTextLabel;
        
        [Header("Colors:")]
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color errorColor = Color.red;

        public void Initialize() {

            if (statusTextLabel == null) {
                statusTextLabel = transform.parent.GetChild(1).GetComponent<TextMeshProUGUI>();
            }

            statusTextLabel.text = string.Empty;
        }
    }
}
