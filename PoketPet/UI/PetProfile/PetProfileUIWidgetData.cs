using System;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetProfile {
    [Serializable]
    public class PetProfileUIWidgetData : WidgetData {
        public PetResponseData PetsDataModel { get; set; }
        public string WeightUnits { get; set; }
    }
}
