using System;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Data;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Photo;
using PoketPet.UI.DataPicker;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetProfile {
    public class PetProfileUIWidgetController : WidgetControllerWithData<PetProfileUIWidget, PetProfileUIWidgetData> {
        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButton += WidgetOnCloseButtonHandler;
            Widget.OnSaveButton += WidgetOnSaveButtonHandler;
            Widget.OnBirthdayButtonClick += WidgetOnBirthdayButtonClickHandler;
            Widget.OnPictureClick += WidgetOnPictureClickHandler;

            PetsProfileManager.OnPetUpdated += OnOnPetModelUpdatedHandler;
            PetsProfileManager.OnPetAvatarUpdated += AddAvatar;

            Widget.Initialize(WidgetData);
            Widget.SetBirthdayDate(PetsProfileManager.GetPetBirthDay());
            AddAvatar(PetsProfileManager.CurrentPet.avatar);
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        private void OnOnPetModelUpdatedHandler(PetResponseData petDataModel) {
            Widget.UpdateWidgetFields(petDataModel);
            Widget.SetBirthdayDate(PetsProfileManager.GetPetBirthDay());
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButton -= WidgetOnCloseButtonHandler;
            Widget.OnSaveButton -= WidgetOnSaveButtonHandler;
            Widget.OnBirthdayButtonClick -= WidgetOnBirthdayButtonClickHandler;
            Widget.OnPictureClick -= WidgetOnPictureClickHandler;

            PetsProfileManager.OnPetUpdated -= OnOnPetModelUpdatedHandler;
            PetsProfileManager.OnPetAvatarUpdated -= AddAvatar;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        #endregion

        #region ButtonHandlers

        private void WidgetOnPictureClickHandler() {
            #if !UNITY_EDITOR && UNITY_IOS
            PhotoManager.TakePhoto((sprite) => {
                SaveImageByRest(sprite.texture);
            });
            #else
            //Make request for texture download
            CoreMobile.Instance.NetworkManager.ImagesDownloader.DownloaderTexture("https://i.pravatar.cc/240", SaveImageByRest);

            #endif
        }

        private void SaveImageByRest(Texture2D texture) {
            var requestData = new SetPetAvatarData() {from = UserProfileManager.CurrentUser.id, image = texture.EncodeToPNG(), action = "set_avatar", petId = PetsProfileManager.CurrentPet.id};

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("petsForm", requestData.GetForm());
            operation.OnDataReceived += CheckStatusHandler;
        }

        private void AddAvatar(string avatar) {
            PhotoManager.GetAvatarById(avatar, sprite => {
                if (Widget.enabled) {
                    Widget.SetAvatarImage(sprite);
                }
            });
        }

        private void WidgetOnCloseButtonHandler() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.Dismiss();
        }

        private void WidgetOnSaveButtonHandler(PetProfileUIWidgetData widgetData) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            PetsProfileManager.UpdatePet(widgetData.PetsDataModel);
        }

        #endregion

        #region Birthday

        private void WidgetOnBirthdayButtonClickHandler() {
            var dataPickerUiWidgetData = new DataPickerUIWidgetData(PetsProfileManager.GetPetBirthDay());
            dataPickerUiWidgetData.OnPickedData += OnPickedDataHandler;
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.DATE_PICKER_UI, dataPickerUiWidgetData);
        }

        private void OnPickedDataHandler(DateTime dateTime) {
            Debug.Log($"Obtained Birthday Data: {dateTime.Month}, {dateTime.Day}, {dateTime.Year}");
            PetsProfileManager.SetPetsBirthDate(dateTime);
            Widget.SetBirthdayDate(PetsProfileManager.GetPetBirthDay());
        }

        #endregion

        private static void CheckStatusHandler(string data) {
            Debug.Log($"Status JSON Response: {data}");
            var response = JsonUtility.FromJson<ResponseData<string>>(data);
            Debug.Log($"Status Response: {response.status}");

            if (response.status != "error") {
            } else {
                // TODO: Release Error Status 
            }
        }
    }
}