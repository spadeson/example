using DG.Tweening;
using System;
using System.Globalization;
using Core.Utilities;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Data;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PoketPet.UI.PetProfile {
    public class PetProfileUIWidget : BasicPopupUIWidget {
        public event Action OnCloseButton;
        public event Action<PetProfileUIWidgetData> OnSaveButton;
        public event Action OnPictureClick;
        public event Action OnBirthdayButtonClick;

        [Header("Avatar")]
        [SerializeField] private Image petAvatarImage;

        [SerializeField] private Button petAvatarButton;

        [Header("Form Fields")]
        [SerializeField] private TMP_InputField nicknameInputField;

        [SerializeField] private Button birthdateButton;
        [SerializeField] private TextMeshProUGUI birthdateInputField;
        [SerializeField] private TMP_InputField breedInputField;
        [SerializeField] private TMP_InputField genderInputField;
        [SerializeField] private TMP_InputField weightInputField;

        private PetProfileUIWidgetData _petProfileUiWidgetData;

        #region BaseUIWidget

        public override void Create() {
            base.Create();
            closeButton.onClick.AddListener(CloseButtonClickHandler);
            actionButton.onClick.AddListener(OnSaveProfileButtonClickHandler);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        #endregion

        #region Public Methods

        public void Initialize(PetProfileUIWidgetData widgetData) {
            _petProfileUiWidgetData = widgetData;

            petAvatarButton.onClick.AddListener(PetAvatarButtonClickHandler);

            nicknameInputField.text = _petProfileUiWidgetData.PetsDataModel.nickname;
            birthdateInputField.text = new DateTime(_petProfileUiWidgetData.PetsDataModel.birth_day).ToString("d");
            breedInputField.text = _petProfileUiWidgetData.PetsDataModel.breed;
            genderInputField.text = _petProfileUiWidgetData.PetsDataModel.gender;
            DisplayPetWeight(_petProfileUiWidgetData.PetsDataModel.weight, _petProfileUiWidgetData.WeightUnits);

            nicknameInputField.onEndEdit.AddListener(delegate(string petName) { _petProfileUiWidgetData.PetsDataModel.nickname = petName; });
            breedInputField.readOnly = true;
            //breedInputField.onEndEdit.AddListener(delegate(string breed) { _petProfileUiWidgetData.PetsDataModel.breed = breed; });
            genderInputField.onEndEdit.AddListener(delegate(string gender) { _petProfileUiWidgetData.PetsDataModel.gender = gender; });
            weightInputField.onEndEdit.AddListener(delegate(string weight) { float.TryParse(weight, out _petProfileUiWidgetData.PetsDataModel.weight); });

            birthdateButton.onClick.AddListener(delegate { OnBirthdayButtonClick?.Invoke(); });
        }

        public void SetBirthdayDate(DateTime dateTime) {
            birthdateInputField.text = dateTime.ToString("MM / dd / yyyy");
        }

        public void SetAvatarImage(Sprite sprite) {
            petAvatarImage.sprite = sprite;
        }

        public void UpdateWidgetFields(PetResponseData petDataModel) {
            _petProfileUiWidgetData.PetsDataModel = petDataModel;

            nicknameInputField.SetTextWithoutNotify(_petProfileUiWidgetData.PetsDataModel.nickname);
            birthdateInputField.text = new DateTime(_petProfileUiWidgetData.PetsDataModel.birth_day).ToString("MM / dd / yyyy");
            breedInputField.text = _petProfileUiWidgetData.PetsDataModel.breed;
            genderInputField.text = _petProfileUiWidgetData.PetsDataModel.gender;
            DisplayPetWeight(_petProfileUiWidgetData.PetsDataModel.weight, _petProfileUiWidgetData.WeightUnits);
        }

        #endregion


        #region Private Methods

        private void PetAvatarButtonClickHandler() {
            OnPictureClick?.Invoke();
        }

        private void CloseButtonClickHandler() {
            closeButton.onClick.RemoveAllListeners();
            actionButton.onClick.RemoveAllListeners();

            closeButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnCloseButton?.Invoke();
                EventSystem.current.SetSelectedGameObject(null);
            });
        }

        private void OnSaveProfileButtonClickHandler() {
            OnSaveButton?.Invoke(_petProfileUiWidgetData);
            EventSystem.current.SetSelectedGameObject(null);
        }

        private void DisplayPetWeight(float petWeight, string weightUnits) {
            weightInputField.text = weightUnits == GlobalVariables.LocalDataKeys.KILOGRAM_WEIGHT_UNITS ? petWeight.ToString(CultureInfo.InvariantCulture) : UnitsUtilities.KilogramToPounds(petWeight).ToString(CultureInfo.InvariantCulture);
            weightInputField.text += $" {weightUnits}";
        }

        #endregion
    }
}