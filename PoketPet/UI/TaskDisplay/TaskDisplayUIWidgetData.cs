using System;
using System.Collections.Generic;
using PoketPet.Network.API.RESTModels.Tasks;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskDisplay {
    [Serializable]
    public class TaskDisplayUIWidgetData : WidgetData {
        public List<TaskResponseData> taskDataModels;
    }
}