using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.TaskDisplay {
    public class PhotosAssigneeContainer : MonoBehaviour, IAssigneeAvatarContainer {
        
        [Header("Image")]
        [SerializeField] private Image avatarImage;
        
        public Image GetAvatarImage() {
            return avatarImage;
        }

        public void Minimize() {
            GetComponent<RectTransform>().localScale = new Vector2(0.64f, 0.64f);
        }

        public void SetEnabled(bool val) {
            gameObject.SetActive(val);
        }

        public void Dismiss() {
            Destroy(gameObject);
        }
    }
}
