using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.TaskDisplay {
    public class IconsAssigneeContainer : MonoBehaviour, IAssigneeAvatarContainer {
        
        [Header("Image")]
        [SerializeField] private Image avatarImage;

        public Image GetAvatarImage() {
            return avatarImage;
        }

        public void Minimize() { }
        public void SetEnabled(bool val) {
            gameObject.SetActive(val);
        }
        
        public void Dismiss() {
            Destroy(gameObject);
        }
    }
}
