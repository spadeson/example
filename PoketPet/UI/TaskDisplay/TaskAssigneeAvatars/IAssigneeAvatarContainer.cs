using UnityEngine.UI;

namespace PoketPet.UI.TaskDisplay {
    public interface IAssigneeAvatarContainer {
        Image GetAvatarImage();
        void Minimize();
        void SetEnabled(bool val);
        void Dismiss();
    }
}
