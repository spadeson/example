using System;
using System.Collections.Generic;
using System.Linq;
using Core.TimersManager.Data;
using DG.Tweening;
using PoketPet.Network.API.RESTModels.Tasks;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskDisplay {
    public class TaskDisplayUIWidget : BaseUIWidget {

        [Header("Columns Container"), SerializeField]
        private TaskUIContainer containerColumns;

        [Header("Active Task Container"), SerializeField]
        private RectTransform activeTaskContainer;

        [Header("Shroud Image"), SerializeField]
        private CanvasGroup shroudImage;

        [Header("Task UI Element Prefab")]
        [SerializeField] private GameObject taskUIElementPrefab;
        
        private const float TASKS_ELEMENTS_MARGIN = 1.1f;

        private readonly Dictionary<string, TaskUIElement> _tasksDictionary = new Dictionary<string, TaskUIElement>();

        public event Action<TaskUIElement> OnElementDown;
        public event Action<TaskUIElement> OnElementUp;

        private TaskDisplayUIWidgetData _widgetData;
        
        private Tween _tasksUpdateCallback;
        
        private Vector2 _taskProgressRange;

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        #endregion

        #region Public Methods

        public void Initialize(TaskDisplayUIWidgetData widgetData) {

            _widgetData = widgetData;
            _taskProgressRange = new Vector2(containerColumns.GetColumnHeight(), -containerColumns.GetColumnHeight());
            
            Debug.Log($"Column Height: {_taskProgressRange}");

            //if (_widgetData.taskDataModels != null) {
            //    CreateTasksUiElements(_widgetData.taskDataModels);
            //}

            shroudImage.alpha = 0f;
            shroudImage.blocksRaycasts = false;
        }
        public void OnApplicationFocus(bool hasFocus)
        {
            Debug.Log($"OnApplicationFocus: {hasFocus} {DateTime.Now}");
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            Debug.Log($"OnApplicationPause: {pauseStatus} {DateTime.Now}");
        }
        public void UpdateTasksUiElements(List<TaskResponseData> tasksDataModelsList) {
            
            _widgetData.taskDataModels = tasksDataModelsList;

            for (var i = _tasksDictionary.Keys.Count - 1; i >= 0; i--) {
                var taskId = _tasksDictionary.Keys.ToList()[i];
                if (tasksDataModelsList.Exists(item => item.id == taskId)) continue;
                _tasksDictionary[taskId].Dismiss();
                _tasksDictionary.Remove(taskId);
            }
            
            for (var i = 0; i < tasksDataModelsList.Count; i++) {
                var timerDate      = DateTime.Now.AddSeconds(20 + 20 * i);
                var timerDataModel = CoreMobile.Instance.TimersManager.CreateTimer(timerDate.Ticks);
                timerDataModel.ResumeTimer();
                UpdateTaskUiElement(tasksDataModelsList[i], timerDataModel, i);
            }
        }
        
        public void RemoveTaskUiElements(TaskResponseData task) {
            shroudImage.DOFade(0.0f, 0.2f);
            shroudImage.blocksRaycasts = false;
            
            if(!_tasksDictionary.ContainsKey(task.id)) return;
            
            _tasksDictionary[task.id].Dismiss();
            _tasksDictionary.Remove(task.id);
        }
        
        #endregion

        #region Private Methods

        private void UpdateTaskUiElement(TaskResponseData taskDataModel, TimerDataModel timerDataModel, int taskElementIndex) {

            TaskUIElement newTaskUiElement;

            if (_tasksDictionary.ContainsKey(taskDataModel.id)) {
                newTaskUiElement = _tasksDictionary[taskDataModel.id];
            } else {
                var newTaskElementGameObject = Instantiate(taskUIElementPrefab, containerColumns.GetRectTransform(), false);
                newTaskUiElement = newTaskElementGameObject.GetComponent<TaskUIElement>();
                newTaskUiElement.Initialize(timerDataModel);
                newTaskUiElement.OnButtonDown += OnElementDownHandler;
                newTaskUiElement.OnButtonUp += OnElementUpHandler;
                _tasksDictionary.Add(taskDataModel.id, newTaskUiElement);
            }
            
            newTaskUiElement.UpdateView(taskDataModel);
            newTaskUiElement.UpdateAssigneeView(taskDataModel);

            var newTaskUiElementWidth = newTaskUiElement.GetRectTransform().rect.width;

            var startXPos = (taskElementIndex * newTaskUiElementWidth * TASKS_ELEMENTS_MARGIN) - newTaskUiElementWidth * TASKS_ELEMENTS_MARGIN;
            newTaskUiElement.MinContainerPosition = containerColumns.GetColumnHeight();
            newTaskUiElement.MaxContainerPosition = 0 - containerColumns.GetColumnHeight();
            newTaskUiElement.TaskColumnX = startXPos;
            
            newTaskUiElement.UpdateTaskProgressFill(TaskTimeConverterUtil.GetTaskProgress(taskDataModel));
            newTaskUiElement.SetTaskIcon(taskDataModel.type);

            newTaskUiElement.UpdateTimerValue(timerDataModel.secondsToDueDate.ToString());
            MoveAllTasksOnTimer();
        }

        private void MoveAllTasksOnTimer() {
            
            _tasksUpdateCallback.Kill();
            
            foreach (var t in _tasksDictionary.Values.Where(t => !t.dragged)) {
                MoveTaskAtTimerValue(t);
            }

            _tasksUpdateCallback = DOVirtual.DelayedCall(1f, MoveAllTasksOnTimer);
        }


        private void MoveTaskAtTimerValue(TaskUIElement task) {
            
            var progress = TaskTimeConverterUtil.GetTaskProgress(task.GetTaskDataModel());
            
            task.SetPosition(progress);
            task.UpdateTaskProgressFill(progress);

            task.UpdateTimerValue("");
        }

        private void OnElementDownHandler(TaskUIElement task) {
            task.transform.SetParent(activeTaskContainer, false);
            task.transform.DOKill();
            shroudImage.DOFade(0.5f, 0.2f);
            shroudImage.blocksRaycasts = true;
            OnElementDown?.Invoke(task);
        }

        private void OnElementUpHandler(TaskUIElement task) {
            Debug.Log($"task.GetRectTransform().anchoredPosition = new Vector2(0.5f, 1f); = {task.GetRectTransform().anchoredPosition}");
            task.transform.SetParent(containerColumns.transform, false);
            task.transform.DOKill();
            shroudImage.DOFade(0.0f, 0.2f);
            shroudImage.blocksRaycasts = false;
            OnElementUp?.Invoke(task);
        }

        #endregion


    }
}