using System;
using PoketPet.Photo;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SpriteIconsGroupKeys;
using static PoketPet.Global.GlobalVariables.AssigneeIconsKeys;
using static PoketPet.Global.GlobalVariables.UsersFamilyRoleKeys;
using Core.SpriteIcons.Manager;

namespace PoketPet.UI.TaskDisplay
{
    public class TaskUIElementAssignee : MonoBehaviour
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private CanvasGroup hintCanvasGroup;
        [SerializeField] private TextMeshProUGUI hintLabel;
        [SerializeField] private Image avatarImage;

        [SerializeField] private RectTransform parent;
        
        [SerializeField] private GameObject iconsContainerPrefab;
        [SerializeField] private GameObject avatarContainerPrefab;
        
        private IAssigneeAvatarContainer _iconsContainer;
        private IAssigneeAvatarContainer _avatarContainer;

        private static SpriteIconsManager IconManager => CoreMobile.Instance.SpritesIconsManager;

        private TaskUIAssigneeData _data;
        //public Image icon;
        public void Init(TaskUIAssigneeData data) {
            _data = data;
            
            hintCanvasGroup.alpha = 0;
            hintLabel.text = _data.Label;
        }
        
        public void AddAvatar()
        {
            _iconsContainer?.Dismiss();
            _avatarContainer?.Dismiss();
            _iconsContainer = Instantiate(iconsContainerPrefab, parent).GetComponent<IconsAssigneeContainer>();
            avatarImage = _iconsContainer.GetAvatarImage();
            
            Debug.Log("Assignee Avatar: " + _data.Avatar);
            
            if (_data.Avatar == DONE || _data.Avatar == ADVISOR || _data.Avatar == UNKNOWN) {
                avatarImage.sprite = IconManager.GetSpriteIconByKey(ASSIGNEE_ICONS, _data.Avatar);
            }
            else {
                Debug.Log($"Assignee Family Role: {_data.FamiliesRole} {_data.Label}");
                avatarImage.sprite = IconManager.GetSpriteIconByKey(ASSIGNEE_ICONS, _data.FamiliesRole);
                PhotoManager.GetAvatarById(_data.Avatar, sprite => {
                    _avatarContainer = Instantiate(avatarContainerPrefab, parent).GetComponent<PhotosAssigneeContainer>();
                    avatarImage = _avatarContainer.GetAvatarImage();
                    avatarImage.sprite = sprite;
                    _iconsContainer.SetEnabled(false);
                });
            }
        }

        public void SetAssigneeIcon(string iconName) {
            if (_iconsContainer == null) {
                _iconsContainer = Instantiate(iconsContainerPrefab, parent).GetComponent<IconsAssigneeContainer>();
                avatarImage = _iconsContainer.GetAvatarImage();
            }

            avatarImage.sprite = IconManager.GetSpriteIconByKey(ASSIGNEE_ICONS, iconName);
        }

        public void SetDisabled(bool val) {
            canvasGroup.alpha = val ? 0.2f : 1;
        }
        
        public void ShowHint(float val, int depth) {
            if (_data.Label == string.Empty) { return; }
            hintCanvasGroup.alpha = val;
            if (val > 0.5) transform.SetSiblingIndex(depth);
        }
    }
}