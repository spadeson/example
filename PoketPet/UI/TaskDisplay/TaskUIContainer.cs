using UnityEngine;

namespace PoketPet.UI.TaskDisplay {
    public class TaskUIContainer : MonoBehaviour {

        [Header("Rect Transform")]
        [SerializeField] private RectTransform rectTransform;
        
        void Awake() {
            if (rectTransform == null) {
                rectTransform = GetComponent<RectTransform>();
            }
        }

        public RectTransform GetRectTransform() {
            if (rectTransform == null) {
                rectTransform = GetComponent<RectTransform>();
            }
            return rectTransform;
        }

        public Vector2 GetRectTransformSize() {
            var rect = rectTransform.rect;
            return new Vector2(rect.width, rect.height);
        }
        
        public Vector2 GetRectTransformSize(float multiplicator) {
            var rect = rectTransform.rect;
            return new Vector2(rect.width, rect.height) * multiplicator;
        }

        /// <summary>
        /// Get column height multiplied by multiplier.
        /// </summary>
        /// <param name="multiplier">Multiplier for the height.</param>
        /// <returns>Column multiplied height.</returns>
        public float GetColumnHeight(float multiplier) {
            return rectTransform.sizeDelta.y * multiplier;
        }
        
        /// <summary>
        /// Get column full height.
        /// </summary>
        /// <returns>Column full height.</returns>
        public float GetColumnHeight() {
            return rectTransform.sizeDelta.y;
        }

        /// <summary>
        /// Returns column top margin.
        /// </summary>
        /// <returns>Top margin value.</returns>
        public float GetTopMargin() {
            return -rectTransform.offsetMax.y;
        }
        
        /// <summary>
        /// Returns column bottom margin.
        /// </summary>
        /// <returns>Bottom margin value.</returns>
        public float GetBottomMargin() {
            return rectTransform.offsetMin.y;
        }
    }
}
