using UnityEditor;

namespace PoketPet.UI.TaskDisplay.Editor {
    [CustomEditor(typeof(TaskUIElement))]
    public class TaskUIElementEditor : UnityEditor.Editor {

        private float _taskProgress;
        
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var taskUiElement = (TaskUIElement)target;

            _taskProgress = EditorGUILayout.Slider("Task Progress", _taskProgress, 0f, 1f);
            taskUiElement.UpdateTaskProgressFill(_taskProgress);
        }
    }
}
