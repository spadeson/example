using UnityEditor;
using UnityEngine;

namespace PoketPet.UI.TaskDisplay.Editor {
    [CustomEditor(typeof(TaskUIElementAssignee))]
    public class TaskUIElementAssigneeEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            var taskUIElementAssignee = (TaskUIElementAssignee)target;

            if (GUILayout.Button("Set Boy", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("boy");
            }
            
            if (GUILayout.Button("Set Girl", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("girl");
            }
            
            if (GUILayout.Button("Set Male", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("male");
            }
            
            if (GUILayout.Button("Set Female", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("female");
            }
            
            if (GUILayout.Button("Set Advisor", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("advisor");
            }
            
            if (GUILayout.Button("Set Unknown", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("unknown");
            }
            
            if (GUILayout.Button("Set Done", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                taskUIElementAssignee.SetAssigneeIcon("done");
            }
        }
    }
}
