using System;
using PoketPet.Network.API.RESTModels.Tasks;

namespace PoketPet.UI.TaskDisplay {
    public class TaskTimeConverterUtil {
        public static float GetTaskProgress(TaskResponseData taskDataModel) {
            var flowData = taskDataModel.life_flow;
            var delta    = flowData.expire               - flowData.create;
            var from     = GetCurrentUtcInServerFormat() - flowData.create;
            var progress = (float) ((float) from / (float) delta);
            return progress;
        }

        private static int GetCurrentUtcInServerFormat() {
            return CurrentTimeUtc() / 60;
        }

        private static int CurrentTimeUtc() {
            DateTime epochStart       = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var      currentEpochTime = (int) (DateTime.UtcNow - epochStart).TotalSeconds;

            return currentEpochTime;
        }
    }
}