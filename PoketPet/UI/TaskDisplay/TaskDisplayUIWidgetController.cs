using System.Collections.Generic;
using System.Linq;
using PoketPet.Family;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Tasks;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Task;
using PoketPet.UI.TaskAdviser;
using PoketPet.UI.TaskCompletedTakePhoto;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.UsersPetRoleKeys;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.UI.TaskDisplay {
    public class TaskDisplayUIWidgetController : WidgetControllerWithData<TaskDisplayUIWidget, TaskDisplayUIWidgetData> {
        private List<UserModel> _users = new List<UserModel>();

        private TaskInterfaceButtonsController _taskElementsController;

        protected override void OnWidgetCreated(Widget widget) {
            FamilyProfileManager.OnUsersListByPetIdUpdated += UpdateTasksView;
            UserProfileManager.OnUserDataModelUpdated += UpdateTasksView;
            UserProfileManager.OnUserAvatarUpdated += UpdateTasksView;
            
            UserProfileManager.OnUserDataModelUpdated += UpdateTasksView;
            UserProfileManager.OnUserAvatarUpdated += UpdateTasksView;

            TaskManager.OnUpdate += UpdateTasksView;
            TaskManager.OnRemove += OnTaskRemoveHandler;

            Widget.Initialize(WidgetData);

            Widget.OnElementDown += OnElementDownHandler;
            Widget.OnElementUp += OnElementUpHandler;

            _taskElementsController = Widget.GetComponent<TaskInterfaceButtonsController>();
            _taskElementsController.Initialize();
            UpdateTasksView();
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            FamilyProfileManager.OnUsersListByPetIdUpdated -= UpdateTasksView;
            UserProfileManager.OnUserDataModelUpdated -= UpdateTasksView;
            UserProfileManager.OnUserAvatarUpdated -= UpdateTasksView;
            
            UserProfileManager.OnUserDataModelUpdated -= UpdateTasksView;
            UserProfileManager.OnUserAvatarUpdated -= UpdateTasksView;

            TaskManager.OnUpdate -= UpdateTasksView;
            TaskManager.OnRemove -= OnTaskRemoveHandler;

            Widget.OnElementDown -= OnElementDownHandler;
            Widget.OnElementUp -= OnElementUpHandler;
        }

        private void UpdateTasksView() {
            Debug.Log("Tasks Updated");
            _users = FamilyProfileManager.GetFamiliesAsList();
            Widget.UpdateTasksUiElements(TaskManager.Tasks);
            _taskElementsController.MakeUsersAssigneeList(_users);
        }

        private void OnTaskRemoveHandler(TaskResponseData task) {
            Debug.Log("Task Removed");
            _taskElementsController.RemoveAssigneeButton();
            Widget.RemoveTaskUiElements(task);
        }

        private void OnElementDownHandler(TaskUIElement task) {
            _taskElementsController.ShowAssigneeButtons(task);
        }

        private void OnElementUpHandler(TaskUIElement task) {
            if (_taskElementsController.CheckForOverlap()) {
                var assigneeIndex = _taskElementsController.targetAssigneeIndex;
                var taskData      = task.GetTaskDataModel();
                if (_users.Count > assigneeIndex) {
                    var targetUser = _users[assigneeIndex];

                    TaskManager.AssignTask(taskData.id, targetUser.id);
                    taskData.assignee = targetUser.id;
                } else {
                    var targetAction = _taskElementsController.GetButtonUpAssigneeAction();
                    switch (targetAction) {
                        case "DONE":
                            if (UserProfileManager.CurrentUser.RoleByCurrentPet == OWNER) {
                                TaskManager.CompleteTask(taskData.id);
                            }
                            else {
                                var widgetData = new TaskCompletedTakePhotoUIWidgetData{taskId = taskData.id};
                                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(TASK_COMPLETED_TAKE_PHOTO_UI, widgetData, true);
                            }
                            break;
                        case "ME":
                            TaskManager.AssignTask(taskData.id, UserProfileManager.CurrentUser.id);
                            break;
                        case "ADVISER":
                            ShowAdviserPopup(task.GetTaskDataModel().type);
                            break;
                    }
                }
            }

            _taskElementsController.RemoveAssigneeButton();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskType">WALK or FEED.</param>
        private void ShowAdviserPopup(string taskType) {
            
            var userRoleByCurrentPet = UserProfileManager.CurrentUser.RoleByCurrentPet;

            var widgetData = new TaskAdviserUIWidgetData {TaskType = taskType, UserRole = userRoleByCurrentPet};
            
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(TASK_ADVISER_UI, widgetData, true);
        }
    }
}