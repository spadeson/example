using System;
using System.Collections.Generic;
using DG.Tweening;
using PoketPet.UI.TaskFlow;
using PoketPet.User;
using UnityEngine;
using static PoketPet.Global.GlobalVariables.UsersFamilyRoleKeys;
using static PoketPet.Global.GlobalVariables.AssigneeIconsKeys;

namespace PoketPet.UI.TaskDisplay {
    public class TaskInterfaceButtonsController : MonoBehaviour {
        public List<TaskUIElementAssignee> taskAssigneeList = new List<TaskUIElementAssignee>();
        [SerializeField] private Transform assigneeElementsContainer;
        [SerializeField] private TaskUIElementAssignee taskAssigneePrefab;
        
        private readonly List<TaskUIAssigneeData> _assignButtonsList = new List<TaskUIAssigneeData>();
        
        public int targetAssigneeIndex;
        
        private float _spinForce;
        private bool _targetOff;
        private bool _isButtonsShowed;
        private bool _isButtonsOpened;
        private TaskUIElement _currentActiveTask;
        private List<float> _sizesList;
        private Vector2 _cachedHoldPos;

        private float _angScale = 0.3f;
        private float _distScale = 0.3f;
        private const float MinScaleAlpha = 0.8f;
        private const float MaxScaleAlpha = 1.1f;
        private const float MinScale = 0.3f;
        private const float MaxScale = 1.3f;
        private const float FadeAlpha = 0.25f;
        private const float TaskIconDiameter = 256f;

        public void Initialize() {
            taskAssigneeList = new List<TaskUIElementAssignee>();
        }
        
        public void MakeUsersAssigneeList(IEnumerable<UserModel> userNames) {
            _assignButtonsList.Clear();
            foreach (var user in userNames) {
                _assignButtonsList.Add(new TaskUIAssigneeData {Label = user.nickname, Avatar = user.avatar, FamiliesRole = user.FamiliesRole});
            }

            var currentUser = UserProfileManager.CurrentUser;
            _assignButtonsList.Add(new TaskUIAssigneeData {Label = "ME", Avatar = currentUser.avatar, FamiliesRole = currentUser.FamiliesRole});
            _assignButtonsList.Add(new TaskUIAssigneeData {Label = "DONE", Avatar = DONE});
            _assignButtonsList.Add(new TaskUIAssigneeData {Label = "ADVISER", Avatar = ADVISOR});
        }

        public void ShowAssigneeButtons(TaskUIElement parentTask) {
            if (_isButtonsShowed) return;
            
            _isButtonsShowed = true;
            _distScale = (MaxScale - MinScale) / 2;
            _angScale = _distScale;
            Debug.Log("create assignee buttons");
            var img = parentTask.GetComponent<CanvasGroup>();
            img.alpha = FadeAlpha;
            _currentActiveTask = parentTask;
            _cachedHoldPos = new Vector2(_currentActiveTask.GetRectTransform().anchoredPosition.x,
                _currentActiveTask.GetRectTransform().anchoredPosition.y);
            var parentRectTransform = parentTask.GetRectTransform();

            var assigneeButtonsCount = _assignButtonsList.Count;
            for (var i = 0; i < assigneeButtonsCount; i++) {
                var newAssigneeButton = Instantiate(taskAssigneePrefab, assigneeElementsContainer, false);
                taskAssigneeList.Add(newAssigneeButton);

                var theta = (float) ((Mathf.PI / 2 / assigneeButtonsCount) * i);
                var size = parentRectTransform.sizeDelta;
                var xPos = Mathf.Sin(theta) * size.x;
                var yPos = Mathf.Cos(theta) * size.y;
                var newAssigneeButtonTransform = (RectTransform) newAssigneeButton.transform;
                var position = parentRectTransform.anchoredPosition;
                newAssigneeButtonTransform.anchoredPosition = new Vector2(position.x, position.y);
                Vector2 anchoredPosition;
                newAssigneeButtonTransform.DOAnchorPos(new Vector2(
                    xPos + (anchoredPosition = parentRectTransform.anchoredPosition).x,
                    yPos + anchoredPosition.y), 0.15f).onComplete += () => {
                    newAssigneeButtonTransform.transform.DOScale(new Vector3(
                            MinScale,
                            MinScale,
                            1), 0.1f).onComplete += () => {_isButtonsOpened = true; };
                };
                
                newAssigneeButton.Init(_assignButtonsList[i]);
                newAssigneeButton.AddAvatar();
                
                newAssigneeButton.SetDisabled(parentTask.GetTaskDataModel().assignee == UserProfileManager.CurrentUser.id && _assignButtonsList[i].Label == "ME");
            }
        }

        private void UpdateSizesCalc() {
            _sizesList = new List<float>();
            var minimalAngDiff = 360.0f;
            var currentTaskPosition = _currentActiveTask.GetRectTransform().anchoredPosition;
            for (var i = 0; i < taskAssigneeList.Count; i++) {
                var elementAssigneePosition = ((RectTransform) taskAssigneeList[i].transform).anchoredPosition;
                var angFromCenterToTask = TaskViewCalculator.CalcAngle(_cachedHoldPos, currentTaskPosition);
                var angFromCenterToIcon = TaskViewCalculator.CalcAngle(_cachedHoldPos, elementAssigneePosition);
                var angDiff = TaskViewCalculator.AngDifference(angFromCenterToTask, angFromCenterToIcon);

                var distToAssignIcon = TaskViewCalculator.CalcDistance(currentTaskPosition, elementAssigneePosition);
                var distCenterToAssignIcon = TaskViewCalculator.CalcDistance(_cachedHoldPos, elementAssigneePosition);
                
                if (angDiff < 1) angDiff = 1;
                if (minimalAngDiff > angDiff) {
                    minimalAngDiff = angDiff;
                    targetAssigneeIndex = i;
                }

                var distScaleRes = (_distScale * (1 - distToAssignIcon / distCenterToAssignIcon));
                var angScaleRes = _angScale * (1 - (angDiff / 90)) * (distScaleRes * (1 / _distScale));
                if (angScaleRes > _angScale) angScaleRes = _angScale;

                var size = MinScale + distScaleRes + angScaleRes;

                if (size < MinScale) size = MinScale;
                if (size > MaxScale) size = MaxScale;
                //push calculated icon scale to array
                _sizesList.Add(size);
            }

            //under scaling for icons count more than 15
            if (taskAssigneeList.Count <= 15) return;
            for (var i = 0; i < taskAssigneeList.Count; i++) {
                var howFar = Math.Abs(targetAssigneeIndex - i);
                if (howFar < 5) continue;
                var rate = ((20 - howFar) / 20.0f);
                if (rate < 0.5f) rate = 0.1f;
                var size = MinScale * rate;
                if (size < 0.05) size = 0.1f;
                _sizesList[i] = size;
            }
        }

        public void RemoveAssigneeButton() {
            if (!_isButtonsShowed) return;

            var img = _currentActiveTask.GetComponent<CanvasGroup>();
            img.alpha = 1;

            foreach (var t in taskAssigneeList) {
                Destroy(t.gameObject);
            }

            taskAssigneeList.Clear();
            _isButtonsShowed = false;
            _isButtonsOpened = false;
            _currentActiveTask = null;
        }

        private void UpdateAssigneeIconsAngleToTaskPosition() {
            var center = _cachedHoldPos;
            var currentTaskPosition = _currentActiveTask.GetRectTransform().anchoredPosition;
            var targetAssigneePosition = ((RectTransform)taskAssigneeList[targetAssigneeIndex].transform).anchoredPosition;
            var curTaskAng = TaskViewCalculator.CalcAngle(_cachedHoldPos, currentTaskPosition);
            var curAng = TaskViewCalculator.CalcAngle(_cachedHoldPos, targetAssigneePosition);

            var toTaskAngDiff = TaskViewCalculator.AngDiffAbs(curTaskAng, curAng);
            var toTaskAngDif = TaskViewCalculator.AngDiff(curTaskAng, curAng);

            var toIconDist = TaskViewCalculator.CalcDistance(_cachedHoldPos, targetAssigneePosition);
            var toTaskDist = TaskViewCalculator.CalcDistance(_cachedHoldPos, currentTaskPosition);

            var isBlockedEdge = TaskViewCalculator.IsBlockedEdge(targetAssigneeIndex, taskAssigneeList.Count, toTaskAngDif);
            
            // checking for no target assignee icon in direction of task angle moved
            _targetOff = false;
            if (targetAssigneeIndex == 0 || targetAssigneeIndex == taskAssigneeList.Count - 1) {
                var angDiff = TaskViewCalculator.AngDifference(curTaskAng, curAng);
                if (Math.Abs(angDiff) > 35) _targetOff = true;
            }

            //check for angle to go next icon
            if (Math.Abs(toTaskAngDif) > 7 && toIconDist * 0.1f < toTaskDist && !_targetOff) {
                if (Math.Abs(_spinForce) < 0.0000001f)
                    _spinForce = toTaskAngDif * 0.35f;
                //force of spin for go next icon, inverting on edge reached for move all icons disk follow rotation angle
                curAng -= _spinForce * (isBlockedEdge ? -1 : 1);
            }
            else {
                _spinForce = 0.0f;
            }

            // move icons disk if move task over the icon
            if (toTaskAngDiff < 35 && toIconDist * 1.1 < toTaskDist) {
                var currentValue = curAng / 180 * Math.PI;
                _cachedHoldPos.x += (float) (currentTaskPosition.x - _cachedHoldPos.x - TaskIconDiameter * Math.Cos(currentValue)) / 2;
                _cachedHoldPos.y += (float) (currentTaskPosition.y - _cachedHoldPos.y - TaskIconDiameter * Math.Sin(currentValue)) / 2;
            }

            // move icons disk if no icon selected and distance more than range from center to icons 
            
            if (toTaskDist >= 10 && _targetOff) {
                _cachedHoldPos.x += (currentTaskPosition.x - _cachedHoldPos.x) / 2;
                _cachedHoldPos.y += (currentTaskPosition.y - _cachedHoldPos.y) / 2;
            }

            // set new position for selected icon
            var newCenter = TaskViewCalculator.CalcPointOnCircleADegR(TaskIconDiameter, 360 - (curAng - 90));
            var tAssigneeButtonTransform = (RectTransform) taskAssigneeList[targetAssigneeIndex].transform;
            tAssigneeButtonTransform.anchoredPosition = new Vector2(center.x + newCenter.x, center.y + newCenter.y);
        }

        private void UpdateAssigneePositions() {
            TaskViewCalculator.sizesList = _sizesList;

            //arrange assignee icons to the left from current active icon
            for (var i = targetAssigneeIndex; i > 0; i--) {
                //calc new position of icon and set it
                var newCenter2 = TaskViewCalculator.GetNewCenter(_cachedHoldPos, taskAssigneeList, TaskIconDiameter, -1, i);
                var newAssigneeButtonTransform = (RectTransform) taskAssigneeList[i - 1].transform;

                newAssigneeButtonTransform.anchoredPosition = new Vector2(_cachedHoldPos.x + newCenter2.x, _cachedHoldPos.y + newCenter2.y);
            }

            //arrange assignee icons to the right from current active icon
            for (var i = targetAssigneeIndex; i < taskAssigneeList.Count - 1; i++) {

                //calc new position of icon and set it
                var newCenter2 = TaskViewCalculator.GetNewCenter(_cachedHoldPos, taskAssigneeList, TaskIconDiameter, 1, i);
                var newAssigneeButtonTransform = (RectTransform) taskAssigneeList[i + 1].transform;
                newAssigneeButtonTransform.anchoredPosition = new Vector2(_cachedHoldPos.x + newCenter2.x, _cachedHoldPos.y + newCenter2.y);
            }
        }

        public void Update() {
            if (!_isButtonsShowed) return;
            // calc scaling of assignee icons
            UpdateSizesCalc();
            // calc new positions for assignee icons
            UpdateAssigneeIconsAngleToTaskPosition();
            // iterate calculation of movement for assignee icons and set positions 
            for (var i = 0; i < 5; i++) {
                UpdateAssigneePositions();
            }

            //set scaling of assignee icons      
            for (var i = 0; i < taskAssigneeList.Count; i++) {
                var val = _sizesList[i];
                if (!(val > 0) && !(val < Single.PositiveInfinity)) continue;
                taskAssigneeList[i].transform.DOScale(new Vector3(val, val, 1), 0.1f);
                var scale = taskAssigneeList[i].transform.localScale.x;
                if (scale > MinScaleAlpha && _isButtonsShowed) {
                    taskAssigneeList[i].ShowHint((scale - MinScaleAlpha) / (MaxScaleAlpha - MinScaleAlpha), taskAssigneeList.Count);
                }
                else {
                    taskAssigneeList[i].ShowHint(0, taskAssigneeList.Count);
                }
            }
        }

        public bool CheckForOverlap() {
            if (targetAssigneeIndex < 0 || !_isButtonsOpened) return false;
            var distToAssignIcon = TaskViewCalculator.CalcDistance(_currentActiveTask.GetRectTransform().anchoredPosition,
                ((RectTransform) taskAssigneeList[targetAssigneeIndex].transform).anchoredPosition);
            return distToAssignIcon < 45;
        }
        
        public string GetButtonUpAssigneeAction() {
            return _assignButtonsList[targetAssigneeIndex].Label;
        }
    }
}