using System;
using Core.TimersManager.Data;
using DG.Tweening;
using PoketPet.Global;
using PoketPet.Photo;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VIS.CoreMobile;
using Core.SpriteIcons.Manager;
using PoketPet.Family;
using PoketPet.Network.API.RESTModels.Tasks;
using static PoketPet.Global.GlobalVariables.SpriteIconsGroupKeys;

namespace PoketPet.UI.TaskDisplay
{
    public class TaskUIElement : MonoBehaviour , IPointerDownHandler, IPointerUpHandler, IDragHandler, IDropHandler{
        private TimerDataModel TimerDataModel { get; set; }

        private TaskResponseData TaskDataModel { get; set; }

        public event Action<TaskUIElement> OnButtonDown;
        public event Action<TaskUIElement> OnButtonUp;

        [Header("Canvas Group")]
        [SerializeField] private CanvasGroup canvasGroup;

        [Header("Timer Value")]
        [SerializeField] private TextMeshProUGUI taskTimerValue;
        
        [Header("Fill Amount Image")]
        [SerializeField] private Image taskProgressBarFillImage;
        
        [Header("Task Icon Image")]
        [SerializeField] private Image taskIconImage;
        
        [Header("Task Assignee Container")]
        [SerializeField] private TaskUIElementAssigneeContainer taskAssigneeContainer;
        [SerializeField] private GameObject iconsContainerPrefab;
        [SerializeField] private GameObject avatarContainerPrefab;
        [SerializeField] private Image avatarImage;
        [SerializeField] private RectTransform parent;

        private IAssigneeAvatarContainer _iconsContainer;
        private IAssigneeAvatarContainer _avatarContainer;

        private RectTransform _rectTransform;
        
        public Vector2 holdedPos;

        //private bool assigneeShown;
        public bool dragged;
        public float MaxContainerPosition { private get; set; }
        public float MinContainerPosition { private get; set; }
        public float TaskColumnX { private get; set; }
        private static SpriteIconsManager IconManager => CoreMobile.Instance.SpritesIconsManager;
        #region Public Methods

        /// <summary>
        /// Initializes TaskUIElement with TaskDataModel and TimerDataModel. 
        /// </summary>
        /// <param name="taskDataModel">Task data model.</param>
        /// <param name="timerDataModel">Timer data model.</param>
        public void Initialize(TimerDataModel timerDataModel) {

            _rectTransform = GetComponent<RectTransform>();

            if (canvasGroup == null) {
                canvasGroup = GetComponent<CanvasGroup>();
            }

            TimerDataModel = timerDataModel;

            taskAssigneeContainer.Initialize();

            UpdateTimerValue(TimerDataModel.secondsToDueDate.ToString());
        }

        public void UpdateView(TaskResponseData taskDataModel) {
            TaskDataModel = taskDataModel;
            
            var userId = UserProfileManager.CurrentUser.id;
            if (TaskDataModel.assignee != "watcher" && TaskDataModel.assignee != userId) {
                SetTaskElementActivity(false);
                ToggleAssigneeContainer(true);
            } else if (TaskDataModel.assignee == "closed") {
                SetTaskElementActivity(false);
            } else {
                SetTaskElementActivity(true);
                ToggleAssigneeContainer(false);
                
                if (TaskDataModel.assignee == userId) {
                    ToggleAssigneeContainer(true);
                }
            }
        }

        public void UpdateAssigneeView(TaskResponseData taskDataModel) {
            TaskDataModel = taskDataModel;
            
            var userId = UserProfileManager.CurrentUser.id;
            if (TaskDataModel.assignee == userId) {
                AddAvatar(UserProfileManager.CurrentUser);
                taskAssigneeContainer.SetAssigneeName(UserProfileManager.CurrentUser.nickname);
            }
            else {
                var family = FamilyProfileManager.GetFamiliesAsList();
                var value = family.Find(item => item.id == TaskDataModel.assignee);
                if (value == null) return;
                AddAvatar(value);
                taskAssigneeContainer.SetAssigneeName(value.nickname);
            }
            
        }
        

        public void Dismiss() {
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            _rectTransform.DOScale(0, 0.2f).SetEase(Ease.OutQuart);
            Destroy(this, 0.3f);
        }

        public void SetAnchorPosition(Vector2 anchorPosition) {
            _rectTransform.anchoredPosition = anchorPosition;
        }
        
        public void SetAnchorPositionX(float anchorPositionX) {
            _rectTransform.anchoredPosition = new Vector2(anchorPositionX, _rectTransform.anchoredPosition.y);
        }
        
        public void SetAnchorPositionY(float anchorPositionY) {
            _rectTransform.anchoredPosition = new Vector2(_rectTransform.anchoredPosition.x, anchorPositionY);
        }

        #endregion

        public TaskResponseData GetTaskDataModel() {
            return TaskDataModel;
        }

        public void SetPosition(float val) {
            var min = new Vector2(TaskColumnX, MinContainerPosition);
            var max = new Vector2(TaskColumnX, MaxContainerPosition);
            GetRectTransform().anchoredPosition = Vector2.Lerp(min, max, val);
        }

        public void UpdateTimerValue(string value)
        {
            taskTimerValue.text = value;
        }

        public RectTransform GetRectTransform()
        {
            return _rectTransform;
        }

        public void OnPointerUp (PointerEventData eventData) {
            OnButtonUp?.Invoke (this);
            _rectTransform.DOAnchorPos(holdedPos,0.2f);
            dragged = false;
        }

        public void OnPointerDown (PointerEventData eventData) {
            OnButtonDown?.Invoke (this);
            //  showAssigneeButtons (assigneeButtonsCount);
            holdedPos = _rectTransform.anchoredPosition;
            dragged = true;
        }


        public void OnDrag (PointerEventData eventData) {
            //Debug.Log($"{name}: OnDrag");
            transform.position = Input.mousePosition;
        }

        public void OnDrop(PointerEventData eventData) {
            //Debug.Log($"{name}: OnDrop");
        }

        public void UpdateTaskProgressFill(float progress) {
            taskProgressBarFillImage.fillAmount = progress;
            if (taskProgressBarFillImage.fillAmount < progress) {
                taskProgressBarFillImage.fillAmount += (progress - taskProgressBarFillImage.fillAmount) / 5;
            }
        }

        public void SetTaskIcon(string taskType) {
            var taskIconSprite = CoreMobile.Instance.SpritesIconsManager.GetSpriteIconByKey(GlobalVariables.SpriteIconsGroupKeys.TASKS_ICONS, taskType);
            taskIconImage.sprite = taskIconSprite;
        }

        private void SetTaskElementActivity(bool value) {
            canvasGroup.alpha = value ? 1 : 0.55f;
            canvasGroup.blocksRaycasts = value;
            canvasGroup.interactable = value;
        }

        /// <summary>
        /// Toggle tasks assignee visibility.
        /// </summary>
        /// <param name="value">Visibility value.</param>
        private void ToggleAssigneeContainer(bool value) {
            taskAssigneeContainer.ToggleActivity(value);
        }

        private void AddAvatar(UserModel user)
        {
            _iconsContainer?.Dismiss();
            _avatarContainer?.Dismiss();

            _iconsContainer = Instantiate(iconsContainerPrefab, parent).GetComponent<IconsAssigneeContainer>();
            avatarImage = _iconsContainer.GetAvatarImage();

            avatarImage.sprite = IconManager.GetSpriteIconByKey(ASSIGNEE_ICONS, user.FamiliesRole);
            PhotoManager.GetAvatarById(user.avatar, sprite => {
                _avatarContainer = Instantiate(avatarContainerPrefab, parent).GetComponent<PhotosAssigneeContainer>();
                avatarImage = _avatarContainer.GetAvatarImage();
                avatarImage.sprite = sprite;
                
                _iconsContainer.SetEnabled(false);
                _avatarContainer.Minimize();
            });
        }
    }
}