using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.TaskDisplay {
    public class TaskUIElementAssigneeContainer : MonoBehaviour {
        [Header("Canvas Group")]
        [SerializeField] private CanvasGroup canvasGroup;
        
        [Header("Assignee Icon")]
        [SerializeField] private Image assigneeIcon;
        
        [Header("Assignee Name")]
        [SerializeField] private TextMeshProUGUI assigneeNameText;

        public void Initialize() {
            
            if (canvasGroup == null) {
                canvasGroup = GetComponent<CanvasGroup>();
            }

            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }

        public void ToggleActivity(bool value) {
            canvasGroup.alpha = value ? 1 : 0;
        }

        public void SetAssigneeName(string assigneeName) {
            assigneeNameText.text = assigneeName != UserProfileManager.CurrentUser.id ? assigneeName : "ME";
        }
    }
}