using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialSendPhoto {
    public class TutorialSendPhotoUIWidget : BaseUIWidget {
        
        public event Action OnActionButtonClick;
		
        [Header("Shroud Image")]
        [SerializeField] private Image shroudImage;

        [Header("Container")]
        [SerializeField] private RectTransform container;
        
        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;
        
        [Header("Photo Image")]
        [SerializeField] private Image photoImage;

        [Header("Action Button")]
        [SerializeField] private Button actionButton;

        private TutorialSendPhotoUIWidgetData _widgetData;
        
        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(TutorialSendPhotoUIWidgetData widgetData) {
            
            _widgetData = widgetData;
            
            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                OnActionButtonClick?.Invoke();
            });
        }
    }
}