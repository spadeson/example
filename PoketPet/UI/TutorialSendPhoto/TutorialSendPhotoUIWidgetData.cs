using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialSendPhoto {
    [Serializable]
    public class TutorialSendPhotoUIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}