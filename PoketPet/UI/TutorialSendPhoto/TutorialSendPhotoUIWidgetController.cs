using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialSendPhoto {
    public class TutorialSendPhotoUIWidgetController : WidgetControllerWithData<TutorialSendPhotoUIWidget, TutorialSendPhotoUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {

        }

        private void WidgetOnActionButtonClick() {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
            Widget.Dismiss();
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
