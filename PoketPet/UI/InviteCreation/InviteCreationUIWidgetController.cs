using System.Collections.Generic;
using Core.ErrorsHandlerSubsystem.Data;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Invites;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.UI.NotificationStatus;
using PoketPet.UI.Progress;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.InviteCreation {
    public class InviteCreationUIWidgetController : WidgetControllerWithData<InviteCreationUIWidget, InviteCreationUIWidgetData> {

        private bool _inviteSending;

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnInputFieldDeselect += OnInputFieldDeselectHandler;
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClickHandler;
            Widget.OnSendButtonClick += OnSendButtonClickHandler;
            
            Widget.Initialize(WidgetData);
        }
        
        protected override void OnWidgetActivated(Widget widget) { }
        
        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnInputFieldDeselect -= OnInputFieldDeselectHandler;
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClickHandler;
            Widget.OnSendButtonClick -= OnSendButtonClickHandler;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.FAMILY_MANAGEMENT_UI, true);
        }

        private void OnInputFieldDeselectHandler(InputFieldsTypes type, string value) {
            Widget.SetInputFieldError(type, CoreMobile.Instance.ErrorsManager.ValidateForError(type, value));
        }

        private void OnSendButtonClickHandler(string email, InviteeType inviteeType) {
            if (_inviteSending) return;

            var progressUiWidgetData = new ProgressUIWidgetData {title = "Sending Invite", description = ""};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PROGRESS_UI, progressUiWidgetData);

            if (PetsProfileManager.CurrentPet == null) {
                
                CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.PROGRESS_UI);
                
                var notificationStatusWidgetData = new NotificationStatusUIWidgetData {Title = "Error", Message = "You cannot invite user into the family while you don't have pet"};
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationStatusWidgetData, true);

                return;
            }

            var inviteUserRequestData = new InviteUserRequestData {@from = UserProfileManager.CurrentUser.id, to = email, petID = PetsProfileManager.CurrentPet.id, permissions = new List<string> {inviteeType == InviteeType.CO_OWNER ? "owner" : "watcher"}};

            var requestData = new RequestData<InviteUserRequestData>(@from: UserProfileManager.CurrentUser.id, data: inviteUserRequestData, "invite_user_by_mail");

            var operation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("friends", requestData);
            operation.OnDataReceived += InviteUserHandler;
            operation.OnErrorReceived += ShowError;
        }

        private void WidgetOnCloseButtonClickHandler() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.Dismiss();
        }
        
        private void InviteUserHandler(string data) {
            
            CoreMobile.Instance.UIManager.DismissWidgetByType(GlobalVariables.UiKeys.PROGRESS_UI);
            
            _inviteSending = false;
            
            var response = JsonUtility.FromJson<ResponseData<string>>(data);

            if (response.status != "error") {
                Widget.Dismiss();
            } else {
                ShowError(response);
                Widget.Dismiss();
            }
        }

        private void ShowError(ResponseData<string> response) {
            Debug.LogError($"Status: {response.status}, Error Code,: {response.error.error_code}, Error: {response.error.error}");
            var notificationStatusWidgetData = new NotificationStatusUIWidgetData {Title = response.status, Message = response.error.error};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationStatusWidgetData, true);
        }
        
        private void ShowError(string error) {
            Debug.LogError($"Error: {error}");
            var notificationStatusWidgetData = new NotificationStatusUIWidgetData {Title = "Error", Message = error};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationStatusWidgetData, true);
        }
    }
}