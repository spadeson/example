namespace PoketPet.UI.InviteCreation {
    public enum InviteeType {
        CO_OWNER = 0,
        WATCHER = 1
    }
}
