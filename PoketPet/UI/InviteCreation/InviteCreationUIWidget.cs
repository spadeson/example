using System;
using Core.ErrorsHandlerSubsystem.Data;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.InviteCreation {
    public class InviteCreationUIWidget : BasicPopupUIWidget {
        
        public event Action<string, InviteeType> OnSendButtonClick;
        public event Action OnCloseButtonClick;
        
        public event Action<InputFieldsTypes, string> OnInputFieldDeselect;

        [Header("Input Fields:")]
        [SerializeField] private TMP_InputField emailField;

        [Header("Input Fields Status:")]
        [SerializeField] private TextMeshProUGUI emailStatusText;

        [Header("Toggles:")]
        [SerializeField] private Toggle coOwnerToggle;
        [SerializeField] private Toggle watcherToggle;

        [Header("Colors:")] 
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color errorColor = Color.red;

        private InviteCreationUIWidgetData _widgetData;
        private bool _emailFieldError;

        public override void Create() {
            base.Create();
            
            emailStatusText.text = string.Empty;
            emailField.text = _widgetData.inviteeEmail;
            emailField.keyboardType = TouchScreenKeyboardType.EmailAddress;

            emailField.onEndEdit.AddListener(delegate(string value) {
                _widgetData.inviteeEmail = value;
                OnInputFieldDeselect?.Invoke(InputFieldsTypes.EMAIL, value);
            });
            
            emailField.onValueChanged.AddListener(delegate(string value) {
                _widgetData.inviteeEmail = value;
            });

            coOwnerToggle.isOn = true;
            watcherToggle.isOn = false;

            coOwnerToggle.onValueChanged.AddListener(delegate(bool value) {
                watcherToggle.SetIsOnWithoutNotify(!value);
                _widgetData.inviteeType = InviteeType.CO_OWNER;
            });

            watcherToggle.onValueChanged.AddListener(delegate(bool value) {
                coOwnerToggle.SetIsOnWithoutNotify(!value);
                _widgetData.inviteeType = InviteeType.WATCHER;
            });
            
            actionButton.onClick.AddListener(delegate {
                OnSendButtonClick?.Invoke(_widgetData.inviteeEmail, _widgetData.inviteeType);
            });
            
            closeButton.onClick.AddListener(CloseButtonClickHandler);

            actionButton.interactable = !string.IsNullOrEmpty(emailField.text);
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(InviteCreationUIWidgetData widgetData) {
            _widgetData = widgetData;
        }

        public string GetEmail() {
            return emailField.text;
        }

        public void SetInputFieldError(InputFieldsTypes type, string error) {
            if (string.IsNullOrEmpty(error)) {
                emailField.image.color = Color.clear;

                emailStatusText.color = normalColor;
                emailStatusText.text  = string.Empty;

                _emailFieldError = false;
            } else {
                emailField.image.color = errorColor;

                emailStatusText.color = errorColor;
                emailStatusText.text  = error;
                
                _emailFieldError = true;
            }
            
            actionButton.interactable = !_emailFieldError && !string.IsNullOrEmpty(emailField.text) && !string.IsNullOrWhiteSpace(emailField.text);
        }

        private void CloseButtonClickHandler() {
            closeButton.onClick.RemoveAllListeners();
            closeButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnCloseButtonClick?.Invoke();
            });
        }
    }
}
