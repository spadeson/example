using System;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.InviteCreation
{
	[Serializable]
	public class InviteCreationUIWidgetData : WidgetData {
		public string inviteeEmail;
		public InviteeType inviteeType;
	}
}