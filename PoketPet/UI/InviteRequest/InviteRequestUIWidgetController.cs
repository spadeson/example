using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Base;
using PoketPet.Network.API.RESTModels.Invites;
using PoketPet.UI.NotificationStatus;
using PoketPet.User;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.InviteRequest {
    public class InviteRequestUIWidgetController : WidgetControllerWithData<InviteRequestUIWidget, InviteRequestUIWidgetData> {
        
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnRequestButtonClick += WidgetOnRequestButtonClick;
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }
        
        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnRequestButtonClick -= WidgetOnRequestButtonClick;
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
        }

        private void WidgetOnRequestButtonClick(string email) {
            
            var requestData = new RequestData<WaitForInviteByEmailRequest>{
                from = UserProfileManager.CurrentUser.id,
                data = new WaitForInviteByEmailRequest(email),
                action = "wait_invite_user_by_mail"
            };

            var requestOperation = CoreMobile.Instance.NetworkManager.RESTNetworkManager.CreateRestRequest("wait_invite_user_by_mail", requestData);
            
            requestOperation.OnErrorReceived += error => {
                Debug.LogError($"Error: {error}");
            };

            requestOperation.OnDataReceived += data => {

                var notificationData = new NotificationStatusUIWidgetData {
                    Title = "Message",
                    Message = "Your invite was sent.\nPlease wait until your invite will be approved."
                };
                
                CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.NOTIFICATION_STATUS_UI, notificationData);
            };
        }
        
        private void WidgetOnCloseButtonClick() {
            Widget.Dismiss();
        }
    }
}