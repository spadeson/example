using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.InviteRequest {
    public class InviteRequestUIWidget : BasicPopupUIWidget {
        public event Action<string> OnRequestButtonClick;
        public event Action OnCloseButtonClick; 

        [Header("E-mail input field")]
        [SerializeField] private TMP_InputField emailInputField;

        public override void Create() {
            base.Create();
            
            actionButton.onClick.AddListener(delegate {
                OnRequestButtonClick?.Invoke(emailInputField.text);
            });
            
            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
        }

        public override void Dismiss() {
            actionButton.onClick.RemoveAllListeners();
            closeButton.onClick.RemoveAllListeners();
            
            base.Dismiss();
        }
    }
}
