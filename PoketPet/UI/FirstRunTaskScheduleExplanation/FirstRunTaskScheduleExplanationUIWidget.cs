using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunTaskScheduleExplanation
{
	public class FirstRunTaskScheduleExplanationUIWidget : BasicPopupUIWidget {

		[Header("Name Text:"), SerializeField]
		private TextMeshProUGUI nameText;
		public event Action OnActionButtonClick;
		private FirstRunTaskScheduleExplanationUIWidgetData _widgetData;
		
		public override void Create() {
			base.Create();
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			base.Dismiss();
		}
		
		public void Initialize(FirstRunTaskScheduleExplanationUIWidgetData widgetData) {
			_widgetData = widgetData;
			nameText.text = UserProfileManager.CurrentUser.nickname;
			actionButton.onClick.AddListener(delegate {
				OnActionButtonClick?.Invoke();
			});
		}

	}
}