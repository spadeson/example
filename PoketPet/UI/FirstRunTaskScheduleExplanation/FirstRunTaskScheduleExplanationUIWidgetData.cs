using System;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirstRunTaskScheduleExplanation {
    [Serializable]
    public class FirstRunTaskScheduleExplanationUIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}