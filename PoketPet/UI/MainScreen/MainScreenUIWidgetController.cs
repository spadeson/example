using System.Collections.Generic;
using PoketPet.Family;
using PoketPet.Games;
using PoketPet.Global;
using PoketPet.Messages;
using PoketPet.Photo;
using PoketPet.UI.AbnormalReport;
using PoketPet.UI.BurgerMenu;
using PoketPet.UI.Messages;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.GameTypesKeys;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.UI.MainScreen {
    public class MainScreenUIWidgetController : WidgetControllerWithData<MainScreenUIWidget, MainScreenUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnVetChatButtonClick += WidgetOnVetChatButtonClick;
            Widget.OnBurgerButtonClick += OnBurgerButtonClickHandler;
            Widget.OnMessagesButtonClick += OnMessagesButtonClickHandler;
            Widget.OnTasksButtonClick += OnTasksButtonClickHandler;
            Widget.OnCameraButtonClick += OnCameraButtonClickHandler;
            Widget.OnGameButtonClick += OnGameButtonClickHandler;

            MessageManager.AddSocketListeners();
            MessageManager.GetMessagesByUserId();
            MessageManager.OnUpdate += UpdateMessagesBadgeHandler;

            GameManager.AddSocketListeners();
            GameManager.RequestGameAwaiting();
            GameManager.OnGameAwaiting += OnGameAwaitingHandler;
            GameManager.OnGameCreateRequestSent += OnGameCreateRequestSentHandler;
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnVetChatButtonClick -= WidgetOnVetChatButtonClick;
            Widget.OnBurgerButtonClick -= OnBurgerButtonClickHandler;
            Widget.OnMessagesButtonClick -= OnMessagesButtonClickHandler;
            Widget.OnTasksButtonClick -= OnTasksButtonClickHandler;
            Widget.OnCameraButtonClick -= OnCameraButtonClickHandler;
            Widget.OnGameButtonClick -= OnGameButtonClickHandler;

            MessageManager.OnUpdate -= UpdateMessagesBadgeHandler;
            GameManager.OnGameAwaiting -= OnGameAwaitingHandler;
            
            //CoreMobile.Instance.SocketNetworkManager.Disconnect();
        }

        private void UpdateMessagesBadgeHandler() {
            Widget.SetBadgeCount(MessageManager.CountUnreadMessages());
        }

        private void WidgetOnVetChatButtonClick() {
            
            if (!CoreMobile.Instance.LocalDataManager.IsDataExists(GlobalVariables.LocalDataKeys.FIRST_TIME_VET_CHAT)) {
                CoreMobile.Instance.LocalDataManager.SaveLocalDataBoolean(GlobalVariables.LocalDataKeys.FIRST_TIME_VET_CHAT, true);
            }
            
            var firstTimeVetChat = CoreMobile.Instance.LocalDataManager.GetLocalDataBoolean(GlobalVariables.LocalDataKeys.FIRST_TIME_VET_CHAT);
            var vetChatUIWidgetKey  = firstTimeVetChat ? VET_CHAT_TUTORIAL_UI : VET_CHAT_UI;

            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(vetChatUIWidgetKey, animate: true);
        }

        private void OnBurgerButtonClickHandler() {
            var burgerMenuWidgetData = new BurgerMenuUIWidgetData {
                burgerMenuButtons = new List<BurgerMenuButtonTypes> {
                    BurgerMenuButtonTypes.USER_PROFILE,
                    BurgerMenuButtonTypes.PET_PROFILE,
                    BurgerMenuButtonTypes.PET_SCHEDULE,
                    BurgerMenuButtonTypes.FAMILY,
                    BurgerMenuButtonTypes.LEADERBOARDS,
                    BurgerMenuButtonTypes.SETTINGS,
                    BurgerMenuButtonTypes.SUPPORT,
                    BurgerMenuButtonTypes.DEVELOPERS
                }
            };
            
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(BURGER_MENU_UI, burgerMenuWidgetData, true);
        }

        private void OnMessagesButtonClickHandler() {
            var messagesUiWidgetData = new MessagesUIWidgetData {messagesDataList = MessageManager.Messages};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(MESSAGES_UI, messagesUiWidgetData);
        }

        private void OnTasksButtonClickHandler() {
            var abnormalReportWidgetData = new AbnormalReportUIWidgetData {abnormalReportType = AbnormalReportType.TAKE_FROM_GALLERY};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(ABNORMAL_REPORT_UI, abnormalReportWidgetData);
        }

        private void OnCameraButtonClickHandler() {
            PhotoManager.TakePhoto((sprite) => {
                PhotoManager.UploadPhoto(sprite.texture);
                // var widgetData = new AbnormalReportUIWidgetData() {
                // 	image = sprite,
                // 	abnormalReportType = AbnormalReportType.TAKE_FROM_CAMERA
                // };
                // CoreMobile.Instance.UIManager.ShowUiWidgetWithData("ABNORMAL_REPORT_UI", widgetData);
            });
        }

        private void OnGameAwaitingHandler() {
            Widget.SetGameButtonEnabled(GameManager.GetGameAwaitingByType(GAME_EMOTIONS) != null);
        }
        
        private void OnGameCreateRequestSentHandler() {
            Widget.SetGameButtonEnabled(false);
        }

        private void OnGameButtonClickHandler() {
            var familyList = FamilyProfileManager.GetFamiliesAsList();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(familyList.Count > 0
                    ? GAME_PHOTO_DESCRIPTION_UI
                    : FIRST_RUN_NOTIFICATION_UI, animate: true);
        }
    }
}