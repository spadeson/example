using System;
using PoketPet.UI.CommonUIElements;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.MainScreen {
    public class MainScreenUIWidget : BaseUIWidget {
        public event Action OnVetChatButtonClick;
        public event Action OnGameButtonClick;
        public event Action OnCameraButtonClick;
        public event Action OnTasksButtonClick;
        public event Action OnMessagesButtonClick;
        public event Action OnBurgerButtonClick;

        [Header("Vet Chat Button")]
        [SerializeField] private Button vetChatButton;
        
        [SerializeField] private Button gameButton;
        [SerializeField] private Button petScheduleButton;
        [SerializeField] private Button cameraButton;
        [SerializeField] private Button tasksButton;
        [SerializeField] private Button messagesButton;
        [SerializeField] private Button burgerButton;

        [SerializeField] private BageUIElement messagesBadge;

        public override void Create() {
            base.Create();
            
            vetChatButton.onClick.AddListener(delegate {
                OnVetChatButtonClick?.Invoke();
            });
            
            gameButton.onClick.AddListener(delegate {
                OnGameButtonClick?.Invoke();
            });
            
            cameraButton.onClick.AddListener(delegate {
                OnCameraButtonClick?.Invoke();
            });
            
            tasksButton.onClick.AddListener(delegate {
                OnTasksButtonClick?.Invoke();
            });
            
            messagesButton.onClick.AddListener(delegate {
                OnMessagesButtonClick?.Invoke();
            });
            
            burgerButton.onClick.AddListener(delegate {
                OnBurgerButtonClick?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            vetChatButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }
        
        public void SetGameButtonEnabled(bool value) {
            gameButton.transform.parent.gameObject.SetActive(value);
        }
        
        public void SetBadgeCount(int value) {
            messagesBadge.SetBageCounter(value);
        }
    }
}