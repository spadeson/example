using Core.ErrorsHandlerSubsystem.Data;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Data;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using PoketPet.Global;
using PoketPet.UI.FirebaseCreateAccount;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirebaseEmailLogin {
    public class FirebaseEmailLoginUIWidgetController : WidgetControllerWithData<FirebaseEmailLoginUIWidget, FirebaseEmailLoginUIWidgetData> {

        #region WidgetController

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnInputFieldValueChanged += OnInputFieldValueChangedHandler;
            Widget.OnCreateAccountButtonClick += OnCreateAccountButtonClickHandler;
            Widget.OnLoginButtonClick += OnLoginButtonClickHandler;
            Widget.OnGoogleSignInButtonClick += OnGoogleSignInButtonClickHandler;
            
            #if UNITY_EDITOR
            CoreMobile.Instance.GesturesManager.OnTabButtonPressed += () => { Widget.ToggleFocus(); };
            #endif
            
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) { }
        
        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnInputFieldValueChanged -= OnInputFieldValueChangedHandler;
            Widget.OnCreateAccountButtonClick -= OnCreateAccountButtonClickHandler;
            Widget.OnLoginButtonClick -= OnLoginButtonClickHandler;
            Widget.OnGoogleSignInButtonClick -= OnGoogleSignInButtonClickHandler;
        }

        #endregion

        #region Private Methods

        private void OnInputFieldValueChangedHandler(InputFieldsTypes type, string value) {
            Widget.SetInputFieldError(type, CoreMobile.Instance.ErrorsManager.ValidateForError(type, value));
        }

        private void OnCreateAccountButtonClickHandler() {
            
            Widget.Deactivate(true);
            
            var firebaseCreateAccountUiWidgetData = new FirebaseCreateAccountUIWidgetData();
            var createAccountWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.FIREBASE_CREATE_ACCOUNT_UI, firebaseCreateAccountUiWidgetData, true);
            
            createAccountWidget.OnDismissed += widget => {
                Widget.Activate(true);    
            };
        }
        
        private void OnLoginButtonClickHandler(string email, string password) {

            var authServiceData = new FirebaseEmailLoginData();
            authServiceData.AddNewDataItem(GlobalVariables.AuthenticationKeys.EMAIL, Widget.GetEmail());
            authServiceData.AddNewDataItem(GlobalVariables.AuthenticationKeys.PASSWORD, Widget.GetPassword());

            CoreMobile.Instance.FirebaseManager.AuthenticationManager.Login(FirebaseAuthenticationTypes.EMAIL, authServiceData);
        }
        
        private void OnGoogleSignInButtonClickHandler() {
            var authServiceData = new FirebaseEmailLoginData();
            CoreMobile.Instance.FirebaseManager.AuthenticationManager.Login(FirebaseAuthenticationTypes.GOOGLE, authServiceData);
            
            #if UNITY_EDITOR
            UnityEditor.ScriptableWizard.DisplayWizard<GoogleSignInHandlerWizard>("Google SignIn", "SignIn", "Cancel");
            #endif
        }

        #endregion
    }
}