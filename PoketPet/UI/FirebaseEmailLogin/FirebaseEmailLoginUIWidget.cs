using System;
using Core.ErrorsHandlerSubsystem.Data;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirebaseEmailLogin {
    public class FirebaseEmailLoginUIWidget : BasicPopupUIWidget {
        public event Action<string, string> OnLoginButtonClick;
        public event Action<InputFieldsTypes, string> OnInputFieldValueChanged;
        public event Action OnCreateAccountButtonClick;
        public event Action OnGoogleSignInButtonClick;

        [Header("Input Fields:")]
        [SerializeField] private TMP_InputField emailField;
        [SerializeField] private TMP_InputField passwordField;

        [Header("Input Fields Status:")]
        [SerializeField] private TextMeshProUGUI emailStatusText;
        [SerializeField] private TextMeshProUGUI passwordStatusText;

        [SerializeField] private Button googleSignInButton;
        [SerializeField] private Button createAccountButton;

        [Header("Colors:")]
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color errorColor = Color.red;
        
        private FirebaseEmailLoginUIWidgetData _widgetData;

        private bool _emailFieldError;
        private bool _passwordFieldError;

        public void Initialize(FirebaseEmailLoginUIWidgetData widgetData) {
            _widgetData = widgetData;

            emailStatusText.text = string.Empty;
            passwordStatusText.text = string.Empty;
            
            emailField.keyboardType = TouchScreenKeyboardType.EmailAddress;
            emailField.characterValidation = TMP_InputField.CharacterValidation.EmailAddress;

            emailField.onSelect.AddListener(delegate(string value) {
                actionButton.interactable = !string.IsNullOrEmpty(value) && !_emailFieldError && !_passwordFieldError;
            });
            
            emailField.onEndEdit.AddListener(delegate(string value) {
                _widgetData.Email = value;
                OnInputFieldValueChanged?.Invoke(InputFieldsTypes.EMAIL, _widgetData.Email);
            });

            passwordField.onSelect.AddListener(delegate(string value) {
                actionButton.interactable = !string.IsNullOrEmpty(value) && !_emailFieldError && !_passwordFieldError;
            });

            passwordField.onEndEdit.AddListener(delegate(string value) {
                _widgetData.Password = value;
                OnInputFieldValueChanged?.Invoke(InputFieldsTypes.PASSWORD, _widgetData.Password);
            });

            createAccountButton.onClick.AddListener(delegate {
                createAccountButton.interactable = false;
                createAccountButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 2, 0.75f).OnComplete(() => {
                    OnCreateAccountButtonClick?.Invoke();
                    createAccountButton.interactable = true;
                });
            });

            actionButton.onClick.AddListener(delegate {
                actionButton.interactable = false;
                actionButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 2, 0.75f).OnComplete(() => {
                    OnLoginButtonClick?.Invoke(emailField.text, passwordField.text);
                    actionButton.interactable = true;
                });
            });

            googleSignInButton.onClick.AddListener(delegate {
                googleSignInButton.interactable = false;
                googleSignInButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 2, 0.75f).OnComplete(() => {
                    OnGoogleSignInButtonClick?.Invoke();
                    googleSignInButton.interactable = true;
                });
            });

            actionButton.interactable = false;
        }

        public string GetEmail() {
            return emailField.text;
        }

        public string GetPassword() {
            return passwordField.text;
        }

        #if UNITY_EDITOR
        public void ToggleFocus() {
            if (emailField.isFocused) {
                EventSystem.current.SetSelectedGameObject(passwordField.gameObject, null);
                passwordField.OnPointerClick(new PointerEventData(EventSystem.current));
            } else if (passwordField.isFocused) {
                EventSystem.current.SetSelectedGameObject(emailField.gameObject, null);
                emailField.OnPointerClick(new PointerEventData(EventSystem.current));
            }
        }
        #endif

        public void SetInputFieldError(InputFieldsTypes type, string error) {
            TMP_InputField  inputField = null;
            TextMeshProUGUI statusText = null;

            switch (type) {
                case InputFieldsTypes.EMAIL:
                    inputField = emailField;
                    statusText = emailStatusText;
                    break;
                case InputFieldsTypes.PASSWORD:
                    inputField = passwordField;
                    statusText = passwordStatusText;
                    break;
            }

            if (string.IsNullOrEmpty(error)) {
                inputField.image.color = Color.clear;

                statusText.color = normalColor;
                statusText.text = string.Empty;

                if (type == InputFieldsTypes.EMAIL) {
                    _emailFieldError = false;
                } else {
                    _passwordFieldError = false;
                }
            } else {
                inputField.image.color = errorColor;

                statusText.color = errorColor;
                statusText.text = error;

                if (type == InputFieldsTypes.EMAIL) {
                    _emailFieldError = true;
                } else {
                    _passwordFieldError = true;
                }
            }

            actionButton.interactable = !_emailFieldError && !string.IsNullOrEmpty(passwordField.text) && !_passwordFieldError;
        }

        public void SetEmailFieldValue(string value, bool silent = true) {
            if (silent) {
                emailField.SetTextWithoutNotify(value);
            } else {
                emailField.text = value;
            }
        }

        public void SetPasswordFieldValue(string value, bool silent = true) {
            if (silent) {
                passwordField.SetTextWithoutNotify(value);
            } else {
                passwordField.text = value;
            }
        }
    }
}