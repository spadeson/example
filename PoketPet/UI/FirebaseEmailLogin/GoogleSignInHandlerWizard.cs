using System;
using UnityEditor;
using UnityEngine;

namespace PoketPet.UI.FirebaseEmailLogin {
    
    #if UNITY_EDITOR
    
    public class GoogleSignInHandlerWizard : ScriptableWizard {

        private static string _code;
        
        [MenuItem("Window/PoketPet/Authentication/Google SignIn")]
        public static void CreateWizard() {
            DisplayWizard<GoogleSignInHandlerWizard>("Google SignIn", "SignIn", "Cancel");
        }

        private void OnEnable() {
            var url = "https://accounts.google.com/o/oauth2/v2/auth?client_id=1092447243135-rqme8huthng09m2dleaofgabd5o3ltne.apps.googleusercontent.com&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&scope=email";
            Application.OpenURL(url);
        }

        public void OnWizardCreate() {
            VIS.CoreMobile.CoreMobile.Instance.GoogleManager.SignIn(_code);
        }

        public void OnWizardUpdate() {

        }

        protected override bool DrawWizardGUI() {

            EditorGUILayout.Space();

            _code = EditorGUILayout.TextField("Code", _code);
            
            EditorGUILayout.Space();
            
            return base.DrawWizardGUI();
        }
    }
    
    #endif
}
