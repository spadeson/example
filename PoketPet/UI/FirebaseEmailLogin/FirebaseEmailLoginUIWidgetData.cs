using System;
using System.Collections.Generic;
using Core.FirebaseSubsystem.Services.Authentication.AuthenticationServices.Services;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirebaseEmailLogin
{
	[Serializable]
	public class FirebaseEmailLoginUIWidgetData : WidgetData {
		public string Email { get; set; }
		public string Password { get; set; }
		
		public List<FirebaseAuthenticationTypes> authenticationTypes;

		public FirebaseEmailLoginUIWidgetData()
		{
		}

		public FirebaseEmailLoginUIWidgetData(string email, string password)
		{
			Email = email;
			Password = password;
		}
	}
}