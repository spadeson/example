using PoketPet.EventsSubsystem;
using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DeveloperMode {
    public class DeveloperModeUIWidgetController : WidgetControllerWithData<DeveloperModeUIWidget, DeveloperModeUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClicked += OnCloseButtonClickedHandler;
            Widget.OnEventButtonClicked += OnEventButtonClickedHandler;
            
            Widget.Initialize(WidgetData);
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) { }

        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClicked -= OnCloseButtonClickedHandler;
            Widget.OnEventButtonClicked -= OnEventButtonClickedHandler;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }
        
        private void OnEventButtonClickedHandler(EventTypes type, int index, string id) {
            FindObjectOfType<EventsManager>().ExecuteEvent(type, index);
            Widget.Dismiss();
        }

        private void OnCloseButtonClickedHandler() {
            Widget.Dismiss();
        }
    }
}