using System;
using PoketPet.EventsSubsystem;
using PoketPet.UI.Messages;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.DeveloperMode {
    public class DeveloperUiEventButton : MonoBehaviour {
        
        public event Action<EventTypes, int, string> OnButtonClick;
        
        [SerializeField] private Button button;
        [SerializeField] private TextMeshProUGUI buttonLabel;

        private EventTypes _eventType;
        private int _eventIndex;
        private string _eventId;

        public void Initialize(EventTypes eventType, int eventIndex, string eventId) {
            _eventType = eventType;
            _eventIndex = eventIndex;
            _eventId = eventId;
            
            if (button == null) {
                button = GetComponent<Button>();
            }

            buttonLabel.text = eventIndex.ToString("00");

            button.onClick.AddListener(OnButtonClickHandler);
        }
        
        public void Dismiss() {
            button.onClick.RemoveAllListeners();
        }

        private void OnButtonClickHandler() {
            OnButtonClick?.Invoke(_eventType, _eventIndex, _eventId);
        }
    }
}
