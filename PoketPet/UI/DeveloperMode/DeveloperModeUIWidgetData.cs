using System;
using System.Collections.Generic;
using PoketPet.EventsSubsystem;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DeveloperMode
{
	[Serializable]
	public class DeveloperModeUIWidgetData : WidgetData {
		public List<EventConfig> eatEventsConfigsList = new List<EventConfig>();
		public List<EventConfig> walkEventConfigsList = new List<EventConfig>();
	}
}