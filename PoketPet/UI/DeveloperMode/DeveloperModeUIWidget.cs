using System;
using System.Collections.Generic;
using PoketPet.EventsSubsystem;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.DeveloperMode {
    public class DeveloperModeUIWidget : BaseUIWidget {
        
        private DeveloperModeUIWidgetData _widgetData;
        
        public event Action<EventTypes, int, string> OnEventButtonClicked;
        public event Action OnCloseButtonClicked;

        [SerializeField] private GameObject eventUiButtonPrefab;

        [SerializeField] private RectTransform eatEventButtonsContainer;
        [SerializeField] private RectTransform walkEventButtonsContainer;

        [SerializeField] private Button closeButton;

        public override void Activate(bool animated) {
            base.Activate(animated);

            closeButton.onClick.AddListener(delegate {
                closeButton.onClick.RemoveAllListeners();
                OnCloseButtonClicked?.Invoke();
            });
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(DeveloperModeUIWidgetData widgetData) {
            _widgetData = widgetData;
            RemoveEventsButtons();
            
            //Create Eat Buttons
            CreateNewMessageUiElement(widgetData.eatEventsConfigsList);
            
            //Remove Walk Buttons
            CreateNewMessageUiElement(widgetData.walkEventConfigsList);
        }
        
        private void RemoveEventsButtons() {
            //Remove Eat Buttons
            for (var i = 0; i < eatEventButtonsContainer.childCount; i++) {
                var messageUiElement = eatEventButtonsContainer.GetChild(i);
                messageUiElement.GetComponent<DeveloperUiEventButton>().Dismiss();
                Destroy(messageUiElement.gameObject);
            }
            
            //Remove Walk Buttons
            for (var i = 0; i < walkEventButtonsContainer.childCount; i++) {
                var messageUiElement = walkEventButtonsContainer.GetChild(i);
                messageUiElement.GetComponent<DeveloperUiEventButton>().Dismiss();
                Destroy(messageUiElement.gameObject);
            }
        }
        
        private void CreateNewMessageUiElement(List<EventConfig> eventsConfigsList) {

            for (var i = 0; i < eventsConfigsList.Count; i++) {
                var config = eventsConfigsList[i];

                RectTransform parent;
                
                switch (config.eventType) {
                    case EventTypes.EAT:
                        parent = eatEventButtonsContainer;
                        break;
                    case EventTypes.WALK:
                        parent = walkEventButtonsContainer;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
                var newEventUiButtonGo = Instantiate(eventUiButtonPrefab, parent, false);

                var newEventUiButton = newEventUiButtonGo.GetComponent<DeveloperUiEventButton>();

                newEventUiButton.Initialize(config.eventType, i, config.eventId);

                newEventUiButton.OnButtonClick += (type, index, id) => {
                    OnEventButtonClicked?.Invoke(type, index, id);
                };
            }
        }
    }
}