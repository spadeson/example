using System;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedOwnerApprove
{
	[Serializable]
	public class TaskCompletedOwnerApproveUIWidgetData : WidgetData {
		public Sprite photo;
		public string userId;
		public string messageId;
	}
}