using System;
using PoketPet.Family;
using PoketPet.Photo;
using PoketPet.UI.TaskDisplay;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Core.SpriteIcons.Manager;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SpriteIconsGroupKeys;

namespace PoketPet.UI.TaskCompletedOwnerApprove
{
	public class TaskCompletedOwnerApproveUIWidget : BasicPopupUIWidget	{
		
		[Header("buttons:"), SerializeField]  protected Button rejectButton;
		[Header("Photo:"), SerializeField] private Image photoImage;
		[Header("Avatar:"), SerializeField] private Image avatarImage;
		[Header("Text"), SerializeField] private TextMeshProUGUI nicknameField;

		public event Action OnCloseButtonClick;
		public event Action<bool> OnActionButtonClick;
		
		private static SpriteIconsManager IconManager => CoreMobile.Instance.SpritesIconsManager;

		private TaskCompletedOwnerApproveUIWidgetData _widgetData;
		public override void Create() {
			base.Create();
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			base.Dismiss();
		}
		
		public void Initialize(TaskCompletedOwnerApproveUIWidgetData widgetData) {
			_widgetData = widgetData;
			photoImage.sprite = widgetData.photo;
			closeButton.onClick.AddListener(delegate {
				OnCloseButtonClick?.Invoke();
			});
			actionButton.onClick.AddListener(delegate {
				OnActionButtonClick?.Invoke(true);
			});
			rejectButton.onClick.AddListener(delegate {
				OnActionButtonClick?.Invoke(false);
			});
			
			var family = FamilyProfileManager.GetFamiliesAsList();
			var user = family.Find(item => item.id == _widgetData.userId);
			AddAvatar(user);
		}
		
		private void AddAvatar(UserModel user) {
			nicknameField.text = user.nickname;

			avatarImage.sprite = IconManager.GetSpriteIconByKey(ASSIGNEE_ICONS, user.FamiliesRole);
			PhotoManager.GetAvatarById(user.avatar, sprite => {
				avatarImage.sprite = sprite;
			});
		}

	}
}