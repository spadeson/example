using PoketPet.Messages;
using PoketPet.Task;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedOwnerApprove
{
	public class TaskCompletedOwnerApproveUIWidgetController : WidgetControllerWithData<TaskCompletedOwnerApproveUIWidget, TaskCompletedOwnerApproveUIWidgetData>	{

		protected override void OnWidgetCreated(Widget widget) {
			Widget.OnActionButtonClick += WidgetOnActionButtonClickHandler;
			Widget.OnCloseButtonClick += WidgetOnCloseButtonClickHandler;
			Widget.Initialize(WidgetData);
		}

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }
		
		private void WidgetOnCloseButtonClickHandler() {
			Widget.Dismiss();
		}

		protected override void OnWidgetDismissed(Widget widget) {
			Widget.OnActionButtonClick -= WidgetOnActionButtonClickHandler;
			Widget.OnCloseButtonClick -= WidgetOnCloseButtonClickHandler;
		}

		private void WidgetOnActionButtonClickHandler(bool isConfirm) {
			MessageManager.CloseMessageById(WidgetData.messageId, isConfirm);
			Widget.Dismiss();   
		}
	}
}