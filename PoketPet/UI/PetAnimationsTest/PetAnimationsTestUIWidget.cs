using System;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetAnimationsTest {
    public class PetAnimationsTestUIWidget : BaseUIWidget {
        public event Action<string> OnWidgetButtonClicked;

        [SerializeField] private Button _idleButton;
        [SerializeField] private Button _walkButton;

        public override void Create() {
            base.Create();
            _idleButton.onClick.AddListener(OnIdleButtonClickHandler);
            _walkButton.onClick.AddListener(OnWalkButtonClickHandler);
        }


        public override void Dismiss() {
            _idleButton.onClick.RemoveAllListeners();
            _walkButton.onClick.RemoveAllListeners();

            base.Dismiss();
        }

        private void OnIdleButtonClickHandler() {
            OnWidgetButtonClicked?.Invoke("idle");
        }

        private void OnWalkButtonClickHandler() {
            OnWidgetButtonClicked?.Invoke("walk");
        }
    }
}