using PoketPet.Pets.Controllers;
using PoketPet.Pets.Controllers.AnimatorController;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetAnimationsTest {
    public class PetAnimationsTestUIWidgetController : WidgetControllerWithData<PetAnimationsTestUIWidget, PetAnimationsTestUIWidgetData> {
        
        [SerializeField] private PetAnimatorController petAnimatorController;

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnWidgetButtonClicked += OnAnimatorButtonClickedHandler;
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnWidgetButtonClicked -= OnAnimatorButtonClickedHandler;
        }

        private void OnAnimatorButtonClickedHandler(string trigger) {
            petAnimatorController.SetTrigger(trigger);
        }
    }
}