using System;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Progress {
    [Serializable]
    public class ProgressUIWidgetData : WidgetData {
        public string title;
        public string description;
    }
}