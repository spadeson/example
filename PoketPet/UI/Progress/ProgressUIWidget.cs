using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using PoketPet.UI.GenericComponents.Spinner;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.Progress {
    public class ProgressUIWidget : BasicPopupUIWidget {

        [Header("Progress Image")]
        [SerializeField] private SpinnerWidget spinnerWidget;

        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;
        
        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        public override void Activate(bool animated) {
            base.Activate(animated);
            ShowAnimation();
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
            HideAnimation();
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        public void Initialize(ProgressUIWidgetData widgetData) {
            titleText.text = widgetData.title;
            descriptionText.text = widgetData.description;
            
            spinnerWidget.Initialize();
        }
        
        
        private void ShowAnimation() {
            spinnerWidget.StartAnimation();
        }
        
        private void HideAnimation() {
            spinnerWidget.StopAnimation();
        }
    }
}