using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Progress {
    public class ProgressUIWidgetController : WidgetControllerWithData<ProgressUIWidget, ProgressUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
