using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial08 {
    public class NewTutorial08UIWidget : BasicPopupUIWidget {
        public event Action OnActionButtonClick;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        [Header("Photo Image")]
        [SerializeField] private Image photoImage;

        private NewTutorial08UIWidgetData _widgetData;

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(false);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        #endregion

        #region Public Methods

        public void Initialize(NewTutorial08UIWidgetData widgetData) {

            _widgetData = widgetData;
            
            descriptionText.text = _widgetData.TutorialSingleStepData.description;
			
            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                actionButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                    OnActionButtonClick?.Invoke();
                });
            });
        }

        #endregion
    }
}