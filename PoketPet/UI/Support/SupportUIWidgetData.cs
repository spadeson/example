using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Support {
    [Serializable]
    public class SupportUIWidgetData : WidgetData {
        public string title;
        public string message;
    }
}