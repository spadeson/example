using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Support {
    public class SupportUIWidgetController : WidgetControllerWithData<SupportUIWidget, SupportUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.OnSendButtonClick += WidgetOnSendButtonClick;
            
            Widget.Initialize(WidgetData);
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
            
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
            Widget.OnSendButtonClick -= WidgetOnSendButtonClick;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }
        
        private void WidgetOnCloseButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.Dismiss();
        }
        
        private void WidgetOnSendButtonClick(SupportUIWidgetData obj) {
            //TODO: Send user feedback to the backend and than send it with e-mail
        }
    }
}