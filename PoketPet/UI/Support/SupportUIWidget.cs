using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Support {
    public class SupportUIWidget : BasicPopupUIWidget {
        
        public event Action OnCloseButtonClick;
        public event Action<SupportUIWidgetData> OnSendButtonClick;

        [Header("Form")]
        [SerializeField] private TMP_InputField titleTextField;
        [SerializeField] private TMP_InputField messageTextField;

        private SupportUIWidgetData _widgetData;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            titleTextField.onValueChanged.RemoveAllListeners();
            messageTextField.onValueChanged.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize(SupportUIWidgetData data) {
            _widgetData = data;
            
            closeButton.onClick.AddListener(CloseButtonClickHandler);
            actionButton.onClick.AddListener(SendButtonClickHandler);

            titleTextField.onValueChanged.AddListener(OnTitleValueChanged);
            messageTextField.onValueChanged.AddListener(OnMessageValueChanged);
        }


        private void OnTitleValueChanged(string value) {
            _widgetData.title = value;
        }
        
        private void OnMessageValueChanged(string value) {
            _widgetData.message = value;
        }

        private void CloseButtonClickHandler() {
            closeButton.onClick.RemoveAllListeners();
            closeButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnCloseButtonClick?.Invoke();
            });
        }
        
        private void SendButtonClickHandler() {
            actionButton.onClick.RemoveAllListeners();
            actionButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnSendButtonClick?.Invoke(_widgetData);
            });
        }
    }
}