using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;

namespace PoketPet.UI.VetChatTutorial {
    public class VetChatTutorialUIWidget : BasicPopupUIWidget {
        public event Action OnActionButtonClick;
        
        public override void Create() {
            base.Create();
            actionButton.onClick.AddListener(delegate {
                OnActionButtonClick?.Invoke();
            });
        }

        public override void Dismiss() {
            actionButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }
    }
}