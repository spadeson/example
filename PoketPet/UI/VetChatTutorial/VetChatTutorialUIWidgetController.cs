using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChatTutorial {
    public class VetChatTutorialUIWidgetController : WidgetControllerWithData<VetChatTutorialUIWidget, VetChatTutorialUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void WidgetOnActionButtonClick() {
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.VET_CHAT_UI, animate: true);
            CoreMobile.Instance.LocalDataManager.SaveLocalDataBoolean(GlobalVariables.LocalDataKeys.FIRST_TIME_VET_CHAT, false);
            Widget.Dismiss();
        }
    }
}