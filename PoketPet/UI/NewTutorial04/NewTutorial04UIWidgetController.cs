using PoketPet.Global;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.TutorialKeys;

namespace PoketPet.UI.NewTutorial04 {
    public class NewTutorial04UIWidgetController : WidgetControllerWithData<NewTutorial04UIWidget, NewTutorial04UIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.OnAgeButtonClick += WidgetOnAgeButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
            Widget.OnAgeButtonClick -= WidgetOnAgeButtonClick;
        }

        private void WidgetOnActionButtonClick() {
            UserAge = Widget.GetUserAge();
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            Widget.Dismiss();
        }
        
        private void WidgetOnAgeButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
        }
    }
}