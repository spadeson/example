using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;

namespace PoketPet.UI.NewTutorial04 {
    public class NewTutorial04UIWidget : BasicPopupUIWidget {
        public event Action OnActionButtonClick;
        public event Action OnAgeButtonClick;

        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        [Header("Age Widget")]
        [SerializeField] private TutorialUserAgeWidget tutorialUserAgeWidget;

        private NewTutorial04UIWidgetData _widgetData;

        #region BaseUIWidget

        public override void Create() {
            base.Create();
            actionButton.onClick.AddListener(ActionButtonClickHandler);
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        #endregion

        #region Public Methods

        public void Initialize(NewTutorial04UIWidgetData widgetData) {
            _widgetData = widgetData;
            tutorialUserAgeWidget.Initialize(12);
            tutorialUserAgeWidget.OnAgeButtonClick += TutorialUserAgeWidgetOnAgeButtonClick;
        }

        public int GetUserAge() {
            return tutorialUserAgeWidget.GetUserAge();
        }

        #endregion

        #region Private Methods

        private void ActionButtonClickHandler() {
            actionButton.onClick.RemoveAllListeners();
            actionButton.targetGraphic.rectTransform.DOPunchScale(Vector3.one * 0.35f, 0.25f, 2, 0.75f).OnComplete(() => { OnActionButtonClick?.Invoke(); });
        }

        private void TutorialUserAgeWidgetOnAgeButtonClick() {
            OnAgeButtonClick?.Invoke();
        }

        #endregion
    }
}