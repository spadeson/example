using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial04 {
    [Serializable]
    public class NewTutorial04UIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}