using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial04 {
    public class TutorialUserAgeWidget : MonoBehaviour {

        public event Action OnAgeButtonClick;

        private int _currentUserAge;

        private const int LOWER_AGE_LIMIT = 1;
        private const int UPPER_AGE_LIMIT = 99;
        
        [Header("Age Text")]
        [SerializeField] private TextMeshProUGUI ageText;

        [Header("Ones Buttons")]
        [SerializeField] private Button oncePlusButton;

        [SerializeField] private Button onceMinusButton;

        private const float BUTTON_ANIMATION_DURATION = 0.15f;
        private const float BUTTON_ANIMATION_SCALE = 0.25f;

        #region MonoBehaviour

        private void OnEnable() {
            oncePlusButton.onClick.AddListener(PlusButtonClickHandler);
            onceMinusButton.onClick.AddListener(MinusButtonClickHandler);
        }

        private void OnDisable() {
            oncePlusButton.onClick.RemoveAllListeners();
            onceMinusButton.onClick.RemoveAllListeners();
        }

        #endregion

        #region Public Methods

        public void Initialize(int userAge) {
            _currentUserAge = userAge;
            UpdateUserAge(_currentUserAge);
        }
        
        public int GetUserAge() {
            return _currentUserAge;
        }

        #endregion

        #region Private Methods

        private void PlusButtonClickHandler() {
            if (_currentUserAge >= UPPER_AGE_LIMIT) return;
            
            _currentUserAge++;

            oncePlusButton.transform.DOPunchScale(Vector3.one * BUTTON_ANIMATION_SCALE, BUTTON_ANIMATION_DURATION, 4, 0.5f).OnStart(() => {
                oncePlusButton.interactable = false;
            }).OnComplete(() => {
                oncePlusButton.interactable = true;
            });
            
            UpdateUserAge(_currentUserAge);
        }

        private void MinusButtonClickHandler() {
            if (_currentUserAge <= LOWER_AGE_LIMIT) return;
            
            _currentUserAge--;
            
            onceMinusButton.transform.DOPunchScale(Vector3.one * BUTTON_ANIMATION_SCALE, BUTTON_ANIMATION_DURATION, 4, 0.5f).OnStart(() => {
                onceMinusButton.interactable = false;
            }).OnComplete(() => {
                onceMinusButton.interactable = true;
            });
            
            UpdateUserAge(_currentUserAge);
        }

        private void UpdateUserAge(int userAge) {
            ageText.rectTransform.DOScale(1.25f, 0.15f).SetEase(Ease.OutBounce).OnComplete(() => { ageText.rectTransform.localScale = Vector3.one; });
            ageText.text = $"{userAge:00}";
            OnAgeButtonClick?.Invoke();
        }

        #endregion
    }
}
