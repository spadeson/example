using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding;
using UnityEngine;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.TutorialKeys;

namespace PoketPet.UI.TutorialUserAge {
    public class TutorialUserAgeUIWidgetController : WidgetControllerWithData<TutorialUserAgeUIWidget, TutorialUserAgeUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }
        
        private void WidgetOnActionButtonClick() {
            var age = Widget.getUserAge();
            var nextTutorialType = age < 18 ? CHILD : ADULT;
            //OnboardingTutorialManager.MoveToNextTutorialNode(nextTutorialType);
            Widget.Dismiss();
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}