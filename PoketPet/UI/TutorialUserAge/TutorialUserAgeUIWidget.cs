using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialUserAge {
    public class TutorialUserAgeUIWidget : BaseUIWidget {
        public event Action OnActionButtonClick;
        
        [Header("Shroud Image")]
        [SerializeField] private Image shroudImage;

        [Header("Container")]
        [SerializeField] private RectTransform container;
        
        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;
        
        [Header("Age Text")]
        [SerializeField] private TextMeshProUGUI ageText;
        
        [Header("Ones Buttons")]
        [SerializeField] private Button oncePlusButton;
        [SerializeField] private Button onceMinusButton;
        
        [Header("Tens Buttons")]
        [SerializeField] private Button tensPlusButton;
        [SerializeField] private Button tensMinusButton;
        
        [Header("Action Button Label")]
        [SerializeField] private TextMeshProUGUI actionButtonLabel;

        [Header("Action Button")]
        [SerializeField] private Button actionButton;

        private int _currentUserAge;

        private TutorialUserAgeUIWidgetData _widgetData;

        #region BaseUIWidget

        public override void Activate(bool animated) {
            base.Activate(animated);

            oncePlusButton.onClick.AddListener(OncePlusButtonClickHandler);
            onceMinusButton.onClick.AddListener(OnceMinusButtonClickHandler);
            
            tensPlusButton.onClick.AddListener(TensPlusButtonClickHandler);
            tensMinusButton.onClick.AddListener(TensMinusButtonClickHandler);
            
            actionButton.onClick.AddListener(ActionButtonClickHandler);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            oncePlusButton.onClick.RemoveAllListeners();
            onceMinusButton.onClick.RemoveAllListeners();
            
            tensPlusButton.onClick.RemoveAllListeners();
            tensMinusButton.onClick.RemoveAllListeners();
            
            actionButton.onClick.RemoveAllListeners();
            
            HideAnimatedWidget();
        }

        #endregion

        private void OnEnable() {
            SetInitialStates();
        }


        public void Initialize(TutorialUserAgeUIWidgetData widgetData) {
            _widgetData = widgetData;
            ShowAnimatedWidget();
        }

        public int getUserAge() {
            return _currentUserAge;
        }

        private void OncePlusButtonClickHandler() {
            if(_currentUserAge < 99) _currentUserAge++;
            UpdateUserAge(_currentUserAge);
        }
        
        private void OnceMinusButtonClickHandler() {
            if(_currentUserAge > 0) _currentUserAge--;
            UpdateUserAge(_currentUserAge);
        }
        
        private void TensPlusButtonClickHandler() {
            if(_currentUserAge < 89) _currentUserAge += 10;
            UpdateUserAge(_currentUserAge);
        }
        
        private void TensMinusButtonClickHandler() {
            if(_currentUserAge > 10) _currentUserAge -= 10;
            UpdateUserAge(_currentUserAge);
        }

        private void UpdateUserAge(int userAge) {
            ageText.text = $"{userAge:00}";
            if(_widgetData != null) _widgetData.userAge = userAge;
        }

        private void ActionButtonClickHandler() {
            actionButton.onClick.RemoveAllListeners();
            actionButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnActionButtonClick?.Invoke();
            });
        }

        private void SetInitialStates() {
            shroudImage.DOFade(0, 0);
            container.DOScale(0, 0f);
        }
        
        private void ShowAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(canvasGroup.DOFade(1, 0));
            sequence.Join(shroudImage.DOFade(0.75f, 0.25f).SetEase(Ease.OutFlash));
            sequence.Join(container.DOScale(1f, 0.2f).SetEase(Ease.OutBounce));
        }

        private void HideAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(0, 0.15f).SetEase(Ease.InBounce));
            sequence.Append(shroudImage.DOFade(0, 0.25f).SetEase(Ease.OutFlash));
            sequence.OnComplete(() => { base.Dismiss(); });
        }
    }
}