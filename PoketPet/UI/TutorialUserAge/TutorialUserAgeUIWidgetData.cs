using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialUserAge {
    [Serializable]
    public class TutorialUserAgeUIWidgetData : WidgetData {
        
        public TutorialSingleStepModelData tutorialSingleStepData;
        public int userAge;
    }
}