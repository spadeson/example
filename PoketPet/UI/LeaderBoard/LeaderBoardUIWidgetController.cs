using PoketPet.Global;
using PoketPet.LeaderBoard;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Schedule;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.LeaderBoard
{
	public class LeaderBoardUIWidgetController : WidgetControllerWithData<LeaderBoardUIWidget, LeaderBoardUIWidgetData>	{

		protected override void OnWidgetCreated(Widget widget) {
			Widget.OnCloseButtonClick += WidgetOnOnCloseButtonClicked;
			LeaderBoardManager.OnUpdate += OnLeaderBoardUpdated;
			LeaderBoardManager.GetLeaderBoard();
			Widget.Initialize(WidgetData);
            
			CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
		}

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) {
			LeaderBoardManager.OnUpdate -= OnLeaderBoardUpdated;
			Widget.OnCloseButtonClick -= WidgetOnOnCloseButtonClicked;
			CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
		}
		
		private void OnLeaderBoardUpdated() {
			Widget.UpdateList();
		}
		
		private void WidgetOnOnCloseButtonClicked() {
			CoreMobile.Instance.UIManager.ActivateWidgetByType("MAIN_SCREEN_UI");
			Widget.Dismiss();
		}
	}
}