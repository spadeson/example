﻿using PoketPet.LeaderBoard;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class LeaderBoardPresenter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI idLabel;
    [SerializeField] private TextMeshProUGUI scoreLabel;
    [SerializeField] private TextMeshProUGUI nameLabel;
    [SerializeField] private Image outline;
    
    public void Dismiss() {
        
    }

    public void Init(LeaderBoardItemModel data) {
        scoreLabel.text = data.score;
        nameLabel.text = data.nickname;
        
        outline.color = UserProfileManager.CurrentUser.id == data.id ? Color.white : Color.clear;
    }

    public void SetId(int id) {
        idLabel.text = id.ToString();
    }
}
