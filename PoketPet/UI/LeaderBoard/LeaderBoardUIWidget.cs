using System;
using PoketPet.LeaderBoard;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.LeaderBoard
{
	public class LeaderBoardUIWidget : BaseUIWidget	{
		public event Action OnCloseButtonClick;
		
		[Header("Close Button")]
		[SerializeField] private Button closeButton;

		[Header("Element Item")]
		[SerializeField] private RectTransform uiItemContainer;
        
		[Header("LeaderBoard UI Item")]
		[SerializeField] private GameObject uiItemPrefab;
		
		[Header("Masks")]
		[SerializeField] private RectTransform topMask;
		[SerializeField] private RectTransform bottomMask;
		[SerializeField] private ScrollRect scrollRect;
		
		private LeaderBoardUIWidgetData _widgetData;

		public override void Create() {
			base.Create();
			closeButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });
			scrollRect.onValueChanged.AddListener(UpdateMasksView);
		}

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			closeButton.onClick.RemoveAllListeners();
			scrollRect.onValueChanged.RemoveAllListeners();
			base.Dismiss();
		}

		public void Initialize(LeaderBoardUIWidgetData widgetData) {
			_widgetData = widgetData;
		}

		public void UpdateList() {
			RemoveLeaderBordPresenters();
			CreateLeaderBordPresenters();
			//CreateLeaderBordPresenters();
		}

		private void CreateLeaderBordPresenters() {
			for (var i = 0; i < LeaderBoardManager.LeaderBoardItems.Count; i++) {
				var newPresenterUiElementGo = Instantiate(uiItemPrefab, uiItemContainer, false);

				var newLeaderBoardUiItem = newPresenterUiElementGo.GetComponent<LeaderBoardPresenter>();
				newLeaderBoardUiItem.Init(LeaderBoardManager.LeaderBoardItems[i]);
				newLeaderBoardUiItem.SetId(i + 1);
			}
		}

		private void RemoveLeaderBordPresenters() {
			for (var i = 0; i < uiItemContainer.childCount; i++) {
				var messageUiElement = uiItemContainer.GetChild(i);
				messageUiElement.GetComponent<LeaderBoardPresenter>().Dismiss();
				Destroy(messageUiElement.gameObject);
			}
		}

		private void UpdateMasksView(Vector2 value) {
			var valRounded = Math.Round(value.y, 2);
			topMask.gameObject.SetActive(valRounded < 1);
			bottomMask.gameObject.SetActive(valRounded > 0);
		}
	}
}