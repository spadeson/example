using System;
using Core.Utilities;
using Firebase.Analytics;
using PoketPet.Global;
using PoketPet.User;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChat {
    public class VetChatUIWidgetController : WidgetControllerWithData<VetChatUIWidget, VetChatUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.OnSendButtonClick += WidgetOnSendButtonClick;
            Widget.Initialize();

            Parameter[] analyticsParameters = {
                new Parameter("user", UserProfileManager.CurrentUser.id),
                new Parameter("time", TimeUtil.GetTimestamp())
            };
            
            CoreMobile.Instance.AnalyticsManager.LogEvent("opened_vet_chat", analyticsParameters);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
            Widget.OnSendButtonClick -= WidgetOnSendButtonClick;
        }

        private void WidgetOnCloseButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.Dismiss();
        }

        private void WidgetOnSendButtonClick() {
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.VET_CHAT_SEEKING_UI, animate: true);
        }
    }
}