using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChat {
    public class VetChatUIWidget : BaseUIWidget {

        public event Action OnSendButtonClick;
        public event Action OnCloseButtonClick;
        
        [Header("Title")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Close Button")]
        [SerializeField] private Button closeButton;
        
        [Header("Send Button")]
        [SerializeField] private Button sendButton;

        [Header("Main Text")]
        [SerializeField] private TextMeshProUGUI mainText;

        [Header("Text Area Input")]
        [SerializeField] private TMP_InputField messageInputField;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            closeButton.onClick.RemoveAllListeners();
            sendButton.onClick.RemoveAllListeners();
            messageInputField.onValueChanged.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize() {
            closeButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });
            sendButton.onClick.AddListener(delegate { OnSendButtonClick?.Invoke(); });
            messageInputField.onValueChanged.AddListener(delegate(string message) {
                sendButton.interactable = !string.IsNullOrEmpty(message);
            });
            
            sendButton.interactable = !string.IsNullOrEmpty(messageInputField.text);
        }
    }
}