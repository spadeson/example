﻿using System;
using System.Globalization;
using DG.Tweening;
using PoketPet.Network.API.RESTModels.Tasks;
using PoketPet.Schedule;
using PoketPet.UI.PetScheduleTaskEditor;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile;
using static PoketPet.Global.GlobalVariables.SpriteIconsGroupKeys;
using static PoketPet.Global.GlobalVariables.ScheduleKeys;
using static PoketPet.Global.GlobalVariables.LocalDataKeys;


namespace PoketPet.UI.PetSchedule {
    public class ScheduleUIElement : MonoBehaviour {

        public event Action<PetScheduleTaskEditorUIWidgetData> OnEditButtonClick;
        
        [SerializeField] private Image icon;
        [SerializeField] private Button editButton;
        [SerializeField] private Image iconStatus;
        [SerializeField] private Image line;
        [SerializeField] private TextMeshProUGUI timeLabel;
        [SerializeField] private GameObject buttonContainer;
        
        [Header("Colors:")]
        [SerializeField] private Color expiredColor;
        [SerializeField] private Color unusedColor;

        //private FlowResponseData _data;
        //private string _taskType;
        //private int _taskTime;
        //private DateTime _localDateTime;

        private TaskScheduleModel taskData;
        
        #region Public Methods

        /// <summary>
        /// Initialize element. Set schedule data. Count current tasks local time.
        /// </summary>
        /// <param name="data">Schedule element data.</param>
        /// <param name="time">Task element's time in minutes.</param>
        public void Initialize(TaskScheduleModel data) {
            taskData = data;
            _setTime(taskData.EveryDayLocalTime);
            _setIcon(taskData.Type);
            _setStatus(taskData.CompletedStatus);

            /*var timeSpan = TimeSpan.FromMinutes(time);
            var utcDate = new DateTime(timeSpan.Ticks);
            _localDateTime = Core.Utilities.TimeUtil.ConvertUtcToLocal(utcDate);*/
        }

        /// <summary>
        /// Set position task element in widget container.
        /// </summary>
        /// <param name="time">Task element's time in minutes.</param>
        /// <param name="minTime">Container's minimal anchor position.</param>
        /// <param name="maxTime">Container's maximal anchor position.</param>
        /// <param name="column">Task element's column.</param>
        public void SetPosition(float minTime, float maxTime, int column) {
            //var time = _localDateTime.Hour * 60 + _localDateTime.Minute;
            GetComponent<RectTransform>().anchorMax = new Vector2(0.5f + 0.25f * column, 1 - (taskData.EveryDayLocal - minTime) / (maxTime - minTime));
            GetComponent<RectTransform>().anchorMin = new Vector2(0f, 1 - (taskData.EveryDayLocal - minTime) / (maxTime - minTime));
        }

        public void EditMode(bool isEditMode) {
            if (isEditMode) {
                buttonContainer.transform.DOShakeRotation(4f, new Vector3(0, 0, 40), randomness:20)
                    .SetLoops(-1, LoopType.Restart).SetEase(Ease.Linear);
                editButton.enabled = true;
                editButton.onClick.AddListener(OnEditButtonClickHandler);
            }
            else {
                buttonContainer.transform.DOKill();
                editButton.enabled = false;
                editButton.onClick.RemoveAllListeners();
            }
        }

        #endregion
        
        #region Private Methods
        
        /// <summary>
        /// Set icon task element by type.
        /// </summary>
        /// <param name="taskType">Task element's type.</param>
        private void _setIcon(string taskType) {
            var taskIconSprite = CoreMobile.Instance.SpritesIconsManager.GetSpriteIconByKey(TASKS_ICONS, taskType);
            icon.sprite = taskIconSprite;
        }
        
        private void _setTime(DateTime time) {
            var timeFormat = UserProfileManager.CurrentUser.settings.time_format == MILITARY_TIME ? "HH:mm" : "hh:mm tt";
            timeLabel.text = time.ToString(timeFormat, CultureInfo.CreateSpecificCulture("en-US"));
        }
        
        private void _setStatus(string value) {
            if (value == STATUS_UNUSED) {
                iconStatus.enabled = false;
            }
            else {
                iconStatus.enabled = true;
                var iconManager = CoreMobile.Instance.SpritesIconsManager;
                var statusSprite = iconManager.GetSpriteIconByKey(TASKS_STATUS, value);
                iconStatus.sprite = statusSprite;
            }
        }
        
        private void OnEditButtonClickHandler() {
            var data = new PetScheduleTaskEditorUIWidgetData {
                TaskType = taskData.Type,
                TaskTime = taskData.EveryDayUtc,
                TaskId = taskData.Id,
                TaskSchedule = taskData.EveryDayList
            };
            OnEditButtonClick?.Invoke(data);
        }
        
        #endregion
    }
}
