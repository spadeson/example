using System;
using System.Linq;
using PoketPet.Global;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Schedule;
using PoketPet.UI.FirstRunGameInvites;
using PoketPet.UI.FirstRunGameOnboarding;
using PoketPet.UI.PetScheduleTaskCreator;
using PoketPet.UI.PetScheduleTaskEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.UiKeys;

namespace PoketPet.UI.PetSchedule {
    public class PetScheduleUIWidgetController : WidgetControllerWithData<PetScheduleUIWidget, PetScheduleUIWidgetData> {
        private float _minTaskTime;
        private float _maxTaskTime;

        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnOnCloseButtonClicked;
            Widget.OnEditButtonClick += WidgetOnEditButtonClicked;
            Widget.OnApplyButtonClick += WidgetOnApplyButtonClicked;
            Widget.OnEditTaskButtonClick += WidgetOnTaskEditButtonClicked;
            Widget.OnAddNewButtonClick += WidgetOnAddNewButtonClicked;
            ScheduleManager.OnUpdate += UpdateTasks;
            Widget.Initialize(WidgetData);
            UpdateTasks();
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        protected override void OnWidgetDeactivated(Widget widget) {
            
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnOnCloseButtonClicked;
            Widget.OnEditButtonClick -= WidgetOnEditButtonClicked;
            Widget.OnApplyButtonClick -= WidgetOnApplyButtonClicked;
            Widget.OnEditTaskButtonClick -= WidgetOnTaskEditButtonClicked;
            Widget.OnAddNewButtonClick -= WidgetOnAddNewButtonClicked;
            ScheduleManager.OnUpdate -= UpdateTasks;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(BURGER_MENU_UI, this);
        }

        private void UpdateTasks() {
            CoreMobile.Instance.UIManager.DismissWidgetByType(PROGRESS_UI);
            GetMinAndMaxTasksTime();
            Widget.ShowTasks(ScheduleManager.Tasks, _minTaskTime, _maxTaskTime);
            SetCurrentTime();
            
            // show it if it's first users run
            if (WidgetData.isFirstRun) {
                Widget.SwitchTaskEditorState(true);
            }
        }

        private void GetMinAndMaxTasksTime() {
            _minTaskTime = float.MaxValue;
            _maxTaskTime = 0;

            foreach (var task in ScheduleManager.Tasks.SelectMany(listTasksByType => listTasksByType)) {
                _minTaskTime = _minTaskTime > task.EveryDayLocal ? task.EveryDayLocal : _minTaskTime;
                _maxTaskTime = _maxTaskTime < task.EveryDayLocal ? task.EveryDayLocal : _maxTaskTime;
            }

            if (!(Math.Abs(_minTaskTime - _maxTaskTime) < 1)) return;
            _minTaskTime -= _minTaskTime;
            _maxTaskTime += _maxTaskTime;
        }

        private void SetCurrentTime() {
            var minuteNow = DateTime.Now.Hour * 60 + DateTime.Now.Minute - _minTaskTime;
            var deltaTime = _maxTaskTime - _minTaskTime;
            var value     = 1 - minuteNow / deltaTime;
            if (value > 1) value = 1;
            if (value < 0) value = 0;
            Widget.SetTimeBarValue(value);
        }

        private void WidgetOnOnCloseButtonClicked() {
            CoreMobile.Instance.UIManager.ActivateWidgetByType("MAIN_SCREEN_UI");
            Widget.Dismiss();
            
            if (!WidgetData.isFirstRun) return;
            var widgetData = new FirstRunGameInvitesUIWidgetData();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(FIRST_RUN_GAME_INVITES_UI, widgetData);
        }

        private void WidgetOnEditButtonClicked() {
            Widget.SwitchTaskEditorState(true);
        }
        
        private void WidgetOnApplyButtonClicked() {
            Widget.SwitchTaskEditorState(false);
        }
        
        private void WidgetOnTaskEditButtonClicked(PetScheduleTaskEditorUIWidgetData data) {
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(PET_SCHEDULE_EDITOR_UI, data);
        }
        private void WidgetOnAddNewButtonClicked() {
            var data = new PetScheduleTaskCreatorUIWidgetData {
                TaskType = ScheduleManager.TaskTypeList[0]
            };
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(PET_SCHEDULE_CREATOR_UI, data);
        }
    }
}