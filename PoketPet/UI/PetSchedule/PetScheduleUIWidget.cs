using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using PoketPet.Global;
using PoketPet.Network.API.RESTModels.Tasks;
using PoketPet.Schedule;
using PoketPet.UI.PetScheduleTaskEditor;
using PoketPet.UI.TaskDisplay;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;
using static PoketPet.Global.GlobalVariables.LocalDataKeys;
using static PoketPet.Global.GlobalVariables.UsersPetRoleKeys;

namespace PoketPet.UI.PetSchedule {
    public class PetScheduleUIWidget : BaseUIWidget {
        public event Action OnCloseButtonClick;
        public event Action OnEditButtonClick;
        public event Action<PetScheduleTaskEditorUIWidgetData> OnEditTaskButtonClick;
        public event Action OnApplyButtonClick;
        public event Action OnAddNewButtonClick;

        public List<ScheduleUIElement> tasksList = new List<ScheduleUIElement>();

        [Header("Close Button")]
        [SerializeField] private Button closeButton;
        
        [Header("Add New Element")]
        [SerializeField] private Button addNewElementButton;
        [SerializeField] private RectTransform addNewElementContainer;
        
        [Header("Apply Schedule")]
        [SerializeField] private Button applyScheduleButton;
        [SerializeField] private RectTransform applyScheduleContainer;
        
        [Header("Edit Schedule")]
        [SerializeField] private Button editScheduleButton;
        [SerializeField] private RectTransform editScheduleContainer;
        
        [Header("Task Container")]
        [SerializeField] private TaskUIContainer taskContainer;

        [SerializeField] private GameObject timeBar;

        [Header("Schedule UI Element")]
        [SerializeField] private GameObject scheduleUiElementPrefab;
        
        [Header("Clock")]
        [SerializeField] private TextMeshProUGUI clockField;

        private bool _isEditMode;
        private string _timeFormat;

        public override void Create() {
            base.Create();
            closeButton.onClick.AddListener(delegate { OnCloseButtonClick?.Invoke(); });
            editScheduleButton.onClick.AddListener(delegate { OnEditButtonClick?.Invoke(); });
            applyScheduleButton.onClick.AddListener(delegate { OnApplyButtonClick?.Invoke(); });
            addNewElementButton.onClick.AddListener(delegate { OnAddNewButtonClick?.Invoke(); });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            
            closeButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize(PetScheduleUIWidgetData widgetData) {
            _timeFormat = UserProfileManager.CurrentUser.settings.time_format == MILITARY_TIME ? "HH:mm" : "hh:mm tt";
            StartCoroutine(CoroutineTimer());
            editScheduleContainer.gameObject.SetActive(UserProfileManager.CurrentUser.RoleByCurrentPet == OWNER);
            applyScheduleContainer.gameObject.SetActive(false);
            addNewElementContainer.gameObject.SetActive(false);
        }

        public void SetTimeBarValue(float value) {
            timeBar.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, value);
            timeBar.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, value);
        }

        public void ShowTasks(List<List<TaskScheduleModel>> tasks, float minTaskTime, float maxTaskTime) {
            DestroyTaskElements();
            tasksList = new List<ScheduleUIElement>();
            for (var i = tasks.Count - 1; i >= 0; i--) {
                for (var t = 0; t < tasks[i].Count; t++) {
                    var element = CreateTaskScheduleElement();
                    element.Initialize(tasks[i][t]);
                    element.SetPosition(minTaskTime, maxTaskTime, i);
                    element.EditMode(_isEditMode);
                    element.OnEditButtonClick += OnEditTaskButtonClick;
                }
            }
        }

        private ScheduleUIElement CreateTaskScheduleElement() {
            var newElementGo = Instantiate(scheduleUiElementPrefab, taskContainer.GetRectTransform(), false);
            var taskElement = newElementGo.GetComponent<ScheduleUIElement>();
            tasksList.Add(taskElement);
            return taskElement;
        }

        private void DestroyTaskElements() {
            foreach (var element in tasksList) {
                element.EditMode(false);
                element.OnEditButtonClick -= OnEditTaskButtonClick;
            }

            for (var i = 0; i < taskContainer.GetRectTransform().childCount; i++) {
                var elementTransform = taskContainer.transform.GetChild(i);
                Destroy(elementTransform.gameObject);
                Debug.Log($"childCount {taskContainer.GetRectTransform().childCount}");
            }
        }
        
        public void SwitchTaskEditorState(bool isEditMode) {
            _isEditMode = isEditMode;
            
            foreach (var element in tasksList) {
                element.EditMode(isEditMode);
            }
            var isCurrentUserOwner = UserProfileManager.CurrentUser.RoleByCurrentPet == OWNER;
            addNewElementContainer.gameObject.SetActive(isEditMode && isCurrentUserOwner);
            applyScheduleContainer.gameObject.SetActive(isEditMode && isCurrentUserOwner);
            editScheduleContainer.gameObject.SetActive(!isEditMode && isCurrentUserOwner);
        }
        
        private IEnumerator CoroutineTimer() {
            while(this.isActiveAndEnabled) {
                clockField.text = DateTime.Now.ToString(_timeFormat, CultureInfo.CreateSpecificCulture("en-US"));
                yield return new WaitForSeconds(1);
            }
        }
    }
}