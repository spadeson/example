using System;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.PetSchedule {
    [Serializable]
    public class PetScheduleUIWidgetData : WidgetData {
        public bool isFirstRun;
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}
