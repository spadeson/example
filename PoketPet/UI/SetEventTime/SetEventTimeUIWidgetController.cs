using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.SetEventTime {
    public class SetEventTimeUIWidgetController : WidgetControllerWithData<SetEventTimeUIWidget, SetEventTimeUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
