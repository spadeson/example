using UnityEditor;
using UnityEngine;

namespace PoketPet.UI.CommonUIElements.Editor {
    [CustomEditor(typeof(BageUIElement))]
    public class BageUIElementEditor : UnityEditor.Editor {

        private int _bageCount;
        private bool _bageAnimate;
        
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            var bageUiElement = (BageUIElement) target;

            _bageCount = EditorGUILayout.IntField("Counter:", _bageCount);
            _bageAnimate = EditorGUILayout.Toggle("Animate:", _bageAnimate);

            if (GUILayout.Button("Update", GUILayout.ExpandWidth(true), GUILayout.Height(32))) {
                bageUiElement.SetBageCounter(_bageCount, _bageAnimate);
            }
        }
    }
}
