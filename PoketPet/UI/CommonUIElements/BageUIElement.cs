﻿using DG.Tweening;
using PoketPet.Global;
using TMPro;
using UnityEngine;
using VIS.CoreMobile;

namespace PoketPet.UI.CommonUIElements {
    [RequireComponent(typeof(CanvasGroup))]

    public class BageUIElement : MonoBehaviour {

        [Header("Canvas Group")]
        [SerializeField] private CanvasGroup canvasGroup;
        
        [Header("Counter Label")]
        [SerializeField] private TextMeshProUGUI bageCounterLabel;
        
        [Header("Bage Image")]
        [SerializeField] private RectTransform bageImageRect;
        
        private RectTransform _rectTransform;
        private Sequence _sequence;

        private void Start() {
            
            _rectTransform = GetComponent<RectTransform>();

            if (canvasGroup == null) {
                canvasGroup = GetComponent<CanvasGroup>();
            }
            
            canvasGroup.blocksRaycasts = false;

            if (bageCounterLabel == null) {
                bageCounterLabel = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            }

            bageCounterLabel.autoSizeTextContainer = true;
            
            SetBageCounter(0);
        }

        public void SetBageCounter(int value, bool animate = false) {

            bageCounterLabel.text = value.ToString();
            
            if (bageCounterLabel.text.Length == 0 || value == 0) {
                ToggleCanvasGroup(false, animate);
            } else if(bageCounterLabel.text.Length == 1) {
                ToggleCanvasGroup(true, animate);
                _rectTransform.sizeDelta = new Vector2(76, _rectTransform.sizeDelta.y);
                bageImageRect.sizeDelta = new Vector2(76, bageImageRect.sizeDelta.y);
                CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BAGE_NOTIFICATION_SFX);
            } else {
                ToggleCanvasGroup(true, animate);
                _rectTransform.sizeDelta = new Vector2((bageCounterLabel.text.Length + 1) * 50, _rectTransform.sizeDelta.y);
                bageImageRect.sizeDelta = new Vector2((bageCounterLabel.text.Length  + 1) * 50, bageImageRect.sizeDelta.y);
                CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BAGE_NOTIFICATION_SFX);
            }
        }

        private void ToggleCanvasGroup(bool value, bool animate = false) {
            var alpha = value ? 1 : 0;
            if (animate) {
                _sequence = DOTween.Sequence();
                _sequence.Append(canvasGroup.DOFade(alpha, 0.45f).SetEase(Ease.OutQuint));
                _sequence.Join(_rectTransform.DOPunchScale(Vector3.one * 0.35f, 0.33f, 5, 0.5f).SetEase(Ease.OutQuint).SetDelay(0.1f));
            } else {
                canvasGroup.alpha = alpha;
            }
            
            canvasGroup.interactable = value;
        }
    }
}
