using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Flashlight {
    public class FlashlightUIWidgetController : WidgetControllerWithData<FlashlightUIWidget, FlashlightUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCompleted += WidgetOnCompleted;
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }

        private void WidgetOnCompleted() {
            Widget.OnCompleted -= WidgetOnCompleted;
            Widget.Dismiss();
        }
    }
}