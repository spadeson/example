using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Flashlight {
    public class FlashlightUIWidget : BaseUIWidget {
        public event Action OnCompleted;

        [Header("Flashlight")]
        [SerializeField] private Image flashlightImage;

        private void OnEnable() {
            flashlightImage.DOFade(0, 0);
        }

        public override void Create() {
            base.Create();
            flashlightImage.DOFade(1, 0.15f).SetEase(Ease.Flash).OnComplete(() => {
                OnCompleted?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            flashlightImage.DOFade(0, 0.25f).SetEase(Ease.Flash).SetDelay(0.5f).OnComplete(() => {
                base.Dismiss(); 
            });
        }
    }
}