using System;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;
using DG.Tweening;

namespace PoketPet.UI.TaskCompleted {
    public class TaskCompletedUIWidget : BaseUIWidget {

        public event Action OnCloseButtonClick;

        [Header("Button")]
        [SerializeField] private Button closeButton;
        
        [SerializeField] private RectTransform raysContainer;
        [SerializeField] private RectTransform petFace;

        private const float RAYS_SCALE_DURATION = 2f;
        private const float RAYS_SCALE_FACTOR = 0.25f;

        private Sequence _sequence;

        public override void Create() {
            base.Create();
            closeButton.onClick.AddListener(delegate {
                OnCloseButtonClick?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            _sequence.Kill(true);
            base.Dismiss();
        }

        public void Initialize() {

            raysContainer.DOScale(Vector3.zero,0);
            petFace.DOScale(Vector3.zero,0);

            _sequence = DOTween.Sequence();
            
            _sequence.Append(raysContainer.DOScale(Vector3.one,1f));
            _sequence.Join(petFace.DOScale(Vector3.one,1f));

            _sequence.Append(raysContainer.DORotate(raysContainer.eulerAngles + Vector3.forward * -360f, 2f, RotateMode.FastBeyond360).SetEase(Ease.OutQuad));
            _sequence.Join(raysContainer.DOPunchScale(Vector3.one * RAYS_SCALE_FACTOR, RAYS_SCALE_DURATION, 3, 0.5f));
            
            _sequence.Join(petFace.DOPunchScale(Vector3.one * RAYS_SCALE_FACTOR, RAYS_SCALE_DURATION, 6, 0.35f));
        }
    }
}
