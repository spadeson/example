using PoketPet.Global;
using PoketPet.UI.FirstRunNickName;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompleted {
    public class TaskCompletedUIWidgetController : WidgetControllerWithData<TaskCompletedUIWidget, TaskCompletedUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClick += WidgetOnCloseButtonClick;
            Widget.Initialize();
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.COMPLETED_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClick -= WidgetOnCloseButtonClick;
        }

        private void WidgetOnCloseButtonClick() {
            Widget.Dismiss();
        }
    }
}