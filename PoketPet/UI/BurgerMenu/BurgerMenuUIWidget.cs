using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using VIS.CoreMobile.UISystem;
using UnityEngine.UI;

namespace PoketPet.UI.BurgerMenu {
    public class BurgerMenuUIWidget : BaseUIWidget {
        public event Action OnBackButtonClick;
        public event Action OnFamilyButtonClick;
        public event Action OnLeaderboardsButtonClick;
        public event Action OnPlayerProfileButtonClick;
        public event Action OnPetProfileButtonClick;
        public event Action OnPetScheduleButtonClick;
        public event Action OnSettingsButtonClick;
        public event Action OnSupportButtonClick;
        public event Action OnDeveloperModeButtonClick;
        
        [Header("Back Button")]
        [SerializeField] private Button backButton;
        [SerializeField] private Button closeButton;
        
        [Header("Main Buttons")]
        [SerializeField] private Button familyButton;
        [SerializeField] private Button leaderboardsButton;
        [SerializeField] private Button playerProfileButton;
        [SerializeField] private Button petProfileButton;
        [SerializeField] private Button petScheduleButton;
        
        [Header("Service Buttons")]
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button supportButton;
        [SerializeField] private Button developerModeButton;
        
        [Header("Build version")]
        [SerializeField] private TextMeshProUGUI buildVersionText;

        [Header("Shroud Image")]
        [SerializeField] private Image shroudImage;
        
        [Header("Shroud Image")]
        [SerializeField] private RectTransform container;

        private const float ANIMATION_IN_TIME = 0.35f;
        private const float ANIMATION_OUT_TIME = 0.25f;

        private float _containerWidth;

        private Dictionary<BurgerMenuButtonTypes, Button> _buttonsByTypes;

        private BurgerMenuUIWidgetData _widgetData;

        private void OnEnable() {
            _containerWidth = container.sizeDelta.x;
            
            shroudImage.color = Color.clear;
            container.anchoredPosition = Vector2.left * _containerWidth;
        }

        public override void Create() {
            base.Create();

            closeButton.onClick.AddListener(OnCloseButtonClickHandler);
            backButton.onClick.AddListener(OnCloseButtonClickHandler);
            
            playerProfileButton.onClick.AddListener(delegate {
                OnPlayerProfileButtonClick?.Invoke();
            });
            
            petProfileButton.onClick.AddListener(delegate {
                OnPetProfileButtonClick?.Invoke();
            });
            
            petScheduleButton.onClick.AddListener(delegate {
                OnPetScheduleButtonClick?.Invoke();
            });
            
            familyButton.onClick.AddListener(delegate {
                OnFamilyButtonClick?.Invoke();
            });
            
            leaderboardsButton.onClick.AddListener(delegate {
                OnLeaderboardsButtonClick?.Invoke();
            });

            settingsButton.onClick.AddListener(delegate {
                OnSettingsButtonClick?.Invoke();
            });
            
            supportButton.onClick.AddListener(delegate {
                OnSupportButtonClick?.Invoke();
            });
            
            developerModeButton.onClick.AddListener(delegate {
                OnDeveloperModeButtonClick?.Invoke();
            });
        }

        public override void Activate(bool animated) {
            if (animated) {
                ShowWidgetAnimation();
            } else {
                shroudImage.DOFade(0.65f, 0);
                container.DOAnchorPosX(0, 0);
                base.Activate(false);
            }
        }

        public override void Deactivate(bool animated) {
            if (animated) {
                HideWidgetAnimation();
            } else {
                base.Deactivate(false);
            }
        }

        public override void Dismiss() {
            HideAndDismissWidgetAnimation();
        }

        public void Initialize(BurgerMenuUIWidgetData widgetData) {
            
            _buttonsByTypes = new Dictionary<BurgerMenuButtonTypes, Button> {
                {BurgerMenuButtonTypes.USER_PROFILE, playerProfileButton},
                {BurgerMenuButtonTypes.PET_PROFILE, petProfileButton},
                {BurgerMenuButtonTypes.PET_SCHEDULE, petScheduleButton},
                {BurgerMenuButtonTypes.FAMILY, familyButton},
                {BurgerMenuButtonTypes.LEADERBOARDS, leaderboardsButton},
                {BurgerMenuButtonTypes.SETTINGS, settingsButton},
                {BurgerMenuButtonTypes.SUPPORT, supportButton},
                {BurgerMenuButtonTypes.DEVELOPERS, developerModeButton} 
            };
            
            _widgetData = widgetData;

            foreach (var burgerMenuButton in _buttonsByTypes) {
                burgerMenuButton.Value.gameObject.SetActive(false);
            }
            
            foreach (var buttonType in _widgetData.burgerMenuButtons) {
                _buttonsByTypes[buttonType].gameObject.SetActive(true);
            }
        }

        public void SetBuildVersion(string version) {
            buildVersionText.text = $"ver {version}";
        }

        private void ShowWidgetAnimation() {
            var sequence = DOTween.Sequence();
            sequence.Append(shroudImage.DOFade(0.65f, ANIMATION_IN_TIME).SetEase(Ease.InFlash));
            sequence.Join(container.DOAnchorPosX(0, ANIMATION_IN_TIME).SetEase(Ease.OutQuint));
            sequence.OnComplete(() => {
                base.Activate(false);
            });
        }

        private void HideWidgetAnimation() {
            var dismissSequence = DOTween.Sequence();
            dismissSequence.Append(shroudImage.DOFade(0, ANIMATION_OUT_TIME)).SetEase(Ease.OutSine);
            dismissSequence.Join(container.DOAnchorPosX(-_containerWidth, ANIMATION_OUT_TIME)).SetEase(Ease.InQuint);
            dismissSequence.OnComplete(() => {
                base.Deactivate(false);
            });
        }
        
        private void HideAndDismissWidgetAnimation() {
            var dismissSequence = DOTween.Sequence();
            dismissSequence.Append(shroudImage.DOFade(0, ANIMATION_OUT_TIME)).SetEase(Ease.OutSine);
            dismissSequence.Join(container.DOAnchorPosX(-_containerWidth, ANIMATION_OUT_TIME)).SetEase(Ease.InQuint);
            dismissSequence.OnComplete(() => {
                base.Dismiss();
            });
        }

        private void OnCloseButtonClickHandler() {
            closeButton.onClick.RemoveAllListeners();
            backButton.onClick.RemoveAllListeners();
            OnBackButtonClick?.Invoke();
        }
    }
}