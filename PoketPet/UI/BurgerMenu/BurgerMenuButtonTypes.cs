namespace PoketPet.UI.BurgerMenu {
    public enum BurgerMenuButtonTypes {
        USER_PROFILE = 0,
        PET_PROFILE = 1,
        PET_SCHEDULE = 2,
        FAMILY = 3,
        LEADERBOARDS = 4,
        SETTINGS = 5,
        SUPPORT = 6,
        DEVELOPERS = 7
    }
}