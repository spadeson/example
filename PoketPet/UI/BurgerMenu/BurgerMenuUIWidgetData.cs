using System;
using System.Collections.Generic;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.BurgerMenu {
    [Serializable]
    public class BurgerMenuUIWidgetData : WidgetData {
        public List<BurgerMenuButtonTypes> burgerMenuButtons = new List<BurgerMenuButtonTypes>();
    }
}