using PoketPet.EventsSubsystem;
using PoketPet.Family;
using PoketPet.Global;
using PoketPet.Pets.PetsSystem.Manager;
using PoketPet.Settings;
using PoketPet.UI.DeveloperMode;
using PoketPet.UI.FamilyManagement;
using PoketPet.UI.LeaderBoard;
using PoketPet.UI.PetProfile;
using PoketPet.UI.PetSchedule;
using PoketPet.UI.Settings;
using PoketPet.UI.Support;
using PoketPet.UI.UserProfile;
using PoketPet.User;
using UnityEditor;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.BurgerMenu {
    public class BurgerMenuUIWidgetController : WidgetControllerWithData<BurgerMenuUIWidget, BurgerMenuUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {

            Widget.OnBackButtonClick += OnBackButtonClickHandler;

            Widget.OnPlayerProfileButtonClick += OnPlayerProfileButtonClickHandler;
            Widget.OnPetProfileButtonClick += OnPetProfileButtonClickHandler;
            Widget.OnPetScheduleButtonClick += OnPetScheduleButtonClickHandler;
            Widget.OnFamilyButtonClick += OnFamilyButtonClickHandler;
            Widget.OnLeaderboardsButtonClick += OnLeaderboardsButtonClickHandler;
            Widget.OnSettingsButtonClick += OnSettingsButtonClickHandler;
            Widget.OnSupportButtonClick += OnSupportButtonClickHandler;
            Widget.OnDeveloperModeButtonClick += OnDeveloperModeButtonClickHandler;

            var buildNumber = "0";
            var version = $"{Application.version}";
            
            #if UNITY_EDITOR && UNITY_IOS
            buildNumber = PlayerSettings.iOS.buildNumber;
            version = $"{Application.version}.{buildNumber}";
            #elif UNITY_EDITOR && UNITY_ANDROID
            version = $"{Application.version}.{buildNumber}";
            #else
            version = $"{Application.version}";
            #endif
            
            Widget.SetBuildVersion(version);
            
            Widget.Initialize(WidgetData);
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.MAIN_SCREEN_UI, false);
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.TASK_DISPLAY_UI, false);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.SWIPE_IN_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.SWIPE_OUT_SFX);
        }

        protected override void OnWidgetDismissed(Widget widget) {
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.MAIN_SCREEN_UI, true);
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.TASK_DISPLAY_UI, true);
            
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.SWIPE_OUT_SFX);
            
            Widget.OnBackButtonClick -= OnBackButtonClickHandler;

            Widget.OnPlayerProfileButtonClick -= OnPlayerProfileButtonClickHandler;
            Widget.OnPetProfileButtonClick -= OnPetProfileButtonClickHandler;
            Widget.OnPetScheduleButtonClick -= OnPetScheduleButtonClickHandler;
            Widget.OnFamilyButtonClick -= OnFamilyButtonClickHandler;
            Widget.OnLeaderboardsButtonClick -= OnLeaderboardsButtonClickHandler;
            Widget.OnSettingsButtonClick -= OnSettingsButtonClickHandler;
            Widget.OnSupportButtonClick -= OnSupportButtonClickHandler;
            Widget.OnDeveloperModeButtonClick -= OnDeveloperModeButtonClickHandler;
        }

        private void OnBackButtonClickHandler() {
            Widget.Dismiss();
        }

        private void OnPlayerProfileButtonClickHandler() {
            var userProfileWidgetData = new UserProfileUIWidgetData {UserDataModel = UserProfileManager.CurrentUser};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.USER_PROFILE_UI, userProfileWidgetData, true);
        }

        private void OnPetProfileButtonClickHandler() {
            var petProfileWidgetData = new PetProfileUIWidgetData {PetsDataModel = PetsProfileManager.CurrentPet, WeightUnits = SettingsManager.GetWeightUnits()};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PET_PROFILE_UI, petProfileWidgetData, true);
            Widget.Deactivate(true);
        }
        
        private void OnPetScheduleButtonClickHandler() {
            var petScheduleWidgetData = new PetScheduleUIWidgetData{isFirstRun = false};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.PET_SCHEDULE_UI, petScheduleWidgetData, true);
            Widget.Deactivate(true);
        }
        
        private void OnFamilyButtonClickHandler() {
            
            var familyWidgetData = new FamilyManagementUIWidgetData {
                petsDataModel = PetsProfileManager.CurrentPet,
                familyDataModels = FamilyProfileManager.GetFamilies()
            };

            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.FAMILY_MANAGEMENT_UI, familyWidgetData, true);
            Widget.Deactivate(true);
        }
        
        private void OnLeaderboardsButtonClickHandler() {
            var leaderBoardUiWidgetData = new LeaderBoardUIWidgetData();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.LEADERBOARD_UI, leaderBoardUiWidgetData, true);
            Widget.Deactivate(true);
            //Social.ShowLeaderboardUI();
        }
        
        private void OnSettingsButtonClickHandler() {
            var settingWidgetData = new SettingsUIWidgetData {
                soundsVolume = SettingsManager.GetSoundsVolume(),
                enablePushNotifications = SettingsManager.GetPushNotificationStatus(),
                timeFormat = SettingsManager.GetTimeFormat(),
                weightUnits = SettingsManager.GetWeightUnits()
            };
                
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.SETTINGS_UI, settingWidgetData, true);
            Widget.Deactivate(true);
        }
        
        private void OnSupportButtonClickHandler() {
            var supportUiWidgetData = new SupportUIWidgetData();
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.SUPPORT_UI, supportUiWidgetData, true);
            Widget.Deactivate(true);
        }
        
        private void OnDeveloperModeButtonClickHandler() {
            CoreMobile.Instance.SceneManager.LoadSceneByKeyFromConfig(GlobalVariables.ScenesKeys.PET_STATES_VIEWER_SCENE);
        }
    }
}