using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.Tooltip {
    public class TooltipUIWidget : MonoBehaviour {

        public event Action OnTooltipShown;
        public event Action OnTooltipHidden;
        public event Action OnTooltipClosed;
        
        [Header("Tooltip Element")]
        [SerializeField] private RectTransform tooltipContainer;
        
        [Header("Tooltip Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;
        
        [Header("Tooltip Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        [Header("Tooltip Close Button")]
        [SerializeField] private Button tooltipCloseButton;

        public void Initialize(string title, string description) {
            titleText.text = title;
            descriptionText.text = description;
            
            tooltipContainer.DOScale(0, 0);
            
            tooltipCloseButton.interactable = false;
            tooltipCloseButton.targetGraphic.DOFade(0f, 0);
        }

        public void Show() {
            tooltipCloseButton.targetGraphic.DOFade(0.75f, 0.25f).SetEase(Ease.Flash).OnComplete(() => {
                tooltipCloseButton.interactable = true;
                tooltipCloseButton.onClick.AddListener(delegate {
                    tooltipCloseButton.onClick.RemoveAllListeners();
                    OnTooltipClosed?.Invoke();
                });
            });
            
            tooltipContainer.DOScale(1, 0.25f).SetEase(Ease.OutSine).OnComplete(() => {
                OnTooltipShown?.Invoke();
            });
        }
        
        public void Hide() {
            tooltipCloseButton.interactable = false;
            tooltipCloseButton.targetGraphic.raycastTarget = false;
            tooltipCloseButton.targetGraphic.DOFade(0f, 0.25f).SetEase(Ease.Flash);
            tooltipContainer.DOScale(0, 0.25f).SetEase(Ease.Flash).OnComplete(() => {
                OnTooltipHidden?.Invoke();
            });
        }
    }
}
