using PoketPet.Messages;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.InviteConfirm {
    public class InviteConfirmUIWidgetController : WidgetControllerWithData<InviteConfirmUIWidget, InviteConfirmUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClicked += OnCloseButtonClickedHandler;
            Widget.OnYesButtonClicked += OnYesButtonClickedHandler;
            Widget.OnNoButtonClicked += OnNoButtonClickedHandler;
            
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) { }
        
        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClicked -= OnCloseButtonClickedHandler;
            Widget.OnYesButtonClicked -= OnYesButtonClickedHandler;
            Widget.OnNoButtonClicked -= OnNoButtonClickedHandler;
        }

        private void OnNoButtonClickedHandler() {
            MessageManager.CloseMessageById(WidgetData.messageId, false);
            Widget.Dismiss();
        }

        private void OnYesButtonClickedHandler() {
            MessageManager.CloseMessageById(WidgetData.messageId, true);
            Widget.Dismiss();
        }

        private void OnCloseButtonClickedHandler() {
            Widget.Dismiss();
        }
    }
}
