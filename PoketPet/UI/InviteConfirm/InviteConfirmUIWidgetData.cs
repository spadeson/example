using System;
using PoketPet.Messages;
using PoketPet.Network.API.REST.Messages;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.InviteConfirm {
    [Serializable]
    public class InviteConfirmUIWidgetData : WidgetData {
        public string userId;
        public string messageId;
    }
}