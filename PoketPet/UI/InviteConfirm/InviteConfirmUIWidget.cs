using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.InviteConfirm {
    public class InviteConfirmUIWidget : BaseUIWidget {
        public event Action OnCloseButtonClicked;
        public event Action OnYesButtonClicked;
        public event Action OnNoButtonClicked;

        [SerializeField] private Button closeButton;
        [SerializeField] private Button yesButton;
        [SerializeField] private Button noButton;

        [SerializeField] private TextMeshProUGUI contentText;

        private InviteConfirmUIWidgetData _widgetData;

        public void Initialize(InviteConfirmUIWidgetData widgetData) {
            _widgetData = widgetData;
            
            contentText.text = $"User {_widgetData.userId} invites you! \nAccept the invitation?";

            closeButton.onClick.AddListener(delegate { OnCloseButtonClicked?.Invoke(); });

            yesButton.onClick.AddListener(delegate { OnYesButtonClicked?.Invoke(); });

            noButton.onClick.AddListener(delegate { OnNoButtonClicked?.Invoke(); });
        }

        public override void Dismiss() {
            base.Dismiss();
        }
    }
}