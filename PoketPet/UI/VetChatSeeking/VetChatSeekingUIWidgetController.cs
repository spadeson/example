using System.Collections;
using PoketPet.Global;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChatSeeking {
    public class VetChatSeekingUIWidgetController : WidgetControllerWithData<VetChatSeekingUIWidget, VetChatSeekingUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
            StartCoroutine(AutoDismissCoroutine(2f));
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }

        private IEnumerator AutoDismissCoroutine(float delay) {
            yield return new WaitForSeconds(delay);
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.VET_CHAT_RESULT_UI, animate: true);
            Widget.Dismiss();
        }
    }
}
