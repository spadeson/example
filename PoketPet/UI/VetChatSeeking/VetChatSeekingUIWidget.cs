using DG.Tweening;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.VetChatSeeking {
    public class VetChatSeekingUIWidget : BaseUIWidget {

        [Header("Spinner Image")]
        [SerializeField] private RectTransform progressSpinner;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            progressSpinner.DORotate(Vector3.forward * 360f, 1f, RotateMode.FastBeyond360).SetLoops(-1);
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            progressSpinner.DOKill(true);
            base.Dismiss();
        }
    }
}