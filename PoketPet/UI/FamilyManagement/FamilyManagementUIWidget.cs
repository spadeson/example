using System;
using System.Collections.Generic;
using DG.Tweening;
using PoketPet.User;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FamilyManagement {
    public class FamilyManagementUIWidget : BaseUIWidget {
        public event Action<UserModel> OnUserIdDeleted;
        public event Action OnInviteClicked;
        public event Action OnCloseButtonClicked;

        [Header("Invite Message")]
        [SerializeField] private TextMeshProUGUI inviteMessageText;

        [Header("UI Elements Root")]
        [SerializeField] private Transform uiElementsRoot;

        [Header("Prefab")]
        [SerializeField] private FamilyMemberUIElementPresenter familyMemberUiElementPrefab;

        [Header("Close Button")]
        [SerializeField] private Button closeButton;
        
        [Header("Action Button")]
        [SerializeField] private Button actionButton;
        
        [Header("Scroll:")]
        [SerializeField] private RectTransform topMask;
        [SerializeField] private RectTransform bottomMask;
        [SerializeField] private ScrollRect scrollRect;
        
        private List<FamilyMemberUIElementPresenter> _familyUserPresentersList = new List<FamilyMemberUIElementPresenter>();

        private const int USERS_SCROLL_COUNT = 6;

        public void Initialize(FamilyManagementUIWidgetData widgetData) {
            actionButton.onClick.AddListener(OnInviteButtonClickedHandler);
            closeButton.onClick.AddListener(OnCloseButtonClickedHandler);

            CreateFamilyElements(widgetData.familyDataModels[widgetData.petsDataModel.id].users);
            
            topMask.gameObject.SetActive(false);
            bottomMask.gameObject.SetActive(false);
        }

        public override void Dismiss() {
            scrollRect.onValueChanged.RemoveAllListeners();
            base.Dismiss();
        }

        public void CreateFamilyElements(List<UserModel> users) {
            
            DeleteFamilyElements();
            
            foreach (var user in users) {
                var familyMember = Instantiate(familyMemberUiElementPrefab, uiElementsRoot);

                familyMember.Initialize(user);
                
                familyMember.OnUserIdDeleted += OnUserIdDeletedClickedHandler;
                
                _familyUserPresentersList.Add(familyMember);
            }
            
            inviteMessageText.gameObject.SetActive(_familyUserPresentersList.Count <= 0);

            if (_familyUserPresentersList.Count >= USERS_SCROLL_COUNT) {
                scrollRect.onValueChanged.AddListener(UpdateMasksView);
            }
        }

        private void DeleteFamilyElements() {
            for (var i = 0; i < _familyUserPresentersList.Count; i++) {
                var familyUiElement = _familyUserPresentersList[i];
                familyUiElement.Dismiss();
                familyUiElement.OnUserIdDeleted -= OnUserIdDeletedClickedHandler;
                Destroy(familyUiElement.gameObject);
            }
            
            _familyUserPresentersList.Clear();
        }

        private void OnCloseButtonClickedHandler() {
            closeButton.onClick.RemoveAllListeners();
            closeButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnCloseButtonClicked?.Invoke();
            });
        }

        private void OnInviteButtonClickedHandler() {
            actionButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.25f, 2, 0.75f).OnComplete(() => {
                OnInviteClicked?.Invoke();
            });
        }

        private void OnUserIdDeletedClickedHandler(UserModel user) {
            OnUserIdDeleted?.Invoke(user);
        }
        
        private void UpdateMasksView(Vector2 value) {
            var valRounded = Math.Round(value.y, 2);
            topMask.gameObject.SetActive(valRounded    < 1);
            bottomMask.gameObject.SetActive(valRounded > 0);
        }
    }
}