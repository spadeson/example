using System;
using System.Globalization;
using PoketPet.User;
using PoketPet.Photo;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.FamilyManagement {
    public class FamilyMemberUIElementPresenter : MonoBehaviour {
        
        [Header("Name:")]
        [SerializeField] private TextMeshProUGUI userNameText;
        
        [Header("Role:")]
        [SerializeField] private TextMeshProUGUI userRoleText;

        [Header("Avatar:")]
        [SerializeField] private Image avatarImage;
        
        [Header("Button:")]
        [SerializeField] private Button deleteButton;
        public event Action<UserModel> OnUserIdDeleted;

        private UserModel _user;

        public void Initialize(UserModel user) {
            
            _user = user;
            var textInfo = new CultureInfo("en-US", false).TextInfo;
            
            userNameText.text = textInfo.ToTitleCase(_user.nickname);

            userRoleText.text = textInfo.ToTitleCase(_user.RoleByCurrentPet);
            
            //Subscribe from buttons onClick events
            deleteButton.onClick.AddListener(OnDeleteButtonClickedHandler);

            PhotoManager.GetAvatarById(user.avatar, sprite => { avatarImage.sprite = sprite; });
        }

        public void Dismiss() {
            //Unsubscribe from buttons onClick events
            deleteButton.onClick.RemoveAllListeners();
        }

        private void OnDeleteButtonClickedHandler() {
            OnUserIdDeleted?.Invoke(_user);
        }
    }
}