using System;
using System.Collections.Generic;
using PoketPet.Family;
using PoketPet.Network.API.RESTModels.Pets;
using PoketPet.Pets.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FamilyManagement {
    [Serializable]
    public class FamilyManagementUIWidgetData : WidgetData {
        public PetResponseData petsDataModel;
        public Dictionary<string, FamilyDataModel> familyDataModels;
    }
}