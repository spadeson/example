using PoketPet.Family;
using PoketPet.Global;
using PoketPet.UI.ConfirmationPopup;
using PoketPet.User;
using PoketPet.UI.InviteCreation;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FamilyManagement {
    public class FamilyManagementUIWidgetController : WidgetControllerWithData<FamilyManagementUIWidget, FamilyManagementUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnUserIdDeleted += WidgetOnUserIdDeleted;
            Widget.OnInviteClicked += WidgetOnInviteButtonClickedHandler;
            Widget.OnCloseButtonClicked  += WidgetOnCloseButtonClickedHandler;
            
            Widget.Initialize(WidgetData);

            FamilyProfileManager.OnUsersListByPetIdUpdated += OnUsersListByPetIdUpdatedHandler;
            
            CoreMobile.Instance.UIManager.DeactivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnUserIdDeleted -= WidgetOnUserIdDeleted;
            Widget.OnInviteClicked -= WidgetOnInviteButtonClickedHandler;
            Widget.OnCloseButtonClicked  -= WidgetOnCloseButtonClickedHandler;
            
            FamilyProfileManager.OnUsersListByPetIdUpdated -= OnUsersListByPetIdUpdatedHandler;
            
            CoreMobile.Instance.UIManager.ActivateWidgetByType(GlobalVariables.UiKeys.BURGER_MENU_UI, this);
        }

        private void WidgetOnUserIdDeleted(UserModel user) {

            var confirmationWidgetData = new ConfirmationPopupUIWidgetData {
                Title = "UNINVITE",
                Message = "Do you want to cancel participation in the family for this person?\n\n<color=#E14520>can’t be undone</color>",
                NegativeButtonLabel = "No",
                PositiveButtonLabel = "Yes"
            };

            confirmationWidgetData.OnPositiveButtonClicked += () => {
                
            };

            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.CONFIRMATION_POPUP_UI, confirmationWidgetData);
        }

        private void OnUsersListByPetIdUpdatedHandler() {
            Widget.CreateFamilyElements(FamilyProfileManager.GetFamiliesAsList());
        }

        private void WidgetOnInviteButtonClickedHandler() {
            var inviteWidgetData = new InviteCreationUIWidgetData{inviteeEmail = string.Empty};
            CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.INVITE_CREATION_UI, inviteWidgetData, true);
            Widget.Deactivate(true);
        }

        private void WidgetOnCloseButtonClickedHandler() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.CLOSE_BUTTON_SFX);
            Widget.OnCloseButtonClicked -= WidgetOnCloseButtonClickedHandler;
            Widget.Dismiss();
        }
    }
}