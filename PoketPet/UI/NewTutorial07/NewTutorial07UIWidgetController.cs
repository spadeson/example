using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutoralFakeCamera {
    public class NewTutorial07UIWidgetController : WidgetControllerWithData<NewTutorial07UIWidget, NewTutorial07UIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnPhotoBegin += WidgetOnPhotoBegin;
            Widget.OnPhotoTaken += WidgetOnPhotoTaken;
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnPhotoBegin -= WidgetOnPhotoBegin;
            Widget.OnPhotoTaken -= WidgetOnPhotoTaken;
        }
        
        private void WidgetOnPhotoBegin() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.IPHONE_SHUTTER_SFX);
        }
        
        private void WidgetOnPhotoTaken() {
            Widget.Dismiss();
        }
    }
}