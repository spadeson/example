using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutoralFakeCamera {
    public class NewTutorial07UIWidget : BaseUIWidget {

        public event Action OnPhotoBegin;
        public event Action OnPhotoTaken;
        
        [Header("Full Size Image")]
        [SerializeField] private Image fullSizeImage;

        [Header("Preview Image")]
        [SerializeField] private Image previewImage;

        [Header("Camera Button Inner")]
        [SerializeField] private RectTransform cameraButtonInner;

        [Header("Camera Button Outer")]
        [SerializeField] private RectTransform cameraButtonOuter;

        [Header("Flash Image")]
        [SerializeField] private Image flashImage;

        private void OnEnable() {
            flashImage.DOFade(0, 0);
        }

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
            ShowAnimation();
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }

        private void ShowAnimation() {

            var sequence = DOTween.Sequence();
            
            sequence.Append(cameraButtonInner.DOScale(0.75f, 0.125f).SetEase(Ease.InOutSine));
            sequence.Append(cameraButtonInner.DOScale(1f, 0.2f).SetEase(Ease.InOutSine));
            
            sequence.Join(flashImage.DOFade(1, 0.125f).SetEase(Ease.Flash).SetDelay(0.1f));
            sequence.Join(flashImage.DOFade(0, 0.15f).SetEase(Ease.Flash).SetDelay(0.325f).OnComplete(() => {
                previewImage.sprite = fullSizeImage.sprite;
            }));
            sequence.AppendInterval(1f);
            sequence.OnStart(() => {
                OnPhotoBegin?.Invoke();
            });
            sequence.OnComplete(() => {
                OnPhotoTaken?.Invoke();
            });
        }
    }
}
