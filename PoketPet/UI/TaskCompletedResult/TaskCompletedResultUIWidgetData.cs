using System;
using UnityEngine;

using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedResult
{
	[Serializable]
	public class TaskCompletedResultUIWidgetData : WidgetData {
		public bool isConfirmed;
		public int scores;
	}
}