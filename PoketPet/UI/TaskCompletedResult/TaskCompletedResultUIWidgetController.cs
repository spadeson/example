using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedResult
{
	public class TaskCompletedResultUIWidgetController : WidgetControllerWithData<TaskCompletedResultUIWidget, TaskCompletedResultUIWidgetData>	{

		protected override void OnWidgetCreated(Widget widget) {
			Widget.OnScoresCounted += WidgetOnScoresCounted;
			Widget.OnActionButtonClick += WidgetOnActionButtonClickHandler;
			Widget.Initialize(WidgetData);
		}

		protected override void OnWidgetActivated(Widget widget) { }

		protected override void OnWidgetDeactivated(Widget widget) { }

		protected override void OnWidgetDismissed(Widget widget) {
			Widget.OnScoresCounted -= WidgetOnScoresCounted;
			Widget.OnActionButtonClick -= WidgetOnActionButtonClickHandler;
		}

		private void WidgetOnActionButtonClickHandler() {
			Widget.Dismiss();   
		}
		
		private void WidgetOnScoresCounted() {
			CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.TUTORIAL_SCORE_COUNTER_SFX);
		}
	}
}