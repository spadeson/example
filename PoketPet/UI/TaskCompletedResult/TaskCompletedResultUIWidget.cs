using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedResult
{
	public class TaskCompletedResultUIWidget : BasicPopupUIWidget	{
		public event Action OnActionButtonClick;
		public event Action OnScoresCounted;
		
		[Header("Labels:")]
		[SerializeField] private RectTransform titleLabel;
		[SerializeField] private RectTransform subtitleConfirmedLabel;
		[SerializeField] private RectTransform subtitleRejectedLabel;
		
		[Header("Containers:")]
		[SerializeField] private RectTransform confirmedContainer;
		[SerializeField] private RectTransform rejectedContainer;
		
		[Header("Scores Slider")]
		[SerializeField] private RectTransform scoresSliderContainer;
		[SerializeField] private Slider scoresSlider;
		[SerializeField] private TextMeshProUGUI scoresLabel;
		
		[Header("Star Image")]
		[SerializeField] private Image starImage;
		
		private TaskCompletedResultUIWidgetData _widgetData;
		
		private int _scores;

		private void OnEnable() {
			scoresSliderContainer.DOScale(0, 0);
			scoresSlider.DOValue(0, 0);
			scoresLabel.text = _scores.ToString();
		}
		
		public override void Create() {
			base.Create();
		}

		public override void Activate(bool animated) {
			ShowAnimation();
			base.Activate(false);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			DOTween.KillAll(true);
			HideAnimation();
		}
		
		public void Initialize(TaskCompletedResultUIWidgetData widgetData) {
			_widgetData = widgetData;
			confirmedContainer.gameObject.SetActive(_widgetData.isConfirmed);
			rejectedContainer.gameObject.SetActive(!_widgetData.isConfirmed);
			actionButton.onClick.AddListener(delegate {
				OnActionButtonClick?.Invoke();
			});
		}
		
		private void ShowAnimation() {
			var showSequence = DOTween.Sequence();
			showSequence.AppendInterval(0.15f);
			showSequence.Append(scoresSliderContainer.DOScale(1, 0.35f).SetEase(Ease.OutBounce));
			showSequence.Append(scoresSlider.DOValue(1f,0.5f).SetDelay(0.15f).SetEase(Ease.Linear));
			showSequence.Join(DOTween.To(() => _scores, s => _scores = s, _widgetData.scores, 0.5f).OnStart(() => { OnScoresCounted?.Invoke(); }).OnUpdate(() => { scoresLabel.text = _scores.ToString(); }));
			showSequence.Join(starImage.rectTransform.DORotate(Vector3.forward * (360f * 2), 0.35f, RotateMode.FastBeyond360).SetEase(Ease.OutExpo));
			showSequence.Join(starImage.rectTransform.DOPunchScale(Vector3.one * 0.15f, 0.5f));
			showSequence.Append(scoresSliderContainer.DOPunchScale(Vector3.one * 0.25f, 0.4f, 6, 0.5f));
		}
		
		private void HideAnimation() {
			var hideSequence = DOTween.Sequence();
			hideSequence.AppendInterval(0.2f);
			hideSequence.OnComplete(() => {
				base.Dismiss();
			});
		}

	}
}