using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial09 {
    public class NewTutorial09UIWidgetController : WidgetControllerWithData<NewTutorial09UIWidget, NewTutorial09UIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnScoresCounted += WidgetOnScoresCounted;
            Widget.OnActionButtonClick += WidgetOnOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnScoresCounted -= WidgetOnScoresCounted;
            Widget.OnActionButtonClick -= WidgetOnOnActionButtonClick;
        }
        
        private void WidgetOnScoresCounted() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.TUTORIAL_SCORE_COUNTER_SFX);
        }

        private void WidgetOnOnActionButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            Widget.Dismiss();
        }
    }
}