using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial09 {
    public class NewTutorial09UIWidget : BasicPopupUIWidget {
        public event Action OnScoresCounted;
        public event Action OnActionButtonClick;

        [Header("Main Image")]
        [SerializeField] private Image mainImage;
        
        [Header("Scores Slider")]
        [SerializeField] private RectTransform scoresSliderContainer;
        [SerializeField] private Slider scoresSlider;
        [SerializeField] private TextMeshProUGUI scoresLabel;
        
        [Header("Star Image")]
        [SerializeField] private Image starImage;

        private NewTutorial09UIWidgetData _widgetData;

        private int _scores;
        
        #region MonoBehaviour

        private void OnEnable() {

            mainImage.DOFade(0, 0);

            scoresSliderContainer.DOScale(0, 0);

            scoresSlider.DOValue(0, 0);

            scoresLabel.text = _scores.ToString();
        }

        #endregion

        #region BaseUIWidget

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            ShowAnimation();
            base.Activate(false);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            DOTween.KillAll(true);
            HideAnimation();
        }

        #endregion

        #region PublicMethods

        public void Initialize(NewTutorial09UIWidgetData widgetData) {

            _widgetData = widgetData;

            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                actionButton.transform.DOPunchScale(Vector3.one * 0.25f, 0.25f, 4, 0.5f).OnComplete(() => {
                    OnActionButtonClick?.Invoke();
                });
            });
        }

        #endregion
        
        #region Private Methods

        private void ShowAnimation() {
            var showSequence = DOTween.Sequence();
            showSequence.AppendInterval(0.25f);
            showSequence.Join(mainImage.DOFade(1f, 0.5f).SetEase(Ease.Flash));
            showSequence.AppendInterval(0.15f);
            showSequence.Append(scoresSliderContainer.DOScale(1, 0.35f).SetEase(Ease.OutBounce));
            showSequence.Append(scoresSlider.DOValue(1f,0.5f).SetDelay(0.15f).SetEase(Ease.Linear));
            showSequence.Join(DOTween.To(() => _scores, s => _scores = s, 1500, 0.5f).OnStart(() => { OnScoresCounted?.Invoke(); }).OnUpdate(() => { scoresLabel.text = _scores.ToString(); }));
            showSequence.Join(starImage.rectTransform.DORotate(Vector3.forward * (360f * 2), 0.35f, RotateMode.FastBeyond360).SetEase(Ease.OutExpo));
            showSequence.Join(starImage.rectTransform.DOPunchScale(Vector3.one * 0.15f, 0.5f));
            showSequence.Append(scoresSliderContainer.DOPunchScale(Vector3.one * 0.25f, 0.4f, 6, 0.5f));
        }
        
        private void HideAnimation() {
            var hideSequence = DOTween.Sequence();
            hideSequence.AppendInterval(0.2f);
            hideSequence.OnComplete(() => {
                base.Dismiss();
            });
        }

        #endregion
    }
}