using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ConfirmationPopup {
    public class ConfirmationPopupUIWidgetController : WidgetControllerWithData<ConfirmationPopupUIWidget, ConfirmationPopupUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnCloseButtonClicked += Widget_OnCloseButtonClickedHandler;
            Widget.OnNoButtonClicked += Widget_OnNoButtonClickedHandler;
            Widget.OnYesButtonClicked += Widget_OnYesButtonClickedHandler;
            
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {

        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnCloseButtonClicked -= Widget_OnCloseButtonClickedHandler;
            Widget.OnNoButtonClicked -= Widget_OnNoButtonClickedHandler;
            Widget.OnYesButtonClicked -= Widget_OnYesButtonClickedHandler;
        }

        private void Widget_OnYesButtonClickedHandler() {
            WidgetData.InvokePositiveButton();
            Widget.Dismiss();
        }

        private void Widget_OnNoButtonClickedHandler() {
            WidgetData.InvokeNegativeButton();
            Widget.Dismiss();
        }

        private void Widget_OnCloseButtonClickedHandler() {
            Widget.Dismiss();
        }
    }
}