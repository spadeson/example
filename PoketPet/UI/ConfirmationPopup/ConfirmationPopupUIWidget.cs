using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ConfirmationPopup {
    public class ConfirmationPopupUIWidget : BaseUIWidget {
        
        [Header("Container")]
        [SerializeField] private RectTransform container;
        
        [Header("Shroud Image"), SerializeField]
        private Image shroudImage;
        
        [Header("Close Button")]
        [SerializeField] private Button closeButton;
        
        [Header("Positive Button")]
        [SerializeField] private Button yesButton;
        [SerializeField] private TextMeshProUGUI yesButtonLabel;
        
        [Header("Negative Button")]
        [SerializeField] private Button noButton;
        [SerializeField] private TextMeshProUGUI noButtonLabel;

        [Header("Title")]
        [SerializeField] private TextMeshProUGUI title;
        
        [Header("Message")]
        [SerializeField] private TextMeshProUGUI message;

        public event Action OnCloseButtonClicked;
        public event Action OnYesButtonClicked;
        public event Action OnNoButtonClicked;

        private bool _animatedWidget;
        
        private ConfirmationPopupUIWidgetData _widgetData;

        private void OnEnable() {
            SetInitialStates();
        }

        public void Initialize(ConfirmationPopupUIWidgetData widgetData) {
            _widgetData = widgetData;

            title.text = _widgetData.Title;
            message.text = _widgetData.Message;

            yesButtonLabel.text = _widgetData.PositiveButtonLabel;
            noButtonLabel.text = _widgetData.NegativeButtonLabel;
            
            closeButton.onClick.AddListener(OnCloseButtonClickedHandler);
            yesButton.onClick.AddListener(OnYesButtonClickedHandler);
            noButton.onClick.AddListener(OnNoButtonClickedHandler);
        }

        public override void Activate(bool animated) {
            _animatedWidget = animated;
            ShowAnimatedWidget();
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            closeButton.onClick.RemoveAllListeners();
            yesButton.onClick.RemoveAllListeners();
            noButton.onClick.RemoveAllListeners();

            if (_animatedWidget) {
                HideAnimatedWidget();
            } else {
                base.Dismiss();
            }
        }

        private void SetInitialStates() {
            container.DOScale(Vector3.zero, 0f);
        }

        private void ShowAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(canvasGroup.DOFade(1, 0));
            sequence.Append(shroudImage.DOFade(0.65f, 0.25f));
            sequence.Append(container.DOScale(1.1f, 0.15f));
            sequence.Append(container.DOScale(Vector3.one, 0.1f));
        }

        private void HideAnimatedWidget() {
            var sequence = DOTween.Sequence();
            sequence.Append(container.DOScale(1.1f, 0.05f));
            sequence.Append(container.DOScale(Vector3.zero, 0.15f));
            sequence.Append(shroudImage.DOFade(0, 0.25f));
            sequence.OnComplete(() => { base.Dismiss(); });
        }

        private void OnNoButtonClickedHandler() {
            noButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.65f, 3, 0.5f).OnComplete(() => { OnNoButtonClicked?.Invoke(); });
        }

        private void OnYesButtonClickedHandler() {
            yesButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.65f, 3, 0.5f).OnComplete(() => { OnYesButtonClicked?.Invoke(); });
        }

        private void OnCloseButtonClickedHandler() {
            closeButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.65f, 3, 0.5f).OnComplete(() => { OnCloseButtonClicked?.Invoke(); });
        }
    }
}