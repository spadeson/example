using System;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.ConfirmationPopup {
    [Serializable]
    public class ConfirmationPopupUIWidgetData : WidgetData {
        public event Action OnPositiveButtonClicked;
        public event Action OnNegativeButtonClicked;

        public string PositiveButtonLabel { get; set; }
        public string NegativeButtonLabel { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }

        public void InvokePositiveButton() {
            OnPositiveButtonClicked?.Invoke();
        }
        
        public void InvokeNegativeButton() {
            OnNegativeButtonClicked?.Invoke();
        }
    }
}
