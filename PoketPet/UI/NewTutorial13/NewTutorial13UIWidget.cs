using System;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.NewTutorial13 {
    public class NewTutorial13UIWidget : BasicPopupUIWidget {
        public event Action OnActionButtonClick;

        [Header("Pet Container")]
        [SerializeField] private RectTransform petContainer;

        [Header("Family Containers")]
        [SerializeField] private RectTransform[] familyContainers;

        [Header("Title Text")]
        [SerializeField] private TextMeshProUGUI titleText;

        [Header("Description Text")]
        [SerializeField] private TextMeshProUGUI descriptionText;

        private NewTutorial13UIWidgetData _widgetData;

        #region MonoBehaviour

        private void OnEnable() {
            petContainer.DOScale(0, 0);

            for (var i = 0; i < familyContainers.Length; i++) {
                familyContainers[i].DOScale(0, 0);
            }
        }

        #endregion

        #region BaseUIWidget

        public override void Activate(bool animated) {
            ShowAnimation();
            base.Activate(false);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            DOTween.KillAll(true);
            HideAnimation();
        }

        #endregion

        #region Public Methods

        public void Initialize(NewTutorial13UIWidgetData widgetData) {
            _widgetData = widgetData;

            titleText.text = _widgetData.TutorialSingleStepData.title;
            descriptionText.text = _widgetData.TutorialSingleStepData.description;

            actionButton.onClick.AddListener(delegate {
                actionButton.onClick.RemoveAllListeners();
                actionButton.targetGraphic.rectTransform.DOPunchScale(Vector3.one * 0.25f, 0.35f, 4, 0.5f).OnComplete(() => { OnActionButtonClick?.Invoke(); });
            });
        }

        #endregion

        #region Private Methods

        private void ShowAnimation() {
            var showSequence = DOTween.Sequence();

            showSequence.AppendInterval(0.25f);
            showSequence.Append(petContainer.DOScale(1, 0.35f).SetEase(Ease.OutBounce));

            showSequence.AppendInterval(0.25f);
            for (var i = 0; i < familyContainers.Length; i++) {
                showSequence.Join(familyContainers[i].DOScale(1, 0.5f + i * 0.25f).SetEase(Ease.OutQuart));
            }
        }

        private void HideAnimation() {
            var hideSequence = DOTween.Sequence();
            hideSequence.AppendInterval(0.25f);
            hideSequence.OnComplete(() => { base.Dismiss(); });
        }

        #endregion
    }
}
