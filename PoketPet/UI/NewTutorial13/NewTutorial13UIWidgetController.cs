using PoketPet.Global;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial13 {
    public class NewTutorial13UIWidgetController : WidgetControllerWithData<NewTutorial13UIWidget, NewTutorial13UIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.POPUP_SHOW_SFX);
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnOnActionButtonClick;
        }

        private void WidgetOnOnActionButtonClick() {
            CoreMobile.Instance.AudioManager.PlaySoundClip(GlobalVariables.AudioUiKeys.BUTTON_CLICK_SFX);
            Widget.Dismiss();
        }
    }
}
