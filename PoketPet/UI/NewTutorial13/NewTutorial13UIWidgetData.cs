using System;
using PoketPet.Tutorial;
using PoketPet.Tutorial.Onboarding.Data;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.NewTutorial13 {
    [Serializable]
    public class NewTutorial13UIWidgetData : WidgetData {
        public TutorialSingleStepModelData TutorialSingleStepData { get; set; }
    }
}