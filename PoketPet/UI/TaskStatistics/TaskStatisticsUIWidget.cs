using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskStatistics {
    public class TaskStatisticsUIWidget : BaseUIWidget {
        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }
    }
}