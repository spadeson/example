using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskStatistics {
    public class TaskStatisticsUIWidgetController : WidgetControllerWithData<TaskStatisticsUIWidget, TaskStatisticsUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
