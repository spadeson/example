using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialTaskBest {
    public class TutorialTaskBestUIWidgetController : WidgetControllerWithData<TutorialTaskBestUIWidget, TutorialTaskBestUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }
    }
}
