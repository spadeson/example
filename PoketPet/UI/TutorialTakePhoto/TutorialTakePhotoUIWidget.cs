using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialTakePhoto
{
	public class TutorialTakePhotoUIWidget : BaseUIWidget {
		
		public event Action OnActionButtonClick;
		
		[Header("Shroud Image")]
		[SerializeField] private Image shroudImage;

		[Header("Container")]
		[SerializeField] private RectTransform container;
        
		[Header("Title Text")]
		[SerializeField] private TextMeshProUGUI titleText;

		[Header("Description Text")]
		[SerializeField] private TextMeshProUGUI descriptionText;

		[Header("Action Button")]
		[SerializeField] private Button actionButton;

		private TutorialTakePhotoUIWidgetData _widgetData;

		public override void Activate(bool animated) {
			base.Activate(animated);
		}

		public override void Deactivate(bool animated) {
			base.Deactivate(animated);
		}

		public override void Dismiss() {
			base.Dismiss();
		}

		public void Initialize(TutorialTakePhotoUIWidgetData widgetData) {
			_widgetData = widgetData;

			titleText.text = _widgetData.TutorialSingleStepData.title;
			descriptionText.text = _widgetData.TutorialSingleStepData.description;
			
			actionButton.onClick.AddListener(delegate {
				actionButton.onClick.RemoveAllListeners();
				actionButton.transform.DOPunchScale(Vector3.one * 1.05f, 0.25f, 2, 0.75f).OnComplete(() => {
					OnActionButtonClick?.Invoke();
				});
			});
		}
	}
}