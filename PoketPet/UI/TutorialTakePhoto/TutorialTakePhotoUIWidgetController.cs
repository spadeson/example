using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TutorialTakePhoto {
    public class TutorialTakePhotoUIWidgetController : WidgetControllerWithData<TutorialTakePhotoUIWidget, TutorialTakePhotoUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {

        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }

        private void WidgetOnActionButtonClick() {
            Widget.Dismiss();
        }
    }
}