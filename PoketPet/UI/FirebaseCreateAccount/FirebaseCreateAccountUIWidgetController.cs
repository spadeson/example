using System;
using Core.ErrorsHandlerSubsystem.Data;
using PoketPet.Global;
using PoketPet.UI.DataPicker;
using PoketPet.User;
using PoketPet.Utilities;
using UnityEngine;
using VIS.CoreMobile;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirebaseCreateAccount {
    public class FirebaseCreateAccountUIWidgetController : WidgetControllerWithData<FirebaseCreateAccountUIWidget, FirebaseCreateAccountUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnRegisterButtonClicked += Widget_OnRegisterButtonClickedHandler;
            Widget.OnCloseButtonClicked += Widget_OnCloseButtonClickedHandler;
            Widget.OnInputFieldEditEnd += OnInputFieldEditEndHandler;
            Widget.OnBirthDateButtonClick += OnBirthDateButtonClickHandler;

            #if UNITY_EDITOR
            CoreMobile.Instance.GesturesManager.OnTabButtonPressed += () => { Widget.ToggleFocus(); };
            #endif
            
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) { }
        
        protected override void OnWidgetDeactivated(Widget widget) { }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnRegisterButtonClicked -= Widget_OnRegisterButtonClickedHandler;
            Widget.OnCloseButtonClicked -= Widget_OnCloseButtonClickedHandler;
            Widget.OnInputFieldEditEnd -= OnInputFieldEditEndHandler;
            Widget.OnBirthDateButtonClick -= OnBirthDateButtonClickHandler;
        }

        private void OnBirthDateButtonClickHandler() {
            
            Widget.Deactivate(true);
            
            var dataPickerUiWidgetData = new DataPickerUIWidgetData(DateTime.Now);
            
            dataPickerUiWidgetData.OnPickedData += dateTime => {
                Debug.Log($"Obtained Birthday Data: {dateTime.Month}, {dateTime.Day}, {dateTime.Year}");
                UserProfileManager.SetUserBirthday(dateTime);

                if (UserProfileManager.CurrentUser == null) {
                    Widget.SetBirthdayDate(DateTimeConversion.TimestampToDateTime(UserProfileManager.GetPotentialUserBirthday()));
                } else {
                    Widget.SetBirthdayDate(DateTimeConversion.TimestampToDateTime(UserProfileManager.CurrentUser.birth_day));
                }
            };

            var dataPickerUIWidget = CoreMobile.Instance.UIManager.CreateUiWidgetWithData(GlobalVariables.UiKeys.DATE_PICKER_UI, dataPickerUiWidgetData, true);
            dataPickerUIWidget.OnDismissed += widget => {
                Widget.Activate(true);
            };
        }

        private void OnInputFieldEditEndHandler(InputFieldsTypes fieldType, FirebaseCreateAccountUIWidgetData firebaseCreateAccountUiWidgetData) {
            var email          = firebaseCreateAccountUiWidgetData.Email;
            var password       = firebaseCreateAccountUiWidgetData.Password;
            var passwordRepeat = firebaseCreateAccountUiWidgetData.PasswordRepeat;

            switch (fieldType) {
                case InputFieldsTypes.EMAIL:
                    Widget.SetInputFieldError(InputFieldsTypes.EMAIL, CoreMobile.Instance.ErrorsManager.ValidateForError(InputFieldsTypes.EMAIL, email));
                    break;
                case InputFieldsTypes.PASSWORD:
                    Widget.SetInputFieldError(InputFieldsTypes.PASSWORD, CoreMobile.Instance.ErrorsManager.ValidateForError(InputFieldsTypes.PASSWORD, password, passwordRepeat));
                    break;
                case InputFieldsTypes.PASSWORD_REPEAT:
                    Widget.SetInputFieldError(InputFieldsTypes.PASSWORD_REPEAT, CoreMobile.Instance.ErrorsManager.ValidateForError(InputFieldsTypes.PASSWORD_REPEAT, passwordRepeat, password));
                    break;
            }
        }

        private void Widget_OnCloseButtonClickedHandler() {
            Widget.Dismiss();
        }

        private void Widget_OnRegisterButtonClickedHandler(FirebaseCreateAccountUIWidgetData firebaseCreateAccountUiWidgetData) {
            var email    = firebaseCreateAccountUiWidgetData.Email;
            var password = firebaseCreateAccountUiWidgetData.Password;
            var passwordRepeat = firebaseCreateAccountUiWidgetData.PasswordRepeat;

            CoreMobile.Instance.FirebaseManager.AuthenticationManager.CreateNewUser(email, password);
        }
    }
}