using System;
using Core.ErrorsHandlerSubsystem.Data;
using DG.Tweening;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PoketPet.UI.FirebaseCreateAccount {
    public class FirebaseCreateAccountUIWidget : BasicPopupUIWidget {
        public event Action<FirebaseCreateAccountUIWidgetData> OnRegisterButtonClicked;
        public event Action OnCloseButtonClicked;
        public event Action OnBirthDateButtonClick;
        public event Action<InputFieldsTypes, FirebaseCreateAccountUIWidgetData> OnInputFieldEditEnd;

        [Header("Input Fields:")]
        [SerializeField] private TMP_InputField emailInputField;
        [SerializeField] private TMP_InputField passwordInputField;
        [SerializeField] private TMP_InputField passwordRepeatInputField;

        [Header("Input Fields Status Labels:")]
        [SerializeField] private TextMeshProUGUI emailStatusText;
        [SerializeField] private TextMeshProUGUI passwordStatusText;
        [SerializeField] private TextMeshProUGUI passwordRepeatStatusText;
        [SerializeField] private TextMeshProUGUI birthdayButtonLabel;

        [Header("Buttons:")]
        [SerializeField] private Button birthdayButton;

        [Header("Colors:")]
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color errorColor = Color.red;

        private FirebaseCreateAccountUIWidgetData _firebaseCreateAccountUiWidgetData;

        private bool _emailFieldError;
        private bool _passwordFieldError;
        private bool _passwordRepeatFieldError;

        public void Initialize(FirebaseCreateAccountUIWidgetData firebaseCreateAccountUiWidgetData) {
            //Set input fields empty values
            emailStatusText.text = string.Empty;
            passwordStatusText.text = string.Empty;
            passwordRepeatStatusText.text = string.Empty;

            _firebaseCreateAccountUiWidgetData = firebaseCreateAccountUiWidgetData;
            
            //Listen of the selection for the E-mail input filed
            emailInputField.onSelect.AddListener(delegate(string email) {
                actionButton.interactable = IsRequiresFieldsSet(_emailFieldError, _passwordFieldError, _passwordRepeatFieldError, birthdayButtonLabel.text);
            });

            //Listen of the value changed for E-mail input filed
            emailInputField.onValueChanged.AddListener(delegate(string email) {
                _firebaseCreateAccountUiWidgetData.Email = email;
            });
            
            //Listen of end edit for input fields
            emailInputField.onEndEdit.AddListener(delegate(string value) {
                OnInputFieldEditEnd?.Invoke(InputFieldsTypes.EMAIL, _firebaseCreateAccountUiWidgetData);
                actionButton.interactable = IsRequiresFieldsSet(emailInputField.text, passwordInputField.text, passwordRepeatInputField.text, birthdayButtonLabel.text);
            });
            
            //Listen of the selection for the password input filed
            passwordInputField.onSelect.AddListener(delegate(string password) {
                actionButton.interactable = IsRequiresFieldsSet(_emailFieldError, _passwordFieldError, _passwordRepeatFieldError, birthdayButtonLabel.text);
            });
            
            //Listen of the value changed for the password input filed
            passwordInputField.onValueChanged.AddListener(delegate(string password) {
                _firebaseCreateAccountUiWidgetData.Password = password;
            });
            
            //Listen of the edit end for the password input filed
            passwordInputField.onEndEdit.AddListener(delegate(string value) {
                OnInputFieldEditEnd?.Invoke(InputFieldsTypes.PASSWORD, _firebaseCreateAccountUiWidgetData);
                actionButton.interactable = IsRequiresFieldsSet(emailInputField.text, passwordInputField.text, passwordRepeatInputField.text, birthdayButtonLabel.text);
            });
            
            //Listen of the selection for the password repeat input filed
            passwordRepeatInputField.onSelect.AddListener(delegate(string password) {
                actionButton.interactable = IsRequiresFieldsSet(_emailFieldError, _passwordFieldError, _passwordRepeatFieldError, birthdayButtonLabel.text);
            });
            
            //Listen of the value changed for the password repeat input filed
            passwordRepeatInputField.onValueChanged.AddListener(delegate(string password) {
                _firebaseCreateAccountUiWidgetData.PasswordRepeat = password;
            });

            //Listen of the edit end for the password repeat input filed
            passwordRepeatInputField.onEndEdit.AddListener(delegate(string value) {
                OnInputFieldEditEnd?.Invoke(InputFieldsTypes.PASSWORD_REPEAT, _firebaseCreateAccountUiWidgetData);
                actionButton.interactable = IsRequiresFieldsSet(emailInputField.text, passwordInputField.text, passwordRepeatInputField.text, birthdayButtonLabel.text);
            });

            //Add listeners for buttons clicks
            birthdayButtonLabel.text = "Enter Birthday...";
            birthdayButton.onClick.AddListener(delegate { OnBirthDateButtonClick?.Invoke(); });

            actionButton.onClick.AddListener(delegate {
                actionButton.interactable = false;
                actionButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.45f, 3, 0.5f).OnComplete(() => {
                    OnRegisterButtonClicked?.Invoke(_firebaseCreateAccountUiWidgetData);
                    actionButton.interactable = true;
                });
            });
            
            closeButton.onClick.AddListener(delegate {
                closeButton.interactable = false;
                closeButton.transform.DOPunchScale(Vector3.one * 0.15f, 0.45f, 3, 0.5f).OnComplete(() => {
                    OnCloseButtonClicked?.Invoke();
                    closeButton.interactable = true;
                });
            });
            
            actionButton.interactable = IsRequiresFieldsSet(emailInputField.text, passwordInputField.text, passwordRepeatInputField.text, birthdayButtonLabel.text);
        }
        
        # if UNITY_EDITOR
        public void ToggleFocus() {
            if (emailInputField.isFocused) {
                EventSystem.current.SetSelectedGameObject(passwordInputField.gameObject, null);
                passwordInputField.OnPointerClick(new PointerEventData(EventSystem.current));
            } else if (passwordInputField.isFocused) {
                EventSystem.current.SetSelectedGameObject(passwordRepeatInputField.gameObject, null);
                passwordRepeatInputField.OnPointerClick(new PointerEventData(EventSystem.current));
            } else if (passwordRepeatInputField.isFocused) {
                EventSystem.current.SetSelectedGameObject(emailInputField.gameObject, null);
                emailInputField.OnPointerClick(new PointerEventData(EventSystem.current));
            }
        }
        #endif

        public void SetInputFieldError(InputFieldsTypes type, string error) {
            TMP_InputField  inputField;
            TextMeshProUGUI statusText;

            switch (type) {
                case InputFieldsTypes.EMAIL:
                    inputField = emailInputField;
                    statusText = emailStatusText;
                    break;
                case InputFieldsTypes.PASSWORD:
                    inputField = passwordInputField;
                    statusText = passwordStatusText;
                    break;
                case InputFieldsTypes.PASSWORD_REPEAT:
                    inputField = passwordRepeatInputField;
                    statusText = passwordRepeatStatusText;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            if (string.IsNullOrEmpty(error)) {
                inputField.image.color = Color.clear;

                statusText.color = normalColor;
                statusText.text = string.Empty;
                
                if (type == InputFieldsTypes.EMAIL) {
                    _emailFieldError = false;
                }
                
                if(type == InputFieldsTypes.PASSWORD) {
                    _passwordFieldError = false;
                }
                
                if(type == InputFieldsTypes.PASSWORD_REPEAT) {
                    _passwordRepeatFieldError = false;
                } 
                
            } else {
                inputField.image.color = errorColor;

                statusText.color = errorColor;
                statusText.text = error;
                
                if (type == InputFieldsTypes.EMAIL) {
                    _emailFieldError = true;
                }
                
                if(type == InputFieldsTypes.PASSWORD) {
                    _passwordFieldError = true;
                }
                
                if(type == InputFieldsTypes.PASSWORD_REPEAT) {
                    _passwordRepeatFieldError = true;
                }
            }
            
            actionButton.interactable = !_emailFieldError && !_passwordFieldError && _passwordRepeatFieldError && !string.IsNullOrEmpty(birthdayButtonLabel.text);
        }

        public void SetBirthdayDate(DateTime dateTime) {
            birthdayButtonLabel.text = dateTime.ToString("MM / dd / yyyy");
            actionButton.interactable = IsRequiresFieldsSet(emailInputField.text, passwordInputField.text, passwordRepeatInputField.text, birthdayButtonLabel.text);
        }

        private bool IsRequiresFieldsSet(string value1, string value2, string value3, string value4) {
            return !string.IsNullOrEmpty(value1) && !_emailFieldError && !string.IsNullOrEmpty(value2) && !_passwordFieldError && !string.IsNullOrEmpty(value3) && !_passwordRepeatFieldError && !string.IsNullOrEmpty(value4) && value4 != "Enter Birthday..." && value2 == value3;
        }
        
        private bool IsRequiresFieldsSet(bool value1, bool value2, bool value3, string value4) {
            return value1 && value2 && value3 && !string.IsNullOrEmpty(value4) && value4 != "Enter Birthday...";
        }
    }
}
