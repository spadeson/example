using System;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.FirebaseCreateAccount {
    [Serializable]
    public class FirebaseCreateAccountUIWidgetData : WidgetData {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordRepeat { get; set; }
        public DateTime Birthday { get; set; }
    }
}
