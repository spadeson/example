using System;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedSendPhoto {
    [Serializable]
    public class TaskCompletedSendPhotoUIWidgetData : WidgetData {
        public Sprite photo;
        public string taskId;
    }
}
