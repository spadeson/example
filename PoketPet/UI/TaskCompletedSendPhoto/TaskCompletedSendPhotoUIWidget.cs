using System;
using PoketPet.UI.GenericComponents.BasicPopupUIWidget;
using UnityEngine;
using UnityEngine.UI;

namespace PoketPet.UI.TaskCompletedSendPhoto {
    public class TaskCompletedSendPhotoUIWidget : BasicPopupUIWidget {

        public event Action OnActionButtonClick;
        
        [Header("Photo:"), SerializeField] private Image photo;
        
        private TaskCompletedSendPhotoUIWidgetData _widgetData;

        public override void Create() {
            base.Create();
        }

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            actionButton.onClick.RemoveAllListeners();
            base.Dismiss();
        }

        public void Initialize(TaskCompletedSendPhotoUIWidgetData widgetData) {
            _widgetData = widgetData;
            photo.sprite = widgetData.photo;
            actionButton.onClick.AddListener(delegate {
                OnActionButtonClick?.Invoke();
            });
        }
    }
}