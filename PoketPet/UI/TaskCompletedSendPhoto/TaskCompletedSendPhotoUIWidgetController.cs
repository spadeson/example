using PoketPet.Task;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.TaskCompletedSendPhoto {
    public class TaskCompletedSendPhotoUIWidgetController : WidgetControllerWithData<TaskCompletedSendPhotoUIWidget, TaskCompletedSendPhotoUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            Widget.OnActionButtonClick += WidgetOnActionButtonClick;
            Widget.Initialize(WidgetData);
        }

        protected override void OnWidgetActivated(Widget widget) {
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
            Widget.OnActionButtonClick -= WidgetOnActionButtonClick;
        }
        
        private void WidgetOnActionButtonClick() {
            TaskManager.CompleteTaskWithConfirm(WidgetData.taskId, WidgetData.photo);
            Widget.Dismiss();   
        }
    }
}