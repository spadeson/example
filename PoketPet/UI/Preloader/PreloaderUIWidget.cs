using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Preloader {
    public class PreloaderUIWidget : BaseUIWidget {
        
        [SerializeField] private Slider loadingSlider;
        [SerializeField] private TextMeshProUGUI loadingProgressText;

        public override void Activate(bool animated) {
            base.Activate(animated);
        }

        public override void Deactivate(bool animated) {
            base.Deactivate(animated);
        }

        public override void Dismiss() {
            base.Dismiss();
        }
        
        public void UpdateLoadingProgressSilent(float progress) {
            loadingSlider.SetValueWithoutNotify(progress);
        }

        public void UpdateLoadingProgress(float progress) {
            loadingSlider.value = progress;
            loadingProgressText.text = $"{Mathf.RoundToInt(progress * 100f)} %";
        }
        
        public void UpdateLoadingProgressTween(float duration, Action onProgressCompleted = null) {
            var progress = 0f;
            DOTween.To(()=> progress, x=> progress = x, 100f, duration)
            .OnUpdate(() => {
                loadingSlider.value = progress / 100f;
                loadingProgressText.text = $"{Mathf.RoundToInt(progress)} %";
            }).OnComplete(() => {
                onProgressCompleted?.Invoke();    
            });
        }
    }
}