using System;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Preloader {
    [Serializable]
    public class PreloaderUIWidgetData : WidgetData {
        public bool useFakeProgress;
        public float fakeProgressDuration;
        
        public AsyncOperation operation;
    }
}