using System.Collections;
using UnityEngine;
using VIS.CoreMobile.UISystem;

namespace PoketPet.UI.Preloader {
    public class PreloaderUIWidgetController : WidgetControllerWithData<PreloaderUIWidget, PreloaderUIWidgetData> {
        protected override void OnWidgetCreated(Widget widget) {
            WidgetData.operation.allowSceneActivation = false;

            if (WidgetData.useFakeProgress) {
                Widget.UpdateLoadingProgressTween(WidgetData.fakeProgressDuration, () => {
                    WidgetData.operation.allowSceneActivation = true;
                });
            } else {
                StartCoroutine(Loading_Co(WidgetData.operation));
            }
        }

        protected override void OnWidgetActivated(Widget widget) {
            
        }

        protected override void OnWidgetDeactivated(Widget widget) {
        }

        protected override void OnWidgetDismissed(Widget widget) {
        }

        private IEnumerator Loading_Co(AsyncOperation operation) {

            while (!operation.isDone) {
                var progress = Mathf.Clamp01(operation.progress / 0.9f);
                Widget.UpdateLoadingProgress(progress);

                if (progress >= 0.9f) {
                    Widget.UpdateLoadingProgress(1f);
                }

                yield return null;
            }

            operation.allowSceneActivation = true;
        }
    }
}