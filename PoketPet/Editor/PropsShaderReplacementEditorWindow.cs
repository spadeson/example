using UnityEditor;
using UnityEngine;
using System.IO;

namespace DefaultNamespace
{
    public class PropsShaderReplacementEditorWindow : UnityEditor.EditorWindow
    {
        [UnityEditor.MenuItem("Utilities/Assets/Props Shader Replacement")]
        private static void ShowWindow()
        {
            var window = GetWindow<PropsShaderReplacementEditorWindow>();
            window.titleContent = new UnityEngine.GUIContent("Props Shader Repacement");
            window.Show();
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Get props prefabs", GUILayout.ExpandWidth(true), GUILayout.Height(32)))
            {
                var baseFolder = "Assets/Models/Props";
                
                //Get all props folders list
                var folders = Directory.GetDirectories(baseFolder);

                foreach (var folder in folders)
                {
                    Debug.Log($"Folder: {folder}");
                    
                    //Get GUIDS for materials
                    var guidsMaterials = AssetDatabase.FindAssets("t:material", new string[] {folder});
                    
                    var guidsTextures =  AssetDatabase.FindAssets("t:texture2D", new string[] {folder});

                    for (var index = 0; index < guidsTextures.Length; index++)
                    {
                        var materialGuid = guidsMaterials[index];
                        var materialPath = AssetDatabase.GUIDToAssetPath(materialGuid);
                        
                        Debug.Log($"Material: {materialPath}");

                        var material = AssetDatabase.LoadAssetAtPath<Material>(materialPath);
                        material.shader = Shader.Find("PoketPet_v0.0.8/Unlit_SGraph_LightMap_3");

                        var textureGuid = guidsTextures[index];
                        var texturePath = AssetDatabase.GUIDToAssetPath(textureGuid);
                        var texture = AssetDatabase.LoadAssetAtPath<Texture2D>(texturePath);
                        
                        Debug.Log($"Texture: {texture}");

                        if (texture != null)
                        {
                            material.SetTexture("_Diffuse", texture);
                        }
                        
                        material.SetFloat("Vector1_C1A04AAB", 1.4f);

                    }
                }
                
                AssetDatabase.Refresh();
            }
        }
    }
}